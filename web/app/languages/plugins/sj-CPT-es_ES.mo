��    ]           �      �     �     �     �     
       	   (     2     ;  	   I  	   S     ]     f  	   t     ~     �     �     �     �     �     �     �     	  
   	     *	     @	     E	     U	     c	     i	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   
     
     !
     .
     :
     K
     X
     u
      �
     �
     �
     �
           )     F     \     b     i  
   u     �     �     �     �     �  	   �     �     �     �     �  	     
              &     ,     3  
   ?     J     Z     f     |     �     �     �  	   �     �     �     �  	     
            ,     5     C     X     n     �     �     �     �     �     �     �                     2     K     e     �     �     �     �     �     �  !        #     *     @     U     ]     p     �     �     �     �     �     �     �     �               #     7     M     [     x     �     �     �     �     �  
   �     �     �     �  	   �     �     �                     .     =     N  	   [     e     {  
   �     �     �     �     �     �     �          	               &     9     N     _     t     �     �     �  
   �     �     �     �  	     
                    E   ;   )   J          R      ,   $   ]               M      1       0   Y         P       #   H                  [   S      '   5      &       U      8   O             K   L       6       W   /           %   "   \   B           2             (               =   +   9   !         X      .   7           :   V   A          I   -   ?         @          <   
       N   F      C          T   >   *   D           	                      3      G   4   Q   Z                 Add New Add New Link Add New New Add New Sentence Add New Text All Links All News All Sentences All Texts Edit Link Edit New Edit Sentence Edit Text Featured Image Filter Links list Filter News list Filter sentences list Filter texts list Insert into Link Insert into New Insert into sentence Insert into text Items list Items list navigation Link Link Categories Link Category Links Links Archives Links Attributes New New Categories New Category New Link New New New Sentence New Tag New Tags New Text News News Archives News Attributes Not found Not found in Trash Parent Link: Parent New: Parent Sentence: Parent Text: Post Type General NameLinks Post Type General NameNews Post Type General NameSentences Post Type General NameTexts Post Type Singular NameLink Post Type Singular NameNew Post Type Singular NameSentence Post Type Singular NameText Remove featured image Scope Scopes Search Link Search New Search Sentence Search Text Sentence Sentence Archives Sentence Attributes Sentences Set featured image Text Text Archives Text Attributes Text Type Text Types Texts Topic Topics Update Link Update New Update Sentence Update Text Uploaded to this Link Uploaded to this New Uploaded to this sentence Uploaded to this text Use as featured image View Link View New View Sentence View Sentences View Text View Texts text-categories Project-Id-Version: Custom Post Type: Example
POT-Creation-Date: 2018-10-27 13:22+0200
PO-Revision-Date: 2018-10-27 13:27+0200
Last-Translator: Aitor <aitor@e451.net>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2
X-Poedit-Basepath: ../../plugins/roots-example-cpt-master
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: plugin.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Añadir nuevo Añadir nuevo enlace Añadir nueva noticia Añadir Nueva sentencia Añadir nuevo texto Todos los Enlaces Todas las Noticias Todas las sentencias Todos los textos Editar enlace Editar noticia Editar sentencia Editar texto Imagén destacada Filtrar lista de enlaces Filtrar lista de noticias Lista de filtros de sentencias Filtrar lista de textos Insertar en enlace Insertar en noticia Insertar en la sentencia Insertar en texto Lista de elementos Navegación de lista de elementos Enlace Categorías de enlace Categoría de enlace Enlaces Archivo de enlaces Atributos del enlace Noticia Categorías de noticia Categoría de noticia Nuevo Enlace Nueva noticia Nueva sentencia Etiqueta de noticia Etiquetas de noticia Nuevo texto Noticias Archivo de noticias Atributos de noticias No encontrado No encontrado en la Papelera Enlace padre: Noticia padre: Sentencia padre: Texto padre: Enlaces Noticias Sentencias Textos Enlace Noticia Sentencia Texto Eliminar imagen destacada Ámbito Ámbitos Buscar enlace Buscar noticia Buscar sentencia Buscar Texto Sentencia Archivo de sentencias Atributos del sentencia Sentencias Establecer imagen destacada Texto Archivo de textos Atributos de texto Tipo de texto Tipos de texto Textos Tema Temas Actualizar enlace Actualizar noticia Actualizar sentencia Actualizar texto Subido a este enlace Subido a esta noticia Subido a esta sentencia Subido a este texto Usar como imagen destacada Ver enlace Ver noticia Ver sentencia Ver sentencias Ver texto Ver textos categorias-de-texto 