��    #      4  /   L           	          (     /     2     F  	   [     e     l     ~     �     �     �  
   �     �     �  	   �     �     �               $     :     G  ;   Z     �     �     �     �     �  %     -   +  C   Y     �  b  �       
   $     /     5     9      O  	   p     z     ~     �     �      �     �     �     �            	   -     7     M     e     m     �     �  :   �  (   �      !	     B	  $   H	     m	  1   |	  0   �	  H   �	     (
                   "                                                                               	                          !                 #      
          &larr; Older comments Articles Author By Commented Sentences Comments are closed. Continued Footer Footer Navigation In short Invalid PHP version Invalid WordPress version Latest Posts Navigation Newer comments &rarr; News Not Found Pages: Primary Navigation Related sentences Scope Search Results for %s See All News Sentence Abstracts Sorry, but the page you were trying to view does not exist. Sorry, no results were found. Superbia Jurídico in the media Topics View all post filed under %s Where we are You must be using PHP 7.1 or greater. You must be using WordPress 4.7.0 or greater. You must run <code>composer install</code> from the Sage directory. texts Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-28 09:16+0100
PO-Revision-Date: 2018-10-28 09:16+0100
Last-Translator: Aitor <aitor@e451.net>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
Plural-Forms: nplurals=2; plural=(n != 1);
 &larr; Comentarios anteriores Artículos Autor Por Sentencias comentadas Los comentarios están cerrados. Leer más Pie Navegación faldón En breve Versión PHP no válida Versión de WordPress no válida Últimas publicaciones Navegación Comentarios posteriores &rarr; Noticias No encontrado Páginas: Navegación Principal Sentencias relacionadas Ámbito Resultados de búsqueda para %s Ver todas las noticias Resúmenes de sentencias Lo sentimos, pero la página que intentabas ver no existe. Lo siento, no se encontraron resultados. Superbia Jurídico en los medios Temas Ver todos los Posts archivados en %s Dónde estamos Debes utilizar la versión 7.1 de PHP o superior. Usted debe estar usando WordPress 4.7.0 o mayor. Debe ejecutar <code>composer install</code> desde el directorio de Sage. textos 