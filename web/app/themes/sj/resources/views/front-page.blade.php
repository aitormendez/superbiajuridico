@extends('layouts.app')

@section('content')

  <section id="slide01" class="hero">

    <div class="bcg">
      @php
        echo wp_get_attachment_image( $img_uno_id, 'very-large' );
      @endphp
    </div>

    <div class="escudo">
      @svg('sj-escudo-blanco')
    </div>

    <div class="marco texto">
      <div class="texto">
        <p><span>En <strong>Superbia Jurídico</strong></span> <span>nos especializamos en derecho de familia</span> <span>y derecho penal.</span></p>
        <p>Nuestra amplia trayectoria nos avala.</p>
      </div>
    </div>
    <div class="marco pregunta">
      <div class="pregunta">
        <a href="contacto">Pregúntanos</a>
      </div>
    </div>
  </section>

  <section class="textos">
    <section class="articulos">
      <div class="header">
        <h1>Últimos artículos</h1>
        <a class="enlace" href="{{ $site_url }}/article-types/articulo/">Ver todos</a>
      </div>

      <div class="contenido">
        @while ($query_article->have_posts()) @php $query_article->the_post() @endphp
          @include('partials.content-owl')
        @endwhile
        {{ wp_reset_postdata() }}
      </div>
    </section>

    <section class="articulos">
      <div class="header">
        <h1>Últimas sentencias comentadas</h1>
        <a class="enlace" href="{{ $site_url }}/article-types/comentario/">Ver todas</a>
      </div>

      <div class="contenido">
        @while ($query_comment->have_posts()) @php $query_comment->the_post() @endphp
          @include('partials.content-owl')
        @endwhile
        {{ wp_reset_postdata() }}
      </div>
    </section>

    <section class="articulos">
      <div class="header">
        <h1>Últimos resúmenes de sentencias</h1>
        <a class="enlace" href="{{ $site_url }}/article-types/resumen/">Ver todos</a>
      </div>

      <div class="contenido">
        @while ($query_abstract->have_posts()) @php $query_abstract->the_post() @endphp
          @include('partials.content-owl')
        @endwhile
        {{ wp_reset_postdata() }}
      </div>
    </section>
  </section>


  <section class="noticias">
    <div class="header">
      <h1>{{ __('News', 'sage') }}</h1>
      <a class="enlace" href="{{ $site_url }}/news">{{ __('See All News', 'sage') }}</a>
      <a class="enlace" href="{{ $site_url }}/new-categories/superbia-juridico/">{{ __('Superbia Jurídico in the media', 'sage') }}</a>
    </div>

    <div class="contenido">
      @while ($query_news->have_posts()) @php $query_news->the_post() @endphp
        @include('partials.content-'.get_post_type())
      @endwhile
      {{ wp_reset_postdata() }}
    </div>
  </section>

@endsection
