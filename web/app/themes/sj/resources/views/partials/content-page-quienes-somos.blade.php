<section id="slide01" class="full-width">
  <div class="bcg">
    @php
      echo wp_get_attachment_image( $img_uno_id, 'very-large' );
    @endphp
  </div>
  <div id="escudo">
    @svg('sj-escudo')
  </div>
</section>

<div class="cabecera">
  <h1>Presentación</h1>
</div>

<section id="slide02" class="centrar">
  <p>
    <strong>Superbia Jurídico</strong> nace en el año 2003 en Valencia, siendo el Derecho de Familia la piedra angular de nuestra andadura profesional desde el inicio. La colaboración con diferentes entidades y asociaciones valencianas, siendo las principales Custodia Compartida ¡Ya! de Valencia y de Gandía, nos permitió tomar conciencia de primera mano de lo necesario que es transmitir a los afectados por una ruptura la tranquilidad necesaria en esos momentos para que, ellos mismos, supieran como afrontarla y proteger lo mejor posible a sus hijos.
  </p>
  <p>
    Desde el año 2008 nos establecimos también en Madrid, consolidando nuestra colaboración con otras asociaciones tales como Custodia Paterna, SOS Papá, Amnistía Infantil, APIF Cádiz, Asociación TAS de Cantabria y Asociación por la Custodia Compartida de Castilla La Mancha, entre otras. Además, nuestra presencia en Madrid nos ha permitido colaborar y asesorar a la UEFACC (Unión Estatal de Federaciones y Asociaciones por la Custodia Compartida) y la Confederación Estatal de Asociaciones de Padres y Madres Separados por la Custodia Compartida. En definitiva, nuestra presencia en la Capital nos permite llegar a cualquier punto de la geografía nacional, siendo conscientes de la importancia de nuestro trabajo para proteger, al igual que en el inicio de nuestra andadura, a los niños que tienen la mala suerte de que sus padres se separen.
  </p>
  <p>
    Con el tiempo y la unión a nuestra firma de los mejores profesionales, hemos conseguido ampliar nuestra rama de actuación también al Derecho Penal, fundamentalmente a aquel relacionado con el Derecho de Familia y que tanto puede influir en las medidas que terminen fijándose para los menores en cuestión. Desde asuntos de Violencia Familiar y de Género a otros ámbito económico, pasando por los siempre desagradables procedimientos contra la libertad sexual, intentamos que los niños que se ven inmersos en procesos de este tipo lo noten lo menos posible y que las situaciones desagradables se reduzcan, cuanto antes, hacia la normalidad.
  </p>
</section>

<section id="slide03" class="full-width">
  <div class="bcg">
    @php
      echo wp_get_attachment_image( $img_dos_id, 'very-large' );
    @endphp
  </div>
  <p class="entresacado">…tomar conciencia de lo necesario que es transmitir a los afectados por una ruptura la tranquilidad necesaria en esos momentos.</p>
</section>

<div class="cabecera">
  <h1><span>Qué nos distingue</span> <span class="sub">La importancia de la especialización</span> </h1>
</div>

<section id="slide04" class="centrar">
  <p>
    Los años de experiencia nos llevan, día a día, a la mejor especialización posible. Los profesionales de SUPERBIA JURÍDICO son especialistas en sus disciplinas, formándose diariamente y teniendo, como no puede ser de otra forma, un trato cercano y directo con nuestros clientes.
  </p>

  <p>
    Sabemos de la importancia que para una persona que se encuentra ante una ruptura familiar representa el soporte de sus profesionales (abogados, psicólogos, trabajadores sociales, etc.) y que la información que reciba sea constante, diaria, cercana en una palabra. Nuestra misión es ofrecerles, desde la necesaria objetividad, el mejor asesoramiento posible para que las decisiones que tomen, sean meditadas y, sobre todo, encaminadas a proteger a sus hijos: esa es nuestra razón de ser, proteger a los niños.
  </p>
</section>

<div class="cabecera">
  <h1>Ramas jurídicas</h1>
</div>

<section id="slide05" class="centrar">
  <p>
    El Derecho de Familia y el Derecho Penal son nuestras ramas de actuación. Pero no por ello descuidamos otras materiales del Derecho, para las cuales contamos con una red de despacho colaboradores que garantizan, en sus diferentes ramas de actuación, el sello de calidad que desde SUPERBIA JURÍDICO damos a nuestros clientes.
  </p>
</section>
