# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-28 09:16+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: resources/functions.php:17
msgid "Sage &rsaquo; Error"
msgstr ""

#: resources/functions.php:27
msgid "You must be using PHP 7.1 or greater."
msgstr ""

#: resources/functions.php:27
msgid "Invalid PHP version"
msgstr ""

#: resources/functions.php:34
msgid "You must be using WordPress 4.7.0 or greater."
msgstr ""

#: resources/functions.php:34
msgid "Invalid WordPress version"
msgstr ""

#: resources/functions.php:43
msgid "You must run <code>composer install</code> from the Sage directory."
msgstr ""

#: resources/functions.php:44
msgid "Autoloader not found."
msgstr ""

#: resources/functions.php:59
#, php-format
msgid "Error locating <code>%s</code> for inclusion."
msgstr ""

#: app/controllers/app.php:30
msgid "Latest Posts"
msgstr ""

#: app/controllers/app.php:33
msgid "Author"
msgstr ""

#: app/controllers/app.php:36
msgid "Superbia Jurídico in the media"
msgstr ""

#: app/controllers/app.php:39
msgid "Articles"
msgstr ""

#: app/controllers/app.php:42
msgid "Sentence Abstracts"
msgstr ""

#: app/controllers/app.php:45
msgid "Commented Sentences"
msgstr ""

#: app/controllers/app.php:51
#, php-format
msgid "%s"
msgstr ""

#: app/controllers/app.php:54
#, php-format
msgid "Search Results for %s"
msgstr ""

#: app/controllers/app.php:57
msgid "Not Found"
msgstr ""

#: app/controllers/archive-text.php:27 app/controllers/archive-text.php:40
#, php-format
msgid "View all post filed under %s"
msgstr ""

#: app/filters.php:34
msgid "Continued"
msgstr ""

#: app/setup.php:44
msgid "Primary Navigation"
msgstr ""

#: app/setup.php:45
msgid "Footer Navigation"
msgstr ""

#: app/setup.php:84
msgid "Primary"
msgstr ""

#: app/setup.php:88
msgid "Footer"
msgstr ""

#: resources/views/404.blade.php:8
msgid "Sorry, but the page you were trying to view does not exist."
msgstr ""

#: resources/views/archive-new.blade.php:8
#: resources/views/archive-sentence.blade.php:7
#: resources/views/archive-text.blade.php:8 resources/views/index.blade.php:10
#: resources/views/search.blade.php:8 resources/views/taxonomy.blade.php:9
msgid "Sorry, no results were found."
msgstr ""

#: resources/views/front-page.blade.php:77
#: resources/views/partials/header.blade.php:50
msgid "News"
msgstr ""

#: resources/views/front-page.blade.php:78
#: resources/views/front-page.blade.php:79
msgid "See All News"
msgstr ""

#: resources/views/partials/comments.blade.php:21
msgid "&larr; Older comments"
msgstr ""

#: resources/views/partials/comments.blade.php:24
msgid "Newer comments &rarr;"
msgstr ""

#: resources/views/partials/comments.blade.php:33
msgid "Comments are closed."
msgstr ""

#: resources/views/partials/content-page-contact.blade.php:6
msgid "Where we are"
msgstr ""

#: resources/views/partials/content-page.blade.php:2
#: resources/views/partials/content-single.blade.php:10
msgid "Pages:"
msgstr ""

#: resources/views/partials/content-single-text.blade.php:10
msgid "In short"
msgstr ""

#: resources/views/partials/content-single-text.blade.php:19
msgid "Related sentences"
msgstr ""

#: resources/views/partials/entry-meta-BAK.blade.php:3
msgid "By"
msgstr ""

#: resources/views/partials/footer.blade.php:4
msgid "Navigation"
msgstr ""

#: resources/views/partials/header.blade.php:46
msgid "texts"
msgstr ""

#: resources/views/partials/header.blade.php:59
#: resources/views/partials/header.blade.php:64
msgid "Scope"
msgstr ""

#: resources/views/partials/header.blade.php:61
#: resources/views/partials/header.blade.php:66
msgid "Topics"
msgstr ""
