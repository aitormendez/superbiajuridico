<?php

namespace App;
use DateTime;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
}, 100);

/**
 * Blade SVG Sage. https://github.com/Log1x/blade-svg-sage
 */
add_filter('bladesvg_spritesheet_path', function () {
 return \BladeSvgSage\get_dist_path('images/svg/icons');
});

add_filter('bladesvg_image_path', function () {
 return \BladeSvgSage\get_dist_path('images/svg/icons');
});

add_filter('bladesvg_inline', function () {
 return true;
});

add_filter('bladesvg_class', function () {
 return 'svg';
});

add_filter('bladesvg_sprite_prefix', function () {
 return '';
});


/**
 * Google maps API
 * https://discourse.roots.io/t/acf-google-maps-mime-type-text-html-is-not-executable-error/12376/4
 */

add_filter('acf/fields/google_map/api', function ($api) {
 $api['key'] = env('GOOGLE_MAPS_API');

 return $api;
});

/**
 * Cambiar títulos sentencias
 */

 function actualizar_titulo_sentencia($post_id) {
   $post_type = get_post_type($post_id);
   if ($post_type == 'sentence') {
       $nombre = get_field('nombre', $post_id);
       $sentencia_no = get_field('sentencia_no', $post_id);
       $tribunal = get_field('tribunal', $post_id);
       $fecha = get_field('fecha', $post_id, false);
       $fecha_datetime = new DateTime($fecha);
       $anio = $fecha_datetime->format('Y');
       $fecha_titulo =  $fecha_datetime->format('j-n-Y');

       if ($sentencia_no == '') {
           $new = $tribunal . ' ' . $fecha_titulo;
       } else {
           $new = $tribunal . ' ' . $sentencia_no . '/' . $anio;
       }
       if (!'' == $nombre) {
           $new = $new . ' ' . $nombre;
       }

       $my_post = array(
         'ID' => $post_id,
         'post_title'   => $new,
       );
       remove_action( 'acf/save_post', 'actualizar_titulo_sentencia', 10, 1 );
       wp_update_post( $my_post );
       add_action( 'acf/save_post', 'actualizar_titulo_sentencia', 10, 1 );
   }
 }
 add_action( 'acf/save_post', __NAMESPACE__ .'\\actualizar_titulo_sentencia', 10, 1 );


/**
* Ordenar noticias
*/
add_action('pre_get_posts', function ($query) {
  if ( ! is_admin() && is_post_type_archive( 'new' ) && $query->is_main_query() ) {
       $query->set( 'meta_key', 'fecha_pub' );
       $query->set( 'orderby', 'meta_value' );
       $query->set( 'order', 'DESC');
  }
});

/**
* recopilar textos en página de autor
*/
add_action('pre_get_posts', function ($query) {
  if ( ! is_admin() && is_author() && $query->is_main_query() ) {
       $query->set( 'post_type', 'text' );
  }
});

/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', __NAMESPACE__ .'\\print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', __NAMESPACE__ .'\\print_emoji_detection_script' );
 remove_action( 'wp_print_styles', __NAMESPACE__ .'\\print_emoji_styles' );
 remove_action( 'admin_print_styles', __NAMESPACE__ .'\\print_emoji_styles' );
 remove_filter( 'the_content_feed', __NAMESPACE__ .'\\wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', __NAMESPACE__ .'\\wp_staticize_emoji' );
 remove_filter( 'wp_mail', __NAMESPACE__ .'\\wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', __NAMESPACE__ .'\\disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', __NAMESPACE__ .'\\disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', __NAMESPACE__ .'\\disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}
// eliminar post type "posts" (entradas por defecto)
add_action('admin_menu', __NAMESPACE__ .'\\remove_default_post_type');
function remove_default_post_type() {
	remove_menu_page('edit.php');
}

/**
 * Incluir CPT en queries
 */
// function sj_add_custom_types( $query ) {
//       if( ! is_admin() && $query->is_search || $query->is_author() || is_home() && $query->is_main_query() )  {
//         $query->set( 'post_type', [
//             'article',
//             'new',
//             'sentence',
//         ]
//     );
//         return $query;
// 	}
// }
// add_filter( 'pre_get_posts', __NAMESPACE__ .'\\sj_add_custom_types' );


/**
 * 20 post por página en sentencias
 */
add_action('pre_get_posts', __NAMESPACE__ .'\\filter_sentences');
function filter_sentences( $query ){
    if( ! is_admin() && $query->is_archive('sentence')):
        $query->set('posts_per_page', 20);
        return;
    endif;
}
