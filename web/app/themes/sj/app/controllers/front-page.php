<?php

namespace App;
use WP_Query;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function queryArticle()
    {
        $args_article = array(
        	'post_type'             => ['text'],
        	'post_status'           => ['publish'],
            'posts_per_page'        => 4,
            'tax_query'             => [
                [
                    'taxonomy' => 'article-type',
        			'field'    => 'slug',
        			'terms'    => ['resumen', 'comentario'],
                    'operator' => 'NOT IN',
                    ]
                ]
        );
        $query_article = new WP_Query( $args_article );
        return $query_article;
    }

    public function queryComment()
    {
        $args_comment = array(
        	'post_type'             => ['text'],
        	'post_status'           => 'publish',
	        'posts_per_page'        => 4,
            'tax_query'             => [
                [
                    'taxonomy' => 'article-type',
        			'field'    => 'slug',
        			'terms'    => ['articulo', 'resumen'],
                    'operator' => 'NOT IN',
                    ]
                ]
        );
        $query_comments = new WP_Query( $args_comment );
        return $query_comments;
    }

    public function queryAbstract()
    {
        $args_abstract = array(
        	'post_type'              => ['text'],
        	'post_status'            => 'publish',
	        'posts_per_page'         => 4,
            'tax_query'             => [
                [
                    'taxonomy' => 'article-type',
        			'field'    => 'slug',
        			'terms'    => ['articulo', 'comentario'],
                    'operator' => 'NOT IN',
                    ]
                ]
        );
        $query_abstracts = new WP_Query( $args_abstract );
        return $query_abstracts;
    }

    public function queryNews()
    {
        $args_news = array(
        	'post_type'              => ['new'],
        	'post_status'            => 'publish',
	        'posts_per_page'         => 21,
            'meta_key'               => 'fecha_pub',
            'orderby'	=> 'meta_value_num',
	        'order'		=> 'DESC'
        );
        $query_news = new WP_Query( $args_news );
        return $query_news;
    }

    public function imgUnoId()
    {
        $args = [
          'name'           => 'myles-tan-84040-unsplash',
          'post_type'      => 'attachment',
        ];
        $img = get_posts( $args );
        return $img[0]->ID;
    }

}
