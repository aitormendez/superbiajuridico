<?php

namespace App;

trait EnlaceExterno
{
    public static function getEnlaceExterno()
    {
        return get_field('enlace_externo');
    }
}

trait ClassNews
{
    public static function setClassNews()
    {
        if ( has_term('superbia-juridico', 'news-category') ) {
            $class = 'infinite-scroll-item sj';
        } else {
            $class = 'infinite-scroll-item';
        }
        return $class;
    }
}
