<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;


$options = [
	'supports'              => [ 'title' ],
	'capability_type'       => 'post',
    'has_archive'           => true,
    'show_in_nav_menus'     => true,
    'menu_position'         => 5,
];

$labels = [
    'name'                  => _x( 'Sentences', 'Post Type General Name', 'sj-CPT' ),
	'singular_name'         => _x( 'Sentence', 'Post Type Singular Name', 'sj-CPT' ),
	'menu_name'             => __( 'Sentences', 'sj-CPT' ),
	'name_admin_bar'        => __( 'Sentence', 'sj-CPT' ),
	'archives'              => __( 'Sentence Archives', 'sj-CPT' ),
	'attributes'            => __( 'Sentence Attributes', 'sj-CPT' ),
	'parent_item_colon'     => __( 'Parent Sentence:', 'sj-CPT' ),
	'all_items'             => __( 'All Sentences', 'sj-CPT' ),
	'add_new_item'          => __( 'Add New Sentence', 'sj-CPT' ),
	'add_new'               => __( 'Add New', 'sj-CPT' ),
	'new_item'              => __( 'New Sentence', 'sj-CPT' ),
	'edit_item'             => __( 'Edit Sentence', 'sj-CPT' ),
	'update_item'           => __( 'Update Sentence', 'sj-CPT' ),
	'view_item'             => __( 'View Sentence', 'sj-CPT' ),
	'view_items'            => __( 'View Sentences', 'sj-CPT' ),
	'search_items'          => __( 'Search Sentence', 'sj-CPT' ),
	'not_found'             => __( 'Not found', 'sj-CPT' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'sj-CPT' ),
	'featured_image'        => __( 'Featured Image', 'sj-CPT' ),
	'set_featured_image'    => __( 'Set featured image', 'sj-CPT' ),
	'remove_featured_image' => __( 'Remove featured image', 'sj-CPT' ),
	'use_featured_image'    => __( 'Use as featured image', 'sj-CPT' ),
	'insert_into_item'      => __( 'Insert into sentence', 'sj-CPT' ),
	'uploaded_to_this_item' => __( 'Uploaded to this sentence', 'sj-CPT' ),
	'items_list'            => __( 'Items list', 'sj-CPT' ),
	'items_list_navigation' => __( 'Items list navigation', 'sj-CPT' ),
	'filter_items_list'     => __( 'Filter sentences list', 'sj-CPT' ),
];

$sentences = new PostType('sentence', $options, $labels);

$sentences->register();
