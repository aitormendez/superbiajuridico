<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;

$options_texts = [
	'supports'              => [ 'title', 'editor', 'excerpt', 'author' ],
	'capability_type'       => 'post',
    'has_archive'           => true,
    'show_in_nav_menus'     => true,
    'menu_position'         => 2,
];

$labels_texts = [
    'name'                  => _x( 'Texts', 'Post Type General Name', 'sj-CPT' ),
	'singular_name'         => _x( 'Text', 'Post Type Singular Name', 'sj-CPT' ),
	'menu_name'             => __( 'Texts', 'sj-CPT' ),
	'name_admin_bar'        => __( 'Text', 'sj-CPT' ),
	'archives'              => __( 'Text Archives', 'sj-CPT' ),
	'attributes'            => __( 'Text Attributes', 'sj-CPT' ),
	'parent_item_colon'     => __( 'Parent Text:', 'sj-CPT' ),
	'all_items'             => __( 'All Texts', 'sj-CPT' ),
	'add_new_item'          => __( 'Add New Text', 'sj-CPT' ),
	'add_new'               => __( 'Add New', 'sj-CPT' ),
	'new_item'              => __( 'New Text', 'sj-CPT' ),
	'edit_item'             => __( 'Edit Text', 'sj-CPT' ),
	'update_item'           => __( 'Update Text', 'sj-CPT' ),
	'view_item'             => __( 'View Text', 'sj-CPT' ),
	'view_items'            => __( 'View Texts', 'sj-CPT' ),
	'search_items'          => __( 'Search Text', 'sj-CPT' ),
	'not_found'             => __( 'Not found', 'sj-CPT' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'sj-CPT' ),
	'featured_image'        => __( 'Featured Image', 'sj-CPT' ),
	'set_featured_image'    => __( 'Set featured image', 'sj-CPT' ),
	'remove_featured_image' => __( 'Remove featured image', 'sj-CPT' ),
	'use_featured_image'    => __( 'Use as featured image', 'sj-CPT' ),
	'insert_into_item'      => __( 'Insert into text', 'sj-CPT' ),
	'uploaded_to_this_item' => __( 'Uploaded to this text', 'sj-CPT' ),
	'items_list'            => __( 'Items list', 'sj-CPT' ),
	'items_list_navigation' => __( 'Items list navigation', 'sj-CPT' ),
	'filter_items_list'     => __( 'Filter texts list', 'sj-CPT' ),
];




$texts = new PostType('text', $options_texts, $labels_texts);
$texts->register();

$options_cats = [
    'hierarchical' => true,
];



$texts->taxonomy('article-category'); // tiene que ser igual que 'name'
$names_cats = [
    'name' => __( 'article-category', 'sj-CPT' ),
    'singular' => __( 'Scope', 'sj-CPT' ),
    'plural' => __( 'Scopes', 'sj-CPT' ),
    'slug' => __( 'text-categories', 'sj-CPT' ),
];
$text_cats = new Taxonomy($names_cats, $options_cats);
$text_cats->register();

$options_tags = [
    'hierarchical' => false,
];

$texts->taxonomy('article-tag'); // tiene que ser igual que 'name'
$names_tags = [
    'name' => __( 'article-tag', 'sj-CPT' ),
    'singular' => __( 'Topic', 'sj-CPT' ),
    'plural' => __( 'Topics', 'sj-CPT' ),
];
$text_tags = new Taxonomy($names_tags, $options_tags);
$text_tags->register();


$options_types = [
    'hierarchical' => true,
];
$texts->taxonomy('article-type'); // tiene que ser igual que 'name'
$names_types = [
    'name' => __( 'article-type', 'sj-CPT' ),
    'singular' => __( 'Text Type', 'sj-CPT' ),
    'plural' => __( 'Text Types', 'sj-CPT' ),
];
$text_types = new Taxonomy($names_types, $options_types);
$text_types->register();
