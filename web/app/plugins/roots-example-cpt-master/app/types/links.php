<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;



// Post type options
$options = [
	'supports' => [ 'title' ],
	'capability_type' => 'post',
    'has_archive'           => true,
    'menu_position'         => 5,
];

$labels = [
    'name'                  => _x( 'Links', 'Post Type General Name', 'sj-CPT' ),
	'singular_name'         => _x( 'Link', 'Post Type Singular Name', 'sj-CPT' ),
	'menu_name'             => __( 'Links', 'sj-CPT' ),
	'name_admin_bar'        => __( 'Link', 'sj-CPT' ),
	'archives'              => __( 'Links Archives', 'sj-CPT' ),
	'attributes'            => __( 'Links Attributes', 'sj-CPT' ),
	'parent_item_colon'     => __( 'Parent Link:', 'sj-CPT' ),
	'all_items'             => __( 'All Links', 'sj-CPT' ),
	'add_new_item'          => __( 'Add New Link', 'sj-CPT' ),
	'add_new'               => __( 'Add New', 'sj-CPT' ),
	'new_item'              => __( 'New Link', 'sj-CPT' ),
	'edit_item'             => __( 'Edit Link', 'sj-CPT' ),
	'update_item'           => __( 'Update Link', 'sj-CPT' ),
	'view_item'             => __( 'View Link', 'sj-CPT' ),
	'view_items'            => __( 'View Link', 'sj-CPT' ),
	'search_items'          => __( 'Search Link', 'sj-CPT' ),
	'not_found'             => __( 'Not found', 'sj-CPT' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'sj-CPT' ),
	'featured_image'        => __( 'Featured Image', 'sj-CPT' ),
	'set_featured_image'    => __( 'Set featured image', 'sj-CPT' ),
	'remove_featured_image' => __( 'Remove featured image', 'sj-CPT' ),
	'use_featured_image'    => __( 'Use as featured image', 'sj-CPT' ),
	'insert_into_item'      => __( 'Insert into Link', 'sj-CPT' ),
	'uploaded_to_this_item' => __( 'Uploaded to this Link', 'sj-CPT' ),
	'items_list'            => __( 'Items list', 'sj-CPT' ),
	'items_list_navigation' => __( 'Items list navigation', 'sj-CPT' ),
	'filter_items_list'     => __( 'Filter Links list', 'sj-CPT' ),
];

$links = new PostType('links', $options, $labels);

// Register the "Example" post type with WordPress
$links->register();

$options = [
    'hierarchical' => true,
];

$links->taxonomy('link-category'); // tiene que ser igual que 'name'
$link_categories = [
    'name' => __( 'link-category', 'sj-CPT' ),
    'singular' => __( 'Link Category', 'sj-CPT' ),
    'plural' => __( 'Link Categories', 'sj-CPT' ),
    'slug' => __( 'link-categories', 'sj-CPT' ),
];
$link_categories = new Taxonomy($link_categories, $options);
$link_categories->register();
