<?php

use PostTypes\PostType;
use PostTypes\Taxonomy;



// Post type options
$options = [
	'supports' => [ 'title', 'editor', 'excerpt' ],
	'capability_type' => 'post',
    'has_archive'           => true,
    'show_in_nav_menus'     => true,
    'menu_position'         => 5,
];

$labels = [
    'name'                  => _x( 'News', 'Post Type General Name', 'sj-CPT' ),
	'singular_name'         => _x( 'New', 'Post Type Singular Name', 'sj-CPT' ),
	'menu_name'             => __( 'News', 'sj-CPT' ),
	'name_admin_bar'        => __( 'New', 'sj-CPT' ),
	'archives'              => __( 'News Archives', 'sj-CPT' ),
	'attributes'            => __( 'News Attributes', 'sj-CPT' ),
	'parent_item_colon'     => __( 'Parent New:', 'sj-CPT' ),
	'all_items'             => __( 'All News', 'sj-CPT' ),
	'add_new_item'          => __( 'Add New New', 'sj-CPT' ),
	'add_new'               => __( 'Add New', 'sj-CPT' ),
	'new_item'              => __( 'New New', 'sj-CPT' ),
	'edit_item'             => __( 'Edit New', 'sj-CPT' ),
	'update_item'           => __( 'Update New', 'sj-CPT' ),
	'view_item'             => __( 'View New', 'sj-CPT' ),
	'view_items'            => __( 'View New', 'sj-CPT' ),
	'search_items'          => __( 'Search New', 'sj-CPT' ),
	'not_found'             => __( 'Not found', 'sj-CPT' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'sj-CPT' ),
	'featured_image'        => __( 'Featured Image', 'sj-CPT' ),
	'set_featured_image'    => __( 'Set featured image', 'sj-CPT' ),
	'remove_featured_image' => __( 'Remove featured image', 'sj-CPT' ),
	'use_featured_image'    => __( 'Use as featured image', 'sj-CPT' ),
	'insert_into_item'      => __( 'Insert into New', 'sj-CPT' ),
	'uploaded_to_this_item' => __( 'Uploaded to this New', 'sj-CPT' ),
	'items_list'            => __( 'Items list', 'sj-CPT' ),
	'items_list_navigation' => __( 'Items list navigation', 'sj-CPT' ),
	'filter_items_list'     => __( 'Filter News list', 'sj-CPT' ),
];

$news = new PostType('new', $options, $labels);
$news->register();

$options_tags = [
    'hierarchical' => false,
];

$news->taxonomy('news-tag'); // tiene que ser igual que 'name'
$names_tags = [
    'name' => __( 'news-tag', 'sj-CPT' ),
    'singular' => __( 'New Tag', 'sj-CPT' ),
    'plural' => __( 'New Tags', 'sj-CPT' ),
];
$news_tags = new Taxonomy($names_tags, $options_tags);
$news_tags->register();

$options_cats = [
    'hierarchical' => true,
];
$news->taxonomy('news-category'); // tiene que ser igual que 'name'
$names_categories = [
    'name' => __( 'news-category', 'sj-CPT' ),
    'singular' => __( 'New Category', 'sj-CPT' ),
    'plural' => __( 'New Categories', 'sj-CPT' ),
    'slug' => 'new-categories',
];
$names_categories = new Taxonomy($names_categories, $options_cats);
$names_categories->register();
