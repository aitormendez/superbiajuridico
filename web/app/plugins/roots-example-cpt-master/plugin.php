<?php
/*
Plugin Name: CPT.SJ
Plugin URI:
Description: Custom Post Types for Superbia Jurídico.
Version: 1.0
Author: Aitor Méndez
Author URI:
Text Domain: sj-CPT
Domain Path: /languages
License: MIT
*/

namespace AM\CPT;

function sj_load_textdomain() {
  load_plugin_textdomain( 'sj-CPT', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'init', __NAMESPACE__ . '\\sj_load_textdomain' );

//Set up autoloader
require __DIR__ . '/vendor/autoload.php';

//Define Constants
define( 'EXAMPLECPT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'EXAMPLECPT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Autoload the Init class
$example_init = new Init();
