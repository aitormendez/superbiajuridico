#
# Powered by XCloner Site Backup
# http://www.xcloner.com
#
# Host: http://superbiajuridico.test/wp
# Generation Time: Sep 16, 2018 at 14:31
# PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1
# Database Charset: utf8mb4
# MYSQL innodb_version: 5.7.21
# MYSQL protocol_version: 10
# MYSQL slave_type_conversions: 
# MYSQL version: 10.2.14-MariaDB-10.2.14+maria~xenial
# MYSQL version_comment: mariadb.org binary distribution
# MYSQL version_compile_machine: x86_64
# MYSQL version_compile_os: debian-linux-gnu
# MYSQL version_malloc_library: system
# MYSQL version_ssl_library: OpenSSL 1.0.2g  1 Mar 2016
# MYSQL wsrep_patch_version: wsrep_25.23
# MYSQL DEFAULT_CHARACTER_SET_NAME: utf8
# MYSQL SCHEMA_NAME: utf8_general_ci
#
# Database : `information_schema`
# --------------------------------------------------------


#
# Table structure for table `ALL_PLUGINS`
#


DROP table IF EXISTS `ALL_PLUGINS`;
CREATE TEMPORARY TABLE `ALL_PLUGINS` (
  `PLUGIN_NAME` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_STATUS` varchar(16) NOT NULL DEFAULT '',
  `PLUGIN_TYPE` varchar(80) NOT NULL DEFAULT '',
  `PLUGIN_TYPE_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_LIBRARY` varchar(64) DEFAULT NULL,
  `PLUGIN_LIBRARY_VERSION` varchar(20) DEFAULT NULL,
  `PLUGIN_AUTHOR` varchar(64) DEFAULT NULL,
  `PLUGIN_DESCRIPTION` longtext DEFAULT NULL,
  `PLUGIN_LICENSE` varchar(80) NOT NULL DEFAULT '',
  `LOAD_OPTION` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_MATURITY` varchar(12) NOT NULL DEFAULT '',
  `PLUGIN_AUTH_VERSION` varchar(80) DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `ALL_PLUGINS`
#

#
# Dumping data for table `ALL_PLUGINS`
#

INSERT INTO `ALL_PLUGINS` VALUES ('binlog', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'This is a pseudo storage engine to represent the binlog in a transaction', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('mysql_native_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Native MySQL authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('mysql_old_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Old MySQL-4.0 authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('wsrep', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Codership Oy', 'A pseudo storage engine to represent transactions in multi-master synchornous replication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MRG_MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Collection of identical MyISAM tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('CSV', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Brian Aker, MySQL AB', 'CSV storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'MyISAM storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MEMORY', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Hash based, stored in memory, useful for temporary tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('CLIENT_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Client Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('INDEX_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Index Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('TABLE_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Table Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('USER_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'User Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('PERFORMANCE_SCHEMA', '0.1', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Marc Alff, Oracle', 'Performance Schema', 'GPL', 'FORCE', 'Stable', '5.6.36');
INSERT INTO `ALL_PLUGINS` VALUES ('Aria', '1.5', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Monty Program Ab', 'Crash-safe tables with MyISAM heritage', 'GPL', 'ON', 'Stable', '1.5');
INSERT INTO `ALL_PLUGINS` VALUES ('InnoDB', '5.7', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Oracle Corporation', 'Supports transactions, row-level locking, foreign keys and encryption for tables', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TRX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB transactions', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_LOCKS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB conflicting locks', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_LOCK_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB which lock is blocking which', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMPMEM', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMPMEM_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_PER_INDEX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index)', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_PER_INDEX_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index); reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_PAGE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page Information', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_PAGE_LRU', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page in LRU', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_POOL_STATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Pool Statistics Information ', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_METRICS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Metrics Info', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_DEFAULT_STOPWORD', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Default stopword list for InnoDB Full Text Search', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_BEING_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS BEING DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_CONFIG', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS CONFIG TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_INDEX_CACHE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX CACHED', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_INDEX_TABLE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLESTATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESTATS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_INDEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_INDEXES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_COLUMNS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_COLUMNS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FIELDS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FIELDS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FOREIGN', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FOREIGN_COLS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN_COLS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLESPACES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESPACES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_DATAFILES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_VIRTUAL', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_VIRTUAL', 'GPL', 'ON', 'Beta', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_MUTEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_SEMAPHORE_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'MariaDB Corporation', 'InnoDB SYS_SEMAPHORE_WAITS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TABLESPACES_ENCRYPTION', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_ENCRYPTION', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TABLESPACES_SCRUBBING', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_SCRUBBING', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('SEQUENCE', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Sergei Golubchik', 'Generated tables filled with sequential values', 'GPL', 'ON', 'Stable', '0.1');
INSERT INTO `ALL_PLUGINS` VALUES ('FEEDBACK', '1.1', 'DISABLED', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergei Golubchik', 'MariaDB User Feedback Plugin', 'GPL', 'OFF', 'Stable', '1.1');
INSERT INTO `ALL_PLUGINS` VALUES ('user_variables', '1.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergey Vojtovich', 'User-defined variables', 'GPL', 'ON', 'Gamma', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('partition', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Mikael Ronstrom, MySQL AB', 'Partition Storage Engine Helper', 'GPL', 'ON', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('FEDERATED', '1.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_federated.so', '1.13', 'Patrick Galbraith and Brian Aker, MySQL AB', 'Federated MySQL storage engine', 'GPL', 'OFF', 'Gamma', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('FEDERATED', '2.1', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_federatedx.so', '1.13', 'Patrick Galbraith', 'FederatedX pluggable storage engine', 'GPL', 'OFF', 'Stable', '2.1');
INSERT INTO `ALL_PLUGINS` VALUES ('SQL_ERROR_LOG', '1.0', 'NOT INSTALLED', 'AUDIT', '3.2', 'sql_errlog.so', '1.13', 'Alexey Botchkov', 'Log SQL level errors to a file with rotation', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('rpl_semi_sync_master', '1.0', 'NOT INSTALLED', 'REPLICATION', '2.0', 'semisync_master.so', '1.13', 'He Zhenxing', 'Semi-synchronous replication master', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('SERVER_AUDIT', '1.4', 'NOT INSTALLED', 'AUDIT', '3.2', 'server_audit.so', '1.13', 'Alexey Botchkov (MariaDB Corporation)', 'Audit the server activity', 'GPL', 'OFF', 'Stable', '1.4.3');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_CACHE_INFO', '1.1', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'query_cache_info.so', '1.13', 'Roland Bouman, Daniel Black', 'Lists all queries in the query cache.', 'BSD', 'OFF', 'Stable', '1.1');
INSERT INTO `ALL_PLUGINS` VALUES ('SPHINX', '2.2', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_sphinx.so', '1.13', 'Sphinx developers', 'Sphinx storage engine 2.2.6-release', 'GPL', 'OFF', 'Gamma', '2.2.6-release');
INSERT INTO `ALL_PLUGINS` VALUES ('METADATA_LOCK_INFO', '0.1', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'metadata_lock_info.so', '1.13', 'Kentoku Shiba', 'Metadata locking viewer', 'GPL', 'OFF', 'Stable', '');
INSERT INTO `ALL_PLUGINS` VALUES ('LOCALES', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'locales.so', '1.13', 'Roberto Spadim, Spaempresarial - Brazil', 'Lists all locales from server.', 'BSD', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('rpl_semi_sync_slave', '1.0', 'NOT INSTALLED', 'REPLICATION', '2.0', 'semisync_slave.so', '1.13', 'He Zhenxing', 'Semi-synchronous replication slave', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_RESPONSE_TIME', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'query_response_time.so', '1.13', 'Percona and Sergey Vojtovich', 'Query Response Time Distribution INFORMATION_SCHEMA Plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_RESPONSE_TIME_AUDIT', '1.0', 'NOT INSTALLED', 'AUDIT', '3.2', 'query_response_time.so', '1.13', 'Percona and Sergey Vojtovich', 'Query Response Time Distribution Audit Plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('WSREP_MEMBERSHIP', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'wsrep_info.so', '1.13', 'Nirbhay Choubey', 'Information about group members', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('WSREP_STATUS', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'wsrep_info.so', '1.13', 'Nirbhay Choubey', 'Group view information', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('simple_password_check', '1.0', 'NOT INSTALLED', 'PASSWORD VALIDATION', '1.0', 'simple_password_check.so', '1.13', 'Sergei Golubchik', 'Simple password strength checks', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('pam', '1.0', 'NOT INSTALLED', 'AUTHENTICATION', '2.1', 'auth_pam.so', '1.13', 'Sergei Golubchik', 'PAM based authentication', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('BLACKHOLE', '1.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_blackhole.so', '1.13', 'MySQL AB', '/dev/null storage engine (anything you write to it disappears)', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('unix_socket', '1.0', 'NOT INSTALLED', 'AUTHENTICATION', '2.1', 'auth_socket.so', '1.13', 'Sergei Golubchik', 'Unix Socket based authentication', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('ARCHIVE', '3.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_archive.so', '1.13', 'Brian Aker, MySQL AB', 'Archive storage engine', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('file_key_management', '1.0', 'NOT INSTALLED', 'ENCRYPTION', '3.0', 'file_key_management.so', '1.13', 'Denis Endro eperi GmbH', 'File-based key management plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('handlersocket', '1.0', 'NOT INSTALLED', 'DAEMON', '100214.0', 'handlersocket.so', '1.13', 'higuchi dot akira at dena dot jp', 'Direct access into InnoDB', 'BSD', 'OFF', 'Beta', '1.0');

#
# Table structure for table `ALL_PLUGINS`
#


DROP table IF EXISTS `ALL_PLUGINS`;
CREATE TEMPORARY TABLE `ALL_PLUGINS` (
  `PLUGIN_NAME` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_STATUS` varchar(16) NOT NULL DEFAULT '',
  `PLUGIN_TYPE` varchar(80) NOT NULL DEFAULT '',
  `PLUGIN_TYPE_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_LIBRARY` varchar(64) DEFAULT NULL,
  `PLUGIN_LIBRARY_VERSION` varchar(20) DEFAULT NULL,
  `PLUGIN_AUTHOR` varchar(64) DEFAULT NULL,
  `PLUGIN_DESCRIPTION` longtext DEFAULT NULL,
  `PLUGIN_LICENSE` varchar(80) NOT NULL DEFAULT '',
  `LOAD_OPTION` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_MATURITY` varchar(12) NOT NULL DEFAULT '',
  `PLUGIN_AUTH_VERSION` varchar(80) DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `ALL_PLUGINS`
#

#
# Dumping data for table `ALL_PLUGINS`
#

INSERT INTO `ALL_PLUGINS` VALUES ('binlog', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'This is a pseudo storage engine to represent the binlog in a transaction', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('mysql_native_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Native MySQL authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('mysql_old_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Old MySQL-4.0 authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('wsrep', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Codership Oy', 'A pseudo storage engine to represent transactions in multi-master synchornous replication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MRG_MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Collection of identical MyISAM tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('CSV', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Brian Aker, MySQL AB', 'CSV storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'MyISAM storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('MEMORY', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Hash based, stored in memory, useful for temporary tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('CLIENT_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Client Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('INDEX_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Index Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('TABLE_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Table Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('USER_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'User Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `ALL_PLUGINS` VALUES ('PERFORMANCE_SCHEMA', '0.1', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Marc Alff, Oracle', 'Performance Schema', 'GPL', 'FORCE', 'Stable', '5.6.36');
INSERT INTO `ALL_PLUGINS` VALUES ('Aria', '1.5', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Monty Program Ab', 'Crash-safe tables with MyISAM heritage', 'GPL', 'ON', 'Stable', '1.5');
INSERT INTO `ALL_PLUGINS` VALUES ('InnoDB', '5.7', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Oracle Corporation', 'Supports transactions, row-level locking, foreign keys and encryption for tables', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TRX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB transactions', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_LOCKS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB conflicting locks', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_LOCK_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB which lock is blocking which', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMPMEM', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMPMEM_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_PER_INDEX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index)', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_CMP_PER_INDEX_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index); reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_PAGE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page Information', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_PAGE_LRU', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page in LRU', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_BUFFER_POOL_STATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Pool Statistics Information ', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_METRICS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Metrics Info', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_DEFAULT_STOPWORD', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Default stopword list for InnoDB Full Text Search', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_BEING_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS BEING DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_CONFIG', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS CONFIG TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_INDEX_CACHE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX CACHED', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_FT_INDEX_TABLE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLESTATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESTATS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_INDEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_INDEXES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_COLUMNS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_COLUMNS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FIELDS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FIELDS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FOREIGN', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_FOREIGN_COLS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN_COLS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_TABLESPACES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESPACES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_DATAFILES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_VIRTUAL', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_VIRTUAL', 'GPL', 'ON', 'Beta', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_MUTEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_SYS_SEMAPHORE_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'MariaDB Corporation', 'InnoDB SYS_SEMAPHORE_WAITS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TABLESPACES_ENCRYPTION', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_ENCRYPTION', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('INNODB_TABLESPACES_SCRUBBING', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_SCRUBBING', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `ALL_PLUGINS` VALUES ('SEQUENCE', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Sergei Golubchik', 'Generated tables filled with sequential values', 'GPL', 'ON', 'Stable', '0.1');
INSERT INTO `ALL_PLUGINS` VALUES ('FEEDBACK', '1.1', 'DISABLED', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergei Golubchik', 'MariaDB User Feedback Plugin', 'GPL', 'OFF', 'Stable', '1.1');
INSERT INTO `ALL_PLUGINS` VALUES ('user_variables', '1.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergey Vojtovich', 'User-defined variables', 'GPL', 'ON', 'Gamma', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('partition', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Mikael Ronstrom, MySQL AB', 'Partition Storage Engine Helper', 'GPL', 'ON', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('FEDERATED', '1.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_federated.so', '1.13', 'Patrick Galbraith and Brian Aker, MySQL AB', 'Federated MySQL storage engine', 'GPL', 'OFF', 'Gamma', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('FEDERATED', '2.1', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_federatedx.so', '1.13', 'Patrick Galbraith', 'FederatedX pluggable storage engine', 'GPL', 'OFF', 'Stable', '2.1');
INSERT INTO `ALL_PLUGINS` VALUES ('SQL_ERROR_LOG', '1.0', 'NOT INSTALLED', 'AUDIT', '3.2', 'sql_errlog.so', '1.13', 'Alexey Botchkov', 'Log SQL level errors to a file with rotation', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('rpl_semi_sync_master', '1.0', 'NOT INSTALLED', 'REPLICATION', '2.0', 'semisync_master.so', '1.13', 'He Zhenxing', 'Semi-synchronous replication master', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('SERVER_AUDIT', '1.4', 'NOT INSTALLED', 'AUDIT', '3.2', 'server_audit.so', '1.13', 'Alexey Botchkov (MariaDB Corporation)', 'Audit the server activity', 'GPL', 'OFF', 'Stable', '1.4.3');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_CACHE_INFO', '1.1', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'query_cache_info.so', '1.13', 'Roland Bouman, Daniel Black', 'Lists all queries in the query cache.', 'BSD', 'OFF', 'Stable', '1.1');
INSERT INTO `ALL_PLUGINS` VALUES ('SPHINX', '2.2', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_sphinx.so', '1.13', 'Sphinx developers', 'Sphinx storage engine 2.2.6-release', 'GPL', 'OFF', 'Gamma', '2.2.6-release');
INSERT INTO `ALL_PLUGINS` VALUES ('METADATA_LOCK_INFO', '0.1', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'metadata_lock_info.so', '1.13', 'Kentoku Shiba', 'Metadata locking viewer', 'GPL', 'OFF', 'Stable', '');
INSERT INTO `ALL_PLUGINS` VALUES ('LOCALES', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'locales.so', '1.13', 'Roberto Spadim, Spaempresarial - Brazil', 'Lists all locales from server.', 'BSD', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('rpl_semi_sync_slave', '1.0', 'NOT INSTALLED', 'REPLICATION', '2.0', 'semisync_slave.so', '1.13', 'He Zhenxing', 'Semi-synchronous replication slave', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_RESPONSE_TIME', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'query_response_time.so', '1.13', 'Percona and Sergey Vojtovich', 'Query Response Time Distribution INFORMATION_SCHEMA Plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('QUERY_RESPONSE_TIME_AUDIT', '1.0', 'NOT INSTALLED', 'AUDIT', '3.2', 'query_response_time.so', '1.13', 'Percona and Sergey Vojtovich', 'Query Response Time Distribution Audit Plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('WSREP_MEMBERSHIP', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'wsrep_info.so', '1.13', 'Nirbhay Choubey', 'Information about group members', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('WSREP_STATUS', '1.0', 'NOT INSTALLED', 'INFORMATION SCHEMA', '100214.0', 'wsrep_info.so', '1.13', 'Nirbhay Choubey', 'Group view information', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('simple_password_check', '1.0', 'NOT INSTALLED', 'PASSWORD VALIDATION', '1.0', 'simple_password_check.so', '1.13', 'Sergei Golubchik', 'Simple password strength checks', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('pam', '1.0', 'NOT INSTALLED', 'AUTHENTICATION', '2.1', 'auth_pam.so', '1.13', 'Sergei Golubchik', 'PAM based authentication', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('BLACKHOLE', '1.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_blackhole.so', '1.13', 'MySQL AB', '/dev/null storage engine (anything you write to it disappears)', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('unix_socket', '1.0', 'NOT INSTALLED', 'AUTHENTICATION', '2.1', 'auth_socket.so', '1.13', 'Sergei Golubchik', 'Unix Socket based authentication', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('ARCHIVE', '3.0', 'NOT INSTALLED', 'STORAGE ENGINE', '100214.0', 'ha_archive.so', '1.13', 'Brian Aker, MySQL AB', 'Archive storage engine', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('file_key_management', '1.0', 'NOT INSTALLED', 'ENCRYPTION', '3.0', 'file_key_management.so', '1.13', 'Denis Endro eperi GmbH', 'File-based key management plugin', 'GPL', 'OFF', 'Stable', '1.0');
INSERT INTO `ALL_PLUGINS` VALUES ('handlersocket', '1.0', 'NOT INSTALLED', 'DAEMON', '100214.0', 'handlersocket.so', '1.13', 'higuchi dot akira at dena dot jp', 'Direct access into InnoDB', 'BSD', 'OFF', 'Beta', '1.0');

#
# Table structure for table `APPLICABLE_ROLES`
#


DROP table IF EXISTS `APPLICABLE_ROLES`;
CREATE TEMPORARY TABLE `APPLICABLE_ROLES` (
  `GRANTEE` varchar(190) NOT NULL DEFAULT '',
  `ROLE_NAME` varchar(128) NOT NULL DEFAULT '',
  `IS_GRANTABLE` varchar(3) NOT NULL DEFAULT '',
  `IS_DEFAULT` varchar(3) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `APPLICABLE_ROLES`
#

#
# Dumping data for table `APPLICABLE_ROLES`
#


#
# Table structure for table `CHARACTER_SETS`
#


DROP table IF EXISTS `CHARACTER_SETS`;
CREATE TEMPORARY TABLE `CHARACTER_SETS` (
  `CHARACTER_SET_NAME` varchar(32) NOT NULL DEFAULT '',
  `DEFAULT_COLLATE_NAME` varchar(32) NOT NULL DEFAULT '',
  `DESCRIPTION` varchar(60) NOT NULL DEFAULT '',
  `MAXLEN` bigint(3) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `CHARACTER_SETS`
#

#
# Dumping data for table `CHARACTER_SETS`
#

INSERT INTO `CHARACTER_SETS` VALUES ('big5', 'big5_chinese_ci', 'Big5 Traditional Chinese', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('dec8', 'dec8_swedish_ci', 'DEC West European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('cp850', 'cp850_general_ci', 'DOS West European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('hp8', 'hp8_english_ci', 'HP West European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('koi8r', 'koi8r_general_ci', 'KOI8-R Relcom Russian', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('latin1', 'latin1_swedish_ci', 'cp1252 West European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('latin2', 'latin2_general_ci', 'ISO 8859-2 Central European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('swe7', 'swe7_swedish_ci', '7bit Swedish', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('ascii', 'ascii_general_ci', 'US ASCII', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('ujis', 'ujis_japanese_ci', 'EUC-JP Japanese', '3');
INSERT INTO `CHARACTER_SETS` VALUES ('sjis', 'sjis_japanese_ci', 'Shift-JIS Japanese', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('hebrew', 'hebrew_general_ci', 'ISO 8859-8 Hebrew', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('tis620', 'tis620_thai_ci', 'TIS620 Thai', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('euckr', 'euckr_korean_ci', 'EUC-KR Korean', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('koi8u', 'koi8u_general_ci', 'KOI8-U Ukrainian', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('gb2312', 'gb2312_chinese_ci', 'GB2312 Simplified Chinese', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('greek', 'greek_general_ci', 'ISO 8859-7 Greek', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('cp1250', 'cp1250_general_ci', 'Windows Central European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('gbk', 'gbk_chinese_ci', 'GBK Simplified Chinese', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('latin5', 'latin5_turkish_ci', 'ISO 8859-9 Turkish', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('armscii8', 'armscii8_general_ci', 'ARMSCII-8 Armenian', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('utf8', 'utf8_general_ci', 'UTF-8 Unicode', '3');
INSERT INTO `CHARACTER_SETS` VALUES ('ucs2', 'ucs2_general_ci', 'UCS-2 Unicode', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('cp866', 'cp866_general_ci', 'DOS Russian', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('keybcs2', 'keybcs2_general_ci', 'DOS Kamenicky Czech-Slovak', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('macce', 'macce_general_ci', 'Mac Central European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('macroman', 'macroman_general_ci', 'Mac West European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('cp852', 'cp852_general_ci', 'DOS Central European', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('latin7', 'latin7_general_ci', 'ISO 8859-13 Baltic', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('utf8mb4', 'utf8mb4_general_ci', 'UTF-8 Unicode', '4');
INSERT INTO `CHARACTER_SETS` VALUES ('cp1251', 'cp1251_general_ci', 'Windows Cyrillic', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('utf16', 'utf16_general_ci', 'UTF-16 Unicode', '4');
INSERT INTO `CHARACTER_SETS` VALUES ('utf16le', 'utf16le_general_ci', 'UTF-16LE Unicode', '4');
INSERT INTO `CHARACTER_SETS` VALUES ('cp1256', 'cp1256_general_ci', 'Windows Arabic', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('cp1257', 'cp1257_general_ci', 'Windows Baltic', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('utf32', 'utf32_general_ci', 'UTF-32 Unicode', '4');
INSERT INTO `CHARACTER_SETS` VALUES ('binary', 'binary', 'Binary pseudo charset', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('geostd8', 'geostd8_general_ci', 'GEOSTD8 Georgian', '1');
INSERT INTO `CHARACTER_SETS` VALUES ('cp932', 'cp932_japanese_ci', 'SJIS for Windows Japanese', '2');
INSERT INTO `CHARACTER_SETS` VALUES ('eucjpms', 'eucjpms_japanese_ci', 'UJIS for Windows Japanese', '3');

#
# Table structure for table `COLLATIONS`
#


DROP table IF EXISTS `COLLATIONS`;
CREATE TEMPORARY TABLE `COLLATIONS` (
  `COLLATION_NAME` varchar(32) NOT NULL DEFAULT '',
  `CHARACTER_SET_NAME` varchar(32) NOT NULL DEFAULT '',
  `ID` bigint(11) NOT NULL DEFAULT 0,
  `IS_DEFAULT` varchar(3) NOT NULL DEFAULT '',
  `IS_COMPILED` varchar(3) NOT NULL DEFAULT '',
  `SORTLEN` bigint(3) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `COLLATIONS`
#

#
# Dumping data for table `COLLATIONS`
#

INSERT INTO `COLLATIONS` VALUES ('big5_chinese_ci', 'big5', '1', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('big5_bin', 'big5', '84', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('big5_chinese_nopad_ci', 'big5', '1025', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('big5_nopad_bin', 'big5', '1108', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('dec8_swedish_ci', 'dec8', '3', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('dec8_bin', 'dec8', '69', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('dec8_swedish_nopad_ci', 'dec8', '1027', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('dec8_nopad_bin', 'dec8', '1093', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp850_general_ci', 'cp850', '4', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp850_bin', 'cp850', '80', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp850_general_nopad_ci', 'cp850', '1028', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp850_nopad_bin', 'cp850', '1104', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hp8_english_ci', 'hp8', '6', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hp8_bin', 'hp8', '72', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hp8_english_nopad_ci', 'hp8', '1030', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hp8_nopad_bin', 'hp8', '1096', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8r_general_ci', 'koi8r', '7', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8r_bin', 'koi8r', '74', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8r_general_nopad_ci', 'koi8r', '1031', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8r_nopad_bin', 'koi8r', '1098', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_german1_ci', 'latin1', '5', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_swedish_ci', 'latin1', '8', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_danish_ci', 'latin1', '15', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_german2_ci', 'latin1', '31', '', 'Yes', '2');
INSERT INTO `COLLATIONS` VALUES ('latin1_bin', 'latin1', '47', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_general_ci', 'latin1', '48', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_general_cs', 'latin1', '49', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_spanish_ci', 'latin1', '94', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_swedish_nopad_ci', 'latin1', '1032', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin1_nopad_bin', 'latin1', '1071', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_czech_cs', 'latin2', '2', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('latin2_general_ci', 'latin2', '9', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_hungarian_ci', 'latin2', '21', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_croatian_ci', 'latin2', '27', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_bin', 'latin2', '77', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_general_nopad_ci', 'latin2', '1033', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin2_nopad_bin', 'latin2', '1101', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('swe7_swedish_ci', 'swe7', '10', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('swe7_bin', 'swe7', '82', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('swe7_swedish_nopad_ci', 'swe7', '1034', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('swe7_nopad_bin', 'swe7', '1106', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ascii_general_ci', 'ascii', '11', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ascii_bin', 'ascii', '65', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ascii_general_nopad_ci', 'ascii', '1035', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ascii_nopad_bin', 'ascii', '1089', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ujis_japanese_ci', 'ujis', '12', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ujis_bin', 'ujis', '91', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ujis_japanese_nopad_ci', 'ujis', '1036', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ujis_nopad_bin', 'ujis', '1115', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('sjis_japanese_ci', 'sjis', '13', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('sjis_bin', 'sjis', '88', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('sjis_japanese_nopad_ci', 'sjis', '1037', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('sjis_nopad_bin', 'sjis', '1112', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hebrew_general_ci', 'hebrew', '16', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hebrew_bin', 'hebrew', '71', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hebrew_general_nopad_ci', 'hebrew', '1040', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('hebrew_nopad_bin', 'hebrew', '1095', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('tis620_thai_ci', 'tis620', '18', 'Yes', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('tis620_bin', 'tis620', '89', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('tis620_thai_nopad_ci', 'tis620', '1042', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('tis620_nopad_bin', 'tis620', '1113', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('euckr_korean_ci', 'euckr', '19', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('euckr_bin', 'euckr', '85', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('euckr_korean_nopad_ci', 'euckr', '1043', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('euckr_nopad_bin', 'euckr', '1109', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8u_general_ci', 'koi8u', '22', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8u_bin', 'koi8u', '75', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8u_general_nopad_ci', 'koi8u', '1046', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('koi8u_nopad_bin', 'koi8u', '1099', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gb2312_chinese_ci', 'gb2312', '24', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gb2312_bin', 'gb2312', '86', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gb2312_chinese_nopad_ci', 'gb2312', '1048', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gb2312_nopad_bin', 'gb2312', '1110', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('greek_general_ci', 'greek', '25', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('greek_bin', 'greek', '70', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('greek_general_nopad_ci', 'greek', '1049', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('greek_nopad_bin', 'greek', '1094', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_general_ci', 'cp1250', '26', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_czech_cs', 'cp1250', '34', '', 'Yes', '2');
INSERT INTO `COLLATIONS` VALUES ('cp1250_croatian_ci', 'cp1250', '44', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_bin', 'cp1250', '66', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_polish_ci', 'cp1250', '99', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_general_nopad_ci', 'cp1250', '1050', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1250_nopad_bin', 'cp1250', '1090', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gbk_chinese_ci', 'gbk', '28', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gbk_bin', 'gbk', '87', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gbk_chinese_nopad_ci', 'gbk', '1052', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('gbk_nopad_bin', 'gbk', '1111', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin5_turkish_ci', 'latin5', '30', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin5_bin', 'latin5', '78', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin5_turkish_nopad_ci', 'latin5', '1054', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin5_nopad_bin', 'latin5', '1102', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('armscii8_general_ci', 'armscii8', '32', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('armscii8_bin', 'armscii8', '64', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('armscii8_general_nopad_ci', 'armscii8', '1056', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('armscii8_nopad_bin', 'armscii8', '1088', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_general_ci', 'utf8', '33', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_bin', 'utf8', '83', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_unicode_ci', 'utf8', '192', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_icelandic_ci', 'utf8', '193', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_latvian_ci', 'utf8', '194', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_romanian_ci', 'utf8', '195', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_slovenian_ci', 'utf8', '196', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_polish_ci', 'utf8', '197', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_estonian_ci', 'utf8', '198', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_spanish_ci', 'utf8', '199', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_swedish_ci', 'utf8', '200', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_turkish_ci', 'utf8', '201', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_czech_ci', 'utf8', '202', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_danish_ci', 'utf8', '203', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_lithuanian_ci', 'utf8', '204', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_slovak_ci', 'utf8', '205', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_spanish2_ci', 'utf8', '206', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_roman_ci', 'utf8', '207', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_persian_ci', 'utf8', '208', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_esperanto_ci', 'utf8', '209', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_hungarian_ci', 'utf8', '210', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_sinhala_ci', 'utf8', '211', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_german2_ci', 'utf8', '212', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_croatian_mysql561_ci', 'utf8', '213', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_unicode_520_ci', 'utf8', '214', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_vietnamese_ci', 'utf8', '215', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_general_mysql500_ci', 'utf8', '223', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_croatian_ci', 'utf8', '576', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_myanmar_ci', 'utf8', '577', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_thai_520_w2', 'utf8', '578', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('utf8_general_nopad_ci', 'utf8', '1057', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_nopad_bin', 'utf8', '1107', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8_unicode_nopad_ci', 'utf8', '1216', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8_unicode_520_nopad_ci', 'utf8', '1238', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_general_ci', 'ucs2', '35', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ucs2_bin', 'ucs2', '90', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ucs2_unicode_ci', 'ucs2', '128', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_icelandic_ci', 'ucs2', '129', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_latvian_ci', 'ucs2', '130', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_romanian_ci', 'ucs2', '131', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_slovenian_ci', 'ucs2', '132', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_polish_ci', 'ucs2', '133', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_estonian_ci', 'ucs2', '134', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_spanish_ci', 'ucs2', '135', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_swedish_ci', 'ucs2', '136', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_turkish_ci', 'ucs2', '137', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_czech_ci', 'ucs2', '138', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_danish_ci', 'ucs2', '139', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_lithuanian_ci', 'ucs2', '140', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_slovak_ci', 'ucs2', '141', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_spanish2_ci', 'ucs2', '142', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_roman_ci', 'ucs2', '143', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_persian_ci', 'ucs2', '144', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_esperanto_ci', 'ucs2', '145', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_hungarian_ci', 'ucs2', '146', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_sinhala_ci', 'ucs2', '147', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_german2_ci', 'ucs2', '148', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_croatian_mysql561_ci', 'ucs2', '149', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_unicode_520_ci', 'ucs2', '150', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_vietnamese_ci', 'ucs2', '151', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_general_mysql500_ci', 'ucs2', '159', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ucs2_croatian_ci', 'ucs2', '640', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_myanmar_ci', 'ucs2', '641', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_thai_520_w2', 'ucs2', '642', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('ucs2_general_nopad_ci', 'ucs2', '1059', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ucs2_nopad_bin', 'ucs2', '1114', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('ucs2_unicode_nopad_ci', 'ucs2', '1152', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('ucs2_unicode_520_nopad_ci', 'ucs2', '1174', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('cp866_general_ci', 'cp866', '36', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp866_bin', 'cp866', '68', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp866_general_nopad_ci', 'cp866', '1060', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp866_nopad_bin', 'cp866', '1092', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('keybcs2_general_ci', 'keybcs2', '37', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('keybcs2_bin', 'keybcs2', '73', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('keybcs2_general_nopad_ci', 'keybcs2', '1061', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('keybcs2_nopad_bin', 'keybcs2', '1097', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macce_general_ci', 'macce', '38', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macce_bin', 'macce', '43', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macce_general_nopad_ci', 'macce', '1062', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macce_nopad_bin', 'macce', '1067', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macroman_general_ci', 'macroman', '39', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macroman_bin', 'macroman', '53', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macroman_general_nopad_ci', 'macroman', '1063', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('macroman_nopad_bin', 'macroman', '1077', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp852_general_ci', 'cp852', '40', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp852_bin', 'cp852', '81', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp852_general_nopad_ci', 'cp852', '1064', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp852_nopad_bin', 'cp852', '1105', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_estonian_cs', 'latin7', '20', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_general_ci', 'latin7', '41', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_general_cs', 'latin7', '42', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_bin', 'latin7', '79', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_general_nopad_ci', 'latin7', '1065', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('latin7_nopad_bin', 'latin7', '1103', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_general_ci', 'utf8mb4', '45', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_bin', 'utf8mb4', '46', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_unicode_ci', 'utf8mb4', '224', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_icelandic_ci', 'utf8mb4', '225', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_latvian_ci', 'utf8mb4', '226', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_romanian_ci', 'utf8mb4', '227', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_slovenian_ci', 'utf8mb4', '228', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_polish_ci', 'utf8mb4', '229', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_estonian_ci', 'utf8mb4', '230', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_spanish_ci', 'utf8mb4', '231', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_swedish_ci', 'utf8mb4', '232', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_turkish_ci', 'utf8mb4', '233', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_czech_ci', 'utf8mb4', '234', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_danish_ci', 'utf8mb4', '235', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_lithuanian_ci', 'utf8mb4', '236', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_slovak_ci', 'utf8mb4', '237', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_spanish2_ci', 'utf8mb4', '238', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_roman_ci', 'utf8mb4', '239', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_persian_ci', 'utf8mb4', '240', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_esperanto_ci', 'utf8mb4', '241', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_hungarian_ci', 'utf8mb4', '242', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_sinhala_ci', 'utf8mb4', '243', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_german2_ci', 'utf8mb4', '244', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_croatian_mysql561_ci', 'utf8mb4', '245', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_unicode_520_ci', 'utf8mb4', '246', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_vietnamese_ci', 'utf8mb4', '247', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_croatian_ci', 'utf8mb4', '608', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_myanmar_ci', 'utf8mb4', '609', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_thai_520_w2', 'utf8mb4', '610', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_general_nopad_ci', 'utf8mb4', '1069', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_nopad_bin', 'utf8mb4', '1070', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_unicode_nopad_ci', 'utf8mb4', '1248', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf8mb4_unicode_520_nopad_ci', 'utf8mb4', '1270', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('cp1251_bulgarian_ci', 'cp1251', '14', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_ukrainian_ci', 'cp1251', '23', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_bin', 'cp1251', '50', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_general_ci', 'cp1251', '51', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_general_cs', 'cp1251', '52', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_nopad_bin', 'cp1251', '1074', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1251_general_nopad_ci', 'cp1251', '1075', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16_general_ci', 'utf16', '54', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16_bin', 'utf16', '55', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16_unicode_ci', 'utf16', '101', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_icelandic_ci', 'utf16', '102', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_latvian_ci', 'utf16', '103', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_romanian_ci', 'utf16', '104', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_slovenian_ci', 'utf16', '105', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_polish_ci', 'utf16', '106', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_estonian_ci', 'utf16', '107', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_spanish_ci', 'utf16', '108', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_swedish_ci', 'utf16', '109', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_turkish_ci', 'utf16', '110', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_czech_ci', 'utf16', '111', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_danish_ci', 'utf16', '112', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_lithuanian_ci', 'utf16', '113', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_slovak_ci', 'utf16', '114', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_spanish2_ci', 'utf16', '115', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_roman_ci', 'utf16', '116', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_persian_ci', 'utf16', '117', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_esperanto_ci', 'utf16', '118', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_hungarian_ci', 'utf16', '119', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_sinhala_ci', 'utf16', '120', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_german2_ci', 'utf16', '121', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_croatian_mysql561_ci', 'utf16', '122', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_unicode_520_ci', 'utf16', '123', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_vietnamese_ci', 'utf16', '124', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_croatian_ci', 'utf16', '672', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_myanmar_ci', 'utf16', '673', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_thai_520_w2', 'utf16', '674', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('utf16_general_nopad_ci', 'utf16', '1078', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16_nopad_bin', 'utf16', '1079', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16_unicode_nopad_ci', 'utf16', '1125', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16_unicode_520_nopad_ci', 'utf16', '1147', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf16le_general_ci', 'utf16le', '56', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16le_bin', 'utf16le', '62', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16le_general_nopad_ci', 'utf16le', '1080', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf16le_nopad_bin', 'utf16le', '1086', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1256_general_ci', 'cp1256', '57', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1256_bin', 'cp1256', '67', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1256_general_nopad_ci', 'cp1256', '1081', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1256_nopad_bin', 'cp1256', '1091', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1257_lithuanian_ci', 'cp1257', '29', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1257_bin', 'cp1257', '58', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1257_general_ci', 'cp1257', '59', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1257_nopad_bin', 'cp1257', '1082', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp1257_general_nopad_ci', 'cp1257', '1083', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf32_general_ci', 'utf32', '60', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf32_bin', 'utf32', '61', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf32_unicode_ci', 'utf32', '160', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_icelandic_ci', 'utf32', '161', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_latvian_ci', 'utf32', '162', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_romanian_ci', 'utf32', '163', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_slovenian_ci', 'utf32', '164', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_polish_ci', 'utf32', '165', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_estonian_ci', 'utf32', '166', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_spanish_ci', 'utf32', '167', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_swedish_ci', 'utf32', '168', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_turkish_ci', 'utf32', '169', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_czech_ci', 'utf32', '170', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_danish_ci', 'utf32', '171', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_lithuanian_ci', 'utf32', '172', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_slovak_ci', 'utf32', '173', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_spanish2_ci', 'utf32', '174', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_roman_ci', 'utf32', '175', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_persian_ci', 'utf32', '176', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_esperanto_ci', 'utf32', '177', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_hungarian_ci', 'utf32', '178', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_sinhala_ci', 'utf32', '179', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_german2_ci', 'utf32', '180', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_croatian_mysql561_ci', 'utf32', '181', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_unicode_520_ci', 'utf32', '182', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_vietnamese_ci', 'utf32', '183', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_croatian_ci', 'utf32', '736', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_myanmar_ci', 'utf32', '737', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_thai_520_w2', 'utf32', '738', '', 'Yes', '4');
INSERT INTO `COLLATIONS` VALUES ('utf32_general_nopad_ci', 'utf32', '1084', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf32_nopad_bin', 'utf32', '1085', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('utf32_unicode_nopad_ci', 'utf32', '1184', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('utf32_unicode_520_nopad_ci', 'utf32', '1206', '', 'Yes', '8');
INSERT INTO `COLLATIONS` VALUES ('binary', 'binary', '63', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('geostd8_general_ci', 'geostd8', '92', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('geostd8_bin', 'geostd8', '93', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('geostd8_general_nopad_ci', 'geostd8', '1116', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('geostd8_nopad_bin', 'geostd8', '1117', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp932_japanese_ci', 'cp932', '95', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp932_bin', 'cp932', '96', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp932_japanese_nopad_ci', 'cp932', '1119', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('cp932_nopad_bin', 'cp932', '1120', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('eucjpms_japanese_ci', 'eucjpms', '97', 'Yes', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('eucjpms_bin', 'eucjpms', '98', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('eucjpms_japanese_nopad_ci', 'eucjpms', '1121', '', 'Yes', '1');
INSERT INTO `COLLATIONS` VALUES ('eucjpms_nopad_bin', 'eucjpms', '1122', '', 'Yes', '1');

#
# Table structure for table `COLLATION_CHARACTER_SET_APPLICABILITY`
#


DROP table IF EXISTS `COLLATION_CHARACTER_SET_APPLICABILITY`;
CREATE TEMPORARY TABLE `COLLATION_CHARACTER_SET_APPLICABILITY` (
  `COLLATION_NAME` varchar(32) NOT NULL DEFAULT '',
  `CHARACTER_SET_NAME` varchar(32) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `COLLATION_CHARACTER_SET_APPLICABILITY`
#

#
# Dumping data for table `COLLATION_CHARACTER_SET_APPLICABILITY`
#

INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('big5_chinese_ci', 'big5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('big5_bin', 'big5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('big5_chinese_nopad_ci', 'big5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('big5_nopad_bin', 'big5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('dec8_swedish_ci', 'dec8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('dec8_bin', 'dec8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('dec8_swedish_nopad_ci', 'dec8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('dec8_nopad_bin', 'dec8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp850_general_ci', 'cp850');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp850_bin', 'cp850');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp850_general_nopad_ci', 'cp850');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp850_nopad_bin', 'cp850');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hp8_english_ci', 'hp8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hp8_bin', 'hp8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hp8_english_nopad_ci', 'hp8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hp8_nopad_bin', 'hp8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8r_general_ci', 'koi8r');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8r_bin', 'koi8r');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8r_general_nopad_ci', 'koi8r');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8r_nopad_bin', 'koi8r');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_german1_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_swedish_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_danish_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_german2_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_bin', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_general_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_general_cs', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_spanish_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_swedish_nopad_ci', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin1_nopad_bin', 'latin1');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_czech_cs', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_general_ci', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_hungarian_ci', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_croatian_ci', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_bin', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_general_nopad_ci', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin2_nopad_bin', 'latin2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('swe7_swedish_ci', 'swe7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('swe7_bin', 'swe7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('swe7_swedish_nopad_ci', 'swe7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('swe7_nopad_bin', 'swe7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ascii_general_ci', 'ascii');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ascii_bin', 'ascii');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ascii_general_nopad_ci', 'ascii');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ascii_nopad_bin', 'ascii');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ujis_japanese_ci', 'ujis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ujis_bin', 'ujis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ujis_japanese_nopad_ci', 'ujis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ujis_nopad_bin', 'ujis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('sjis_japanese_ci', 'sjis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('sjis_bin', 'sjis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('sjis_japanese_nopad_ci', 'sjis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('sjis_nopad_bin', 'sjis');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hebrew_general_ci', 'hebrew');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hebrew_bin', 'hebrew');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hebrew_general_nopad_ci', 'hebrew');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('hebrew_nopad_bin', 'hebrew');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('tis620_thai_ci', 'tis620');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('tis620_bin', 'tis620');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('tis620_thai_nopad_ci', 'tis620');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('tis620_nopad_bin', 'tis620');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('euckr_korean_ci', 'euckr');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('euckr_bin', 'euckr');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('euckr_korean_nopad_ci', 'euckr');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('euckr_nopad_bin', 'euckr');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8u_general_ci', 'koi8u');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8u_bin', 'koi8u');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8u_general_nopad_ci', 'koi8u');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('koi8u_nopad_bin', 'koi8u');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gb2312_chinese_ci', 'gb2312');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gb2312_bin', 'gb2312');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gb2312_chinese_nopad_ci', 'gb2312');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gb2312_nopad_bin', 'gb2312');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('greek_general_ci', 'greek');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('greek_bin', 'greek');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('greek_general_nopad_ci', 'greek');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('greek_nopad_bin', 'greek');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_general_ci', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_czech_cs', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_croatian_ci', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_bin', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_polish_ci', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_general_nopad_ci', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1250_nopad_bin', 'cp1250');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gbk_chinese_ci', 'gbk');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gbk_bin', 'gbk');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gbk_chinese_nopad_ci', 'gbk');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('gbk_nopad_bin', 'gbk');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin5_turkish_ci', 'latin5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin5_bin', 'latin5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin5_turkish_nopad_ci', 'latin5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin5_nopad_bin', 'latin5');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('armscii8_general_ci', 'armscii8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('armscii8_bin', 'armscii8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('armscii8_general_nopad_ci', 'armscii8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('armscii8_nopad_bin', 'armscii8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_general_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_bin', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_unicode_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_icelandic_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_latvian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_romanian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_slovenian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_polish_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_estonian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_spanish_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_swedish_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_turkish_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_czech_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_danish_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_lithuanian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_slovak_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_spanish2_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_roman_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_persian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_esperanto_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_hungarian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_sinhala_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_german2_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_croatian_mysql561_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_unicode_520_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_vietnamese_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_general_mysql500_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_croatian_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_myanmar_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_thai_520_w2', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_general_nopad_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_nopad_bin', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_unicode_nopad_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8_unicode_520_nopad_ci', 'utf8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_general_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_bin', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_unicode_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_icelandic_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_latvian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_romanian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_slovenian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_polish_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_estonian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_spanish_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_swedish_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_turkish_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_czech_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_danish_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_lithuanian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_slovak_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_spanish2_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_roman_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_persian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_esperanto_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_hungarian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_sinhala_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_german2_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_croatian_mysql561_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_unicode_520_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_vietnamese_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_general_mysql500_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_croatian_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_myanmar_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_thai_520_w2', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_general_nopad_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_nopad_bin', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_unicode_nopad_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('ucs2_unicode_520_nopad_ci', 'ucs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp866_general_ci', 'cp866');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp866_bin', 'cp866');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp866_general_nopad_ci', 'cp866');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp866_nopad_bin', 'cp866');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('keybcs2_general_ci', 'keybcs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('keybcs2_bin', 'keybcs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('keybcs2_general_nopad_ci', 'keybcs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('keybcs2_nopad_bin', 'keybcs2');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macce_general_ci', 'macce');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macce_bin', 'macce');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macce_general_nopad_ci', 'macce');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macce_nopad_bin', 'macce');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macroman_general_ci', 'macroman');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macroman_bin', 'macroman');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macroman_general_nopad_ci', 'macroman');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('macroman_nopad_bin', 'macroman');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp852_general_ci', 'cp852');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp852_bin', 'cp852');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp852_general_nopad_ci', 'cp852');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp852_nopad_bin', 'cp852');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_estonian_cs', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_general_ci', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_general_cs', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_bin', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_general_nopad_ci', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('latin7_nopad_bin', 'latin7');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_general_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_bin', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_unicode_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_icelandic_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_latvian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_romanian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_slovenian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_polish_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_estonian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_spanish_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_swedish_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_turkish_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_czech_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_danish_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_lithuanian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_slovak_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_spanish2_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_roman_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_persian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_esperanto_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_hungarian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_sinhala_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_german2_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_croatian_mysql561_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_unicode_520_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_vietnamese_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_croatian_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_myanmar_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_thai_520_w2', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_general_nopad_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_nopad_bin', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_unicode_nopad_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf8mb4_unicode_520_nopad_ci', 'utf8mb4');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_bulgarian_ci', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_ukrainian_ci', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_bin', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_general_ci', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_general_cs', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_nopad_bin', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1251_general_nopad_ci', 'cp1251');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_general_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_bin', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_unicode_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_icelandic_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_latvian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_romanian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_slovenian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_polish_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_estonian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_spanish_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_swedish_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_turkish_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_czech_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_danish_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_lithuanian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_slovak_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_spanish2_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_roman_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_persian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_esperanto_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_hungarian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_sinhala_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_german2_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_croatian_mysql561_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_unicode_520_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_vietnamese_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_croatian_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_myanmar_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_thai_520_w2', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_general_nopad_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_nopad_bin', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_unicode_nopad_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16_unicode_520_nopad_ci', 'utf16');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16le_general_ci', 'utf16le');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16le_bin', 'utf16le');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16le_general_nopad_ci', 'utf16le');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf16le_nopad_bin', 'utf16le');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1256_general_ci', 'cp1256');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1256_bin', 'cp1256');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1256_general_nopad_ci', 'cp1256');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1256_nopad_bin', 'cp1256');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1257_lithuanian_ci', 'cp1257');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1257_bin', 'cp1257');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1257_general_ci', 'cp1257');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1257_nopad_bin', 'cp1257');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp1257_general_nopad_ci', 'cp1257');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_general_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_bin', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_unicode_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_icelandic_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_latvian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_romanian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_slovenian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_polish_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_estonian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_spanish_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_swedish_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_turkish_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_czech_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_danish_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_lithuanian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_slovak_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_spanish2_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_roman_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_persian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_esperanto_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_hungarian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_sinhala_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_german2_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_croatian_mysql561_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_unicode_520_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_vietnamese_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_croatian_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_myanmar_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_thai_520_w2', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_general_nopad_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_nopad_bin', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_unicode_nopad_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('utf32_unicode_520_nopad_ci', 'utf32');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('binary', 'binary');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('geostd8_general_ci', 'geostd8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('geostd8_bin', 'geostd8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('geostd8_general_nopad_ci', 'geostd8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('geostd8_nopad_bin', 'geostd8');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp932_japanese_ci', 'cp932');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp932_bin', 'cp932');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp932_japanese_nopad_ci', 'cp932');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('cp932_nopad_bin', 'cp932');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('eucjpms_japanese_ci', 'eucjpms');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('eucjpms_bin', 'eucjpms');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('eucjpms_japanese_nopad_ci', 'eucjpms');
INSERT INTO `COLLATION_CHARACTER_SET_APPLICABILITY` VALUES ('eucjpms_nopad_bin', 'eucjpms');

#
# Table structure for table `COLUMNS`
#


DROP table IF EXISTS `COLUMNS`;
CREATE TEMPORARY TABLE `COLUMNS` (
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `COLUMN_NAME` varchar(64) NOT NULL DEFAULT '',
  `ORDINAL_POSITION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `COLUMN_DEFAULT` longtext DEFAULT NULL,
  `IS_NULLABLE` varchar(3) NOT NULL DEFAULT '',
  `DATA_TYPE` varchar(64) NOT NULL DEFAULT '',
  `CHARACTER_MAXIMUM_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `CHARACTER_OCTET_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `NUMERIC_PRECISION` bigint(21) unsigned DEFAULT NULL,
  `NUMERIC_SCALE` bigint(21) unsigned DEFAULT NULL,
  `DATETIME_PRECISION` bigint(21) unsigned DEFAULT NULL,
  `CHARACTER_SET_NAME` varchar(32) DEFAULT NULL,
  `COLLATION_NAME` varchar(32) DEFAULT NULL,
  `COLUMN_TYPE` longtext NOT NULL DEFAULT '',
  `COLUMN_KEY` varchar(3) NOT NULL DEFAULT '',
  `EXTRA` varchar(30) NOT NULL DEFAULT '',
  `PRIVILEGES` varchar(80) NOT NULL DEFAULT '',
  `COLUMN_COMMENT` varchar(1024) NOT NULL DEFAULT '',
  `IS_GENERATED` varchar(6) NOT NULL DEFAULT '',
  `GENERATION_EXPRESSION` longtext DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `COLUMNS`
#

#
# Dumping data for table `COLUMNS`
#

INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_VERSION', '2', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_STATUS', '3', '\'\'', 'NO', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_TYPE', '4', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_TYPE_VERSION', '5', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_LIBRARY', '6', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_LIBRARY_VERSION', '7', 'NULL', 'YES', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_AUTHOR', '8', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_DESCRIPTION', '9', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_LICENSE', '10', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'LOAD_OPTION', '11', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_MATURITY', '12', '\'\'', 'NO', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'PLUGIN_AUTH_VERSION', '13', 'NULL', 'YES', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', 'GRANTEE', '1', '\'\'', 'NO', 'varchar', '190', '570', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(190)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', 'ROLE_NAME', '2', '\'\'', 'NO', 'varchar', '128', '384', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(128)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', 'IS_GRANTABLE', '3', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', 'IS_DEFAULT', '4', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CHARACTER_SETS', 'CHARACTER_SET_NAME', '1', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CHARACTER_SETS', 'DEFAULT_COLLATE_NAME', '2', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CHARACTER_SETS', 'DESCRIPTION', '3', '\'\'', 'NO', 'varchar', '60', '180', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(60)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CHARACTER_SETS', 'MAXLEN', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'COLLATION_NAME', '1', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'CHARACTER_SET_NAME', '2', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'ID', '3', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'IS_DEFAULT', '4', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'IS_COMPILED', '5', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATIONS', 'SORTLEN', '6', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATION_CHARACTER_SET_APPLICABILITY', 'COLLATION_NAME', '1', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLLATION_CHARACTER_SET_APPLICABILITY', 'CHARACTER_SET_NAME', '2', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLUMN_NAME', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'ORDINAL_POSITION', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLUMN_DEFAULT', '6', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'IS_NULLABLE', '7', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'DATA_TYPE', '8', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'CHARACTER_MAXIMUM_LENGTH', '9', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'CHARACTER_OCTET_LENGTH', '10', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'NUMERIC_PRECISION', '11', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'NUMERIC_SCALE', '12', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'DATETIME_PRECISION', '13', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'CHARACTER_SET_NAME', '14', 'NULL', 'YES', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLLATION_NAME', '15', 'NULL', 'YES', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLUMN_TYPE', '16', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLUMN_KEY', '17', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'EXTRA', '18', '\'\'', 'NO', 'varchar', '30', '90', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(30)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'PRIVILEGES', '19', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'COLUMN_COMMENT', '20', '\'\'', 'NO', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'IS_GENERATED', '21', '\'\'', 'NO', 'varchar', '6', '18', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMNS', 'GENERATION_EXPRESSION', '22', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'GRANTEE', '1', '\'\'', 'NO', 'varchar', '190', '570', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(190)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'TABLE_CATALOG', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'TABLE_SCHEMA', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'TABLE_NAME', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'COLUMN_NAME', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'PRIVILEGE_TYPE', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'IS_GRANTABLE', '7', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENABLED_ROLES', 'ROLE_NAME', '1', 'NULL', 'YES', 'varchar', '128', '384', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(128)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'ENGINE', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'SUPPORT', '2', '\'\'', 'NO', 'varchar', '8', '24', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'COMMENT', '3', '\'\'', 'NO', 'varchar', '160', '480', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(160)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'TRANSACTIONS', '4', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'XA', '5', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ENGINES', 'SAVEPOINTS', '6', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_CATALOG', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'DEFINER', '4', '\'\'', 'NO', 'varchar', '189', '567', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(189)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'TIME_ZONE', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_BODY', '6', '\'\'', 'NO', 'varchar', '8', '24', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_DEFINITION', '7', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_TYPE', '8', '\'\'', 'NO', 'varchar', '9', '27', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(9)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EXECUTE_AT', '9', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'INTERVAL_VALUE', '10', 'NULL', 'YES', 'varchar', '256', '768', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(256)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'INTERVAL_FIELD', '11', 'NULL', 'YES', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'SQL_MODE', '12', '\'\'', 'NO', 'varchar', '8192', '24576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'STARTS', '13', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'ENDS', '14', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'STATUS', '15', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'ON_COMPLETION', '16', '\'\'', 'NO', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'CREATED', '17', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'LAST_ALTERED', '18', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'LAST_EXECUTED', '19', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'EVENT_COMMENT', '20', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'ORIGINATOR', '21', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'CHARACTER_SET_CLIENT', '22', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'COLLATION_CONNECTION', '23', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'EVENTS', 'DATABASE_COLLATION', '24', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'FILE_ID', '1', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'FILE_NAME', '2', 'NULL', 'YES', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'FILE_TYPE', '3', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TABLESPACE_NAME', '4', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TABLE_CATALOG', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TABLE_SCHEMA', '6', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TABLE_NAME', '7', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'LOGFILE_GROUP_NAME', '8', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'LOGFILE_GROUP_NUMBER', '9', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'ENGINE', '10', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'FULLTEXT_KEYS', '11', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'DELETED_ROWS', '12', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'UPDATE_COUNT', '13', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'FREE_EXTENTS', '14', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TOTAL_EXTENTS', '15', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'EXTENT_SIZE', '16', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'INITIAL_SIZE', '17', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'MAXIMUM_SIZE', '18', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'AUTOEXTEND_SIZE', '19', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'CREATION_TIME', '20', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'LAST_UPDATE_TIME', '21', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'LAST_ACCESS_TIME', '22', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'RECOVER_TIME', '23', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TRANSACTION_COUNTER', '24', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'VERSION', '25', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'ROW_FORMAT', '26', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'TABLE_ROWS', '27', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'AVG_ROW_LENGTH', '28', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'DATA_LENGTH', '29', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'MAX_DATA_LENGTH', '30', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'INDEX_LENGTH', '31', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'DATA_FREE', '32', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'CREATE_TIME', '33', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'UPDATE_TIME', '34', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'CHECK_TIME', '35', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'CHECKSUM', '36', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'STATUS', '37', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'FILES', 'EXTRA', '38', 'NULL', 'YES', 'varchar', '255', '765', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(255)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GLOBAL_STATUS', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GLOBAL_STATUS', 'VARIABLE_VALUE', '2', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GLOBAL_VARIABLES', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GLOBAL_VARIABLES', 'VARIABLE_VALUE', '2', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'KEY_CACHE_NAME', '1', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'SEGMENTS', '2', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(3) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'SEGMENT_NUMBER', '3', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(3) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'FULL_SIZE', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'BLOCK_SIZE', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'USED_BLOCKS', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'UNUSED_BLOCKS', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'DIRTY_BLOCKS', '8', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'READ_REQUESTS', '9', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'READS', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'WRITE_REQUESTS', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_CACHES', 'WRITES', '12', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'CONSTRAINT_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'CONSTRAINT_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'CONSTRAINT_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'TABLE_CATALOG', '4', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'TABLE_SCHEMA', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'TABLE_NAME', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'COLUMN_NAME', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'ORDINAL_POSITION', '8', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'POSITION_IN_UNIQUE_CONSTRAINT', '9', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'REFERENCED_TABLE_SCHEMA', '10', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'REFERENCED_TABLE_NAME', '11', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'REFERENCED_COLUMN_NAME', '12', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'SPECIFIC_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'SPECIFIC_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'SPECIFIC_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'ORDINAL_POSITION', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'PARAMETER_MODE', '5', 'NULL', 'YES', 'varchar', '5', '15', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'PARAMETER_NAME', '6', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'DATA_TYPE', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'CHARACTER_MAXIMUM_LENGTH', '8', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'CHARACTER_OCTET_LENGTH', '9', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'NUMERIC_PRECISION', '10', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'NUMERIC_SCALE', '11', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'DATETIME_PRECISION', '12', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'CHARACTER_SET_NAME', '13', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'COLLATION_NAME', '14', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'DTD_IDENTIFIER', '15', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARAMETERS', 'ROUTINE_TYPE', '16', '\'\'', 'NO', 'varchar', '9', '27', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(9)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_NAME', '4', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'SUBPARTITION_NAME', '5', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_ORDINAL_POSITION', '6', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'SUBPARTITION_ORDINAL_POSITION', '7', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_METHOD', '8', 'NULL', 'YES', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'SUBPARTITION_METHOD', '9', 'NULL', 'YES', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_EXPRESSION', '10', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'SUBPARTITION_EXPRESSION', '11', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_DESCRIPTION', '12', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'TABLE_ROWS', '13', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'AVG_ROW_LENGTH', '14', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'DATA_LENGTH', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'MAX_DATA_LENGTH', '16', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'INDEX_LENGTH', '17', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'DATA_FREE', '18', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'CREATE_TIME', '19', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'UPDATE_TIME', '20', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'CHECK_TIME', '21', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'CHECKSUM', '22', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'PARTITION_COMMENT', '23', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'NODEGROUP', '24', '\'\'', 'NO', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PARTITIONS', 'TABLESPACE_NAME', '25', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_VERSION', '2', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_STATUS', '3', '\'\'', 'NO', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_TYPE', '4', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_TYPE_VERSION', '5', '\'\'', 'NO', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_LIBRARY', '6', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_LIBRARY_VERSION', '7', 'NULL', 'YES', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_AUTHOR', '8', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_DESCRIPTION', '9', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_LICENSE', '10', '\'\'', 'NO', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'LOAD_OPTION', '11', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_MATURITY', '12', '\'\'', 'NO', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PLUGINS', 'PLUGIN_AUTH_VERSION', '13', 'NULL', 'YES', 'varchar', '80', '240', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(80)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'ID', '1', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'USER', '2', '\'\'', 'NO', 'varchar', '128', '384', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(128)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'HOST', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'DB', '4', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'COMMAND', '5', '\'\'', 'NO', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'TIME', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'STATE', '7', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'INFO', '8', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'TIME_MS', '9', '0.000', 'NO', 'decimal', '', '', '22', '3', '', '', '', 'decimal(22,3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'STAGE', '10', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'MAX_STAGE', '11', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'PROGRESS', '12', '0.000', 'NO', 'decimal', '', '', '7', '3', '', '', '', 'decimal(7,3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'MEMORY_USED', '13', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'EXAMINED_ROWS', '14', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'QUERY_ID', '15', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'INFO_BINARY', '16', 'NULL', 'YES', 'blob', '65535', '65535', '', '', '', '', '', 'blob', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROCESSLIST', 'TID', '17', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'QUERY_ID', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'SEQ', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'STATE', '3', '\'\'', 'NO', 'varchar', '30', '90', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(30)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'DURATION', '4', '0.000000', 'NO', 'decimal', '', '', '9', '6', '', '', '', 'decimal(9,6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'CPU_USER', '5', 'NULL', 'YES', 'decimal', '', '', '9', '6', '', '', '', 'decimal(9,6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'CPU_SYSTEM', '6', 'NULL', 'YES', 'decimal', '', '', '9', '6', '', '', '', 'decimal(9,6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'CONTEXT_VOLUNTARY', '7', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'CONTEXT_INVOLUNTARY', '8', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'BLOCK_OPS_IN', '9', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'BLOCK_OPS_OUT', '10', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'MESSAGES_SENT', '11', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'MESSAGES_RECEIVED', '12', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'PAGE_FAULTS_MAJOR', '13', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'PAGE_FAULTS_MINOR', '14', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'SWAPS', '15', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'SOURCE_FUNCTION', '16', 'NULL', 'YES', 'varchar', '30', '90', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(30)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'SOURCE_FILE', '17', 'NULL', 'YES', 'varchar', '20', '60', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'PROFILING', 'SOURCE_LINE', '18', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(20)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'CONSTRAINT_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'CONSTRAINT_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'CONSTRAINT_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'UNIQUE_CONSTRAINT_CATALOG', '4', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'UNIQUE_CONSTRAINT_SCHEMA', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'UNIQUE_CONSTRAINT_NAME', '6', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'MATCH_OPTION', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'UPDATE_RULE', '8', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'DELETE_RULE', '9', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'TABLE_NAME', '10', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'REFERENCED_TABLE_NAME', '11', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'SPECIFIC_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_CATALOG', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_SCHEMA', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_NAME', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_TYPE', '5', '\'\'', 'NO', 'varchar', '9', '27', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(9)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'DATA_TYPE', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'CHARACTER_MAXIMUM_LENGTH', '7', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'CHARACTER_OCTET_LENGTH', '8', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'NUMERIC_PRECISION', '9', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'NUMERIC_SCALE', '10', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'DATETIME_PRECISION', '11', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'CHARACTER_SET_NAME', '12', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'COLLATION_NAME', '13', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'DTD_IDENTIFIER', '14', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_BODY', '15', '\'\'', 'NO', 'varchar', '8', '24', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_DEFINITION', '16', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'EXTERNAL_NAME', '17', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'EXTERNAL_LANGUAGE', '18', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'PARAMETER_STYLE', '19', '\'\'', 'NO', 'varchar', '8', '24', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'IS_DETERMINISTIC', '20', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'SQL_DATA_ACCESS', '21', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'SQL_PATH', '22', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'SECURITY_TYPE', '23', '\'\'', 'NO', 'varchar', '7', '21', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'CREATED', '24', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'LAST_ALTERED', '25', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'SQL_MODE', '26', '\'\'', 'NO', 'varchar', '8192', '24576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'ROUTINE_COMMENT', '27', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'DEFINER', '28', '\'\'', 'NO', 'varchar', '189', '567', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(189)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'CHARACTER_SET_CLIENT', '29', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'COLLATION_CONNECTION', '30', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'ROUTINES', 'DATABASE_COLLATION', '31', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMATA', 'CATALOG_NAME', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMATA', 'SCHEMA_NAME', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMATA', 'DEFAULT_CHARACTER_SET_NAME', '3', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMATA', 'DEFAULT_COLLATION_NAME', '4', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMATA', 'SQL_PATH', '5', 'NULL', 'YES', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'GRANTEE', '1', '\'\'', 'NO', 'varchar', '190', '570', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(190)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'TABLE_CATALOG', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'TABLE_SCHEMA', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'PRIVILEGE_TYPE', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'IS_GRANTABLE', '5', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SESSION_STATUS', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SESSION_STATUS', 'VARIABLE_VALUE', '2', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SESSION_VARIABLES', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SESSION_VARIABLES', 'VARIABLE_VALUE', '2', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'NON_UNIQUE', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'INDEX_SCHEMA', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'INDEX_NAME', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'SEQ_IN_INDEX', '7', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'COLUMN_NAME', '8', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'COLLATION', '9', 'NULL', 'YES', 'varchar', '1', '3', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'CARDINALITY', '10', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'SUB_PART', '11', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'PACKED', '12', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'NULLABLE', '13', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'INDEX_TYPE', '14', '\'\'', 'NO', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'COMMENT', '15', 'NULL', 'YES', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'STATISTICS', 'INDEX_COMMENT', '16', '\'\'', 'NO', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'SESSION_VALUE', '2', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'GLOBAL_VALUE', '3', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'GLOBAL_VALUE_ORIGIN', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'DEFAULT_VALUE', '5', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'VARIABLE_SCOPE', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'VARIABLE_TYPE', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'VARIABLE_COMMENT', '8', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'NUMERIC_MIN_VALUE', '9', 'NULL', 'YES', 'varchar', '21', '63', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'NUMERIC_MAX_VALUE', '10', 'NULL', 'YES', 'varchar', '21', '63', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'NUMERIC_BLOCK_SIZE', '11', 'NULL', 'YES', 'varchar', '21', '63', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'ENUM_VALUE_LIST', '12', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'READ_ONLY', '13', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'COMMAND_LINE_ARGUMENT', '14', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_TYPE', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'ENGINE', '5', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'VERSION', '6', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'ROW_FORMAT', '7', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_ROWS', '8', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'AVG_ROW_LENGTH', '9', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'DATA_LENGTH', '10', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'MAX_DATA_LENGTH', '11', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'INDEX_LENGTH', '12', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'DATA_FREE', '13', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'AUTO_INCREMENT', '14', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'CREATE_TIME', '15', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'UPDATE_TIME', '16', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'CHECK_TIME', '17', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_COLLATION', '18', 'NULL', 'YES', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'CHECKSUM', '19', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'CREATE_OPTIONS', '20', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLES', 'TABLE_COMMENT', '21', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'TABLESPACE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'ENGINE', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'TABLESPACE_TYPE', '3', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'LOGFILE_GROUP_NAME', '4', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'EXTENT_SIZE', '5', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'AUTOEXTEND_SIZE', '6', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'MAXIMUM_SIZE', '7', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'NODEGROUP_ID', '8', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLESPACES', 'TABLESPACE_COMMENT', '9', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'CONSTRAINT_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'CONSTRAINT_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'CONSTRAINT_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'TABLE_SCHEMA', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'TABLE_NAME', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'CONSTRAINT_TYPE', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'GRANTEE', '1', '\'\'', 'NO', 'varchar', '190', '570', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(190)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'TABLE_CATALOG', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'TABLE_SCHEMA', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'TABLE_NAME', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'PRIVILEGE_TYPE', '5', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'IS_GRANTABLE', '6', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'TRIGGER_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'TRIGGER_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'TRIGGER_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'EVENT_MANIPULATION', '4', '\'\'', 'NO', 'varchar', '6', '18', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'EVENT_OBJECT_CATALOG', '5', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'EVENT_OBJECT_SCHEMA', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'EVENT_OBJECT_TABLE', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_ORDER', '8', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(4)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_CONDITION', '9', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_STATEMENT', '10', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_ORIENTATION', '11', '\'\'', 'NO', 'varchar', '9', '27', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(9)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_TIMING', '12', '\'\'', 'NO', 'varchar', '6', '18', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(6)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_REFERENCE_OLD_TABLE', '13', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_REFERENCE_NEW_TABLE', '14', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_REFERENCE_OLD_ROW', '15', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'ACTION_REFERENCE_NEW_ROW', '16', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'CREATED', '17', 'NULL', 'YES', 'datetime', '', '', '', '', '2', '', '', 'datetime(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'SQL_MODE', '18', '\'\'', 'NO', 'varchar', '8192', '24576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'DEFINER', '19', '\'\'', 'NO', 'varchar', '189', '567', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(189)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'CHARACTER_SET_CLIENT', '20', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'COLLATION_CONNECTION', '21', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TRIGGERS', 'DATABASE_COLLATION', '22', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', 'GRANTEE', '1', '\'\'', 'NO', 'varchar', '190', '570', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(190)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', 'TABLE_CATALOG', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', 'PRIVILEGE_TYPE', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', 'IS_GRANTABLE', '4', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'VIEW_DEFINITION', '4', '\'\'', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8', 'utf8_general_ci', 'longtext', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'CHECK_OPTION', '5', '\'\'', 'NO', 'varchar', '8', '24', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'IS_UPDATABLE', '6', '\'\'', 'NO', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'DEFINER', '7', '\'\'', 'NO', 'varchar', '189', '567', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(189)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'SECURITY_TYPE', '8', '\'\'', 'NO', 'varchar', '7', '21', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'CHARACTER_SET_CLIENT', '9', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'COLLATION_CONNECTION', '10', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'VIEWS', 'ALGORITHM', '11', '\'\'', 'NO', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'F_TABLE_CATALOG', '1', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'F_TABLE_SCHEMA', '2', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'F_TABLE_NAME', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'F_GEOMETRY_COLUMN', '4', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'G_TABLE_CATALOG', '5', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'G_TABLE_SCHEMA', '6', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'G_TABLE_NAME', '7', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'G_GEOMETRY_COLUMN', '8', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'STORAGE_TYPE', '9', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'GEOMETRY_TYPE', '10', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(7)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'COORD_DIMENSION', '11', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'MAX_PPR', '12', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(2)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'SRID', '13', '0', 'NO', 'smallint', '', '', '5', '0', '', '', '', 'smallint(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', 'SRID', '1', '0', 'NO', 'smallint', '', '', '5', '0', '', '', '', 'smallint(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', 'AUTH_NAME', '2', '\'\'', 'NO', 'varchar', '512', '1536', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(512)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', 'AUTH_SRID', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', 'SRTEXT', '4', '\'\'', 'NO', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'CLIENT', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'TOTAL_CONNECTIONS', '2', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'CONCURRENT_CONNECTIONS', '3', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'CONNECTED_TIME', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'BUSY_TIME', '5', '0', 'NO', 'double', '', '', '21', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'CPU_TIME', '6', '0', 'NO', 'double', '', '', '21', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'BYTES_RECEIVED', '7', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'BYTES_SENT', '8', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'BINLOG_BYTES_WRITTEN', '9', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROWS_READ', '10', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROWS_SENT', '11', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROWS_DELETED', '12', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROWS_INSERTED', '13', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROWS_UPDATED', '14', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'SELECT_COMMANDS', '15', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'UPDATE_COMMANDS', '16', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'OTHER_COMMANDS', '17', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'COMMIT_TRANSACTIONS', '18', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ROLLBACK_TRANSACTIONS', '19', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'DENIED_CONNECTIONS', '20', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'LOST_CONNECTIONS', '21', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'ACCESS_DENIED', '22', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'EMPTY_QUERIES', '23', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'TOTAL_SSL_CONNECTIONS', '24', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'MAX_STATEMENT_TIME_EXCEEDED', '25', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', 'TABLE_SCHEMA', '1', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', 'TABLE_NAME', '2', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', 'INDEX_NAME', '3', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', 'ROWS_READ', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_DATAFILES', 'SPACE', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_DATAFILES', 'PATH', '2', '\'\'', 'NO', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'USER', '1', '\'\'', 'NO', 'varchar', '128', '384', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(128)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'TOTAL_CONNECTIONS', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'CONCURRENT_CONNECTIONS', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'CONNECTED_TIME', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'BUSY_TIME', '5', '0', 'NO', 'double', '', '', '21', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'CPU_TIME', '6', '0', 'NO', 'double', '', '', '21', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'BYTES_RECEIVED', '7', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'BYTES_SENT', '8', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'BINLOG_BYTES_WRITTEN', '9', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROWS_READ', '10', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROWS_SENT', '11', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROWS_DELETED', '12', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROWS_INSERTED', '13', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROWS_UPDATED', '14', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'SELECT_COMMANDS', '15', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'UPDATE_COMMANDS', '16', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'OTHER_COMMANDS', '17', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'COMMIT_TRANSACTIONS', '18', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ROLLBACK_TRANSACTIONS', '19', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'DENIED_CONNECTIONS', '20', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'LOST_CONNECTIONS', '21', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'ACCESS_DENIED', '22', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'EMPTY_QUERIES', '23', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'TOTAL_SSL_CONNECTIONS', '24', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'MAX_STATEMENT_TIME_EXCEEDED', '25', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'TABLE_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'STATS_INITIALIZED', '3', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'NUM_ROWS', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'CLUST_INDEX_SIZE', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'OTHER_INDEX_SIZE', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'MODIFIED_COUNTER', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'AUTOINC', '8', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'REF_COUNT', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_id', '1', '\'\'', 'NO', 'varchar', '81', '243', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(81)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_trx_id', '2', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_mode', '3', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_type', '4', '\'\'', 'NO', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_table', '5', '\'\'', 'NO', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_index', '6', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_space', '7', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_page', '8', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_rec', '9', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'lock_data', '10', 'NULL', 'YES', 'varchar', '8192', '24576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(8192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', 'NAME', '1', '\'\'', 'NO', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', 'CREATE_FILE', '2', '\'\'', 'NO', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', 'CREATE_LINE', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', 'OS_WAITS', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'page_size', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'buffer_pool_instance', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'pages_used', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'pages_free', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'relocation_ops', '5', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'relocation_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'database_name', '1', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'table_name', '2', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'index_name', '3', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'compress_ops', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'compress_ops_ok', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'compress_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'uncompress_ops', '7', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'uncompress_time', '8', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'page_size', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'compress_ops', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'compress_ops_ok', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'compress_time', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'uncompress_ops', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP', 'uncompress_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_DELETED', 'DOC_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'page_size', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'compress_ops', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'compress_ops_ok', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'compress_time', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'uncompress_ops', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'uncompress_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', 'requesting_trx_id', '1', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', 'requested_lock_id', '2', '\'\'', 'NO', 'varchar', '81', '243', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(81)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', 'blocking_trx_id', '3', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', 'blocking_lock_id', '4', '\'\'', 'NO', 'varchar', '81', '243', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(81)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'TABLE_SCHEMA', '1', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'TABLE_NAME', '2', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'ROWS_READ', '3', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'ROWS_CHANGED', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'ROWS_CHANGED_X_INDEXES', '5', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'SPACE', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'NAME', '2', 'NULL', 'YES', 'varchar', '655', '1965', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(655)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'ENCRYPTION_SCHEME', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'KEYSERVER_REQUESTS', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'MIN_KEY_VERSION', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'CURRENT_KEY_VERSION', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'KEY_ROTATION_PAGE_NUMBER', '7', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'KEY_ROTATION_MAX_PAGE_NUMBER', '8', 'NULL', 'YES', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'CURRENT_KEY_ID', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'ROTATING_OR_FLUSHING', '10', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'POOL_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'LRU_POSITION', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'SPACE', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'PAGE_NUMBER', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'PAGE_TYPE', '5', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'FLUSH_TYPE', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'FIX_COUNT', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'IS_HASHED', '8', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'NEWEST_MODIFICATION', '9', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'OLDEST_MODIFICATION', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'ACCESS_TIME', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'TABLE_NAME', '12', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'INDEX_NAME', '13', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'NUMBER_RECORDS', '14', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'DATA_SIZE', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'COMPRESSED_SIZE', '16', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'COMPRESSED', '17', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'IO_FIX', '18', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'IS_OLD', '19', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'FREE_PAGE_CLOCK', '20', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FIELDS', 'INDEX_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FIELDS', 'NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FIELDS', 'POS', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'page_size', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(5)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'buffer_pool_instance', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'pages_used', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'pages_free', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'relocation_ops', '5', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'relocation_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'TABLE_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'POS', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'MTYPE', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'PRTYPE', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'LEN', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'WORD', '1', '\'\'', 'NO', 'varchar', '337', '1011', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(337)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'FIRST_DOC_ID', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'LAST_DOC_ID', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'DOC_COUNT', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'DOC_ID', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'POSITION', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'database_name', '1', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'table_name', '2', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'index_name', '3', '\'\'', 'NO', 'varchar', '192', '576', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(192)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'compress_ops', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'compress_ops_ok', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'compress_time', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'uncompress_ops', '7', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'uncompress_time', '8', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'user_variables', 'VARIABLE_NAME', '1', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'user_variables', 'VARIABLE_VALUE', '2', 'NULL', 'YES', 'varchar', '2048', '6144', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(2048)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'user_variables', 'VARIABLE_TYPE', '3', '\'\'', 'NO', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'user_variables', 'CHARACTER_SET_NAME', '4', 'NULL', 'YES', 'varchar', '32', '96', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(32)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'WORD', '1', '\'\'', 'NO', 'varchar', '337', '1011', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(337)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'FIRST_DOC_ID', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'LAST_DOC_ID', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'DOC_COUNT', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'DOC_ID', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'POSITION', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', 'ID', '1', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', 'FOR_COL_NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', 'REF_COL_NAME', '3', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', 'POS', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_BEING_DELETED', 'DOC_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'POOL_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'POOL_SIZE', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'FREE_BUFFERS', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'DATABASE_PAGES', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'OLD_DATABASE_PAGES', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'MODIFIED_DATABASE_PAGES', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PENDING_DECOMPRESS', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PENDING_READS', '8', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PENDING_FLUSH_LRU', '9', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PENDING_FLUSH_LIST', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_MADE_YOUNG', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_NOT_MADE_YOUNG', '12', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_MADE_YOUNG_RATE', '13', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_MADE_NOT_YOUNG_RATE', '14', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_PAGES_READ', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_PAGES_CREATED', '16', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_PAGES_WRITTEN', '17', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_READ_RATE', '18', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_CREATE_RATE', '19', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'PAGES_WRITTEN_RATE', '20', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_PAGES_GET', '21', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'HIT_RATE', '22', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'YOUNG_MAKE_PER_THOUSAND_GETS', '23', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NOT_YOUNG_MAKE_PER_THOUSAND_GETS', '24', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_PAGES_READ_AHEAD', '25', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'NUMBER_READ_AHEAD_EVICTED', '26', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'READ_AHEAD_RATE', '27', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'READ_AHEAD_EVICTED_RATE', '28', '0', 'NO', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'LRU_IO_TOTAL', '29', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'LRU_IO_CURRENT', '30', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'UNCOMPRESS_TOTAL', '31', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'UNCOMPRESS_CURRENT', '32', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_id', '1', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_state', '2', '\'\'', 'NO', 'varchar', '13', '39', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(13)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_started', '3', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_requested_lock_id', '4', 'NULL', 'YES', 'varchar', '81', '243', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(81)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_wait_started', '5', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_weight', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_mysql_thread_id', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_query', '8', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_operation_state', '9', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_tables_in_use', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_tables_locked', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_lock_structs', '12', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_lock_memory_bytes', '13', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_rows_locked', '14', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_rows_modified', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_concurrency_tickets', '16', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_isolation_level', '17', '\'\'', 'NO', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_unique_checks', '18', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_foreign_key_checks', '19', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_last_foreign_key_error', '20', 'NULL', 'YES', 'varchar', '256', '768', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(256)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_adaptive_hash_latched', '21', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_is_read_only', '22', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TRX', 'trx_autocommit_non_locking', '23', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'ID', '1', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'FOR_NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'REF_NAME', '3', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'N_COLS', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'TYPE', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'TABLE_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'NAME', '2', '\'\'', 'NO', 'varchar', '655', '1965', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(655)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'FLAG', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'N_COLS', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'SPACE', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'FILE_FORMAT', '6', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'ROW_FORMAT', '7', 'NULL', 'YES', 'varchar', '12', '36', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(12)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'ZIP_PAGE_SIZE', '8', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'SPACE_TYPE', '9', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_DEFAULT_STOPWORD', 'value', '1', '\'\'', 'NO', 'varchar', '18', '54', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(18)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_CONFIG', 'KEY', '1', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_FT_CONFIG', 'VALUE', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'POOL_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'BLOCK_ID', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'SPACE', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'PAGE_NUMBER', '4', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'PAGE_TYPE', '5', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'FLUSH_TYPE', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'FIX_COUNT', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'IS_HASHED', '8', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'NEWEST_MODIFICATION', '9', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'OLDEST_MODIFICATION', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'ACCESS_TIME', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'TABLE_NAME', '12', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'INDEX_NAME', '13', 'NULL', 'YES', 'varchar', '1024', '3072', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(1024)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'NUMBER_RECORDS', '14', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'DATA_SIZE', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'COMPRESSED_SIZE', '16', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'PAGE_STATE', '17', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'IO_FIX', '18', 'NULL', 'YES', 'varchar', '64', '192', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(64)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'IS_OLD', '19', 'NULL', 'YES', 'varchar', '3', '9', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(3)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'FREE_PAGE_CLOCK', '20', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'SPACE', '1', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'NAME', '2', '\'\'', 'NO', 'varchar', '655', '1965', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(655)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'FLAG', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'FILE_FORMAT', '4', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'ROW_FORMAT', '5', 'NULL', 'YES', 'varchar', '22', '66', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(22)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'PAGE_SIZE', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'ZIP_PAGE_SIZE', '7', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'SPACE_TYPE', '8', 'NULL', 'YES', 'varchar', '10', '30', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(10)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'FS_BLOCK_SIZE', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'FILE_SIZE', '10', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'ALLOCATED_SIZE', '11', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'NAME', '1', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'SUBSYSTEM', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'COUNT', '3', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'MAX_COUNT', '4', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'MIN_COUNT', '5', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'AVG_COUNT', '6', 'NULL', 'YES', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'COUNT_RESET', '7', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'MAX_COUNT_RESET', '8', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'MIN_COUNT_RESET', '9', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'AVG_COUNT_RESET', '10', 'NULL', 'YES', 'double', '', '', '12', '', '', '', '', 'double', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'TIME_ENABLED', '11', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'TIME_DISABLED', '12', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'TIME_ELAPSED', '13', 'NULL', 'YES', 'bigint', '', '', '19', '0', '', '', '', 'bigint(21)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'TIME_RESET', '14', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'STATUS', '15', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'TYPE', '16', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'COMMENT', '17', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'INDEX_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'NAME', '2', '\'\'', 'NO', 'varchar', '193', '579', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(193)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'TABLE_ID', '3', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'TYPE', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'N_FIELDS', '5', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'PAGE_NO', '6', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'SPACE', '7', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'MERGE_THRESHOLD', '8', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_VIRTUAL', 'TABLE_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_VIRTUAL', 'POS', '2', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_VIRTUAL', 'BASE_POS', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'SPACE', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'NAME', '2', 'NULL', 'YES', 'varchar', '655', '1965', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(655)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'COMPRESSED', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'LAST_SCRUB_COMPLETED', '4', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'CURRENT_SCRUB_STARTED', '5', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'CURRENT_SCRUB_ACTIVE_THREADS', '6', 'NULL', 'YES', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'CURRENT_SCRUB_PAGE_NUMBER', '7', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'CURRENT_SCRUB_MAX_PAGE_NUMBER', '8', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'ROTATING_OR_FLUSHING', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'THREAD_ID', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'OBJECT_NAME', '2', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'FILE', '3', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LINE', '4', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'WAIT_TIME', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'WAIT_OBJECT', '6', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'WAIT_TYPE', '7', 'NULL', 'YES', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'HOLDER_THREAD_ID', '8', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'HOLDER_FILE', '9', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'HOLDER_LINE', '10', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'CREATED_FILE', '11', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'CREATED_LINE', '12', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'WRITER_THREAD', '13', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'RESERVATION_MODE', '14', 'NULL', 'YES', 'varchar', '16', '48', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(16)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'READERS', '15', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'WAITERS_FLAG', '16', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LOCK_WORD', '17', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(21) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LAST_READER_FILE', '18', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LAST_READER_LINE', '19', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LAST_WRITER_FILE', '20', 'NULL', 'YES', 'varchar', '4000', '12000', '', '', '', 'utf8', 'utf8_general_ci', 'varchar(4000)', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'LAST_WRITER_LINE', '21', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'OS_WAIT_COUNT', '22', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11) unsigned', '', '', 'select', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', 'meta_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', 'comment_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', 'meta_key', '3', 'NULL', 'YES', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', 'meta_value', '4', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_ID', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_post_ID', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_author', '3', '', 'NO', 'tinytext', '255', '255', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'tinytext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_author_email', '4', '\'\'', 'NO', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_author_url', '5', '\'\'', 'NO', 'varchar', '200', '800', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(200)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_author_IP', '6', '\'\'', 'NO', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_date', '7', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_date_gmt', '8', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_content', '9', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_karma', '10', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_approved', '11', '\'1\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_agent', '12', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_type', '13', '\'\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'comment_parent', '14', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'user_id', '15', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_url', '2', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_name', '3', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_image', '4', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_target', '5', '\'\'', 'NO', 'varchar', '25', '100', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(25)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_description', '6', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_visible', '7', '\'Y\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_owner', '8', '1', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_rating', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_updated', '10', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_rel', '11', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_notes', '12', '', 'NO', 'mediumtext', '16777215', '16777215', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'mediumtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'link_rss', '13', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', 'option_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', 'option_name', '2', '\'\'', 'NO', 'varchar', '191', '764', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(191)', 'UNI', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', 'option_value', '3', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', 'autoload', '4', '\'yes\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', 'meta_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', 'post_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', 'meta_key', '3', 'NULL', 'YES', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', 'meta_value', '4', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'ID', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_author', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_date', '3', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_date_gmt', '4', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_content', '5', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_title', '6', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_excerpt', '7', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_status', '8', '\'publish\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'comment_status', '9', '\'open\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'ping_status', '10', '\'open\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_password', '11', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_name', '12', '\'\'', 'NO', 'varchar', '200', '800', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(200)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'to_ping', '13', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'pinged', '14', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_modified', '15', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_modified_gmt', '16', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_content_filtered', '17', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_parent', '18', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'guid', '19', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'menu_order', '20', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_type', '21', '\'post\'', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'post_mime_type', '22', '\'\'', 'NO', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'comment_count', '23', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', 'object_id', '1', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', 'term_taxonomy_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', 'term_order', '3', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'term_taxonomy_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'term_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'taxonomy', '3', '\'\'', 'NO', 'varchar', '32', '128', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(32)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'description', '4', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'parent', '5', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'count', '6', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', 'meta_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', 'term_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', 'meta_key', '3', 'NULL', 'YES', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', 'meta_value', '4', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', 'term_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', 'name', '2', '\'\'', 'NO', 'varchar', '200', '800', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(200)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', 'slug', '3', '\'\'', 'NO', 'varchar', '200', '800', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(200)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', 'term_group', '4', '0', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(10)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', 'umeta_id', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', 'user_id', '2', '0', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', 'meta_key', '3', 'NULL', 'YES', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', 'meta_value', '4', 'NULL', 'YES', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'ID', '1', '', 'NO', 'bigint', '', '', '20', '0', '', '', '', 'bigint(20) unsigned', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_login', '2', '\'\'', 'NO', 'varchar', '60', '240', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(60)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_pass', '3', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_nicename', '4', '\'\'', 'NO', 'varchar', '50', '200', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(50)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_email', '5', '\'\'', 'NO', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', 'MUL', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_url', '6', '\'\'', 'NO', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_registered', '7', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_activation_key', '8', '\'\'', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'user_status', '9', '0', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'display_name', '10', '\'\'', 'NO', 'varchar', '250', '1000', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(250)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'ID', '1', '', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(20)', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'site_id', '2', '', 'NO', 'bigint', '', '', '19', '0', '', '', '', 'bigint(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'title', '3', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'description', '4', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'snippet', '5', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'wrap', '6', '1', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(1)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'placement', '7', '', 'NO', 'varchar', '20', '80', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(20)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'plugins', '8', '', 'NO', 'longtext', '4294967295', '4294967295', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'longtext', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'required', '9', '0', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(1)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'active', '10', '1', 'NO', 'tinyint', '', '', '3', '0', '', '', '', 'tinyint(1)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'date_modified', '11', '\'0000-00-00 00:00:00\'', 'NO', 'timestamp', '', '', '', '', '0', '', '', 'timestamp', '', 'on update current_timestamp()', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'date_created', '12', '\'0000-00-00 00:00:00\'', 'NO', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'id', '1', '', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(11)', 'PRI', 'auto_increment', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'name', '2', '', 'NO', 'varchar', '255', '1020', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(255)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'recurrence', '3', '', 'NO', 'varchar', '25', '100', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(25)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'params', '4', '', 'NO', 'text', '65535', '65535', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'text', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'start_at', '5', 'NULL', 'YES', 'datetime', '', '', '', '', '0', '', '', 'datetime', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'remote_storage', '6', 'NULL', 'YES', 'varchar', '10', '40', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(10)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'hash', '7', 'NULL', 'YES', 'varchar', '10', '40', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(10)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'status', '8', '', 'NO', 'int', '', '', '10', '0', '', '', '', 'int(1)', '', '', 'select,insert,update,references', '', 'NEVER', '');
INSERT INTO `COLUMNS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'last_backup', '9', 'NULL', 'YES', 'varchar', '100', '400', '', '', '', 'utf8mb4', 'utf8mb4_unicode_ci', 'varchar(100)', '', '', 'select,insert,update,references', '', 'NEVER', '');

#
# Table structure for table `COLUMN_PRIVILEGES`
#


DROP table IF EXISTS `COLUMN_PRIVILEGES`;
CREATE TEMPORARY TABLE `COLUMN_PRIVILEGES` (
  `GRANTEE` varchar(190) NOT NULL DEFAULT '',
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `COLUMN_NAME` varchar(64) NOT NULL DEFAULT '',
  `PRIVILEGE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `IS_GRANTABLE` varchar(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `COLUMN_PRIVILEGES`
#

#
# Dumping data for table `COLUMN_PRIVILEGES`
#


#
# Table structure for table `ENABLED_ROLES`
#


DROP table IF EXISTS `ENABLED_ROLES`;
CREATE TEMPORARY TABLE `ENABLED_ROLES` (
  `ROLE_NAME` varchar(128) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `ENABLED_ROLES`
#

#
# Dumping data for table `ENABLED_ROLES`
#

INSERT INTO `ENABLED_ROLES` VALUES ('');

#
# Table structure for table `ENGINES`
#


DROP table IF EXISTS `ENGINES`;
CREATE TEMPORARY TABLE `ENGINES` (
  `ENGINE` varchar(64) NOT NULL DEFAULT '',
  `SUPPORT` varchar(8) NOT NULL DEFAULT '',
  `COMMENT` varchar(160) NOT NULL DEFAULT '',
  `TRANSACTIONS` varchar(3) DEFAULT NULL,
  `XA` varchar(3) DEFAULT NULL,
  `SAVEPOINTS` varchar(3) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `ENGINES`
#

#
# Dumping data for table `ENGINES`
#

INSERT INTO `ENGINES` VALUES ('CSV', 'YES', 'CSV storage engine', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('MRG_MyISAM', 'YES', 'Collection of identical MyISAM tables', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('SEQUENCE', 'YES', 'Generated tables filled with sequential values', 'YES', 'NO', 'YES');
INSERT INTO `ENGINES` VALUES ('MyISAM', 'YES', 'MyISAM storage engine', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('MEMORY', 'YES', 'Hash based, stored in memory, useful for temporary tables', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('PERFORMANCE_SCHEMA', 'YES', 'Performance Schema', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('Aria', 'YES', 'Crash-safe tables with MyISAM heritage', 'NO', 'NO', 'NO');
INSERT INTO `ENGINES` VALUES ('InnoDB', 'DEFAULT', 'Supports transactions, row-level locking, foreign keys and encryption for tables', 'YES', 'YES', 'YES');

#
# Table structure for table `EVENTS`
#


DROP table IF EXISTS `EVENTS`;
CREATE TEMPORARY TABLE `EVENTS` (
  `EVENT_CATALOG` varchar(64) NOT NULL DEFAULT '',
  `EVENT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `EVENT_NAME` varchar(64) NOT NULL DEFAULT '',
  `DEFINER` varchar(189) NOT NULL DEFAULT '',
  `TIME_ZONE` varchar(64) NOT NULL DEFAULT '',
  `EVENT_BODY` varchar(8) NOT NULL DEFAULT '',
  `EVENT_DEFINITION` longtext NOT NULL DEFAULT '',
  `EVENT_TYPE` varchar(9) NOT NULL DEFAULT '',
  `EXECUTE_AT` datetime DEFAULT NULL,
  `INTERVAL_VALUE` varchar(256) DEFAULT NULL,
  `INTERVAL_FIELD` varchar(18) DEFAULT NULL,
  `SQL_MODE` varchar(8192) NOT NULL DEFAULT '',
  `STARTS` datetime DEFAULT NULL,
  `ENDS` datetime DEFAULT NULL,
  `STATUS` varchar(18) NOT NULL DEFAULT '',
  `ON_COMPLETION` varchar(12) NOT NULL DEFAULT '',
  `CREATED` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_ALTERED` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_EXECUTED` datetime DEFAULT NULL,
  `EVENT_COMMENT` varchar(64) NOT NULL DEFAULT '',
  `ORIGINATOR` bigint(10) NOT NULL DEFAULT 0,
  `CHARACTER_SET_CLIENT` varchar(32) NOT NULL DEFAULT '',
  `COLLATION_CONNECTION` varchar(32) NOT NULL DEFAULT '',
  `DATABASE_COLLATION` varchar(32) NOT NULL DEFAULT ''
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `EVENTS`
#

#
# Dumping data for table `EVENTS`
#


#
# Table structure for table `FILES`
#


DROP table IF EXISTS `FILES`;
CREATE TEMPORARY TABLE `FILES` (
  `FILE_ID` bigint(4) NOT NULL DEFAULT 0,
  `FILE_NAME` varchar(512) DEFAULT NULL,
  `FILE_TYPE` varchar(20) NOT NULL DEFAULT '',
  `TABLESPACE_NAME` varchar(64) DEFAULT NULL,
  `TABLE_CATALOG` varchar(64) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) DEFAULT NULL,
  `TABLE_NAME` varchar(64) DEFAULT NULL,
  `LOGFILE_GROUP_NAME` varchar(64) DEFAULT NULL,
  `LOGFILE_GROUP_NUMBER` bigint(4) DEFAULT NULL,
  `ENGINE` varchar(64) NOT NULL DEFAULT '',
  `FULLTEXT_KEYS` varchar(64) DEFAULT NULL,
  `DELETED_ROWS` bigint(4) DEFAULT NULL,
  `UPDATE_COUNT` bigint(4) DEFAULT NULL,
  `FREE_EXTENTS` bigint(4) DEFAULT NULL,
  `TOTAL_EXTENTS` bigint(4) DEFAULT NULL,
  `EXTENT_SIZE` bigint(4) NOT NULL DEFAULT 0,
  `INITIAL_SIZE` bigint(21) unsigned DEFAULT NULL,
  `MAXIMUM_SIZE` bigint(21) unsigned DEFAULT NULL,
  `AUTOEXTEND_SIZE` bigint(21) unsigned DEFAULT NULL,
  `CREATION_TIME` datetime DEFAULT NULL,
  `LAST_UPDATE_TIME` datetime DEFAULT NULL,
  `LAST_ACCESS_TIME` datetime DEFAULT NULL,
  `RECOVER_TIME` bigint(4) DEFAULT NULL,
  `TRANSACTION_COUNTER` bigint(4) DEFAULT NULL,
  `VERSION` bigint(21) unsigned DEFAULT NULL,
  `ROW_FORMAT` varchar(10) DEFAULT NULL,
  `TABLE_ROWS` bigint(21) unsigned DEFAULT NULL,
  `AVG_ROW_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `DATA_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `MAX_DATA_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `INDEX_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `DATA_FREE` bigint(21) unsigned DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `CHECK_TIME` datetime DEFAULT NULL,
  `CHECKSUM` bigint(21) unsigned DEFAULT NULL,
  `STATUS` varchar(20) NOT NULL DEFAULT '',
  `EXTRA` varchar(255) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `FILES`
#

#
# Dumping data for table `FILES`
#


#
# Table structure for table `GLOBAL_STATUS`
#


DROP table IF EXISTS `GLOBAL_STATUS`;
CREATE TEMPORARY TABLE `GLOBAL_STATUS` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_VALUE` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `GLOBAL_STATUS`
#

#
# Dumping data for table `GLOBAL_STATUS`
#

INSERT INTO `GLOBAL_STATUS` VALUES ('ABORTED_CLIENTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ABORTED_CONNECTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACCESS_DENIED_ERRORS', '68');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_COLUMN_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_DATABASE_GRANTS', '13');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_FUNCTION_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_PROCEDURE_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_PROXY_USERS', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_ROLE_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_ROLES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_TABLE_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ACL_USERS', '18');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_NOT_FLUSHED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_UNUSED', '15706');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_USED', '19');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_READ_REQUESTS', '647413');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_READS', '10643');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_WRITE_REQUESTS', '91884');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_PAGECACHE_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('ARIA_TRANSACTION_LOG_SYNCS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_COMMITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_GROUP_COMMITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_COUNT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_LOCK_WAIT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_TIMEOUT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_SNAPSHOT_FILE', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_SNAPSHOT_POSITION', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_BYTES_WRITTEN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_CACHE_DISK_USE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_CACHE_USE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_STMT_CACHE_DISK_USE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BINLOG_STMT_CACHE_USE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('BUSY_TIME', '0.000000');
INSERT INTO `GLOBAL_STATUS` VALUES ('BYTES_RECEIVED', '11078994');
INSERT INTO `GLOBAL_STATUS` VALUES ('BYTES_SENT', '309253172');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ADMIN_COMMANDS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_DB', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_DB_UPGRADE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_EVENT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_FUNCTION', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_PROCEDURE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_SERVER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_TABLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_TABLESPACE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ALTER_USER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ANALYZE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ASSIGN_TO_KEYCACHE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_BEGIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_BINLOG', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CALL_PROCEDURE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CHANGE_DB', '1497');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CHANGE_MASTER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CHECK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CHECKSUM', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_COMMIT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_COMPOUND_SQL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_DB', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_EVENT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_FUNCTION', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_INDEX', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_PROCEDURE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_ROLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_SERVER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_TABLE', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_TEMPORARY_TABLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_TRIGGER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_UDF', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_USER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_CREATE_VIEW', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DEALLOC_SQL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DELETE', '185');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DELETE_MULTI', '12');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DO', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_DB', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_EVENT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_FUNCTION', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_INDEX', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_PROCEDURE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_ROLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_SERVER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_TABLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_TEMPORARY_TABLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_TRIGGER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_USER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_DROP_VIEW', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_EMPTY_QUERY', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_EXECUTE_IMMEDIATE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_EXECUTE_SQL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_FLUSH', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_GET_DIAGNOSTICS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_GRANT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_GRANT_ROLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_HA_CLOSE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_HA_OPEN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_HA_READ', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_HELP', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_INSERT', '270');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_INSERT_SELECT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_INSTALL_PLUGIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_KILL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_LOAD', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_LOCK_TABLES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_MULTI', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_OPTIMIZE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_PRELOAD_KEYS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_PREPARE_SQL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_PURGE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_PURGE_BEFORE_DATE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_RELEASE_SAVEPOINT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_RENAME_TABLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_RENAME_USER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REPAIR', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REPLACE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REPLACE_SELECT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_RESET', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_RESIGNAL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REVOKE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REVOKE_ALL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_REVOKE_ROLE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ROLLBACK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_ROLLBACK_TO_SAVEPOINT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SAVEPOINT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SELECT', '52409');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SET_OPTION', '4601');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_AUTHORS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_BINLOG_EVENTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_BINLOGS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CHARSETS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_COLLATIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CONTRIBUTORS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_DB', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_EVENT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_FUNC', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_PROC', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_TABLE', '27');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_TRIGGER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_CREATE_USER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_DATABASES', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_ENGINE_LOGS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_ENGINE_MUTEX', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_ENGINE_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_ERRORS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_EVENTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_EXPLAIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_FIELDS', '169');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_FUNCTION_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_GENERIC', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_GRANTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_KEYS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_MASTER_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_OPEN_TABLES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PLUGINS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PRIVILEGES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PROCEDURE_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PROCESSLIST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PROFILE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_PROFILES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_RELAYLOG_EVENTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_SLAVE_HOSTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_SLAVE_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_STORAGE_ENGINES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_TABLE_STATUS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_TABLES', '145');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_TRIGGERS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_VARIABLES', '59');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHOW_WARNINGS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SHUTDOWN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_SIGNAL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_START_ALL_SLAVES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_START_SLAVE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_CLOSE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_EXECUTE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_FETCH', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_PREPARE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_REPREPARE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_RESET', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STMT_SEND_LONG_DATA', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STOP_ALL_SLAVES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_STOP_SLAVE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_TRUNCATE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_UNINSTALL_PLUGIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_UNLOCK_TABLES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_UPDATE', '443');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_UPDATE_MULTI', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_COMMIT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_END', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_PREPARE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_RECOVER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_ROLLBACK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COM_XA_START', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('COMPRESSION', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_ACCEPT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_INTERNAL', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_MAX_CONNECTIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_PEER_ADDRESS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_SELECT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTION_ERRORS_TCPWRAP', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('CONNECTIONS', '1530');
INSERT INTO `GLOBAL_STATUS` VALUES ('CPU_TIME', '0.000000');
INSERT INTO `GLOBAL_STATUS` VALUES ('CREATED_TMP_DISK_TABLES', '10925');
INSERT INTO `GLOBAL_STATUS` VALUES ('CREATED_TMP_FILES', '5');
INSERT INTO `GLOBAL_STATUS` VALUES ('CREATED_TMP_TABLES', '12391');
INSERT INTO `GLOBAL_STATUS` VALUES ('DELAYED_ERRORS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('DELAYED_INSERT_THREADS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('DELAYED_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('DELETE_SCAN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('EMPTY_QUERIES', '8499');
INSERT INTO `GLOBAL_STATUS` VALUES ('EXECUTED_EVENTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('EXECUTED_TRIGGERS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_CHECK_CONSTRAINT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_DELAY_KEY_WRITE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_DYNAMIC_COLUMNS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_FULLTEXT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_GIS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_LOCALE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_SUBQUERY', '26');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_TIMEZONE', '26');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_TRIGGER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_WINDOW_FUNCTIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FEATURE_XML', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('FLUSH_COMMANDS', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_COMMIT', '51552');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_DELETE', '189');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_DISCOVER', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_EXTERNAL_LOCK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_ICP_ATTEMPTS', '2342557');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_ICP_MATCH', '2342557');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_MRR_INIT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_MRR_KEY_REFILLS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_MRR_ROWID_REFILLS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_PREPARE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_FIRST', '2140');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_KEY', '511823');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_LAST', '4');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_NEXT', '2874346');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_PREV', '4475');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_RETRY', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_RND', '2491455');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_RND_DELETED', '3');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_READ_RND_NEXT', '748534');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_ROLLBACK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_SAVEPOINT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_SAVEPOINT_ROLLBACK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_TMP_UPDATE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_TMP_WRITE', '308353');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_UPDATE', '443');
INSERT INTO `GLOBAL_STATUS` VALUES ('HANDLER_WRITE', '282');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_DUMP_STATUS', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_LOAD_STATUS', 'Buffer pool(s) load completed at 180916  8:02:39');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_RESIZE_STATUS', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_DATA', '2475');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_BYTES_DATA', '40550400');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_DIRTY', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_BYTES_DIRTY', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_FLUSHED', '4039');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_FREE', '5685');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_MISC', '32');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_TOTAL', '8192');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD_RND', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD_EVICTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_REQUESTS', '7298197');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_READS', '2311');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_WAIT_FREE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_BUFFER_POOL_WRITE_REQUESTS', '12454');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_FSYNCS', '1950');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_PENDING_FSYNCS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_PENDING_READS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_PENDING_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_READ', '37947904');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_READS', '2511');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_WRITES', '5513');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DATA_WRITTEN', '134899712');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DBLWR_PAGES_WRITTEN', '3908');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DBLWR_WRITES', '174');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_LOG_WAITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_LOG_WRITE_REQUESTS', '6514');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_LOG_WRITES', '1137');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_OS_LOG_FSYNCS', '1290');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_OS_LOG_PENDING_FSYNCS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_OS_LOG_PENDING_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_OS_LOG_WRITTEN', '4617728');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGE_SIZE', '16384');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGES_CREATED', '164');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGES_READ', '2311');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGES0_READ', '189');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGES_WRITTEN', '4039');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROW_LOCK_CURRENT_WAITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROW_LOCK_TIME', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROW_LOCK_TIME_AVG', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROW_LOCK_TIME_MAX', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROW_LOCK_WAITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROWS_DELETED', '189');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROWS_INSERTED', '268');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROWS_READ', '5825411');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ROWS_UPDATED', '443');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SYSTEM_ROWS_DELETED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SYSTEM_ROWS_INSERTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SYSTEM_ROWS_READ', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SYSTEM_ROWS_UPDATED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_OPEN_FILES', '193');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_TRUNCATED_STATUS_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_AVAILABLE_UNDO_LOGS', '128');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_PAGE_COMPRESSION_SAVED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_INDEX_PAGES_WRITTEN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_NON_INDEX_PAGES_WRITTEN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_COMPRESSED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGE_COMPRESSED_TRIM_OP', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_DECOMPRESSED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_COMPRESSION_ERROR', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGES_ENCRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_NUM_PAGES_DECRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_LZ4', 'ON');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_LZO', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_LZMA', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_BZIP2', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_SNAPPY', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_HAVE_PUNCH_HOLE', 'ON');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DEFRAGMENT_COMPRESSION_FAILURES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DEFRAGMENT_FAILURES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_DEFRAGMENT_COUNT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ONLINEDDL_ROWLOG_ROWS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ONLINEDDL_ROWLOG_PCT_USED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ONLINEDDL_PCT_PROGRESS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SECONDARY_INDEX_TRIGGERED_CLUSTER_READS', '148170');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SECONDARY_INDEX_TRIGGERED_CLUSTER_READS_AVOIDED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_READ_FROM_CACHE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_READ_FROM_DISK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_MODIFIED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_FLUSHED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_ESTIMATED_IOPS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_KEY_ROTATION_LIST_LENGTH', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_N_MERGE_BLOCKS_ENCRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_N_MERGE_BLOCKS_DECRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_N_ROWLOG_BLOCKS_ENCRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_N_ROWLOG_BLOCKS_DECRYPTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_REORGANIZATIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_UNDERFLOW', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_OUT_OF_FILESPACE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_MISSING_INDEX', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_UNKNOWN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_SCRUB_LOG', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('INNODB_ENCRYPTION_NUM_KEY_REQUESTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_BLOCKS_NOT_FLUSHED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_BLOCKS_UNUSED', '107161');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_BLOCKS_USED', '2');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_BLOCKS_WARM', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_READ_REQUESTS', '16');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_READS', '2');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_WRITE_REQUESTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('KEY_WRITES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('LAST_QUERY_COST', '0.000000');
INSERT INTO `GLOBAL_STATUS` VALUES ('MASTER_GTID_WAIT_COUNT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('MASTER_GTID_WAIT_TIME', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('MASTER_GTID_WAIT_TIMEOUTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('MAX_STATEMENT_TIME_EXCEEDED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('MAX_USED_CONNECTIONS', '13');
INSERT INTO `GLOBAL_STATUS` VALUES ('MEMORY_USED', '277987472');
INSERT INTO `GLOBAL_STATUS` VALUES ('NOT_FLUSHED_DELAYED_ROWS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPEN_FILES', '27');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPEN_STREAMS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPEN_TABLE_DEFINITIONS', '112');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPEN_TABLES', '111');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPENED_FILES', '43888');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPENED_PLUGIN_LIBRARIES', '76');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPENED_TABLE_DEFINITIONS', '112');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPENED_TABLES', '117');
INSERT INTO `GLOBAL_STATUS` VALUES ('OPENED_VIEWS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_ACCOUNTS_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_COND_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_COND_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_DIGEST_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_HANDLES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_HOSTS_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_LOCKER_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_MUTEX_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_MUTEX_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_RWLOCK_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_RWLOCK_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_SESSION_CONNECT_ATTRS_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_SOCKET_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_SOCKET_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_STAGE_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_STATEMENT_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_TABLE_HANDLES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_TABLE_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_THREAD_CLASSES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_THREAD_INSTANCES_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PERFORMANCE_SCHEMA_USERS_LOST', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('PREPARED_STMT_COUNT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_FREE_BLOCKS', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_FREE_MEMORY', '1031336');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_HITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_INSERTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_LOWMEM_PRUNES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_NOT_CACHED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_QUERIES_IN_CACHE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('QCACHE_TOTAL_BLOCKS', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('QUERIES', '61368');
INSERT INTO `GLOBAL_STATUS` VALUES ('QUESTIONS', '61368');
INSERT INTO `GLOBAL_STATUS` VALUES ('ROWS_READ', '5825451');
INSERT INTO `GLOBAL_STATUS` VALUES ('ROWS_SENT', '2861920');
INSERT INTO `GLOBAL_STATUS` VALUES ('ROWS_TMP_READ', '527455');
INSERT INTO `GLOBAL_STATUS` VALUES ('RPL_STATUS', 'AUTH_MASTER');
INSERT INTO `GLOBAL_STATUS` VALUES ('SELECT_FULL_JOIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SELECT_FULL_RANGE_JOIN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SELECT_RANGE', '10650');
INSERT INTO `GLOBAL_STATUS` VALUES ('SELECT_RANGE_CHECK', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SELECT_SCAN', '7545');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_CONNECTIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_HEARTBEAT_PERIOD', '0.000');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_OPEN_TEMP_TABLES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_RECEIVED_HEARTBEATS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_RETRIED_TRANSACTIONS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_RUNNING', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVE_SKIPPED_ERRORS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVES_CONNECTED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLAVES_RUNNING', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLOW_LAUNCH_THREADS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SLOW_QUERIES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SORT_MERGE_PASSES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SORT_PRIORITY_QUEUE_SORTS', '55');
INSERT INTO `GLOBAL_STATUS` VALUES ('SORT_RANGE', '10525');
INSERT INTO `GLOBAL_STATUS` VALUES ('SORT_ROWS', '2492452');
INSERT INTO `GLOBAL_STATUS` VALUES ('SORT_SCAN', '10496');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_ACCEPT_RENEGOTIATES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_ACCEPTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CALLBACK_CACHE_HITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CIPHER', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CIPHER_LIST', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CLIENT_CONNECTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CONNECT_RENEGOTIATES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CTX_VERIFY_DEPTH', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_CTX_VERIFY_MODE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_DEFAULT_TIMEOUT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_FINISHED_ACCEPTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_FINISHED_CONNECTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SERVER_NOT_AFTER', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SERVER_NOT_BEFORE', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_HITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_MISSES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_MODE', 'NONE');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_OVERFLOWS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_SIZE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSION_CACHE_TIMEOUTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_SESSIONS_REUSED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_USED_SESSION_CACHE_ENTRIES', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_VERIFY_DEPTH', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_VERIFY_MODE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SSL_VERSION', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('SUBQUERY_CACHE_HIT', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SUBQUERY_CACHE_MISS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('SYNCS', '4');
INSERT INTO `GLOBAL_STATUS` VALUES ('TABLE_LOCKS_IMMEDIATE', '27');
INSERT INTO `GLOBAL_STATUS` VALUES ('TABLE_LOCKS_WAITED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('TC_LOG_MAX_PAGES_USED', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('TC_LOG_PAGE_SIZE', '4096');
INSERT INTO `GLOBAL_STATUS` VALUES ('TC_LOG_PAGE_WAITS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADPOOL_IDLE_THREADS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADPOOL_THREADS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADS_CACHED', '11');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADS_CONNECTED', '2');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADS_CREATED', '52');
INSERT INTO `GLOBAL_STATUS` VALUES ('THREADS_RUNNING', '1');
INSERT INTO `GLOBAL_STATUS` VALUES ('UPDATE_SCAN', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('UPTIME', '23333');
INSERT INTO `GLOBAL_STATUS` VALUES ('UPTIME_SINCE_FLUSH_STATUS', '23333');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_CLUSTER_CONF_ID', '18446744073709551615');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_CLUSTER_SIZE', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_CLUSTER_STATE_UUID', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_CLUSTER_STATUS', 'Disconnected');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_CONNECTED', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_LOCAL_BF_ABORTS', '0');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_LOCAL_INDEX', '18446744073709551615');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_PROVIDER_NAME', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_PROVIDER_VENDOR', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_PROVIDER_VERSION', '');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_READY', 'OFF');
INSERT INTO `GLOBAL_STATUS` VALUES ('WSREP_THREAD_COUNT', '0');

#
# Table structure for table `GLOBAL_VARIABLES`
#


DROP table IF EXISTS `GLOBAL_VARIABLES`;
CREATE TEMPORARY TABLE `GLOBAL_VARIABLES` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_VALUE` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `GLOBAL_VARIABLES`
#

#
# Dumping data for table `GLOBAL_VARIABLES`
#

INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FORCE_RECOVERY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_MODE', 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_LONG_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_USE_MMAP', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_CLASSES', '80');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_RUN_TRIGGERS_FOR_RBR', 'NO');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION', '10.2.14-MariaDB-10.2.14+maria~xenial');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_CLASSES', '40');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TIMEOUT', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_PAGECACHE_FILE_HASH_SIZE', '512');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSH_SYNC', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FORCE_LOAD_CORRUPTED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_PARALLEL_WORKERS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_ALLOWED_PACKET', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_FILES_IN_GROUP', '2');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_CLASSES', '200');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_TRANSIENT_SAMPLE_PAGES', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPTIMIZER_SWITCH', 'index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,index_merge_sort_intersection=off,engine_condition_pushdown=off,index_condition_pushdown=on,derived_merge=on,derived_with_keys=on,firstmatch=on,loosescan=on,materialization=on,in_to_exists=on,semijoin=on,partial_match_rowid_merge=on,partial_match_table_scan=on,subquery_cache=on,mrr=off,mrr_cost_based=off,mrr_sort_keys=off,outer_join_with_cache=on,semijoin_with_cache=on,join_cache_incremental=on,join_cache_hashed=on,join_cache_bka=on,optimize_join_buffer_size=off,table_elimination=on,extended_keys=on,exists_to_in=on,orderby_uses_equalities=on,condition_pushdown_for_derived=on');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLOW_LAUNCH_TIME', '2');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_ACCOUNTS_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPTIMIZER_PRUNE_LEVEL', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_CONNECT_ERRORS', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTS', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('NET_BUFFER_LENGTH', '16384');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_OVERSUBSCRIBE', '3');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_CACHE_SEGMENTS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_NUM_WORD_OPTIMIZE', '2000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_NOTIFY_CMD', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DRUPAL_282555_WORKAROUND', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STAGE_CLASSES', '150');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TMP_MEMORY_TABLE_SIZE', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MULTI_RANGE_COUNT', '256');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPORT_HOST', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EXPLICIT_DEFAULTS_FOR_TIMESTAMP', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SORT_BUFFER_SIZE', '1048576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_ACTORS_SIZE', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ROLLBACK_SEGMENTS', '128');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EVENT_SCHEDULER', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LARGE_FILES_SUPPORT', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FT_MAX_WORD_LEN', '84');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_PROFILING', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SESSION_TRACK_STATE_CHANGE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_AUTO_INCREMENT_CONTROL', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_COMPRESSED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_RESULTS', 'latin1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GROUP_CONCAT_MAX_LEN', '1048576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_WRITE_AHEAD_SIZE', '8192');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RANGE_ALLOC_BLOCK_SIZE', '4096');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_LONG_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_PARALLEL_MAX_QUEUED', '131072');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_CONNECTION', 'latin1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_TMPDIR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('COMPLETION_TYPE', 'NO_CHAIN');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ENCRYPTION_THREADS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OLD_PASSWORDS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_RECOVER_OPTIONS', 'BACKUP,QUICK');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN_INDEX', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_ERROR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PRELOAD_BUFFER_SIZE', '32768');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOW_PRIORITY_UPDATES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_CACHE_SIZE', '32768');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPORT_PASSWORD', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOWER_CASE_FILE_SYSTEM', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DBUG_OPTION', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_CONNECTIONS', '151');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_BIG_SELECTS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_OBJECTS_SIZE', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TRANSACTION_ALLOC_BLOCK_SIZE', '8192');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('JOIN_BUFFER_SIZE', '262144');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSHING_AVG_LOOPS', '30');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SETS_DIR', '/usr/share/mysql/charsets/');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYNC_FRM', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_DO_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_TRIM', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('AUTOMATIC_SP_PRIVILEGES', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_INSTRUMENT_SEMAPHORES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CONCURRENT_INSERT', 'AUTO');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_IO_CAPACITY_MAX', '2000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CONNECT_TIMEOUT', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_IO_CAPACITY', '200');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LARGE_PAGES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_CLUSTER_ADDRESS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_CACHE_FILE_HASH_SIZE', '512');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('USE_STAT_TABLES', 'NEVER');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_LONG_DATA_SIZE', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SOCKET', '/var/run/mysqld/mysqld.sock');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_CLASSES', '50');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMPRESSION_DEFAULT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LC_TIME_NAMES', 'en_US');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_AUTOEXTEND_INCREMENT', '64');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DELAY_KEY_WRITE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_DATA_POINTER_SIZE', '6');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WAIT_TIMEOUT', '28800');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FILE_FORMAT_MAX', 'Barracuda');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_STATEMENT_TIME', '0.000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_AUTO_IS_NULL', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LC_MESSAGES_DIR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_USED_FOR_TEMP_TABLES', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_PROVIDER', 'none');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_INDEX', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_DIGESTS_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_RETRY_AUTOCOMMIT', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_CLIENT', 'latin1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT_LOCKS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_BINLOG_CACHE_SIZE', '18446744073709547520');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_PREALLOC_SIZE', '24576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_BINLOG_SIZE', '1073741824');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SORT_BUFFER_SIZE', '2097152');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_MAX_WS_ROWS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ROLLBACK_ON_TIMEOUT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_QUERIES_NOT_USING_INDEXES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('STANDARD_COMPLIANT_CTE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('IGNORE_BUILTIN_INNODB', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_REPAIR_THREADS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN_COMPRESS_MIN_LEN', '256');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('JOIN_BUFFER_SPACE_LIMIT', '2097152');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOCK_WAIT_TIMEOUT', '86400');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FILE_FORMAT', 'Barracuda');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SESSION_CONNECT_ATTRS_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_LOG_FILE_SIZE', '1073741824');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DELAYED_QUEUE_SIZE', '1000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_WILD_DO_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_USEC', '100000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_BASENAME', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_DIGEST_LENGTH', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('READ_ONLY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_LOAD_DATA_SPLITTING', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TX_ISOLATION', 'REPEATABLE-READ');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_MAX_SLEEP_DELAY', '150000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('NET_READ_TIMEOUT', '30');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_IGNORE_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_PARALLEL_THREADS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DIRTY_READS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('AUTO_INCREMENT_OFFSET', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STRICT_MODE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CA', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PROGRESS_REPORT_TIME', '5');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OLD_ALTER_TABLE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_REJECT_QUERIES', 'NONE');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_USERS_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLOW_ADMIN_STATEMENTS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLOW_VERBOSITY', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_HEAP_TABLE_SIZE', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_IDLE_TIMEOUT', '60');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('NET_RETRY_COUNT', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOCK_SCHEDULE_ALGORITHM', 'vats');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('METADATA_LOCKS_HASH_INSTANCES', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_MAX_ALLOWED_PACKET', '1073741824');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TABLE_OPEN_CACHE_INSTANCES', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_INFO_FILE', 'relay-log.info');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_STMT_CACHE_SIZE', '32768');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_AUTOINC_LOCK_MODE', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_SELECT_LIMIT', '18446744073709551615');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LC_MESSAGES', 'en_US');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GENERAL_LOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOWER_CASE_TABLE_NAMES', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN_COMPRESS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_FILENAME', 'ib_buffer_pool');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_SERVER', 'latin1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG_DELAY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLOW_SLAVE_STATEMENTS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_NOW', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_PAGECACHE_DIVISION_LIMIT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_INTERVAL', '604800');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPTIMIZER_USE_CONDITION_SELECTIVITY', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SHOW_LOCKS_HELD', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_WARNINGS', '2');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_CLASSES', '50');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ENCRYPT_LOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_INSERT_DELAYED_THREADS', '20');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_OUTPUT', 'FILE');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('NET_WRITE_TIMEOUT', '60');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('STORED_PROGRAM_CACHE', '256');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FLUSH_TIME', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CRL', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_THREAD_CONCURRENCY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MTFLUSH_THREADS', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_SIZE', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_SESSION_MEM_USED', '9223372036854775807');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TABLE_OPEN_CACHE', '2000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DATA_HOME_DIR', '/var/lib/mysql/');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BULK_INSERT_BUFFER_SIZE', '8388608');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_COMPRESSED_PAGES', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FT_QUERY_EXPANSION_LIMIT', '20');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DISABLE_SORT_FILE_CACHE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INIT_FILE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATION_IOPS', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_DYNAMIC_LOADING', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_HANDLES', '32768');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SST_METHOD', 'rsync');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SKIP_NETWORKING', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PRINT_ALL_DEADLOCKS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_WLOCK_INVALIDATE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_TRADITIONAL', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_GEOMETRY', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OLD', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('UNIQUE_CHECKS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT', '75.000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_RESTART_SLAVE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_PATCH_VERSION', 'wsrep_25.23');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PLUGIN_DIR', '/usr/lib/mysql/plugin/');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_BUFFER_SIZE', '134217728');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PID_FILE', '/var/lib/mysql/telmo.pid');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_FALLOCATE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('READ_BUFFER_SIZE', '131072');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MIN_EXAMINED_ROW_LIMIT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYNC_RELAY_LOG', '10000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TMPDIR', '/tmp');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFAULT_ENCRYPTION_KEY_ID', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HOSTNAME', 'telmo');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_UNDO_LOG_SIZE', '10485760');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYSTEM_TIME_ZONE', 'UTC');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SKIP_EXTERNAL_LOCKING', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_MAX_THREADS', '65536');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_NET_TIMEOUT', '60');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_DIRECT_NON_TRANSACTIONAL_UPDATES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_SAMPLE_PAGES', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_PRIORITY', 'auto');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CAPATH', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INTERACTIVE_TIMEOUT', '28800');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION_COMMENT', 'mariadb.org binary distribution');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING_LWM', '10.000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_HOSTS_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FREQUENCY', '40');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_USER_CONNECTIONS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_WARNINGS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_COUNT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('STRICT_PASSWORD_VALIDATION', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SCHED_PRIORITY_CLEANER', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_MMAP_SIZE', '18446744073709551615');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_TRACK_REDO_LOG_NOW', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_SORT_LENGTH', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_STALL_LIMIT', '500');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TIMED_MUTEXES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_INSTANCES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TX_READ_ONLY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_CLASSES', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_RANDOM_READ_AHEAD', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_CURRENT_POS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_UNDO_LOGS', '128');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_SERVER_STOPWORD_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PORT', '3306');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION_COMPILE_MACHINE', 'x86_64');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INIT_CONNECT', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHECK_CONSTRAINT_CHECKS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN_TRUST_FUNCTION_CREATORS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TABLE_DEFINITION_CACHE', '400');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_CHANGED_PAGES', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_ALLOC_BLOCK_SIZE', '16384');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_BLOCK_SIZE', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('METADATA_LOCKS_CACHE_SIZE', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_BINLOG_STMT_CACHE_SIZE', '18446744073709547520');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TMP_TABLE_SIZE', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_POPULATE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_HANDLING', 'one-thread-per-connection');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SPIN_WAIT_DELAY', '6');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_TYPE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTITIONS', '8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION_MALLOC_LIBRARY', 'system');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT_STATS_ACCURACY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_SLAVE_POS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOCK_WAIT_TIMEOUT', '50');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_SP_RECURSION_DEPTH', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_TIME', '1000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPORT_PORT', '3306');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_LOG_BIN', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLOW_QUERY_LOG_FILE', 'telmo-slow.log');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_SYSTEM', 'utf8');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EXPIRE_LOGS_DAYS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMPRESSION_FAILURE_THRESHOLD_PCT', '5');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_EXEC_MODE', 'STRICT');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOCKED_IN_MEMORY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_FORMAT', 'MIXED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOCKING_FAKE_CHANGES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_LENGTH_FOR_SORT_DATA', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DISALLOW_WRITES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEEP_FILES_ON_CREATE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PREFIX_INDEX_CLUSTER_OPTIMIZATION', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FT_BOOLEAN_SYNTAX', '+ -><()~*:\"\"&|');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_RECOVER_OPTIONS', 'BACKUP,QUICK');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_DDL_EXEC_MODE', 'IDEMPOTENT');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_SKIP_ERRORS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_DATABASE', 'latin1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_PCT', '25');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DATE_FORMAT', '%Y-%m-%d');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ENCRYPT_TABLES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LONG_QUERY_TIME', '10.000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('COLLATION_DATABASE', 'latin1_swedish_ci');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_STACKTRACE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_COMPRESSED_PROTOCOL', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_BUFFER_SIZE', '16777216');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_UNDO_LOG_TRUNCATE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HOST_CACHE_SIZE', '279');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_CONVERT_LOCK_TO_TRX', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPORT_USER', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_MAX_SORT_FILE_SIZE', '9223372036853727232');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_CHECKPOINT_INTERVAL', '30');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_ARCH_EXPIRE_SEC', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN_BASENAME', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_LOG_CONFLICTS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLOW_QUERY_LOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OLD_MODE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_SIZE', '1048576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEFAULT_REGEX_FLAGS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SST_DONOR_REJECTS_QUERIES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HISTOGRAM_TYPE', 'SINGLE_PREC_HB');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPEN_FILES_LIMIT', '16364');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_COMPRESS', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('COLLATION_CONNECTION', 'latin1_swedish_ci');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_ATOMIC_WRITES', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TIME_FORMAT', '%H:%i:%s');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_CACHE_SIZE', '151');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PROTOCOL_VERSION', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DOUBLEWRITE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('AUTO_INCREMENT_INCREMENT', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEFAULT_WEEK_FORMAT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_IGNORE_DUPLICATES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CHANGE_BUFFER_MAX_SIZE', '25');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DEBUG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFAULT_ROW_FORMAT', 'dynamic');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BASEDIR', '/usr/');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_NATIVE_AIO', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TMP_DISK_TABLE_SIZE', '18446744073709551615');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_CACHE_DIVISION_LIMIT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_CRYPT', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_CHECKSUM_ALGORITHM', 'DEPRECATED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SLAVE_THREADS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_TRACK_CHANGED_PAGES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_OPENSSL', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_SHORT', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_ON', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_MIN_TOKEN_SIZE', '3');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEFAULT_STORAGE_ENGINE', 'InnoDB');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_USER_STOPWORD_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_RECURSIVE_ITERATIONS', '4294967295');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SERVER_ID', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('JOIN_CACHE_LEVEL', '2');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_MAX_SORT_FILE_SIZE', '9223372036853727232');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATE_KEY_AGE', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_TEMP_DATA_FILE_PATH', 'ibtmp1:12M:autoextend');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_NOTES', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SCRUB_LOG_SPEED', '256');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_MAX_WS_SIZE', '2147483647');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_SYNC_LOG_DIR', 'NEWFILE');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_ENABLE_STOPWORD', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYNC_RELAY_LOG_INFO', '10000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_CHECK_INTERVAL', '3600');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SLAVE_FK_CHECKS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_GROUP_COMMIT_INTERVAL', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SLAVE_UK_CHECKS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PROFILING_HISTORY_SIZE', '15');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_EMPTY_FREE_LIST_ALGORITHM', 'DEPRECATED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CHANGE_BUFFERING', 'all');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_START_POSITION', '00000000-0000-0000-0000-000000000000:-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_MODIFIED_COUNTER', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_SPACE_LIMIT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_OPTIMIZE_THREAD_SCHEDULING', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ENCRYPT_BINLOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_DIGEST_LENGTH', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_MTFLUSH', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_NODE_INCOMING_ADDRESS', 'AUTO');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CORRUPT_TABLE_ACTION', 'deprecated');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_IGNORE_DB', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_DO_DB', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMPRESSION_ALGORITHM', 'zlib');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_NODE_NAME', 'telmo');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PLUGIN_MATURITY', 'unknown');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_RTREE_KEYS', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PURGE_THREADS', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BIG_TABLES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PROFILING', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MASTER_VERIFY_CHECKSUM', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_PAGECACHE_BUFFER_SIZE', '134217728');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_VERSION', '5.7.21');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EXPENSIVE_SUBQUERY_LIMIT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_JOIN_SIZE', '18446744073709551615');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_AT_SHUTDOWN', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_BINLOG_POS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CRLPATH', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_WRITE_LOCK_COUNT', '4294967295');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_LONG', '15');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_PAGECACHE_AGE_THRESHOLD', '300');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MRR_BUFFER_SIZE', '262144');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EXTRA_MAX_CONNECTIONS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT_LWM', '0.000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_SLAVE_SKIP_COUNTER', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_CACHE_AGE_THRESHOLD', '300');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_DOMAIN_ID', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FOREIGN_KEY_CHECKS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_QUERY_CACHE', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_KILL_IDLE_TRANSACTION', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_SSL', 'DISABLED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_AUTO_RECALC', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_WILD_IGNORE_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMMIT_CONCURRENCY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SECURE_FILE_PRIV', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MIRRORED_LOG_GROUPS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_TC_SIZE', '24576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FATAL_SEMAPHORE_WAIT_THRESHOLD', '600');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SECURE_AUTH', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_IDLE_FLUSH_PCT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DIV_PRECISION_INCREMENT', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SUPPORT_XA', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DATETIME_FORMAT', '%Y-%m-%d %H:%i:%s');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SESSION_TRACK_TRANSACTION_INFO', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_INCLUDE_DELETE_MARKED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FILL_FACTOR', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_STRIP_COMMENTS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_BUFFER_RESULT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FILE_PER_TABLE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEFAULT_TMP_STORAGE_ENGINE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('EXTRA_PORT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_ANNOTATE_ROW_EVENTS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_BLOCK_SIZE', '8192');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_RELAY_LOG_SIZE', '1073741824');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_POOL_PRIO_KICKUP_TIMER', '1000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_ABORT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DATA_HOME_DIR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CERT', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_READ_IO_THREADS', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR', '0.900000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SHOW_VERBOSE_LOCKS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_INSTANCES', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_UNDO_DIRECTORY', './');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LICENSE', 'GPL');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GENERAL_LOG_FILE', 'telmo.log');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_RESULT_CACHE_LIMIT', '2000000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SYNC_WAIT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ROWID_MERGE_BUFF_SIZE', '8388608');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_KEY', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_CAUSAL_READS', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CLEANER_LSN_AGE_FACTOR', 'DEPRECATED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_LOAD_TMPDIR', '/tmp');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_TYPE_CONVERSIONS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FILE_FORMAT_CHECK', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_CONCURRENCY', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_READ_AHEAD_THRESHOLD', '56');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('KEY_CACHE_BLOCK_SIZE', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_READ_ONLY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_TMP_TABLES', '32');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_IMMEDIATE_SCRUB_DATA_UNCOMPRESSED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION_SSL_LIBRARY', 'OpenSSL 1.0.2g  1 Mar 2016');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MONITOR_RESET_ALL', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_LOG_OFF', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_OPTIMIZE_FULLTEXT_ONLY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_CHUNK_SIZE', '134217728');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_METHOD', 'nulls_equal');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR_N_RECS', '20');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SSL_CIPHER', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_TRANSACTION_RETRIES', '10');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLOW_RATE_LIMIT', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_SQL_VERIFY_CHECKSUM', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_BLOCK_SIZE', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FT_STOPWORD_FILE', '(built-in)');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_UNCOMPRESSED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPTIMIZER_SEARCH_DEPTH', '62');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_SIZE', '134217728');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_STATS_METHOD', 'NULLS_UNEQUAL');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOCAL_INFILE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMPRESSION_LEVEL', '6');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_NOW', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STATEMENT_CLASSES', '188');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('IGNORE_DB_DIRS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_ERROR_COUNT', '64');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_CHECKSUM', 'CRC32');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FT_MIN_WORD_LEN', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PAGE_SIZE', '16384');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_FORCED_BINLOG_FORMAT', 'NONE');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_ENCRYPT_TABLES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_FORCE_START_AFTER_RECOVERY_FAILURES', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_LONG_SIZE', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSH_METHOD', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DELAYED_INSERT_LIMIT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_DELAYED_THREADS', '20');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_CHECKPOINT_LOG_ACTIVITY', '1048576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SST_AUTH', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TRX_COMMIT', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('STORAGE_ENGINE', 'InnoDB');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_PROVIDER_OPTIONS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOCKS_UNSAFE_FOR_BINLOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_PREPARED_STMT_COUNT', '16382');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_CERTIFY_NONPK', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SST_DONOR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BINLOG_ROW_IMAGE', 'FULL');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_COMPRESSION_PAD_PCT_MAX', '50');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ENCRYPT_TMP_DISK_TABLES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_AT_STARTUP', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_CACHE_SIZE', '8000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('FLUSH', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_SAFE_UPDATES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLAVE_UPDATES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DELAYED_INSERT_TIMEOUT', '300');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_LONG', '50000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_GTID_MODE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_ANNOTATE_ROW_EVENTS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CMP_PER_INDEX_ENABLED', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEBUG_NO_THREAD_ALARM', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_ONLINE_ALTER_LOG_MAX_SIZE', '134217728');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYNC_MASTER_INFO', '10000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('UPDATABLE_VIEWS_WITH_LIMIT', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SKIP_NAME_RESOLVE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEADLOCK_DETECT', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_ON_METADATA', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_MAX_TOKEN_SIZE', '84');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('READ_BINLOG_SPEED_LIMIT', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_REPLICATE_MYISAM', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_STRICT_MODE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SQL_QUOTE_SHOW_CREATE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_GROUP_HOME_DIR', './');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_NODE_ADDRESS', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LARGE_PREFIX', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_SORT_PLL_DEGREE', '2');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DEFRAGMENT_N_PAGES', '7');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_HANDLES', '-1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_SLOW_FILTER', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_CHECKSUMS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PURGE_RSEG_TRUNCATE_FREQUENCY', '128');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_SORT_BUFFER_SIZE', '268434432');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_DATA_FILE_PATH', 'ibdata1:12M:autoextend');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_SORT_BUFFER_SIZE', '134216704');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DATADIR', '/var/lib/mysql/');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_PCT', '37');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('COLLATION_SERVER', 'latin1_swedish_ci');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYISAM_REPAIR_THREADS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SYNC_BINLOG', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LOG_BIN', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_AUX_TABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_PARALLEL_MODE', 'conservative');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_MYSQL_REPLICATION_BUNDLE', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SKIP_SHOW_DATABASE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('AUTOCOMMIT', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_NUMA_INTERLEAVE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('CHARACTER_SET_FILESYSTEM', 'binary');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('THREAD_STACK', '299008');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PAGE_CLEANERS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CHECKSUM_ALGORITHM', 'crc32');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CHECKSUMS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_LOG_PURGE_TYPE', 'immediate');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TRANSACTION_PREALLOC_SIZE', '4096');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MYSQL56_TEMPORAL_FORMAT', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('VERSION_COMPILE_OS', 'debian-linux-gnu');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('GTID_BINLOG_STATE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SYNC_ARRAY_SIZE', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_SHORT', '10000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_OPEN_FILES', '2000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_CLUSTER_NAME', 'my_wsrep_cluster');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_DESYNC', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_GROUP_COMMIT', 'none');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INIT_SLAVE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_FILE_SIZE', '50331648');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('REPLICATE_EVENTS_MARKED_FOR_SKIP', 'REPLICATE');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('TIME_ZONE', 'SYSTEM');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_RECOVERY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('USERSTAT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_OSU_METHOD', 'TOI');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SESSION_TRACK_SYSTEM_VARIABLES', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_TABLE_LOCKS', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_PURGE_BATCH_SIZE', '300');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_SST_RECEIVE_ADDRESS', 'AUTO');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FORCE_PRIMARY_KEY', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_ARCH_DIR', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_LIMIT', '1048576');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SLAVE_DOMAIN_PARALLEL_THREADS', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('RELAY_LOG_PURGE', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ENFORCE_STORAGE_ENGINE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_TOTAL_CACHE_SIZE', '640000000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FT_ENABLE_DIAG_PRINT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_PAGE_CHECKSUM', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HAVE_SYMLINK', 'YES');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('SESSION_TRACK_SCHEMA', 'ON');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FAST_SHUTDOWN', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('OPTIMIZER_SELECTIVITY_SAMPLING_LIMIT', '100');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FLUSH_NEIGHBORS', '1');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('BACK_LOG', '80');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SYNC_SPIN_LOOPS', '30');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT_SAMPLE_PAGES', '20');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_SCRUB_LOG', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_THREAD_SLEEP_DELAY', '10000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ENCRYPT_TMP_FILES', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_RECOVER', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('ARIA_STATS_METHOD', 'nulls_unequal');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_BUF_DUMP_STATUS_FREQUENCY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MONITOR_ENABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('WSREP_GTID_DOMAIN_ID', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MONITOR_RESET', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('MAX_SEEKS_FOR_KEY', '4294967295');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('HISTOGRAM_SIZE', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_REPLICATION_DELAY', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LRU_SCAN_DEPTH', '1024');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('QUERY_CACHE_MIN_RES_UNIT', '4096');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_UNDO_TABLESPACES', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_WRITE_IO_THREADS', '4');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MONITOR_DISABLE', '');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_CONCURRENCY_TICKETS', '5000');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_LOG_ARCHIVE', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_USE_GLOBAL_FLUSH_LOG_AT_TRX_COMMIT', 'OFF');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_MAX_BITMAP_FILE_SIZE', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('LARGE_PAGE_SIZE', '0');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FOREGROUND_PREFLUSH', 'DEPRECATED');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('READ_RND_BUFFER_SIZE', '262144');
INSERT INTO `GLOBAL_VARIABLES` VALUES ('INNODB_FAKE_CHANGES', 'OFF');

#
# Table structure for table `KEY_CACHES`
#


DROP table IF EXISTS `KEY_CACHES`;
CREATE TEMPORARY TABLE `KEY_CACHES` (
  `KEY_CACHE_NAME` varchar(192) NOT NULL DEFAULT '',
  `SEGMENTS` int(3) unsigned DEFAULT NULL,
  `SEGMENT_NUMBER` int(3) unsigned DEFAULT NULL,
  `FULL_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `BLOCK_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `USED_BLOCKS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `UNUSED_BLOCKS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DIRTY_BLOCKS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `READ_REQUESTS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `READS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `WRITE_REQUESTS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `WRITES` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `KEY_CACHES`
#

#
# Dumping data for table `KEY_CACHES`
#

INSERT INTO `KEY_CACHES` VALUES ('default', '', '', '134217728', '1024', '2', '107161', '0', '16', '2', '0', '0');

#
# Table structure for table `KEY_COLUMN_USAGE`
#


DROP table IF EXISTS `KEY_COLUMN_USAGE`;
CREATE TEMPORARY TABLE `KEY_COLUMN_USAGE` (
  `CONSTRAINT_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `CONSTRAINT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `CONSTRAINT_NAME` varchar(64) NOT NULL DEFAULT '',
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `COLUMN_NAME` varchar(64) NOT NULL DEFAULT '',
  `ORDINAL_POSITION` bigint(10) NOT NULL DEFAULT 0,
  `POSITION_IN_UNIQUE_CONSTRAINT` bigint(10) DEFAULT NULL,
  `REFERENCED_TABLE_SCHEMA` varchar(64) DEFAULT NULL,
  `REFERENCED_TABLE_NAME` varchar(64) DEFAULT NULL,
  `REFERENCED_COLUMN_NAME` varchar(64) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `KEY_COLUMN_USAGE`
#

#
# Dumping data for table `KEY_COLUMN_USAGE`
#

INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_commentmeta', 'meta_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_comments', 'comment_ID', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_links', 'link_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_options', 'option_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'option_name', 'def', 'superbiajuridico_test_development', 'wp_options', 'option_name', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_postmeta', 'meta_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_posts', 'ID', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_term_relationships', 'object_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_term_relationships', 'term_taxonomy_id', '2', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'term_taxonomy_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'term_id_taxonomy', 'def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'term_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'term_id_taxonomy', 'def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'taxonomy', '2', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_termmeta', 'meta_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_terms', 'term_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_usermeta', 'umeta_id', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_users', 'ID', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'ID', '1', '', '', '', '');
INSERT INTO `KEY_COLUMN_USAGE` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'id', '1', '', '', '', '');

#
# Table structure for table `PARAMETERS`
#


DROP table IF EXISTS `PARAMETERS`;
CREATE TEMPORARY TABLE `PARAMETERS` (
  `SPECIFIC_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `SPECIFIC_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `SPECIFIC_NAME` varchar(64) NOT NULL DEFAULT '',
  `ORDINAL_POSITION` int(21) NOT NULL DEFAULT 0,
  `PARAMETER_MODE` varchar(5) DEFAULT NULL,
  `PARAMETER_NAME` varchar(64) DEFAULT NULL,
  `DATA_TYPE` varchar(64) NOT NULL DEFAULT '',
  `CHARACTER_MAXIMUM_LENGTH` int(21) DEFAULT NULL,
  `CHARACTER_OCTET_LENGTH` int(21) DEFAULT NULL,
  `NUMERIC_PRECISION` int(21) DEFAULT NULL,
  `NUMERIC_SCALE` int(21) DEFAULT NULL,
  `DATETIME_PRECISION` bigint(21) unsigned DEFAULT NULL,
  `CHARACTER_SET_NAME` varchar(64) DEFAULT NULL,
  `COLLATION_NAME` varchar(64) DEFAULT NULL,
  `DTD_IDENTIFIER` longtext NOT NULL DEFAULT '',
  `ROUTINE_TYPE` varchar(9) NOT NULL DEFAULT ''
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `PARAMETERS`
#

#
# Dumping data for table `PARAMETERS`
#


#
# Table structure for table `PARTITIONS`
#


DROP table IF EXISTS `PARTITIONS`;
CREATE TEMPORARY TABLE `PARTITIONS` (
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `PARTITION_NAME` varchar(64) DEFAULT NULL,
  `SUBPARTITION_NAME` varchar(64) DEFAULT NULL,
  `PARTITION_ORDINAL_POSITION` bigint(21) unsigned DEFAULT NULL,
  `SUBPARTITION_ORDINAL_POSITION` bigint(21) unsigned DEFAULT NULL,
  `PARTITION_METHOD` varchar(18) DEFAULT NULL,
  `SUBPARTITION_METHOD` varchar(12) DEFAULT NULL,
  `PARTITION_EXPRESSION` longtext DEFAULT NULL,
  `SUBPARTITION_EXPRESSION` longtext DEFAULT NULL,
  `PARTITION_DESCRIPTION` longtext DEFAULT NULL,
  `TABLE_ROWS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `AVG_ROW_LENGTH` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DATA_LENGTH` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MAX_DATA_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `INDEX_LENGTH` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DATA_FREE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `CHECK_TIME` datetime DEFAULT NULL,
  `CHECKSUM` bigint(21) unsigned DEFAULT NULL,
  `PARTITION_COMMENT` varchar(80) NOT NULL DEFAULT '',
  `NODEGROUP` varchar(12) NOT NULL DEFAULT '',
  `TABLESPACE_NAME` varchar(64) DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `PARTITIONS`
#

#
# Dumping data for table `PARTITIONS`
#

INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'ALL_PLUGINS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', '', '', '', '', '', '', '', '', '', '0', '979', '0', '16691950', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'CHARACTER_SETS', '', '', '', '', '', '', '', '', '', '0', '384', '0', '16434816', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'COLLATIONS', '', '', '', '', '', '', '', '', '', '0', '231', '0', '16704765', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'COLLATION_CHARACTER_SET_APPLICABILITY', '', '', '', '', '', '', '', '', '', '0', '195', '0', '16357770', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'COLUMNS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', '', '', '', '', '', '', '', '', '', '0', '2893', '0', '16759149', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'ENABLED_ROLES', '', '', '', '', '', '', '', '', '', '0', '387', '0', '16563213', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'ENGINES', '', '', '', '', '', '', '', '', '', '0', '731', '0', '16663145', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'EVENTS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'FILES', '', '', '', '', '', '', '', '', '', '0', '4022', '0', '16767718', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'GLOBAL_STATUS', '', '', '', '', '', '', '', '', '', '0', '6340', '0', '16762960', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'GLOBAL_VARIABLES', '', '', '', '', '', '', '', '', '', '0', '6340', '0', '16762960', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'KEY_CACHES', '', '', '', '', '', '', '', '', '', '0', '659', '0', '16650294', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', '', '', '', '', '', '', '', '', '', '0', '4637', '0', '16762755', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'PARAMETERS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'PARTITIONS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'PLUGINS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'PROCESSLIST', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'PROFILING', '', '', '', '', '', '', '', '', '', '0', '308', '0', '16562084', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', '', '', '', '', '', '', '', '', '', '0', '4814', '0', '16767162', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'ROUTINES', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SCHEMATA', '', '', '', '', '', '', '', '', '', '0', '3464', '0', '16738048', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', '', '', '', '', '', '', '', '', '', '0', '2507', '0', '16741746', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SESSION_STATUS', '', '', '', '', '', '', '', '', '', '0', '6340', '0', '16762960', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SESSION_VARIABLES', '', '', '', '', '', '', '', '', '', '0', '6340', '0', '16762960', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'STATISTICS', '', '', '', '', '', '', '', '', '', '0', '5753', '0', '16752736', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TABLES', '', '', '', '', '', '', '', '', '', '0', '14829', '0', '16771599', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TABLESPACES', '', '', '', '', '', '', '', '', '', '0', '6951', '0', '16772763', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', '', '', '', '', '', '', '', '', '', '0', '2504', '0', '16721712', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', '', '', '', '', '', '', '', '', '', '0', '2700', '0', '16750800', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TRIGGERS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', '', '', '', '', '', '', '', '', '', '0', '2314', '0', '16732534', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'VIEWS', '', '', '', '', '', '', '', '', '', '0', '0', '8192', '4503599627288576', '8192', '0', '2018-09-16 14:31:35', '2018-09-16 14:31:35', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', '', '', '', '', '', '', '', '', '', '0', '4244', '0', '16759556', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', '', '', '', '', '', '', '', '', '', '0', '7691', '0', '16758689', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', '', '', '', '', '', '', '', '', '', '0', '386', '0', '16520414', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', '', '', '', '', '', '', '', '', '', '0', '1743', '0', '16765917', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_DATAFILES', '', '', '', '', '', '', '', '', '', '0', '12007', '0', '16773779', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'USER_STATISTICS', '', '', '', '', '', '', '', '', '', '0', '567', '0', '16747479', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', '', '', '', '', '', '', '', '', '', '0', '1215', '0', '16763355', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_LOCKS', '', '', '', '', '', '', '', '', '', '0', '31244', '0', '16746784', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', '', '', '', '', '', '', '', '', '', '0', '24017', '0', '16763866', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', '', '', '', '', '', '', '', '', '', '0', '29', '0', '15204352', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', '', '', '', '', '', '', '', '', '', '0', '1755', '0', '16728660', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMP', '', '', '', '', '', '', '', '', '', '0', '25', '0', '13107200', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_DELETED', '', '', '', '', '', '', '', '', '', '0', '9', '0', '9437184', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', '', '', '', '', '', '', '', '', '', '0', '25', '0', '13107200', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', '', '', '', '', '', '', '', '', '', '0', '599', '0', '16749238', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', '', '', '', '', '', '', '', '', '', '0', '1181', '0', '16733589', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', '', '', '', '', '', '', '', '', '', '0', '2012', '0', '16743864', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', '', '', '', '', '', '', '', '', '', '0', '6669', '0', '16765866', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_FIELDS', '', '', '', '', '', '', '', '', '', '0', '594', '0', '16609428', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', '', '', '', '', '', '', '', '', '', '0', '29', '0', '15204352', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', '', '', '', '', '', '', '', '', '', '0', '610', '0', '16613350', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', '', '', '', '', '', '', '', '', '', '0', '1054', '0', '16744898', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', '', '', '', '', '', '', '', '', '', '0', '1755', '0', '16728660', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'user_variables', '', '', '', '', '', '', '', '', '', '0', '6630', '0', '16767270', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', '', '', '', '', '', '', '', '', '', '0', '1054', '0', '16744898', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', '', '', '', '', '', '', '', '', '', '0', '1748', '0', '16738848', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_BEING_DELETED', '', '', '', '', '', '', '', '', '', '0', '9', '0', '9437184', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', '', '', '', '', '', '', '', '', '', '0', '257', '0', '16332350', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_TRX', '', '', '', '', '', '', '', '', '', '0', '4534', '0', '16766732', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', '', '', '', '', '', '', '', '', '', '0', '1752', '0', '16700064', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', '', '', '', '', '', '', '', '', '', '0', '2091', '0', '16736364', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_DEFAULT_STOPWORD', '', '', '', '', '', '', '', '', '', '0', '56', '0', '14680064', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_FT_CONFIG', '', '', '', '', '', '', '', '', '', '0', '1163', '0', '16705332', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', '', '', '', '', '', '', '', '', '', '0', '6852', '0', '16766844', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', '', '', '', '', '', '', '', '', '', '0', '2133', '0', '16752582', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_METRICS', '', '', '', '', '', '', '', '', '', '0', '3003', '0', '16747731', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', '', '', '', '', '', '', '', '', '', '0', '618', '0', '16615548', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_VIRTUAL', '', '', '', '', '', '', '', '', '', '0', '17', '0', '11883850', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', '', '', '', '', '', '', '', '', '', '0', '2020', '0', '16743780', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', '', '', '', '', '', '', '', '', '', '0', '72196', '0', '16749472', '0', '0', '2018-09-16 14:31:35', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '32768', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '81920', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '16384', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', '', '', '', '', '', '', '', '', '', '245', '4480', '1097728', '', '16384', '0', '2018-05-29 09:24:54', '2018-09-16 14:30:08', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', '', '', '', '', '', '', '', '', '', '3003', '92', '278528', '', '262144', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '', '', '', '', '', '', '', '', '', '303', '1568', '475136', '', '98304', '0', '2018-05-29 09:24:54', '2018-09-16 14:18:00', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', '', '', '', '', '', '', '', '', '', '112', '146', '16384', '', '16384', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', '', '', '', '', '', '', '', '', '', '23', '712', '16384', '', '32768', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '32768', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', '', '', '', '', '', '', '', '', '', '19', '862', '16384', '', '32768', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', '', '', '', '', '', '', '', '', '', '59', '277', '16384', '', '32768', '0', '2018-05-29 09:24:54', '2018-09-16 14:18:00', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '49152', '0', '2018-05-29 09:24:54', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', '', '', '', '', '', '', '', '', '', '1', '16384', '16384', '', '0', '0', '2018-07-06 16:43:32', '', '', '', '', '', '');
INSERT INTO `PARTITIONS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', '', '', '', '', '', '', '', '', '', '0', '0', '16384', '', '0', '0', '2018-09-16 14:18:46', '', '', '', '', '', '');

#
# Table structure for table `PLUGINS`
#


DROP table IF EXISTS `PLUGINS`;
CREATE TEMPORARY TABLE `PLUGINS` (
  `PLUGIN_NAME` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_STATUS` varchar(16) NOT NULL DEFAULT '',
  `PLUGIN_TYPE` varchar(80) NOT NULL DEFAULT '',
  `PLUGIN_TYPE_VERSION` varchar(20) NOT NULL DEFAULT '',
  `PLUGIN_LIBRARY` varchar(64) DEFAULT NULL,
  `PLUGIN_LIBRARY_VERSION` varchar(20) DEFAULT NULL,
  `PLUGIN_AUTHOR` varchar(64) DEFAULT NULL,
  `PLUGIN_DESCRIPTION` longtext DEFAULT NULL,
  `PLUGIN_LICENSE` varchar(80) NOT NULL DEFAULT '',
  `LOAD_OPTION` varchar(64) NOT NULL DEFAULT '',
  `PLUGIN_MATURITY` varchar(12) NOT NULL DEFAULT '',
  `PLUGIN_AUTH_VERSION` varchar(80) DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `PLUGINS`
#

#
# Dumping data for table `PLUGINS`
#

INSERT INTO `PLUGINS` VALUES ('binlog', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'This is a pseudo storage engine to represent the binlog in a transaction', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('mysql_native_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Native MySQL authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('mysql_old_password', '1.0', 'ACTIVE', 'AUTHENTICATION', '2.1', '', '', 'R.J.Silk, Sergei Golubchik', 'Old MySQL-4.0 authentication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('wsrep', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Codership Oy', 'A pseudo storage engine to represent transactions in multi-master synchornous replication', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('MRG_MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Collection of identical MyISAM tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('CSV', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Brian Aker, MySQL AB', 'CSV storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('MyISAM', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'MyISAM storage engine', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('MEMORY', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'MySQL AB', 'Hash based, stored in memory, useful for temporary tables', 'GPL', 'FORCE', 'Stable', '1.0');
INSERT INTO `PLUGINS` VALUES ('CLIENT_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Client Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `PLUGINS` VALUES ('INDEX_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Index Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `PLUGINS` VALUES ('TABLE_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'Table Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `PLUGINS` VALUES ('USER_STATISTICS', '2.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Percona and Sergei Golubchik', 'User Statistics', 'GPL', 'FORCE', 'Stable', '2.0');
INSERT INTO `PLUGINS` VALUES ('PERFORMANCE_SCHEMA', '0.1', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Marc Alff, Oracle', 'Performance Schema', 'GPL', 'FORCE', 'Stable', '5.6.36');
INSERT INTO `PLUGINS` VALUES ('Aria', '1.5', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Monty Program Ab', 'Crash-safe tables with MyISAM heritage', 'GPL', 'ON', 'Stable', '1.5');
INSERT INTO `PLUGINS` VALUES ('InnoDB', '5.7', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Oracle Corporation', 'Supports transactions, row-level locking, foreign keys and encryption for tables', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_TRX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB transactions', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_LOCKS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB conflicting locks', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_LOCK_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB which lock is blocking which', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMP', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMP_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMPMEM', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMPMEM_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compressed buffer pool; reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMP_PER_INDEX', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index)', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_CMP_PER_INDEX_RESET', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Statistics for the InnoDB compression (per index); reset cumulated counts', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_BUFFER_PAGE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page Information', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_BUFFER_PAGE_LRU', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Page in LRU', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_BUFFER_POOL_STATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Buffer Pool Statistics Information ', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_METRICS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB Metrics Info', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_DEFAULT_STOPWORD', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'Default stopword list for InnoDB Full Text Search', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_BEING_DELETED', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS BEING DELETED TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_CONFIG', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS CONFIG TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_INDEX_CACHE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX CACHED', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_FT_INDEX_TABLE', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'INNODB AUXILIARY FTS INDEX TABLE', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_TABLES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_TABLESTATS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESTATS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_INDEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_INDEXES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_COLUMNS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_COLUMNS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_FIELDS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FIELDS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_FOREIGN', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_FOREIGN_COLS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_FOREIGN_COLS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_TABLESPACES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_TABLESPACES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_DATAFILES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_VIRTUAL', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_VIRTUAL', 'GPL', 'ON', 'Beta', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_MUTEXES', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Oracle Corporation', 'InnoDB SYS_DATAFILES', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_SYS_SEMAPHORE_WAITS', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'MariaDB Corporation', 'InnoDB SYS_SEMAPHORE_WAITS', 'GPL', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_TABLESPACES_ENCRYPTION', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_ENCRYPTION', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('INNODB_TABLESPACES_SCRUBBING', '5.7', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Google Inc', 'InnoDB TABLESPACES_SCRUBBING', 'BSD', 'ON', 'Stable', '5.7.21');
INSERT INTO `PLUGINS` VALUES ('SEQUENCE', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Sergei Golubchik', 'Generated tables filled with sequential values', 'GPL', 'ON', 'Stable', '0.1');
INSERT INTO `PLUGINS` VALUES ('FEEDBACK', '1.1', 'DISABLED', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergei Golubchik', 'MariaDB User Feedback Plugin', 'GPL', 'OFF', 'Stable', '1.1');
INSERT INTO `PLUGINS` VALUES ('user_variables', '1.0', 'ACTIVE', 'INFORMATION SCHEMA', '100214.0', '', '', 'Sergey Vojtovich', 'User-defined variables', 'GPL', 'ON', 'Gamma', '1.0');
INSERT INTO `PLUGINS` VALUES ('partition', '1.0', 'ACTIVE', 'STORAGE ENGINE', '100214.0', '', '', 'Mikael Ronstrom, MySQL AB', 'Partition Storage Engine Helper', 'GPL', 'ON', 'Stable', '1.0');

#
# Table structure for table `PROCESSLIST`
#


DROP table IF EXISTS `PROCESSLIST`;
CREATE TEMPORARY TABLE `PROCESSLIST` (
  `ID` bigint(4) NOT NULL DEFAULT 0,
  `USER` varchar(128) NOT NULL DEFAULT '',
  `HOST` varchar(64) NOT NULL DEFAULT '',
  `DB` varchar(64) DEFAULT NULL,
  `COMMAND` varchar(16) NOT NULL DEFAULT '',
  `TIME` int(7) NOT NULL DEFAULT 0,
  `STATE` varchar(64) DEFAULT NULL,
  `INFO` longtext DEFAULT NULL,
  `TIME_MS` decimal(22,3) NOT NULL DEFAULT 0.000,
  `STAGE` tinyint(2) NOT NULL DEFAULT 0,
  `MAX_STAGE` tinyint(2) NOT NULL DEFAULT 0,
  `PROGRESS` decimal(7,3) NOT NULL DEFAULT 0.000,
  `MEMORY_USED` bigint(7) NOT NULL DEFAULT 0,
  `EXAMINED_ROWS` int(7) NOT NULL DEFAULT 0,
  `QUERY_ID` bigint(4) NOT NULL DEFAULT 0,
  `INFO_BINARY` blob DEFAULT NULL,
  `TID` bigint(4) NOT NULL DEFAULT 0
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `PROCESSLIST`
#

#
# Dumping data for table `PROCESSLIST`
#

INSERT INTO `PROCESSLIST` VALUES ('1544', 'superbiajuridico_test', 'localhost', 'superbiajuridico_test_development', 'Query', '0', 'Filling schema table', 'SELECT * from `information_schema`.`PROCESSLIST` Limit 0, 10000', '0.153', '0', '0', '0.000', '85296', '0', '61669', 'SELECT * from `information_schema`.`PROCESSLIST` Limit 0, 10000', '3305');
INSERT INTO `PROCESSLIST` VALUES ('1543', 'superbiajuridico_test', 'localhost', 'superbiajuridico_test_development', 'Sleep', '0', '', '', '25.784', '0', '0', '0.000', '68176', '0', '61663', '', '3354');

#
# Table structure for table `PROFILING`
#


DROP table IF EXISTS `PROFILING`;
CREATE TEMPORARY TABLE `PROFILING` (
  `QUERY_ID` int(20) NOT NULL DEFAULT 0,
  `SEQ` int(20) NOT NULL DEFAULT 0,
  `STATE` varchar(30) NOT NULL DEFAULT '',
  `DURATION` decimal(9,6) NOT NULL DEFAULT 0.000000,
  `CPU_USER` decimal(9,6) DEFAULT NULL,
  `CPU_SYSTEM` decimal(9,6) DEFAULT NULL,
  `CONTEXT_VOLUNTARY` int(20) DEFAULT NULL,
  `CONTEXT_INVOLUNTARY` int(20) DEFAULT NULL,
  `BLOCK_OPS_IN` int(20) DEFAULT NULL,
  `BLOCK_OPS_OUT` int(20) DEFAULT NULL,
  `MESSAGES_SENT` int(20) DEFAULT NULL,
  `MESSAGES_RECEIVED` int(20) DEFAULT NULL,
  `PAGE_FAULTS_MAJOR` int(20) DEFAULT NULL,
  `PAGE_FAULTS_MINOR` int(20) DEFAULT NULL,
  `SWAPS` int(20) DEFAULT NULL,
  `SOURCE_FUNCTION` varchar(30) DEFAULT NULL,
  `SOURCE_FILE` varchar(20) DEFAULT NULL,
  `SOURCE_LINE` int(20) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `PROFILING`
#

#
# Dumping data for table `PROFILING`
#


#
# Table structure for table `REFERENTIAL_CONSTRAINTS`
#


DROP table IF EXISTS `REFERENTIAL_CONSTRAINTS`;
CREATE TEMPORARY TABLE `REFERENTIAL_CONSTRAINTS` (
  `CONSTRAINT_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `CONSTRAINT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `CONSTRAINT_NAME` varchar(64) NOT NULL DEFAULT '',
  `UNIQUE_CONSTRAINT_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `UNIQUE_CONSTRAINT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `UNIQUE_CONSTRAINT_NAME` varchar(64) DEFAULT NULL,
  `MATCH_OPTION` varchar(64) NOT NULL DEFAULT '',
  `UPDATE_RULE` varchar(64) NOT NULL DEFAULT '',
  `DELETE_RULE` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `REFERENCED_TABLE_NAME` varchar(64) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `REFERENTIAL_CONSTRAINTS`
#

#
# Dumping data for table `REFERENTIAL_CONSTRAINTS`
#


#
# Table structure for table `ROUTINES`
#


DROP table IF EXISTS `ROUTINES`;
CREATE TEMPORARY TABLE `ROUTINES` (
  `SPECIFIC_NAME` varchar(64) NOT NULL DEFAULT '',
  `ROUTINE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `ROUTINE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `ROUTINE_NAME` varchar(64) NOT NULL DEFAULT '',
  `ROUTINE_TYPE` varchar(9) NOT NULL DEFAULT '',
  `DATA_TYPE` varchar(64) NOT NULL DEFAULT '',
  `CHARACTER_MAXIMUM_LENGTH` int(21) DEFAULT NULL,
  `CHARACTER_OCTET_LENGTH` int(21) DEFAULT NULL,
  `NUMERIC_PRECISION` int(21) DEFAULT NULL,
  `NUMERIC_SCALE` int(21) DEFAULT NULL,
  `DATETIME_PRECISION` bigint(21) unsigned DEFAULT NULL,
  `CHARACTER_SET_NAME` varchar(64) DEFAULT NULL,
  `COLLATION_NAME` varchar(64) DEFAULT NULL,
  `DTD_IDENTIFIER` longtext DEFAULT NULL,
  `ROUTINE_BODY` varchar(8) NOT NULL DEFAULT '',
  `ROUTINE_DEFINITION` longtext DEFAULT NULL,
  `EXTERNAL_NAME` varchar(64) DEFAULT NULL,
  `EXTERNAL_LANGUAGE` varchar(64) DEFAULT NULL,
  `PARAMETER_STYLE` varchar(8) NOT NULL DEFAULT '',
  `IS_DETERMINISTIC` varchar(3) NOT NULL DEFAULT '',
  `SQL_DATA_ACCESS` varchar(64) NOT NULL DEFAULT '',
  `SQL_PATH` varchar(64) DEFAULT NULL,
  `SECURITY_TYPE` varchar(7) NOT NULL DEFAULT '',
  `CREATED` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_ALTERED` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SQL_MODE` varchar(8192) NOT NULL DEFAULT '',
  `ROUTINE_COMMENT` longtext NOT NULL DEFAULT '',
  `DEFINER` varchar(189) NOT NULL DEFAULT '',
  `CHARACTER_SET_CLIENT` varchar(32) NOT NULL DEFAULT '',
  `COLLATION_CONNECTION` varchar(32) NOT NULL DEFAULT '',
  `DATABASE_COLLATION` varchar(32) NOT NULL DEFAULT ''
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `ROUTINES`
#

#
# Dumping data for table `ROUTINES`
#


#
# Table structure for table `SCHEMATA`
#


DROP table IF EXISTS `SCHEMATA`;
CREATE TEMPORARY TABLE `SCHEMATA` (
  `CATALOG_NAME` varchar(512) NOT NULL DEFAULT '',
  `SCHEMA_NAME` varchar(64) NOT NULL DEFAULT '',
  `DEFAULT_CHARACTER_SET_NAME` varchar(32) NOT NULL DEFAULT '',
  `DEFAULT_COLLATION_NAME` varchar(32) NOT NULL DEFAULT '',
  `SQL_PATH` varchar(512) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `SCHEMATA`
#

#
# Dumping data for table `SCHEMATA`
#

INSERT INTO `SCHEMATA` VALUES ('def', 'information_schema', 'utf8', 'utf8_general_ci', '');
INSERT INTO `SCHEMATA` VALUES ('def', 'superbiajuridico_test_development', 'latin1', 'latin1_swedish_ci', '');

#
# Table structure for table `SCHEMA_PRIVILEGES`
#


DROP table IF EXISTS `SCHEMA_PRIVILEGES`;
CREATE TEMPORARY TABLE `SCHEMA_PRIVILEGES` (
  `GRANTEE` varchar(190) NOT NULL DEFAULT '',
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `PRIVILEGE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `IS_GRANTABLE` varchar(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `SCHEMA_PRIVILEGES`
#

#
# Dumping data for table `SCHEMA_PRIVILEGES`
#

INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'SELECT', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'INSERT', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'UPDATE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'DELETE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'CREATE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'DROP', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'REFERENCES', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'INDEX', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'ALTER', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'CREATE TEMPORARY TABLES', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'LOCK TABLES', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'EXECUTE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'CREATE VIEW', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'SHOW VIEW', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'CREATE ROUTINE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'ALTER ROUTINE', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'EVENT', 'NO');
INSERT INTO `SCHEMA_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'superbiajuridico_test_development', 'TRIGGER', 'NO');

#
# Table structure for table `SESSION_STATUS`
#


DROP table IF EXISTS `SESSION_STATUS`;
CREATE TEMPORARY TABLE `SESSION_STATUS` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_VALUE` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `SESSION_STATUS`
#

#
# Dumping data for table `SESSION_STATUS`
#

INSERT INTO `SESSION_STATUS` VALUES ('ABORTED_CLIENTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ABORTED_CONNECTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACCESS_DENIED_ERRORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_COLUMN_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_DATABASE_GRANTS', '13');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_FUNCTION_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_PROCEDURE_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_PROXY_USERS', '1');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_ROLE_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_ROLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_TABLE_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ACL_USERS', '18');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_NOT_FLUSHED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_UNUSED', '15706');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_BLOCKS_USED', '19');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_READ_REQUESTS', '647564');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_READS', '10648');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_WRITE_REQUESTS', '91891');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_PAGECACHE_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ARIA_TRANSACTION_LOG_SYNCS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_COMMITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_GROUP_COMMITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_COUNT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_LOCK_WAIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_GROUP_COMMIT_TRIGGER_TIMEOUT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_SNAPSHOT_FILE', '');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_SNAPSHOT_POSITION', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_BYTES_WRITTEN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_CACHE_DISK_USE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_CACHE_USE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_STMT_CACHE_DISK_USE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BINLOG_STMT_CACHE_USE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('BUSY_TIME', '0.000000');
INSERT INTO `SESSION_STATUS` VALUES ('BYTES_RECEIVED', '622');
INSERT INTO `SESSION_STATUS` VALUES ('BYTES_SENT', '738');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ADMIN_COMMANDS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_DB', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_DB_UPGRADE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_EVENT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_FUNCTION', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_PROCEDURE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_SERVER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_TABLESPACE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ALTER_USER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ANALYZE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ASSIGN_TO_KEYCACHE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_BEGIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_BINLOG', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CALL_PROCEDURE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CHANGE_DB', '1');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CHANGE_MASTER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CHECK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CHECKSUM', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_COMMIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_COMPOUND_SQL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_DB', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_EVENT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_FUNCTION', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_INDEX', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_PROCEDURE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_ROLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_SERVER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_TEMPORARY_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_TRIGGER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_UDF', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_USER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_CREATE_VIEW', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DEALLOC_SQL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DELETE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DELETE_MULTI', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DO', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_DB', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_EVENT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_FUNCTION', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_INDEX', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_PROCEDURE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_ROLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_SERVER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_TEMPORARY_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_TRIGGER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_USER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_DROP_VIEW', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_EMPTY_QUERY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_EXECUTE_IMMEDIATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_EXECUTE_SQL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_FLUSH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_GET_DIAGNOSTICS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_GRANT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_GRANT_ROLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_HA_CLOSE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_HA_OPEN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_HA_READ', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_HELP', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_INSERT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_INSERT_SELECT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_INSTALL_PLUGIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_KILL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_LOAD', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_LOCK_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_MULTI', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_OPTIMIZE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_PRELOAD_KEYS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_PREPARE_SQL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_PURGE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_PURGE_BEFORE_DATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_RELEASE_SAVEPOINT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_RENAME_TABLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_RENAME_USER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REPAIR', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REPLACE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REPLACE_SELECT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_RESET', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_RESIGNAL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REVOKE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REVOKE_ALL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_REVOKE_ROLE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ROLLBACK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_ROLLBACK_TO_SAVEPOINT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SAVEPOINT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SELECT', '2');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SET_OPTION', '6');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_AUTHORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_BINLOG_EVENTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_BINLOGS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CHARSETS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_COLLATIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CONTRIBUTORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_DB', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_EVENT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_FUNC', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_PROC', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_TABLE', '1');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_TRIGGER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_CREATE_USER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_DATABASES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_ENGINE_LOGS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_ENGINE_MUTEX', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_ENGINE_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_ERRORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_EVENTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_EXPLAIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_FIELDS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_FUNCTION_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_GENERIC', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_GRANTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_KEYS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_MASTER_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_OPEN_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PLUGINS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PRIVILEGES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PROCEDURE_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PROCESSLIST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PROFILE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_PROFILES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_RELAYLOG_EVENTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_SLAVE_HOSTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_SLAVE_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_STORAGE_ENGINES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_TABLE_STATUS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_TRIGGERS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_VARIABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHOW_WARNINGS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SHUTDOWN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_SIGNAL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_START_ALL_SLAVES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_START_SLAVE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_CLOSE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_EXECUTE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_FETCH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_PREPARE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_REPREPARE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_RESET', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STMT_SEND_LONG_DATA', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STOP_ALL_SLAVES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_STOP_SLAVE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_TRUNCATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_UNINSTALL_PLUGIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_UNLOCK_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_UPDATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_UPDATE_MULTI', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_COMMIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_END', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_PREPARE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_RECOVER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_ROLLBACK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COM_XA_START', '0');
INSERT INTO `SESSION_STATUS` VALUES ('COMPRESSION', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_ACCEPT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_INTERNAL', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_MAX_CONNECTIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_PEER_ADDRESS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_SELECT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTION_ERRORS_TCPWRAP', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CONNECTIONS', '1556');
INSERT INTO `SESSION_STATUS` VALUES ('CPU_TIME', '0.000000');
INSERT INTO `SESSION_STATUS` VALUES ('CREATED_TMP_DISK_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('CREATED_TMP_FILES', '5');
INSERT INTO `SESSION_STATUS` VALUES ('CREATED_TMP_TABLES', '2');
INSERT INTO `SESSION_STATUS` VALUES ('DELAYED_ERRORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('DELAYED_INSERT_THREADS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('DELAYED_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('DELETE_SCAN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('EMPTY_QUERIES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('EXECUTED_EVENTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('EXECUTED_TRIGGERS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_CHECK_CONSTRAINT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_DELAY_KEY_WRITE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_DYNAMIC_COLUMNS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_FULLTEXT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_GIS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_LOCALE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_SUBQUERY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_TIMEZONE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_TRIGGER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_WINDOW_FUNCTIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FEATURE_XML', '0');
INSERT INTO `SESSION_STATUS` VALUES ('FLUSH_COMMANDS', '1');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_COMMIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_DELETE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_DISCOVER', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_EXTERNAL_LOCK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_ICP_ATTEMPTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_ICP_MATCH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_MRR_INIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_MRR_KEY_REFILLS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_MRR_ROWID_REFILLS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_PREPARE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_FIRST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_KEY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_LAST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_NEXT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_PREV', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_RETRY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_RND', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_RND_DELETED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_READ_RND_NEXT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_ROLLBACK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_SAVEPOINT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_SAVEPOINT_ROLLBACK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_TMP_UPDATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_TMP_WRITE', '245');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_UPDATE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('HANDLER_WRITE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_DUMP_STATUS', '');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_LOAD_STATUS', 'Buffer pool(s) load completed at 180916  8:02:39');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_RESIZE_STATUS', '');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_DATA', '2475');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_BYTES_DATA', '40550400');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_DIRTY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_BYTES_DIRTY', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_FLUSHED', '4039');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_FREE', '5685');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_MISC', '32');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_PAGES_TOTAL', '8192');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD_RND', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_AHEAD_EVICTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_READ_REQUESTS', '7304333');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_READS', '2311');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_WAIT_FREE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_BUFFER_POOL_WRITE_REQUESTS', '12454');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_FSYNCS', '1950');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_PENDING_FSYNCS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_PENDING_READS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_PENDING_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_READ', '37947904');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_READS', '2511');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_WRITES', '5513');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DATA_WRITTEN', '134899712');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DBLWR_PAGES_WRITTEN', '3908');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DBLWR_WRITES', '174');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_LOG_WAITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_LOG_WRITE_REQUESTS', '6514');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_LOG_WRITES', '1137');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_OS_LOG_FSYNCS', '1290');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_OS_LOG_PENDING_FSYNCS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_OS_LOG_PENDING_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_OS_LOG_WRITTEN', '4617728');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGE_SIZE', '16384');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGES_CREATED', '164');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGES_READ', '2311');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGES0_READ', '189');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGES_WRITTEN', '4039');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROW_LOCK_CURRENT_WAITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROW_LOCK_TIME', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROW_LOCK_TIME_AVG', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROW_LOCK_TIME_MAX', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROW_LOCK_WAITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROWS_DELETED', '189');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROWS_INSERTED', '268');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROWS_READ', '5829363');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ROWS_UPDATED', '443');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SYSTEM_ROWS_DELETED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SYSTEM_ROWS_INSERTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SYSTEM_ROWS_READ', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SYSTEM_ROWS_UPDATED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_OPEN_FILES', '193');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_TRUNCATED_STATUS_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_AVAILABLE_UNDO_LOGS', '128');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_PAGE_COMPRESSION_SAVED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_INDEX_PAGES_WRITTEN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_NON_INDEX_PAGES_WRITTEN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_COMPRESSED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGE_COMPRESSED_TRIM_OP', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_DECOMPRESSED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGES_PAGE_COMPRESSION_ERROR', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGES_ENCRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_NUM_PAGES_DECRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_LZ4', 'ON');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_LZO', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_LZMA', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_BZIP2', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_SNAPPY', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_HAVE_PUNCH_HOLE', 'ON');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DEFRAGMENT_COMPRESSION_FAILURES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DEFRAGMENT_FAILURES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_DEFRAGMENT_COUNT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ONLINEDDL_ROWLOG_ROWS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ONLINEDDL_ROWLOG_PCT_USED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ONLINEDDL_PCT_PROGRESS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SECONDARY_INDEX_TRIGGERED_CLUSTER_READS', '148859');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SECONDARY_INDEX_TRIGGERED_CLUSTER_READS_AVOIDED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_READ_FROM_CACHE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_READ_FROM_DISK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_MODIFIED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_PAGES_FLUSHED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_ROTATION_ESTIMATED_IOPS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_KEY_ROTATION_LIST_LENGTH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_N_MERGE_BLOCKS_ENCRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_N_MERGE_BLOCKS_DECRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_N_ROWLOG_BLOCKS_ENCRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_N_ROWLOG_BLOCKS_DECRYPTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_REORGANIZATIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_UNDERFLOW', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_OUT_OF_FILESPACE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_MISSING_INDEX', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_BACKGROUND_PAGE_SPLIT_FAILURES_UNKNOWN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_SCRUB_LOG', '0');
INSERT INTO `SESSION_STATUS` VALUES ('INNODB_ENCRYPTION_NUM_KEY_REQUESTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_BLOCKS_NOT_FLUSHED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_BLOCKS_UNUSED', '107161');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_BLOCKS_USED', '2');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_BLOCKS_WARM', '0');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_READ_REQUESTS', '24');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_READS', '2');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_WRITE_REQUESTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('KEY_WRITES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('LAST_QUERY_COST', '10.499000');
INSERT INTO `SESSION_STATUS` VALUES ('MASTER_GTID_WAIT_COUNT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('MASTER_GTID_WAIT_TIME', '0');
INSERT INTO `SESSION_STATUS` VALUES ('MASTER_GTID_WAIT_TIMEOUTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('MAX_STATEMENT_TIME_EXCEEDED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('MAX_USED_CONNECTIONS', '13');
INSERT INTO `SESSION_STATUS` VALUES ('MEMORY_USED', '2494712');
INSERT INTO `SESSION_STATUS` VALUES ('NOT_FLUSHED_DELAYED_ROWS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('OPEN_FILES', '27');
INSERT INTO `SESSION_STATUS` VALUES ('OPEN_STREAMS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('OPEN_TABLE_DEFINITIONS', '112');
INSERT INTO `SESSION_STATUS` VALUES ('OPEN_TABLES', '111');
INSERT INTO `SESSION_STATUS` VALUES ('OPENED_FILES', '44060');
INSERT INTO `SESSION_STATUS` VALUES ('OPENED_PLUGIN_LIBRARIES', '76');
INSERT INTO `SESSION_STATUS` VALUES ('OPENED_TABLE_DEFINITIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('OPENED_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('OPENED_VIEWS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_ACCOUNTS_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_COND_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_COND_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_DIGEST_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_HANDLES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_FILE_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_HOSTS_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_LOCKER_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_MUTEX_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_MUTEX_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_RWLOCK_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_RWLOCK_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_SESSION_CONNECT_ATTRS_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_SOCKET_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_SOCKET_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_STAGE_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_STATEMENT_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_TABLE_HANDLES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_TABLE_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_THREAD_CLASSES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_THREAD_INSTANCES_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PERFORMANCE_SCHEMA_USERS_LOST', '0');
INSERT INTO `SESSION_STATUS` VALUES ('PREPARED_STMT_COUNT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_FREE_BLOCKS', '1');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_FREE_MEMORY', '1031336');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_HITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_INSERTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_LOWMEM_PRUNES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_NOT_CACHED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_QUERIES_IN_CACHE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('QCACHE_TOTAL_BLOCKS', '1');
INSERT INTO `SESSION_STATUS` VALUES ('QUERIES', '61927');
INSERT INTO `SESSION_STATUS` VALUES ('QUESTIONS', '11');
INSERT INTO `SESSION_STATUS` VALUES ('ROWS_READ', '0');
INSERT INTO `SESSION_STATUS` VALUES ('ROWS_SENT', '1');
INSERT INTO `SESSION_STATUS` VALUES ('ROWS_TMP_READ', '0');
INSERT INTO `SESSION_STATUS` VALUES ('RPL_STATUS', 'AUTH_MASTER');
INSERT INTO `SESSION_STATUS` VALUES ('SELECT_FULL_JOIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SELECT_FULL_RANGE_JOIN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SELECT_RANGE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SELECT_RANGE_CHECK', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SELECT_SCAN', '1');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_CONNECTIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_HEARTBEAT_PERIOD', '0.000');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_OPEN_TEMP_TABLES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_RECEIVED_HEARTBEATS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_RETRIED_TRANSACTIONS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_RUNNING', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVE_SKIPPED_ERRORS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVES_CONNECTED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLAVES_RUNNING', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLOW_LAUNCH_THREADS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SLOW_QUERIES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SORT_MERGE_PASSES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SORT_PRIORITY_QUEUE_SORTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SORT_RANGE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SORT_ROWS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SORT_SCAN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_ACCEPT_RENEGOTIATES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_ACCEPTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CALLBACK_CACHE_HITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CIPHER', '');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CIPHER_LIST', '');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CLIENT_CONNECTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CONNECT_RENEGOTIATES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CTX_VERIFY_DEPTH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_CTX_VERIFY_MODE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_DEFAULT_TIMEOUT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_FINISHED_ACCEPTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_FINISHED_CONNECTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SERVER_NOT_AFTER', '');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SERVER_NOT_BEFORE', '');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_HITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_MISSES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_MODE', 'NONE');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_OVERFLOWS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_SIZE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSION_CACHE_TIMEOUTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_SESSIONS_REUSED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_USED_SESSION_CACHE_ENTRIES', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_VERIFY_DEPTH', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_VERIFY_MODE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SSL_VERSION', '');
INSERT INTO `SESSION_STATUS` VALUES ('SUBQUERY_CACHE_HIT', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SUBQUERY_CACHE_MISS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('SYNCS', '4');
INSERT INTO `SESSION_STATUS` VALUES ('TABLE_LOCKS_IMMEDIATE', '29');
INSERT INTO `SESSION_STATUS` VALUES ('TABLE_LOCKS_WAITED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('TC_LOG_MAX_PAGES_USED', '0');
INSERT INTO `SESSION_STATUS` VALUES ('TC_LOG_PAGE_SIZE', '4096');
INSERT INTO `SESSION_STATUS` VALUES ('TC_LOG_PAGE_WAITS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('THREADPOOL_IDLE_THREADS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('THREADPOOL_THREADS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('THREADS_CACHED', '11');
INSERT INTO `SESSION_STATUS` VALUES ('THREADS_CONNECTED', '2');
INSERT INTO `SESSION_STATUS` VALUES ('THREADS_CREATED', '52');
INSERT INTO `SESSION_STATUS` VALUES ('THREADS_RUNNING', '1');
INSERT INTO `SESSION_STATUS` VALUES ('UPDATE_SCAN', '0');
INSERT INTO `SESSION_STATUS` VALUES ('UPTIME', '23342');
INSERT INTO `SESSION_STATUS` VALUES ('UPTIME_SINCE_FLUSH_STATUS', '23342');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_CLUSTER_CONF_ID', '18446744073709551615');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_CLUSTER_SIZE', '0');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_CLUSTER_STATE_UUID', '');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_CLUSTER_STATUS', 'Disconnected');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_CONNECTED', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_LOCAL_BF_ABORTS', '0');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_LOCAL_INDEX', '18446744073709551615');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_PROVIDER_NAME', '');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_PROVIDER_VENDOR', '');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_PROVIDER_VERSION', '');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_READY', 'OFF');
INSERT INTO `SESSION_STATUS` VALUES ('WSREP_THREAD_COUNT', '0');

#
# Table structure for table `SESSION_VARIABLES`
#


DROP table IF EXISTS `SESSION_VARIABLES`;
CREATE TEMPORARY TABLE `SESSION_VARIABLES` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_VALUE` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `SESSION_VARIABLES`
#

#
# Dumping data for table `SESSION_VARIABLES`
#

INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FORCE_RECOVERY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_MODE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_LONG_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_USE_MMAP', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_CLASSES', '80');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_RUN_TRIGGERS_FOR_RBR', 'NO');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION', '10.2.14-MariaDB-10.2.14+maria~xenial');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_CLASSES', '40');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TIMEOUT', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_PAGECACHE_FILE_HASH_SIZE', '512');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSH_SYNC', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FORCE_LOAD_CORRUPTED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_PARALLEL_WORKERS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_ALLOWED_PACKET', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_FILES_IN_GROUP', '2');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_CLASSES', '200');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_TRANSIENT_SAMPLE_PAGES', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPTIMIZER_SWITCH', 'index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,index_merge_sort_intersection=off,engine_condition_pushdown=off,index_condition_pushdown=on,derived_merge=on,derived_with_keys=on,firstmatch=on,loosescan=on,materialization=on,in_to_exists=on,semijoin=on,partial_match_rowid_merge=on,partial_match_table_scan=on,subquery_cache=on,mrr=off,mrr_cost_based=off,mrr_sort_keys=off,outer_join_with_cache=on,semijoin_with_cache=on,join_cache_incremental=on,join_cache_hashed=on,join_cache_bka=on,optimize_join_buffer_size=off,table_elimination=on,extended_keys=on,exists_to_in=on,orderby_uses_equalities=on,condition_pushdown_for_derived=on');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLOW_LAUNCH_TIME', '2');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_ACCOUNTS_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPTIMIZER_PRUNE_LEVEL', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_CONNECT_ERRORS', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTS', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('NET_BUFFER_LENGTH', '16384');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_OVERSUBSCRIBE', '3');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_CACHE_SEGMENTS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_NUM_WORD_OPTIMIZE', '2000');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_NOTIFY_CMD', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DRUPAL_282555_WORKAROUND', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STAGE_CLASSES', '150');
INSERT INTO `SESSION_VARIABLES` VALUES ('TMP_MEMORY_TABLE_SIZE', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('MULTI_RANGE_COUNT', '256');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPORT_HOST', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXPLICIT_DEFAULTS_FOR_TIMESTAMP', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SORT_BUFFER_SIZE', '1048576');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_ACTORS_SIZE', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ROLLBACK_SEGMENTS', '128');
INSERT INTO `SESSION_VARIABLES` VALUES ('EVENT_SCHEDULER', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LARGE_FILES_SUPPORT', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('FT_MAX_WORD_LEN', '84');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_PROFILING', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('SESSION_TRACK_STATE_CHANGE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_AUTO_INCREMENT_CONTROL', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_COMPRESSED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_RESULTS', 'utf8');
INSERT INTO `SESSION_VARIABLES` VALUES ('GROUP_CONCAT_MAX_LEN', '1048576');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_WRITE_AHEAD_SIZE', '8192');
INSERT INTO `SESSION_VARIABLES` VALUES ('RANGE_ALLOC_BLOCK_SIZE', '4096');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_LONG_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_PARALLEL_MAX_QUEUED', '131072');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_CONNECTION', 'utf8');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_TMPDIR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('COMPLETION_TYPE', 'NO_CHAIN');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ENCRYPTION_THREADS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('OLD_PASSWORDS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_RECOVER_OPTIONS', 'BACKUP,QUICK');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN_INDEX', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_ERROR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('PRELOAD_BUFFER_SIZE', '32768');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOW_PRIORITY_UPDATES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_CACHE_SIZE', '32768');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPORT_PASSWORD', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOWER_CASE_FILE_SYSTEM', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DBUG_OPTION', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_CONNECTIONS', '151');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_BIG_SELECTS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_OBJECTS_SIZE', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('TRANSACTION_ALLOC_BLOCK_SIZE', '8192');
INSERT INTO `SESSION_VARIABLES` VALUES ('JOIN_BUFFER_SIZE', '262144');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSHING_AVG_LOOPS', '30');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SETS_DIR', '/usr/share/mysql/charsets/');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYNC_FRM', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_DO_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_TRIM', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('AUTOMATIC_SP_PRIVILEGES', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_INSTRUMENT_SEMAPHORES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('CONCURRENT_INSERT', 'AUTO');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_IO_CAPACITY_MAX', '2000');
INSERT INTO `SESSION_VARIABLES` VALUES ('CONNECT_TIMEOUT', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_IO_CAPACITY', '200');
INSERT INTO `SESSION_VARIABLES` VALUES ('LARGE_PAGES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_CLUSTER_ADDRESS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_CACHE_FILE_HASH_SIZE', '512');
INSERT INTO `SESSION_VARIABLES` VALUES ('USE_STAT_TABLES', 'NEVER');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_LONG_DATA_SIZE', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('SOCKET', '/var/run/mysqld/mysqld.sock');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_CLASSES', '50');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMPRESSION_DEFAULT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LC_TIME_NAMES', 'en_US');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_AUTOEXTEND_INCREMENT', '64');
INSERT INTO `SESSION_VARIABLES` VALUES ('DELAY_KEY_WRITE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_DATA_POINTER_SIZE', '6');
INSERT INTO `SESSION_VARIABLES` VALUES ('WAIT_TIMEOUT', '28800');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FILE_FORMAT_MAX', 'Barracuda');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_STATEMENT_TIME', '0.000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_AUTO_IS_NULL', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LC_MESSAGES_DIR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_USED_FOR_TEMP_TABLES', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_PROVIDER', 'none');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_INDEX', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_DIGESTS_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_RETRY_AUTOCOMMIT', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_CLIENT', 'utf8');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT_LOCKS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_BINLOG_CACHE_SIZE', '18446744073709547520');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_PREALLOC_SIZE', '24576');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_BINLOG_SIZE', '1073741824');
INSERT INTO `SESSION_VARIABLES` VALUES ('SORT_BUFFER_SIZE', '2097152');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_MAX_WS_ROWS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ROLLBACK_ON_TIMEOUT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_QUERIES_NOT_USING_INDEXES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('STANDARD_COMPLIANT_CTE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('IGNORE_BUILTIN_INNODB', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_REPAIR_THREADS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN_COMPRESS_MIN_LEN', '256');
INSERT INTO `SESSION_VARIABLES` VALUES ('JOIN_BUFFER_SPACE_LIMIT', '2097152');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOCK_WAIT_TIMEOUT', '86400');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FILE_FORMAT', 'Barracuda');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SESSION_CONNECT_ATTRS_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_LOG_FILE_SIZE', '1073741824');
INSERT INTO `SESSION_VARIABLES` VALUES ('DELAYED_QUEUE_SIZE', '1000');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_WILD_DO_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_USEC', '100000');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_BASENAME', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_DIGEST_LENGTH', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('READ_ONLY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_LOAD_DATA_SPLITTING', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('RAND_SEED1', '28303971');
INSERT INTO `SESSION_VARIABLES` VALUES ('TX_ISOLATION', 'REPEATABLE-READ');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_MAX_SLEEP_DELAY', '150000');
INSERT INTO `SESSION_VARIABLES` VALUES ('NET_READ_TIMEOUT', '30');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_IGNORE_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_PARALLEL_THREADS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DIRTY_READS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('AUTO_INCREMENT_OFFSET', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STRICT_MODE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CA', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('PROGRESS_REPORT_TIME', '5');
INSERT INTO `SESSION_VARIABLES` VALUES ('OLD_ALTER_TABLE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_REJECT_QUERIES', 'NONE');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_USERS_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLOW_ADMIN_STATEMENTS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLOW_VERBOSITY', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_HEAP_TABLE_SIZE', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_IDLE_TIMEOUT', '60');
INSERT INTO `SESSION_VARIABLES` VALUES ('NET_RETRY_COUNT', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOCK_SCHEDULE_ALGORITHM', 'vats');
INSERT INTO `SESSION_VARIABLES` VALUES ('METADATA_LOCKS_HASH_INSTANCES', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_MAX_ALLOWED_PACKET', '1073741824');
INSERT INTO `SESSION_VARIABLES` VALUES ('TABLE_OPEN_CACHE_INSTANCES', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('PSEUDO_THREAD_ID', '1558');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_INFO_FILE', 'relay-log.info');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_STMT_CACHE_SIZE', '32768');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_AUTOINC_LOCK_MODE', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_SELECT_LIMIT', '18446744073709551615');
INSERT INTO `SESSION_VARIABLES` VALUES ('LC_MESSAGES', 'en_US');
INSERT INTO `SESSION_VARIABLES` VALUES ('GENERAL_LOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOWER_CASE_TABLE_NAMES', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN_COMPRESS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_FILENAME', 'ib_buffer_pool');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_SERVER', 'latin1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG_DELAY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLOW_SLAVE_STATEMENTS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_NOW', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_PAGECACHE_DIVISION_LIMIT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_INTERVAL', '604800');
INSERT INTO `SESSION_VARIABLES` VALUES ('PROXY_USER', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPTIMIZER_USE_CONDITION_SELECTIVITY', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SHOW_LOCKS_HELD', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_WARNINGS', '2');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_CLASSES', '50');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ENCRYPT_LOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_INSERT_DELAYED_THREADS', '20');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_OUTPUT', 'FILE');
INSERT INTO `SESSION_VARIABLES` VALUES ('NET_WRITE_TIMEOUT', '60');
INSERT INTO `SESSION_VARIABLES` VALUES ('STORED_PROGRAM_CACHE', '256');
INSERT INTO `SESSION_VARIABLES` VALUES ('FLUSH_TIME', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CRL', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_THREAD_CONCURRENCY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MTFLUSH_THREADS', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_SIZE', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_SESSION_MEM_USED', '9223372036854775807');
INSERT INTO `SESSION_VARIABLES` VALUES ('TABLE_OPEN_CACHE', '2000');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DATA_HOME_DIR', '/var/lib/mysql/');
INSERT INTO `SESSION_VARIABLES` VALUES ('BULK_INSERT_BUFFER_SIZE', '8388608');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_COMPRESSED_PAGES', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('FT_QUERY_EXPANSION_LIMIT', '20');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DISABLE_SORT_FILE_CACHE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INIT_FILE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATION_IOPS', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_DYNAMIC_LOADING', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_HANDLES', '32768');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SST_METHOD', 'rsync');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_NETWORKING', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PRINT_ALL_DEADLOCKS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_WLOCK_INVALIDATE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_TRADITIONAL', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_GEOMETRY', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('OLD', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('UNIQUE_CHECKS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT', '75.000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_RESTART_SLAVE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_PATCH_VERSION', 'wsrep_25.23');
INSERT INTO `SESSION_VARIABLES` VALUES ('PLUGIN_DIR', '/usr/lib/mysql/plugin/');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_BUFFER_SIZE', '134217728');
INSERT INTO `SESSION_VARIABLES` VALUES ('PID_FILE', '/var/lib/mysql/telmo.pid');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_FALLOCATE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('READ_BUFFER_SIZE', '131072');
INSERT INTO `SESSION_VARIABLES` VALUES ('PSEUDO_SLAVE_MODE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MIN_EXAMINED_ROW_LIMIT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYNC_RELAY_LOG', '10000');
INSERT INTO `SESSION_VARIABLES` VALUES ('TMPDIR', '/tmp');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFAULT_ENCRYPTION_KEY_ID', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('HOSTNAME', 'telmo');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_UNDO_LOG_SIZE', '10485760');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYSTEM_TIME_ZONE', 'UTC');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_EXTERNAL_LOCKING', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_MAX_THREADS', '65536');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_NET_TIMEOUT', '60');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_DIRECT_NON_TRANSACTIONAL_UPDATES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_SAMPLE_PAGES', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_PRIORITY', 'auto');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CAPATH', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INTERACTIVE_TIMEOUT', '28800');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION_COMMENT', 'mariadb.org binary distribution');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING_LWM', '10.000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_HOSTS_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FREQUENCY', '40');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_USER_CONNECTIONS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_WARNINGS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_COUNT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('STRICT_PASSWORD_VALIDATION', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEFAULT_MASTER_CONNECTION', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SCHED_PRIORITY_CLEANER', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_MMAP_SIZE', '18446744073709551615');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_TRACK_REDO_LOG_NOW', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_SORT_LENGTH', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_STALL_LIMIT', '500');
INSERT INTO `SESSION_VARIABLES` VALUES ('TIMED_MUTEXES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_INSTANCES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXTERNAL_USER', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('TX_READ_ONLY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_CLASSES', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_RANDOM_READ_AHEAD', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_CURRENT_POS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_UNDO_LOGS', '128');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_SERVER_STOPWORD_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('PORT', '3306');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION_COMPILE_MACHINE', 'x86_64');
INSERT INTO `SESSION_VARIABLES` VALUES ('INIT_CONNECT', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHECK_CONSTRAINT_CHECKS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN_TRUST_FUNCTION_CREATORS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('TABLE_DEFINITION_CACHE', '400');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_CHANGED_PAGES', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_ALLOC_BLOCK_SIZE', '16384');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_BLOCK_SIZE', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('METADATA_LOCKS_CACHE_SIZE', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_BINLOG_STMT_CACHE_SIZE', '18446744073709547520');
INSERT INTO `SESSION_VARIABLES` VALUES ('TMP_TABLE_SIZE', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_POPULATE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_HANDLING', 'one-thread-per-connection');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SPIN_WAIT_DELAY', '6');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_TYPE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTITIONS', '8');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION_MALLOC_LIBRARY', 'system');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT_STATS_ACCURACY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_SLAVE_POS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOCK_WAIT_TIMEOUT', '50');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_SP_RECURSION_DEPTH', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_TIME', '1000');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPORT_PORT', '3306');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_LOG_BIN', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLOW_QUERY_LOG_FILE', 'telmo-slow.log');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_SYSTEM', 'utf8');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXPIRE_LOGS_DAYS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMPRESSION_FAILURE_THRESHOLD_PCT', '5');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_EXEC_MODE', 'STRICT');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOCKED_IN_MEMORY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_FORMAT', 'MIXED');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOCKING_FAKE_CHANGES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_LENGTH_FOR_SORT_DATA', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DISALLOW_WRITES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEEP_FILES_ON_CREATE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PREFIX_INDEX_CLUSTER_OPTIMIZATION', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('FT_BOOLEAN_SYNTAX', '+ -><()~*:\"\"&|');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_RECOVER_OPTIONS', 'BACKUP,QUICK');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_DDL_EXEC_MODE', 'IDEMPOTENT');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_SKIP_ERRORS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_DATABASE', 'latin1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_PCT', '25');
INSERT INTO `SESSION_VARIABLES` VALUES ('DATE_FORMAT', '%Y-%m-%d');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ENCRYPT_TABLES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LONG_QUERY_TIME', '10.000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('COLLATION_DATABASE', 'latin1_swedish_ci');
INSERT INTO `SESSION_VARIABLES` VALUES ('TIMESTAMP', '1537108301.560834');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_STACKTRACE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_COMPRESSED_PROTOCOL', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_BUFFER_SIZE', '16777216');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_UNDO_LOG_TRUNCATE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('HOST_CACHE_SIZE', '279');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_CONVERT_LOCK_TO_TRX', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPORT_USER', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_MAX_SORT_FILE_SIZE', '9223372036853727232');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_CHECKPOINT_INTERVAL', '30');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_ARCH_EXPIRE_SEC', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN_BASENAME', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_LOG_CONFLICTS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLOW_QUERY_LOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('OLD_MODE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_SIZE', '1048576');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEFAULT_REGEX_FLAGS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SST_DONOR_REJECTS_QUERIES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('HISTOGRAM_TYPE', 'SINGLE_PREC_HB');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPEN_FILES_LIMIT', '16364');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_COMPRESS', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('COLLATION_CONNECTION', 'utf8_general_ci');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_ATOMIC_WRITES', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('TIME_FORMAT', '%H:%i:%s');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_CACHE_SIZE', '151');
INSERT INTO `SESSION_VARIABLES` VALUES ('PROTOCOL_VERSION', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DOUBLEWRITE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('AUTO_INCREMENT_INCREMENT', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEFAULT_WEEK_FORMAT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_IGNORE_DUPLICATES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CHANGE_BUFFER_MAX_SIZE', '25');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DEBUG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFAULT_ROW_FORMAT', 'dynamic');
INSERT INTO `SESSION_VARIABLES` VALUES ('BASEDIR', '/usr/');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_NATIVE_AIO', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('TMP_DISK_TABLE_SIZE', '18446744073709551615');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_CACHE_DIVISION_LIMIT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_CRYPT', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_CHECKSUM_ALGORITHM', 'DEPRECATED');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SLAVE_THREADS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_TRACK_CHANGED_PAGES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_OPENSSL', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_SHORT', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_ON', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_MIN_TOKEN_SIZE', '3');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEFAULT_STORAGE_ENGINE', 'InnoDB');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_USER_STOPWORD_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_RECURSIVE_ITERATIONS', '4294967295');
INSERT INTO `SESSION_VARIABLES` VALUES ('SERVER_ID', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('JOIN_CACHE_LEVEL', '2');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_MAX_SORT_FILE_SIZE', '9223372036853727232');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATE_KEY_AGE', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_TEMP_DATA_FILE_PATH', 'ibtmp1:12M:autoextend');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_NOTES', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SCRUB_LOG_SPEED', '256');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_MAX_WS_SIZE', '2147483647');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_SYNC_LOG_DIR', 'NEWFILE');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_SEQ_NO', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_ENABLE_STOPWORD', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYNC_RELAY_LOG_INFO', '10000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_CHECK_INTERVAL', '3600');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SLAVE_FK_CHECKS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_GROUP_COMMIT_INTERVAL', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SLAVE_UK_CHECKS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PROFILING_HISTORY_SIZE', '15');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_EMPTY_FREE_LIST_ALGORITHM', 'DEPRECATED');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CHANGE_BUFFERING', 'all');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_START_POSITION', '00000000-0000-0000-0000-000000000000:-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_MODIFIED_COUNTER', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_SPACE_LIMIT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_OPTIMIZE_THREAD_SCHEDULING', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_PARALLEL_REPLICATION', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ENCRYPT_BINLOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_DIGEST_LENGTH', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_MTFLUSH', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_NODE_INCOMING_ADDRESS', 'AUTO');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CORRUPT_TABLE_ACTION', 'deprecated');
INSERT INTO `SESSION_VARIABLES` VALUES ('ERROR_COUNT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_IGNORE_DB', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_DO_DB', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMPRESSION_ALGORITHM', 'zlib');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_NODE_NAME', 'telmo');
INSERT INTO `SESSION_VARIABLES` VALUES ('PLUGIN_MATURITY', 'unknown');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_RTREE_KEYS', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PURGE_THREADS', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('BIG_TABLES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PROFILING', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MASTER_VERIFY_CHECKSUM', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_PAGECACHE_BUFFER_SIZE', '134217728');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_VERSION', '5.7.21');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXPENSIVE_SUBQUERY_LIMIT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_JOIN_SIZE', '18446744073709551615');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_AT_SHUTDOWN', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_BINLOG_POS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CRLPATH', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_WRITE_LOCK_COUNT', '4294967295');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_LONG', '15');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_PAGECACHE_AGE_THRESHOLD', '300');
INSERT INTO `SESSION_VARIABLES` VALUES ('MRR_BUFFER_SIZE', '262144');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXTRA_MAX_CONNECTIONS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT_LWM', '0.000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_SLAVE_SKIP_COUNTER', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_CACHE_AGE_THRESHOLD', '300');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_DOMAIN_ID', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('FOREIGN_KEY_CHECKS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_QUERY_CACHE', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_KILL_IDLE_TRANSACTION', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_SSL', 'DISABLED');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_AUTO_RECALC', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_WILD_IGNORE_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMMIT_CONCURRENCY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('SECURE_FILE_PRIV', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MIRRORED_LOG_GROUPS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_TC_SIZE', '24576');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FATAL_SEMAPHORE_WAIT_THRESHOLD', '600');
INSERT INTO `SESSION_VARIABLES` VALUES ('SECURE_AUTH', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_IDLE_FLUSH_PCT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('DIV_PRECISION_INCREMENT', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SUPPORT_XA', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('DATETIME_FORMAT', '%Y-%m-%d %H:%i:%s');
INSERT INTO `SESSION_VARIABLES` VALUES ('SESSION_TRACK_TRANSACTION_INFO', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_INCLUDE_DELETE_MARKED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FILL_FACTOR', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_STRIP_COMMENTS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_BUFFER_RESULT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FILE_PER_TABLE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEFAULT_TMP_STORAGE_ENGINE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('EXTRA_PORT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_ANNOTATE_ROW_EVENTS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_BLOCK_SIZE', '8192');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_RELAY_LOG_SIZE', '1073741824');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_POOL_PRIO_KICKUP_TIMER', '1000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_ABORT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DATA_HOME_DIR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CERT', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_READ_IO_THREADS', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR', '0.900000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SHOW_VERBOSE_LOCKS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('WARNING_COUNT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_INSTANCES', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('LAST_INSERT_ID', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_UNDO_DIRECTORY', './');
INSERT INTO `SESSION_VARIABLES` VALUES ('LICENSE', 'GPL');
INSERT INTO `SESSION_VARIABLES` VALUES ('GENERAL_LOG_FILE', 'telmo.log');
INSERT INTO `SESSION_VARIABLES` VALUES ('INSERT_ID', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_RESULT_CACHE_LIMIT', '2000000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('IN_TRANSACTION', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SYNC_WAIT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('ROWID_MERGE_BUFF_SIZE', '8388608');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_KEY', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_CAUSAL_READS', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CLEANER_LSN_AGE_FACTOR', 'DEPRECATED');
INSERT INTO `SESSION_VARIABLES` VALUES ('LAST_GTID', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_LOAD_TMPDIR', '/tmp');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_TYPE_CONVERSIONS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FILE_FORMAT_CHECK', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_CONCURRENCY', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_READ_AHEAD_THRESHOLD', '56');
INSERT INTO `SESSION_VARIABLES` VALUES ('KEY_CACHE_BLOCK_SIZE', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_READ_ONLY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_TMP_TABLES', '32');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_IMMEDIATE_SCRUB_DATA_UNCOMPRESSED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION_SSL_LIBRARY', 'OpenSSL 1.0.2g  1 Mar 2016');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MONITOR_RESET_ALL', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_LOG_OFF', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_OPTIMIZE_FULLTEXT_ONLY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_CHUNK_SIZE', '134217728');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_METHOD', 'nulls_equal');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR_N_RECS', '20');
INSERT INTO `SESSION_VARIABLES` VALUES ('SSL_CIPHER', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_TRANSACTION_RETRIES', '10');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLOW_RATE_LIMIT', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_SQL_VERIFY_CHECKSUM', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_BLOCK_SIZE', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('FT_STOPWORD_FILE', '(built-in)');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_UNCOMPRESSED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPTIMIZER_SEARCH_DEPTH', '62');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_SIZE', '134217728');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_STATS_METHOD', 'NULLS_UNEQUAL');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOCAL_INFILE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMPRESSION_LEVEL', '6');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_NOW', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STATEMENT_CLASSES', '188');
INSERT INTO `SESSION_VARIABLES` VALUES ('IGNORE_DB_DIRS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_ERROR_COUNT', '64');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_CHECKSUM', 'CRC32');
INSERT INTO `SESSION_VARIABLES` VALUES ('FT_MIN_WORD_LEN', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PAGE_SIZE', '16384');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_FORCED_BINLOG_FORMAT', 'NONE');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_ENCRYPT_TABLES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_FORCE_START_AFTER_RECOVERY_FAILURES', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_LONG_SIZE', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSH_METHOD', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('DELAYED_INSERT_LIMIT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_DELAYED_THREADS', '20');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_CHECKPOINT_LOG_ACTIVITY', '1048576');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SST_AUTH', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TRX_COMMIT', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('STORAGE_ENGINE', 'InnoDB');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_PROVIDER_OPTIONS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOCKS_UNSAFE_FOR_BINLOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_PREPARED_STMT_COUNT', '16382');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_CERTIFY_NONPK', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SST_DONOR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('BINLOG_ROW_IMAGE', 'FULL');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_COMPRESSION_PAD_PCT_MAX', '50');
INSERT INTO `SESSION_VARIABLES` VALUES ('ENCRYPT_TMP_DISK_TABLES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_AT_STARTUP', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_CACHE_SIZE', '8000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_REPLICATION', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('FLUSH', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_SAFE_UPDATES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLAVE_UPDATES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('DELAYED_INSERT_TIMEOUT', '300');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_LONG', '50000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_GTID_MODE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_ANNOTATE_ROW_EVENTS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CMP_PER_INDEX_ENABLED', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEBUG_NO_THREAD_ALARM', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_ONLINE_ALTER_LOG_MAX_SIZE', '134217728');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYNC_MASTER_INFO', '10000');
INSERT INTO `SESSION_VARIABLES` VALUES ('UPDATABLE_VIEWS_WITH_LIMIT', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_NAME_RESOLVE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEADLOCK_DETECT', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_ON_METADATA', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_MAX_TOKEN_SIZE', '84');
INSERT INTO `SESSION_VARIABLES` VALUES ('READ_BINLOG_SPEED_LIMIT', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_REPLICATE_MYISAM', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_STRICT_MODE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('SQL_QUOTE_SHOW_CREATE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_GROUP_HOME_DIR', './');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_NODE_ADDRESS', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LARGE_PREFIX', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_SORT_PLL_DEGREE', '2');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DEFRAGMENT_N_PAGES', '7');
INSERT INTO `SESSION_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_HANDLES', '-1');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_SLOW_FILTER', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_CHECKSUMS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PURGE_RSEG_TRUNCATE_FREQUENCY', '128');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_SORT_BUFFER_SIZE', '268434432');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_DATA_FILE_PATH', 'ibdata1:12M:autoextend');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_SORT_BUFFER_SIZE', '134216704');
INSERT INTO `SESSION_VARIABLES` VALUES ('DATADIR', '/var/lib/mysql/');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_PCT', '37');
INSERT INTO `SESSION_VARIABLES` VALUES ('COLLATION_SERVER', 'latin1_swedish_ci');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYISAM_REPAIR_THREADS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('SYNC_BINLOG', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LOG_BIN', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_AUX_TABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_PARALLEL_MODE', 'conservative');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_MYSQL_REPLICATION_BUNDLE', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('SKIP_SHOW_DATABASE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('IDENTITY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('AUTOCOMMIT', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_NUMA_INTERLEAVE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('CHARACTER_SET_FILESYSTEM', 'binary');
INSERT INTO `SESSION_VARIABLES` VALUES ('THREAD_STACK', '299008');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PAGE_CLEANERS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CHECKSUM_ALGORITHM', 'crc32');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CHECKSUMS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_LOG_PURGE_TYPE', 'immediate');
INSERT INTO `SESSION_VARIABLES` VALUES ('TRANSACTION_PREALLOC_SIZE', '4096');
INSERT INTO `SESSION_VARIABLES` VALUES ('MYSQL56_TEMPORAL_FORMAT', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('VERSION_COMPILE_OS', 'debian-linux-gnu');
INSERT INTO `SESSION_VARIABLES` VALUES ('GTID_BINLOG_STATE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SYNC_ARRAY_SIZE', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_SHORT', '10000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_OPEN_FILES', '2000');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_CLUSTER_NAME', 'my_wsrep_cluster');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_DESYNC', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_GROUP_COMMIT', 'none');
INSERT INTO `SESSION_VARIABLES` VALUES ('INIT_SLAVE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_FILE_SIZE', '50331648');
INSERT INTO `SESSION_VARIABLES` VALUES ('REPLICATE_EVENTS_MARKED_FOR_SKIP', 'REPLICATE');
INSERT INTO `SESSION_VARIABLES` VALUES ('TIME_ZONE', 'SYSTEM');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_RECOVERY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('USERSTAT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_OSU_METHOD', 'TOI');
INSERT INTO `SESSION_VARIABLES` VALUES ('SESSION_TRACK_SYSTEM_VARIABLES', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_TABLE_LOCKS', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_PURGE_BATCH_SIZE', '300');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_SST_RECEIVE_ADDRESS', 'AUTO');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FORCE_PRIMARY_KEY', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_ARCH_DIR', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_LIMIT', '1048576');
INSERT INTO `SESSION_VARIABLES` VALUES ('SLAVE_DOMAIN_PARALLEL_THREADS', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('RELAY_LOG_PURGE', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('ENFORCE_STORAGE_ENGINE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_TOTAL_CACHE_SIZE', '640000000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FT_ENABLE_DIAG_PRINT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_PAGE_CHECKSUM', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('HAVE_SYMLINK', 'YES');
INSERT INTO `SESSION_VARIABLES` VALUES ('SESSION_TRACK_SCHEMA', 'ON');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FAST_SHUTDOWN', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('OPTIMIZER_SELECTIVITY_SAMPLING_LIMIT', '100');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FLUSH_NEIGHBORS', '1');
INSERT INTO `SESSION_VARIABLES` VALUES ('BACK_LOG', '80');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SYNC_SPIN_LOOPS', '30');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT_SAMPLE_PAGES', '20');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_SCRUB_LOG', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('RAND_SEED2', '582383358');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_THREAD_SLEEP_DELAY', '10000');
INSERT INTO `SESSION_VARIABLES` VALUES ('ENCRYPT_TMP_FILES', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_RECOVER', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('ARIA_STATS_METHOD', 'nulls_unequal');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_BUF_DUMP_STATUS_FREQUENCY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MONITOR_ENABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('WSREP_GTID_DOMAIN_ID', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MONITOR_RESET', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('MAX_SEEKS_FOR_KEY', '4294967295');
INSERT INTO `SESSION_VARIABLES` VALUES ('HISTOGRAM_SIZE', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_REPLICATION_DELAY', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LRU_SCAN_DEPTH', '1024');
INSERT INTO `SESSION_VARIABLES` VALUES ('QUERY_CACHE_MIN_RES_UNIT', '4096');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_UNDO_TABLESPACES', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_WRITE_IO_THREADS', '4');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MONITOR_DISABLE', '');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_CONCURRENCY_TICKETS', '5000');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_LOG_ARCHIVE', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_USE_GLOBAL_FLUSH_LOG_AT_TRX_COMMIT', 'OFF');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_MAX_BITMAP_FILE_SIZE', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('LARGE_PAGE_SIZE', '0');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FOREGROUND_PREFLUSH', 'DEPRECATED');
INSERT INTO `SESSION_VARIABLES` VALUES ('READ_RND_BUFFER_SIZE', '262144');
INSERT INTO `SESSION_VARIABLES` VALUES ('INNODB_FAKE_CHANGES', 'OFF');

#
# Table structure for table `STATISTICS`
#


DROP table IF EXISTS `STATISTICS`;
CREATE TEMPORARY TABLE `STATISTICS` (
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `NON_UNIQUE` bigint(1) NOT NULL DEFAULT 0,
  `INDEX_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `INDEX_NAME` varchar(64) NOT NULL DEFAULT '',
  `SEQ_IN_INDEX` bigint(2) NOT NULL DEFAULT 0,
  `COLUMN_NAME` varchar(64) NOT NULL DEFAULT '',
  `COLLATION` varchar(1) DEFAULT NULL,
  `CARDINALITY` bigint(21) DEFAULT NULL,
  `SUB_PART` bigint(3) DEFAULT NULL,
  `PACKED` varchar(10) DEFAULT NULL,
  `NULLABLE` varchar(3) NOT NULL DEFAULT '',
  `INDEX_TYPE` varchar(16) NOT NULL DEFAULT '',
  `COMMENT` varchar(16) DEFAULT NULL,
  `INDEX_COMMENT` varchar(1024) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `STATISTICS`
#

#
# Dumping data for table `STATISTICS`
#

INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'meta_id', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', '1', 'superbiajuridico_test_development', 'comment_id', '1', 'comment_id', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', '1', 'superbiajuridico_test_development', 'meta_key', '1', 'meta_key', 'A', '0', '191', '', 'YES', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'comment_ID', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_post_ID', '1', 'comment_post_ID', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_approved_date_gmt', '1', 'comment_approved', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_approved_date_gmt', '2', 'comment_date_gmt', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_date_gmt', '1', 'comment_date_gmt', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_parent', '1', 'comment_parent', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', '1', 'superbiajuridico_test_development', 'comment_author_email', '1', 'comment_author_email', 'A', '0', '10', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'link_id', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', '1', 'superbiajuridico_test_development', 'link_visible', '1', 'link_visible', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'option_id', 'A', '245', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', '0', 'superbiajuridico_test_development', 'option_name', '1', 'option_name', 'A', '245', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'meta_id', 'A', '3003', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', '1', 'superbiajuridico_test_development', 'post_id', '1', 'post_id', 'A', '750', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', '1', 'superbiajuridico_test_development', 'meta_key', '1', 'meta_key', 'A', '176', '191', '', 'YES', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'ID', 'A', '303', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'post_name', '1', 'post_name', 'A', '303', '191', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'type_status_date', '1', 'post_type', 'A', '27', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'type_status_date', '2', 'post_status', 'A', '43', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'type_status_date', '3', 'post_date', 'A', '303', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'type_status_date', '4', 'ID', 'A', '303', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'post_parent', '1', 'post_parent', 'A', '303', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', '1', 'superbiajuridico_test_development', 'post_author', '1', 'post_author', 'A', '4', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'object_id', 'A', '112', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', '0', 'superbiajuridico_test_development', 'PRIMARY', '2', 'term_taxonomy_id', 'A', '112', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', '1', 'superbiajuridico_test_development', 'term_taxonomy_id', '1', 'term_taxonomy_id', 'A', '37', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'term_taxonomy_id', 'A', '23', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', '0', 'superbiajuridico_test_development', 'term_id_taxonomy', '1', 'term_id', 'A', '23', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', '0', 'superbiajuridico_test_development', 'term_id_taxonomy', '2', 'taxonomy', 'A', '23', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', '1', 'superbiajuridico_test_development', 'taxonomy', '1', 'taxonomy', 'A', '23', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'meta_id', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', '1', 'superbiajuridico_test_development', 'term_id', '1', 'term_id', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', '1', 'superbiajuridico_test_development', 'meta_key', '1', 'meta_key', 'A', '0', '191', '', 'YES', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'term_id', 'A', '19', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', '1', 'superbiajuridico_test_development', 'slug', '1', 'slug', 'A', '19', '191', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', '1', 'superbiajuridico_test_development', 'name', '1', 'name', 'A', '19', '191', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'umeta_id', 'A', '59', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', '1', 'superbiajuridico_test_development', 'user_id', '1', 'user_id', 'A', '4', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', '1', 'superbiajuridico_test_development', 'meta_key', '1', 'meta_key', 'A', '59', '191', '', 'YES', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'ID', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', '1', 'superbiajuridico_test_development', 'user_login_key', '1', 'user_login', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', '1', 'superbiajuridico_test_development', 'user_nicename', '1', 'user_nicename', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', '1', 'superbiajuridico_test_development', 'user_email', '1', 'user_email', 'A', '0', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'ID', 'A', '1', '', '', '', 'BTREE', '', '');
INSERT INTO `STATISTICS` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', '0', 'superbiajuridico_test_development', 'PRIMARY', '1', 'id', 'A', '0', '', '', '', 'BTREE', '', '');

#
# Table structure for table `SYSTEM_VARIABLES`
#


DROP table IF EXISTS `SYSTEM_VARIABLES`;
CREATE TEMPORARY TABLE `SYSTEM_VARIABLES` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `SESSION_VALUE` varchar(2048) DEFAULT NULL,
  `GLOBAL_VALUE` varchar(2048) DEFAULT NULL,
  `GLOBAL_VALUE_ORIGIN` varchar(64) NOT NULL DEFAULT '',
  `DEFAULT_VALUE` varchar(2048) DEFAULT NULL,
  `VARIABLE_SCOPE` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_COMMENT` varchar(2048) NOT NULL DEFAULT '',
  `NUMERIC_MIN_VALUE` varchar(21) DEFAULT NULL,
  `NUMERIC_MAX_VALUE` varchar(21) DEFAULT NULL,
  `NUMERIC_BLOCK_SIZE` varchar(21) DEFAULT NULL,
  `ENUM_VALUE_LIST` longtext DEFAULT NULL,
  `READ_ONLY` varchar(3) NOT NULL DEFAULT '',
  `COMMAND_LINE_ARGUMENT` varchar(64) DEFAULT NULL
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `SYSTEM_VARIABLES`
#

#
# Dumping data for table `SYSTEM_VARIABLES`
#

INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FORCE_RECOVERY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Helps to save your data in case the disk image of the database becomes corrupt.', '0', '6', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable the performance schema.', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_MODE', '', 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION', 'COMPILE-TIME', 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION', 'SESSION', 'SET', 'Sets the sql mode', '', '', '', 'REAL_AS_FLOAT,PIPES_AS_CONCAT,ANSI_QUOTES,IGNORE_SPACE,IGNORE_BAD_TABLE_OPTIONS,ONLY_FULL_GROUP_BY,NO_UNSIGNED_SUBTRACTION,NO_DIR_IN_CREATE,POSTGRESQL,ORACLE,MSSQL,DB2,MAXDB,NO_KEY_OPTIONS,NO_TABLE_OPTIONS,NO_FIELD_OPTIONS,MYSQL323,MYSQL40,ANSI,NO_AUTO_VALUE_ON_ZERO,NO_BACKSLASH_ESCAPES,STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,HIGH_NOT_PRECEDENCE,NO_ENGINE_SUBSTITUTION,PAD_CHAR_TO_FULL_LENGTH', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_LONG_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows in EVENTS_WAITS_HISTORY_LONG. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_USE_MMAP', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Use memory mapping for reading and writing MyISAM tables', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_CLASSES', '', '80', 'COMPILE-TIME', '80', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of condition instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_RUN_TRIGGERS_FOR_RBR', '', 'NO', 'COMPILE-TIME', 'NO', 'GLOBAL', 'ENUM', 'Modes for how triggers in row-base replication on slave side will be executed. Legal values are NO (default), YES and LOGGING. NO means that trigger for RBR will not be running on slave. YES and LOGGING means that triggers will be running on slave, if there was not triggers running on the master for the statement. LOGGING also means results of that the executed triggers work will be written to the binlog.', '', '', '', 'NO,YES,LOGGING', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_COND_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented condition objects. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION', '', '10.2.14-MariaDB-10.2.14+maria~xenial', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Server version number. It may also include a suffix with configuration or build information. -debug indicates debugging support was enabled on the server, and -log indicates at least one of the binary log, general log or slow query log are enabled, for example 10.1.1-MariaDB-mariadb1precise-log.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_CLASSES', '', '40', 'COMPILE-TIME', '40', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of rwlock instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TIMEOUT', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'INT UNSIGNED', 'Write and flush logs every (n) second.', '0', '2700', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_PAGECACHE_FILE_HASH_SIZE', '', '512', 'COMPILE-TIME', '512', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of hash buckets for open and changed files.  If you have a lot of Aria files open you should increase this for faster flush of changes. A good value is probably 1/10 of number of possible open Aria files.', '128', '16384', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSH_SYNC', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Allow IO bursts at the checkpoints ignoring io_capacity setting.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FORCE_LOAD_CORRUPTED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Force InnoDB to load metadata of corrupted table.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_PARALLEL_WORKERS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Alias for slave_parallel_threads', '0', '16383', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented MUTEX objects. Use 0 to disable, -1 for automated sizing.', '-1', '104857600', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_ALLOWED_PACKET', '16777216', '16777216', 'COMPILE-TIME', '16777216', 'SESSION', 'BIGINT UNSIGNED', 'Max packet length to send to or receive from the server', '1024', '1073741824', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_RWLOCK_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented RWLOCK objects. Use 0 to disable, -1 for automated sizing.', '-1', '104857600', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_FILES_IN_GROUP', '', '2', 'COMPILE-TIME', '2', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of log files in the log group. InnoDB writes to the files in a circular fashion.', '1', '100', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_MUTEX_CLASSES', '', '200', 'COMPILE-TIME', '200', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of mutex instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_TRANSIENT_SAMPLE_PAGES', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of leaf index pages to sample when calculating transient statistics (if persistent statistics are not used, default 8)', '1', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPTIMIZER_SWITCH', 'index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,index_merge_sort_intersection=off,engine_condition_pushdown=off,index_condition_pushdown=on,derived_merge=on,derived_with_keys=on,firstmatch=on,loosescan=on,materialization=on,in_to_exists=on,semijoin=on,partial_match_rowid_merge=on,partial_match_table_scan=on,subquery_cache=on,mrr=off,mrr_cost_based=off,mrr_sort_keys=off,outer_join_with_cache=on,semijoin_with_cache=on,join_cache_incremental=on,join_cache_hashed=on,join_cache_bka=on,optimize_join_buffer_size=off,table_elimination=on,extended_keys=on,exists_to_in=on,orderby_uses_equalities=on,condition_pushdown_for_derived=on', 'index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,index_merge_sort_intersection=off,engine_condition_pushdown=off,index_condition_pushdown=on,derived_merge=on,derived_with_keys=on,firstmatch=on,loosescan=on,materialization=on,in_to_exists=on,semijoin=on,partial_match_rowid_merge=on,partial_match_table_scan=on,subquery_cache=on,mrr=off,mrr_cost_based=off,mrr_sort_keys=off,outer_join_with_cache=on,semijoin_with_cache=on,join_cache_incremental=on,join_cache_hashed=on,join_cache_bka=on,optimize_join_buffer_size=off,table_elimination=on,extended_keys=on,exists_to_in=on,orderby_uses_equalities=on,condition_pushdown_for_derived=on', 'COMPILE-TIME', 'index_merge=on,index_merge_union=on,index_merge_sort_union=on,index_merge_intersection=on,index_merge_sort_intersection=off,engine_condition_pushdown=off,index_condition_pushdown=on,derived_merge=on,derived_with_keys=on,firstmatch=on,loosescan=on,materialization=on,in_to_exists=on,semijoin=on,partial_match_rowid_merge=on,partial_match_table_scan=on,subquery_cache=on,mrr=off,mrr_cost_based=off,mrr_sort_keys=off,outer_join_with_cache=on,semijoin_with_cache=on,join_cache_incremental=on,join_cache_hashed=on,join_cache_bka=on,optimize_join_buffer_size=off,table_elimination=on,extended_keys=on,exists_to_in=on,orderby_uses_equalities=on,condition_pushdown_for_derived=on', 'SESSION', 'FLAGSET', 'Fine-tune the optimizer behavior', '', '', '', 'index_merge,index_merge_union,index_merge_sort_union,index_merge_intersection,index_merge_sort_intersection,engine_condition_pushdown,index_condition_pushdown,derived_merge,derived_with_keys,firstmatch,loosescan,materialization,in_to_exists,semijoin,partial_match_rowid_merge,partial_match_table_scan,subquery_cache,mrr,mrr_cost_based,mrr_sort_keys,outer_join_with_cache,semijoin_with_cache,join_cache_incremental,join_cache_hashed,join_cache_bka,optimize_join_buffer_size,table_elimination,extended_keys,exists_to_in,orderby_uses_equalities,condition_pushdown_for_derived,default', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLOW_LAUNCH_TIME', '', '2', 'COMPILE-TIME', '2', 'GLOBAL', 'BIGINT UNSIGNED', 'If creating the thread takes longer than this value (in seconds), the Slow_launch_threads counter will be incremented', '0', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_ACCOUNTS_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented user@host accounts. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPTIMIZER_PRUNE_LEVEL', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Controls the heuristic(s) applied during query optimization to prune less-promising partial plans from the optimizer search space. Meaning: 0 - do not apply any heuristic, thus perform exhaustive search; 1 - prune plans based on number of retrieved rows', '0', '1', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_CONNECT_ERRORS', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'If there is more than this number of interrupted connections from a host this host will be blocked from further connections', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTS', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of InnoDB Adaptive Hash Index Partitions (default 8)', '1', '512', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('NET_BUFFER_LENGTH', '16384', '16384', 'COMPILE-TIME', '16384', 'SESSION', 'BIGINT UNSIGNED', 'Buffer length for TCP/IP and socket communication', '1024', '1048576', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_OVERSUBSCRIBE', '', '3', 'COMPILE-TIME', '3', 'GLOBAL', 'INT UNSIGNED', 'How many additional active worker threads in a group are allowed.', '1', '1000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_WAITS_HISTORY_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows per thread in EVENTS_WAITS_HISTORY. Use 0 to disable, -1 for automated sizing.', '-1', '1024', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_CACHE_SEGMENTS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of segments in a key cache', '0', '64', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows per thread in EVENTS_STAGES_HISTORY. Use 0 to disable, -1 for automated sizing.', '-1', '1024', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_NUM_WORD_OPTIMIZE', '', '2000', 'COMPILE-TIME', '2000', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search number of words to optimize for each optimize table call ', '1000', '10000', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_NOTIFY_CMD', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', '', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DRUPAL_282555_WORKAROUND', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable a workaround to handle the cases where inserting a DEFAULT value into an auto-increment column could fail with duplicate key error', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STAGE_CLASSES', '', '150', 'COMPILE-TIME', '150', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of stage instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TMP_MEMORY_TABLE_SIZE', '16777216', '16777216', 'COMPILE-TIME', '16777216', 'SESSION', 'BIGINT UNSIGNED', 'If an internal in-memory temporary table exceeds this size, MariaDB will automatically convert it to an on-disk MyISAM or Aria table. Same as tmp_table_size.', '1024', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MULTI_RANGE_COUNT', '256', '256', 'COMPILE-TIME', '256', 'SESSION', 'BIGINT UNSIGNED', 'Ignored. Use mrr_buffer_size instead', '1', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPORT_HOST', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Hostname or IP of the slave to be reported to the master during slave registration. Will appear in the output of SHOW SLAVE HOSTS. Leave unset if you do not want the slave to register itself with the master. Note that it is not sufficient for the master to simply read the IP of the slave off the socket once the slave connects. Due to NAT and other routing issues, that IP may not be valid for connecting to the slave from the master or other hosts', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXPLICIT_DEFAULTS_FOR_TIMESTAMP', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'This option causes CREATE TABLE to create all TIMESTAMP columns as NULL with DEFAULT NULL attribute, Without this option, TIMESTAMP columns are NOT NULL and have implicit DEFAULT clauses. The old behavior is deprecated.', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SORT_BUFFER_SIZE', '', '1048576', 'COMPILE-TIME', '1048576', 'GLOBAL', 'BIGINT UNSIGNED', 'Memory buffer size for index creation', '65536', '67108864', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_ACTORS_SIZE', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of rows in SETUP_ACTORS.', '0', '1024', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ROLLBACK_SEGMENTS', '', '128', 'COMPILE-TIME', '128', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of undo logs to use (deprecated).', '1', '128', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EVENT_SCHEDULER', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'ENUM', 'Enable the event scheduler. Possible values are ON, OFF, and DISABLED (keep the event scheduler completely deactivated, it cannot be activated run-time)', '', '', '', 'OFF,ON,DISABLED,ORIGINAL', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LARGE_FILES_SUPPORT', '', 'ON', 'COMPILE-TIME', '', 'GLOBAL', 'BOOLEAN', 'Whether mysqld was compiled with options for large file support', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FT_MAX_WORD_LEN', '', '84', 'COMPILE-TIME', '84', 'GLOBAL', 'BIGINT UNSIGNED', 'The maximum length of the word to be included in a FULLTEXT index. Note: FULLTEXT indexes must be rebuilt after changing this variable', '10', '84', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_PROFILING', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If statement profiling is available, will be set to YES, otherwise will be set to NO. See SHOW PROFILES and SHOW PROFILE.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SESSION_TRACK_STATE_CHANGE', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Track changes to the session state.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_AUTO_INCREMENT_CONTROL', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'To automatically control the assignment of autoincrement variables', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented threads. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_COMPRESSED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable scrubbing of compressed data by background threads (same as encryption_threads)', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_RESULTS', 'utf8', 'latin1', 'COMPILE-TIME', 'latin1', 'SESSION', 'ENUM', 'The character set used for returning query results to the client', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GROUP_CONCAT_MAX_LEN', '1048576', '1048576', 'COMPILE-TIME', '1048576', 'SESSION', 'BIGINT UNSIGNED', 'The maximum length of the result of function GROUP_CONCAT()', '4', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_WRITE_AHEAD_SIZE', '', '8192', 'COMPILE-TIME', '8192', 'GLOBAL', 'BIGINT UNSIGNED', 'Redo log write ahead unit size to avoid read-on-write, it should match the OS cache block IO size', '512', '16384', '512', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RANGE_ALLOC_BLOCK_SIZE', '4096', '4096', 'COMPILE-TIME', '4096', 'SESSION', 'BIGINT UNSIGNED', 'Allocation block size for storing ranges during optimization', '4096', '4294967295', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STAGES_HISTORY_LONG_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows in EVENTS_STAGES_HISTORY_LONG. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_PARALLEL_MAX_QUEUED', '', '131072', 'COMPILE-TIME', '131072', 'GLOBAL', 'BIGINT UNSIGNED', 'Limit on how much memory SQL threads should use per parallel replication thread when reading ahead in the relay log looking for opportunities for parallel replication. Only used when --slave-parallel-threads > 0.', '0', '2147483647', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_CONNECTION', 'utf8', 'latin1', 'COMPILE-TIME', 'latin1', 'SESSION', 'ENUM', 'The character set used for literals that do not have a character set introducer and for number-to-string conversion', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_TMPDIR', '', '', 'COMPILE-TIME', '', 'SESSION', 'VARCHAR', 'Directory for temporary non-tablespace files.', '', '', '', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('COMPLETION_TYPE', 'NO_CHAIN', 'NO_CHAIN', 'COMPILE-TIME', 'NO_CHAIN', 'SESSION', 'ENUM', 'The transaction completion type', '', '', '', 'NO_CHAIN,CHAIN,RELEASE', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ENCRYPTION_THREADS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'Number of threads performing background key rotation and scrubbing', '0', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OLD_PASSWORDS', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Use old password encryption method (needed for 4.0 and older clients)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_RECOVER_OPTIONS', '', 'BACKUP,QUICK', 'COMPILE-TIME', 'BACKUP,QUICK', 'GLOBAL', 'SET', 'Specifies how corrupted tables should be automatically repaired', '', '', '', 'DEFAULT,BACKUP,FORCE,QUICK,BACKUP_ALL,OFF', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN_INDEX', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'File that holds the names for last binary log files.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_ERROR', '', '', 'COMPILE-TIME', '0', 'GLOBAL', 'VARCHAR', 'Log errors to file (instead of stdout).  If file name is not specified then \'datadir\'/\'log-basename\'.err or the \'pid-file\' path with extension .err is used', '', '', '', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PRELOAD_BUFFER_SIZE', '32768', '32768', 'COMPILE-TIME', '32768', 'SESSION', 'BIGINT UNSIGNED', 'The size of the buffer that is allocated when preloading indexes', '1024', '1073741824', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOW_PRIORITY_UPDATES', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'INSERT/DELETE/UPDATE has lower priority than selects', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_CACHE_SIZE', '', '32768', 'COMPILE-TIME', '32768', 'GLOBAL', 'BIGINT UNSIGNED', 'The size of the transactional cache for updates to transactional engines for the binary log. If you often use transactions containing many statements, you can increase this to get more performance', '4096', '18446744073709551615', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPORT_PASSWORD', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The account password of the slave to be reported to the master during slave registration', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOWER_CASE_FILE_SYSTEM', '', 'OFF', 'AUTO', '', 'GLOBAL', 'BOOLEAN', 'Case sensitivity of file names on the file system where the data directory is located', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DBUG_OPTION', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'DBUG options to provider library', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_CONNECTIONS', '', '151', 'COMPILE-TIME', '151', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of simultaneous clients allowed', '1', '100000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_BIG_SELECTS', 'ON', 'ON', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 0, MariaDB will not perform large SELECTs. See max_join_size for details. If max_join_size is set to anything but DEFAULT, sql_big_selects is automatically set to 0. If sql_big_selects is again set, max_join_size will be ignored.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SETUP_OBJECTS_SIZE', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of rows in SETUP_OBJECTS.', '0', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TRANSACTION_ALLOC_BLOCK_SIZE', '8192', '8192', 'COMPILE-TIME', '8192', 'SESSION', 'BIGINT UNSIGNED', 'Allocation block size for transactions to be stored in binary log', '1024', '134217728', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('JOIN_BUFFER_SIZE', '262144', '262144', 'COMPILE-TIME', '262144', 'SESSION', 'BIGINT UNSIGNED', 'The size of the buffer that is used for joins', '128', '18446744073709551615', '128', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSHING_AVG_LOOPS', '', '30', 'COMPILE-TIME', '30', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of iterations over which the background flushing is averaged.', '1', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SETS_DIR', '', '/usr/share/mysql/charsets/', 'AUTO', '', 'GLOBAL', 'VARCHAR', 'Directory where character sets are', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYNC_FRM', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Sync .frm files to disk on creation', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_DO_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tells the slave to restrict replication to tables in the comma-separated list.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_TRIM', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Deallocate (punch_hole|trim) unused portions of the page compressed page (on by default)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('AUTOMATIC_SP_PRIVILEGES', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Creating and dropping stored procedures alters ACLs', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_INSTRUMENT_SEMAPHORES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'DEPRECATED. This setting has no effect.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CONCURRENT_INSERT', '', 'AUTO', 'COMPILE-TIME', 'AUTO', 'GLOBAL', 'ENUM', 'Use concurrent insert with MyISAM', '', '', '', 'NEVER,AUTO,ALWAYS', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_IO_CAPACITY_MAX', '', '2000', 'COMPILE-TIME', '18446744073709551615', 'GLOBAL', 'BIGINT UNSIGNED', 'Limit to which innodb_io_capacity can be inflated.', '100', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CONNECT_TIMEOUT', '', '10', 'COMPILE-TIME', '10', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of seconds the mysqld server is waiting for a connect packet before responding with \'Bad handshake\'', '2', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_IO_CAPACITY', '', '200', 'COMPILE-TIME', '200', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of IOPs the server can do. Tunes the background IO rate', '100', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LARGE_PAGES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable support for large pages', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_CLUSTER_ADDRESS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Address to initially connect to cluster', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_CACHE_FILE_HASH_SIZE', '', '512', 'COMPILE-TIME', '512', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of hash buckets for open and changed files.  If you have a lot of MyISAM files open you should increase this for faster flush of changes. A good value is probably 1/10 of number of possible open MyISAM files.', '128', '16384', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('USE_STAT_TABLES', 'NEVER', 'NEVER', 'COMPILE-TIME', 'NEVER', 'SESSION', 'ENUM', 'Specifies how to use system statistics tables', '', '', '', 'NEVER,COMPLEMENTARY,PREFERABLY', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_LONG_DATA_SIZE', '', '16777216', 'AUTO', '1048576', 'GLOBAL', 'BIGINT UNSIGNED', 'The maximum BLOB length to send to server from mysql_send_long_data API. Deprecated option; use max_allowed_packet instead.', '1024', '4294967295', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SOCKET', '', '/var/run/mysqld/mysqld.sock', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Socket file to use for connection', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_THREAD_CLASSES', '', '50', 'COMPILE-TIME', '50', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of thread instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMPRESSION_DEFAULT', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Is compression the default for new tables', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LC_TIME_NAMES', 'en_US', 'en_US', 'COMPILE-TIME', 'en_US', 'SESSION', 'ENUM', 'Set the language used for the month names and the days of the week', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_AUTOEXTEND_INCREMENT', '', '64', 'COMPILE-TIME', '64', 'GLOBAL', 'BIGINT UNSIGNED', 'Data file autoextend increment in megabytes', '1', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DELAY_KEY_WRITE', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'ENUM', 'Specifies how MyISAM tables handles CREATE TABLE DELAY_KEY_WRITE. If set to ON, the default, any DELAY KEY WRITEs are honored. The key buffer is then flushed only when the table closes, speeding up writes. MyISAM tables should be automatically checked upon startup in this case, and --external locking should not be used, as it can lead to index corruption. If set to OFF, DELAY KEY WRITEs are ignored, while if set to ALL, all new opened tables are treated as if created with DELAY KEY WRITEs enabled.', '', '', '', 'OFF,ON,ALL', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_DATA_POINTER_SIZE', '', '6', 'COMPILE-TIME', '6', 'GLOBAL', 'BIGINT UNSIGNED', 'Default pointer size to be used for MyISAM tables', '2', '7', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WAIT_TIMEOUT', '28800', '28800', 'COMPILE-TIME', '28800', 'SESSION', 'BIGINT UNSIGNED', 'The number of seconds the server waits for activity on a connection before closing it', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FILE_FORMAT_MAX', '', 'Barracuda', 'COMPILE-TIME', 'Antelope', 'GLOBAL', 'VARCHAR', 'The highest file format in the tablespace.', '', '', '', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_STATEMENT_TIME', '0.000000', '0.000000', 'COMPILE-TIME', '0.000000', 'SESSION', 'DOUBLE', 'A query that has taken more than max_statement_time seconds will be aborted. The argument will be treated as a decimal value with microsecond precision. A value of 0 (default) means no timeout', '0', '31536000', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_AUTO_IS_NULL', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1, the query SELECT * FROM table_name WHERE auto_increment_column IS NULL will return an auto-increment that has just been successfully inserted, the same as the LAST_INSERT_ID() function. Some ODBC programs make use of this IS NULL comparison.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LC_MESSAGES_DIR', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Directory where error messages are', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_USED_FOR_TEMP_TABLES', '', 'ON', 'COMPILE-TIME', '', 'GLOBAL', 'BOOLEAN', 'Whether temporary tables should be MyISAM or Aria', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_PROVIDER', '', 'none', 'COMPILE-TIME', 'none', 'GLOBAL', 'VARCHAR', 'Path to replication provider library', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_INDEX', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The location and name to use for the file that keeps a list of the last relay logs.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_DIGESTS_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Size of the statement digest. Use 0 to disable, -1 for automated sizing.', '-1', '200', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_RETRY_AUTOCOMMIT', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Max number of times to retry a failed autocommit statement', '0', '10000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_CLIENT', 'utf8', 'latin1', 'COMPILE-TIME', 'latin1', 'SESSION', 'ENUM', 'The character set for statements that arrive from the client', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT_LOCKS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable InnoDB lock monitor output to the error log. Requires innodb_status_output=ON.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_BINLOG_CACHE_SIZE', '', '18446744073709547520', 'COMPILE-TIME', '18446744073709547520', 'GLOBAL', 'BIGINT UNSIGNED', 'Sets the total size of the transactional cache', '4096', '18446744073709551615', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_PREALLOC_SIZE', '24576', '24576', 'COMPILE-TIME', '24576', 'SESSION', 'BIGINT UNSIGNED', 'Persistent buffer for query parsing and execution', '1024', '4294967295', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_BINLOG_SIZE', '', '1073741824', 'COMPILE-TIME', '1073741824', 'GLOBAL', 'BIGINT UNSIGNED', 'Binary log will be rotated automatically when the size exceeds this value.', '4096', '1073741824', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SORT_BUFFER_SIZE', '2097152', '2097152', 'COMPILE-TIME', '2097152', 'SESSION', 'BIGINT UNSIGNED', 'Each thread that needs to do a sort allocates a buffer of this size', '1024', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_MAX_WS_ROWS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Max number of rows in write set', '0', '1048576', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ROLLBACK_ON_TIMEOUT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Roll back the complete transaction on lock wait timeout, for 4.x compatibility (disabled by default)', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_QUERIES_NOT_USING_INDEXES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Log queries that are executed without benefit of any index to the slow log if it is open', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('STANDARD_COMPLIANT_CTE', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Allow only CTEs compliant to SQL standard', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('IGNORE_BUILTIN_INNODB', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Disable initialization of builtin InnoDB plugin', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_REPAIR_THREADS', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Number of threads to use when repairing Aria tables. The value of 1 disables parallel repair.', '1', '128', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN_COMPRESS_MIN_LEN', '', '256', 'COMPILE-TIME', '256', 'GLOBAL', 'INT UNSIGNED', 'Minimum length of sql statement(in statement mode) or record(in row mode)that can be compressed.', '10', '1024', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('JOIN_BUFFER_SPACE_LIMIT', '2097152', '2097152', 'COMPILE-TIME', '2097152', 'SESSION', 'BIGINT UNSIGNED', 'The limit of the space for all join buffers used by a query', '2048', '18446744073709551615', '2048', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOCK_WAIT_TIMEOUT', '86400', '86400', 'COMPILE-TIME', '86400', 'SESSION', 'BIGINT UNSIGNED', 'Timeout in seconds to wait for a lock before returning an error.', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FILE_FORMAT', '', 'Barracuda', 'COMPILE-TIME', 'Barracuda', 'GLOBAL', 'VARCHAR', 'File format to use for new tables in .ibd files.', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_SESSION_CONNECT_ATTRS_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Size of session attribute string buffer per thread. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_LOG_FILE_SIZE', '', '1073741824', 'COMPILE-TIME', '1073741824', 'GLOBAL', 'BIGINT UNSIGNED', 'Limit for transaction log size', '8388608', '4294967295', '8192', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DELAYED_QUEUE_SIZE', '', '1000', 'COMPILE-TIME', '1000', 'GLOBAL', 'BIGINT UNSIGNED', 'What size queue (in rows) should be allocated for handling INSERT DELAYED. If the queue becomes full, any client that does INSERT DELAYED will wait until there is room in the queue again', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_WILD_DO_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tells the slave thread to restrict replication to statements where any of the updated tables match the specified database and table name patterns.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_USEC', '', '100000', 'COMPILE-TIME', '100000', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum time, in microseconds, to wait for more commits to queue up for binlog group commit. Only takes effect if the value of binlog_commit_wait_count is non-zero.', '0', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_BASENAME', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The full path of the relay log file names, excluding the extension.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_DIGEST_LENGTH', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'INT UNSIGNED', 'Maximum length considered for digest text.', '0', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('READ_ONLY', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Make all non-temporary tables read-only, with the exception for replication (slave) threads and users with the SUPER privilege', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_LOAD_DATA_SPLITTING', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'To commit LOAD DATA transaction after every 10K rows inserted', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RAND_SEED1', '329941576', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'Sets the internal state of the RAND() generator for replication purposes', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TX_ISOLATION', 'REPEATABLE-READ', 'REPEATABLE-READ', 'COMPILE-TIME', 'REPEATABLE-READ', 'SESSION', 'ENUM', 'Default transaction isolation level', '', '', '', 'READ-UNCOMMITTED,READ-COMMITTED,REPEATABLE-READ,SERIALIZABLE', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_MAX_SLEEP_DELAY', '', '150000', 'COMPILE-TIME', '150000', 'GLOBAL', 'BIGINT UNSIGNED', 'The upper limit of the sleep delay in usec. Value of 0 disables it.', '0', '1000000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('NET_READ_TIMEOUT', '30', '30', 'COMPILE-TIME', '30', 'SESSION', 'BIGINT UNSIGNED', 'Number of seconds to wait for more data from a connection before aborting the read', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_IGNORE_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tells the slave thread not to replicate any statement that updates the specified table, even if any other tables might be updated by the same statement.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_PARALLEL_THREADS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'If non-zero, number of threads to spawn to apply in parallel events on the slave that were group-committed on the master or were logged with GTID in different replication domains. Note that these threads are in addition to the IO and SQL threads, which are always created by a replication slave', '0', '16383', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DIRTY_READS', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Allow reads even when the node is not in the primary component.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('AUTO_INCREMENT_OFFSET', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Offset added to Auto-increment columns. Used when auto-increment-increment != 1', '1', '65535', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STRICT_MODE', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Use strict mode when evaluating create options.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CA', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'CA file in PEM format (check OpenSSL docs, implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PROGRESS_REPORT_TIME', '5', '5', 'COMPILE-TIME', '5', 'SESSION', 'BIGINT UNSIGNED', 'Seconds between sending progress reports to the client for time-consuming statements. Set to 0 to disable progress reporting.', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OLD_ALTER_TABLE', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Use old, non-optimized alter table', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_REJECT_QUERIES', '', 'NONE', 'COMPILE-TIME', 'NONE', 'GLOBAL', 'ENUM', 'Variable to set to reject queries', '', '', '', 'NONE,ALL,ALL_KILL', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows per thread in EVENTS_STATEMENTS_HISTORY. Use 0 to disable, -1 for automated sizing.', '-1', '1024', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_USERS_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented users. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLOW_ADMIN_STATEMENTS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Log slow OPTIMIZE, ANALYZE, ALTER and other administrative statements to the slow log if it is open.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLOW_VERBOSITY', '', '', 'COMPILE-TIME', '', 'SESSION', 'SET', 'Verbosity level for the slow log', '', '', '', 'innodb,query_plan,explain', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_HEAP_TABLE_SIZE', '16777216', '16777216', 'COMPILE-TIME', '16777216', 'SESSION', 'BIGINT UNSIGNED', 'Don\'t allow creation of heap tables bigger than this', '16384', '18446744073709551615', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_IDLE_TIMEOUT', '', '60', 'COMPILE-TIME', '60', 'GLOBAL', 'INT UNSIGNED', 'Timeout in seconds for an idle thread in the thread pool.Worker thread will be shut down after timeout', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('NET_RETRY_COUNT', '10', '10', 'COMPILE-TIME', '10', 'SESSION', 'BIGINT UNSIGNED', 'If a read on a communication port is interrupted, retry this many times before giving up', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOCK_SCHEDULE_ALGORITHM', '', 'vats', 'COMPILE-TIME', 'vats', 'GLOBAL', 'ENUM', 'The algorithm Innodb uses for deciding which locks to grant next when a lock is released. Possible values are FCFS grant the locks in First-Come-First-Served order; VATS use the Variance-Aware-Transaction-Scheduling algorithm, which uses an Eldest-Transaction-First heuristic.', '', '', '', 'fcfs,vats', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('METADATA_LOCKS_HASH_INSTANCES', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT UNSIGNED', 'Unused', '1', '1024', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented tables. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_MAX_ALLOWED_PACKET', '', '1073741824', 'COMPILE-TIME', '1073741824', 'GLOBAL', 'BIGINT UNSIGNED', 'The maximum packet length to sent successfully from the master to slave.', '1024', '1073741824', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TABLE_OPEN_CACHE_INSTANCES', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'INT UNSIGNED', 'Maximum number of table cache instances', '1', '64', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PSEUDO_THREAD_ID', '1562', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'This variable is for internal server use', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_INFO_FILE', '', 'relay-log.info', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The location and name of the file that remembers where the SQL replication thread is in the relay logs.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_STMT_CACHE_SIZE', '', '32768', 'COMPILE-TIME', '32768', 'GLOBAL', 'BIGINT UNSIGNED', 'The size of the statement cache for updates to non-transactional engines for the binary log. If you often use statements updating a great number of rows, you can increase this to get more performance.', '4096', '18446744073709551615', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_AUTOINC_LOCK_MODE', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT', 'The AUTOINC lock modes supported by InnoDB: 0 => Old style AUTOINC locking (for backward compatibility); 1 => New style AUTOINC locking; 2 => No AUTOINC locking (unsafe for SBR)', '0', '2', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_SELECT_LIMIT', '18446744073709551615', '18446744073709551615', 'COMPILE-TIME', '18446744073709551615', 'SESSION', 'BIGINT UNSIGNED', 'The maximum number of rows to return from SELECT statements', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LC_MESSAGES', 'en_US', 'en_US', 'COMPILE-TIME', 'en_US', 'SESSION', 'ENUM', 'Set the language used for the error messages', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GENERAL_LOG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Log connections and queries to a table or log file. Defaults logging to a file \'hostname\'.log or a table mysql.general_logif --log-output=TABLE is used.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOWER_CASE_TABLE_NAMES', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'If set to 1 table names are stored in lowercase on disk and table names will be case-insensitive.  Should be set to 2 if you are using a case insensitive file system', '0', '2', '1', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN_COMPRESS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Whether the binary log can be compressed', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_FILENAME', '', 'ib_buffer_pool', 'COMPILE-TIME', 'ib_buffer_pool', 'GLOBAL', 'VARCHAR', 'Filename to/from which to dump/load the InnoDB buffer pool', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_SERVER', 'latin1', 'latin1', 'COMPILE-TIME', 'latin1', 'SESSION', 'ENUM', 'The default character set', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG_DELAY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum delay of user threads in micro-seconds', '0', '10000000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLOW_SLAVE_STATEMENTS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Log slow statements executed by slave thread to the slow log if it is open.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_NOW', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Trigger an immediate dump of the buffer pool into a file named @@innodb_buffer_pool_filename', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_PAGECACHE_DIVISION_LIMIT', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'The minimum percentage of warm blocks in key cache', '1', '100', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_INTERVAL', '', '604800', 'COMPILE-TIME', '604800', 'GLOBAL', 'INT UNSIGNED', 'scrub spaces that were last scrubbed longer than  innodb_background_scrub_data_interval seconds ago', '1', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PROXY_USER', '', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'VARCHAR', 'The proxy user account name used when logging in', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATUS_OUTPUT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable InnoDB monitor output to the error log.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SHOW_LOCKS_HELD', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_WARNINGS', '2', '2', 'COMPILE-TIME', '2', 'SESSION', 'BIGINT UNSIGNED', 'Log some not critical warnings to the general log file.Value can be between 0 and 11. Higher values mean more verbosity', '0', '4294967295', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable InnoDB adaptive hash index (enabled by default).  Disable with --skip-innodb-adaptive-hash-index.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_CLASSES', '', '50', 'COMPILE-TIME', '50', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of file instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPTIMIZER_USE_CONDITION_SELECTIVITY', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Controls selectivity of which conditions the optimizer takes into account to calculate cardinality of a partial join when it searches for the best execution plan Meaning: 1 - use selectivity of index backed range conditions to calculate the cardinality of a partial join if the last joined table is accessed by full table scan or an index scan, 2 - use selectivity of index backed range conditions to calculate the cardinality of a partial join in any case, 3 - additionally always use selectivity of range conditions that are not backed by any index to calculate the cardinality of a partial join, 4 - use histograms to calculate selectivity of range conditions that are not backed by any index to calculate the cardinality of a partial join.5 - additionally use selectivity of certain non-range predicates calculated on record samples', '1', '5', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ENCRYPT_LOG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable redo log encryption', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_INSERT_DELAYED_THREADS', '20', '20', 'COMPILE-TIME', '20', 'SESSION', 'BIGINT UNSIGNED', 'Don\'t start more than this number of threads to handle INSERT DELAYED statements. If set to zero INSERT DELAYED will be not used', '0', '16384', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_OUTPUT', '', 'FILE', 'COMPILE-TIME', 'FILE', 'GLOBAL', 'SET', 'How logs should be written', '', '', '', 'NONE,FILE,TABLE', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('NET_WRITE_TIMEOUT', '60', '60', 'COMPILE-TIME', '60', 'SESSION', 'BIGINT UNSIGNED', 'Number of seconds to wait for a block to be written to a connection before aborting the write', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('STORED_PROGRAM_CACHE', '', '256', 'COMPILE-TIME', '256', 'GLOBAL', 'BIGINT UNSIGNED', 'The soft upper limit for number of cached stored routines for one connection.', '0', '524288', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FLUSH_TIME', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'A dedicated thread is created to flush all tables at the given interval', '0', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CRL', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'CRL file in PEM format (check OpenSSL docs, implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_THREAD_CONCURRENCY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Helps in performance tuning in heavily concurrent environments. Sets the maximum number of threads allowed inside InnoDB. Value 0 will disable the thread throttling.', '0', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MTFLUSH_THREADS', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT', 'DEPRECATED. Number of multi-threaded flush threads', '1', '64', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_SIZE', '', '1', 'AUTO', '8', 'GLOBAL', 'INT UNSIGNED', 'Number of thread groups in the pool. This parameter is roughly equivalent to maximum number of concurrently executing threads (threads in a waiting state do not count as executing).', '1', '100000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_SESSION_MEM_USED', '9223372036854775807', '9223372036854775807', 'COMPILE-TIME', '9223372036854775807', 'SESSION', 'BIGINT UNSIGNED', 'Amount of memory a single user session is allowed to allocate. This limits the value of the session variable MEM_USED', '8192', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TABLE_OPEN_CACHE', '', '2000', 'COMPILE-TIME', '2000', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of cached open tables', '1', '1048576', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DATA_HOME_DIR', '', '/var/lib/mysql/', 'COMPILE-TIME', '/var/lib/mysql/', 'GLOBAL', 'VARCHAR', 'home directory for wsrep provider', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BULK_INSERT_BUFFER_SIZE', '8388608', '8388608', 'COMPILE-TIME', '8388608', 'SESSION', 'BIGINT UNSIGNED', 'Size of tree cache used in bulk insert optimisation. Note that this is a limit per thread!', '0', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_COMPRESSED_PAGES', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enables/disables the logging of entire compressed page images. InnoDB logs the compressed pages to prevent corruption if the zlib compression algorithm changes. When turned OFF, InnoDB will assume that the zlib compression algorithm doesn\'t change.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FT_QUERY_EXPANSION_LIMIT', '', '20', 'COMPILE-TIME', '20', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of best matches to use for query expansion', '0', '1000', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DISABLE_SORT_FILE_CACHE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Whether to disable OS system file cache for sort I/O', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INIT_FILE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Read SQL commands from this file at startup', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATION_IOPS', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'INT UNSIGNED', 'Use this many iops for background key rotation', '0', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable/disable InnoDB defragmentation (default FALSE). When set to FALSE, all existing defragmentation will be paused. And new defragmentation command will fail.Paused defragmentation commands will resume when this variable is set to true again.', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_DYNAMIC_LOADING', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the server supports dynamic loading of plugins, will be set to YES, otherwise will be set to NO.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_HANDLES', '', '32768', 'COMPILE-TIME', '32768', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of opened instrumented files.', '0', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SST_METHOD', '', 'rsync', 'COMPILE-TIME', 'rsync', 'GLOBAL', 'VARCHAR', 'State snapshot transfer method', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_NETWORKING', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Don\'t allow connection with TCP/IP', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PRINT_ALL_DEADLOCKS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Print all deadlocks to MariaDB error log (off by default)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_WLOCK_INVALIDATE', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Invalidate queries in query cache on LOCK for write', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_TRADITIONAL', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable traditional statistic calculation based on number of configured pages (default true)', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_GEOMETRY', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the server supports spatial data types, will be set to YES, otherwise will be set to NO.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OLD', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Use compatible behavior from previous MariaDB version. See also --old-mode', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('UNIQUE_CHECKS', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 1, the default, secondary indexes in InnoDB tables are performed. If set to 0, storage engines can (but are not required to) assume that duplicate keys are not present in input data. Set to 0 to speed up imports of large tables to InnoDB. The storage engine will still issue a duplicate key error if it detects one, even if set to 0.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT', '', '75.000000', 'COMPILE-TIME', '75.000000', 'GLOBAL', 'DOUBLE', 'Percentage of dirty pages allowed in bufferpool.', '0', '99.999', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_RESTART_SLAVE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Should MariaDB slave be restarted automatically, when node joins back to cluster', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_PATCH_VERSION', '', 'wsrep_25.23', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Wsrep patch version, for example wsrep_25.10.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PLUGIN_DIR', '', '/usr/lib/mysql/plugin/', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Directory for plugins', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_BUFFER_SIZE', '', '134217728', 'COMPILE-TIME', '134217728', 'GLOBAL', 'BIGINT UNSIGNED', 'The size of the buffer used for index blocks for MyISAM tables. Increase this to get better index handling (for all reads and multiple writes) to as much as you can afford', '0', '18446744073709551615', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PID_FILE', '', '/var/lib/mysql/telmo.pid', 'AUTO', '', 'GLOBAL', 'VARCHAR', 'Pid file used by safe_mysqld', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_FALLOCATE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Use posix_fallocate() to allocate files. DEPRECATED, has no effect.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('READ_BUFFER_SIZE', '131072', '131072', 'COMPILE-TIME', '131072', 'SESSION', 'BIGINT UNSIGNED', 'Each thread that does a sequential scan allocates a buffer of this size for each table it scans. If you do many sequential scans, you may want to increase this value', '8192', '2147483647', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PSEUDO_SLAVE_MODE', 'OFF', '', 'COMPILE-TIME', 'OFF', 'SESSION ONLY', 'BOOLEAN', 'SET pseudo_slave_mode= 0,1 are commands that mysqlbinlog adds to beginning and end of binary log dumps. While zero value indeed disables, the actual enabling of the slave applier execution mode is done implicitly when a Format_description_event is sent through the session.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MIN_EXAMINED_ROW_LIMIT', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'BIGINT UNSIGNED', 'Don\'t write queries to slow log that examine fewer rows than that', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYNC_RELAY_LOG', '', '10000', 'COMPILE-TIME', '10000', 'GLOBAL', 'INT UNSIGNED', 'Synchronously flush relay log to disk after every #th event. Use 0 to disable synchronous flushing', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TMPDIR', '', '/tmp', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Path for temporary files. Several paths may be specified, separated by a colon (:), in this case they are used in a round-robin fashion', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFAULT_ENCRYPTION_KEY_ID', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'INT UNSIGNED', 'Default encryption key id used for table encryption.', '1', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HOSTNAME', '', 'telmo', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Server host name', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_UNDO_LOG_SIZE', '', '10485760', 'COMPILE-TIME', '10485760', 'GLOBAL', 'BIGINT UNSIGNED', 'Desired maximum UNDO tablespace size in bytes', '10485760', '281474976710656', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYSTEM_TIME_ZONE', '', 'UTC', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The server system time zone', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_EXTERNAL_LOCKING', '', 'ON', 'COMPILE-TIME', '', 'GLOBAL', 'BOOLEAN', 'Don\'t use system (external) locking', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_MAX_THREADS', '', '65536', 'COMPILE-TIME', '65536', 'GLOBAL', 'INT UNSIGNED', 'Maximum allowed number of worker threads in the thread pool', '1', '65536', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_NET_TIMEOUT', '', '60', 'COMPILE-TIME', '60', 'GLOBAL', 'INT UNSIGNED', 'Number of seconds to wait for more data from any master/slave connection before aborting the read', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_DIRECT_NON_TRANSACTIONAL_UPDATES', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Causes updates to non-transactional engines using statement format to be written directly to binary log. Before using this option make sure that there are no dependencies between transactional and non-transactional tables such as in the statement INSERT INTO t_myisam SELECT * FROM t_innodb; otherwise, slaves may diverge from the master.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_SAMPLE_PAGES', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated, use innodb_stats_transient_sample_pages instead', '1', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_FILE_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented files. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_PRIORITY', 'auto', 'auto', 'COMPILE-TIME', 'auto', 'SESSION', 'ENUM', 'Threadpool priority. High priority connections usually start executing earlier than low priority.If priority set to \'auto\', the the actual priority(low or high) is determined based on whether or not connection is inside transaction.', '', '', '', 'high,low,auto', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CAPATH', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'CA directory (check OpenSSL docs, implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INTERACTIVE_TIMEOUT', '28800', '28800', 'COMPILE-TIME', '28800', 'SESSION', 'BIGINT UNSIGNED', 'The number of seconds the server waits for activity on an interactive connection before closing it', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION_COMMENT', '', 'mariadb.org binary distribution', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Value of the COMPILATION_COMMENT option specified by CMake when building MariaDB, for example mariadb.org binary distribution.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING_LWM', '', '10.000000', 'COMPILE-TIME', '10.000000', 'GLOBAL', 'DOUBLE', 'Percentage of log capacity below which no adaptive flushing happens.', '0', '70', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_HOSTS_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of instrumented hosts. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FREQUENCY', '', '40', 'COMPILE-TIME', '40', 'GLOBAL', 'INT UNSIGNED', 'Do not defragment a single index more than this number of time per second.This controls the number of time defragmentation thread can request X_LOCK on an index. Defragmentation thread will check whether 1/defragment_frequency (s) has passed since it worked on this index last time, and put the index back to the queue if not enough time has passed. The actual frequency can only be lower than this given number.', '1', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_USER_CONNECTIONS', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'INT', 'The maximum number of active connections for a single user (0 = no limit)', '-1', '2147483647', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_WARNINGS', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1, single-row INSERTs will produce a string containing warning information if a warning occurs.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_COMMIT_WAIT_COUNT', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'If non-zero, binlog write will wait at most binlog_commit_wait_usec microseconds for at least this many commits to queue up for group commit to the binlog. This can reduce I/O on the binlog and provide increased opportunity for parallel apply on the slave, but too high a value will decrease commit throughput.', '0', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('STRICT_PASSWORD_VALIDATION', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'When password validation plugins are enabled, reject passwords that cannot be validated (passwords specified as a hash)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEFAULT_MASTER_CONNECTION', '', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'VARCHAR', 'Master connection to use for all slave variables and slave commands', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SCHED_PRIORITY_CLEANER', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '39', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_MMAP_SIZE', '', '18446744073709551615', 'COMPILE-TIME', '18446744073709551615', 'GLOBAL', 'BIGINT UNSIGNED', 'Restricts the total memory used for memory mapping of MySQL tables', '7', '18446744073709551615', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_TRACK_REDO_LOG_NOW', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_SORT_LENGTH', '1024', '1024', 'COMPILE-TIME', '1024', 'SESSION', 'BIGINT UNSIGNED', 'The number of bytes to use when sorting BLOB or TEXT values (only the first max_sort_length bytes of each value are used; the rest are ignored)', '4', '8388608', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_STALL_LIMIT', '', '500', 'COMPILE-TIME', '500', 'GLOBAL', 'INT UNSIGNED', 'Maximum query execution time in milliseconds,before an executing non-yielding thread is considered stalled.If a worker thread is stalled, additional worker thread may be created to handle remaining clients.', '10', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TIMED_MUTEXES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Specify whether to time mutexes. Deprecated, has no effect.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_INSTANCES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of opened instrumented sockets. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXTERNAL_USER', '', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'VARCHAR', 'The external user account used when logging in', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TX_READ_ONLY', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Default transaction access mode. If set to OFF, the default, access is read/write. If set to ON, access is read-only. The SET TRANSACTION statement can also change the value of this variable. See SET TRANSACTION and START TRANSACTION.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_SOCKET_CLASSES', '', '10', 'COMPILE-TIME', '10', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of socket instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_RANDOM_READ_AHEAD', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Whether to use read ahead for random access within an extent.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_CURRENT_POS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Current GTID position of the server. Per replication domain, this is either the last GTID replicated by a slave thread, or the GTID logged to the binary log, whichever is most recent.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_UNDO_LOGS', '', '128', 'COMPILE-TIME', '128', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of undo logs to use.', '1', '128', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_SERVER_STOPWORD_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The user supplied stopword table name.', '', '', '', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PORT', '', '3306', 'AUTO', '0', 'GLOBAL', 'INT UNSIGNED', 'Port number to use for connection or 0 to default to, my.cnf, $MYSQL_TCP_PORT, /etc/services, built-in default (3306), whatever comes first', '0', '4294967295', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION_COMPILE_MACHINE', '', 'x86_64', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The machine type or architecture MariaDB was built on, for example i686.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INIT_CONNECT', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Command(s) that are executed for each new connection (unless the user has SUPER privilege)', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHECK_CONSTRAINT_CHECKS', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'check_constraint_checks', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN_TRUST_FUNCTION_CREATORS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'If set to FALSE (the default), then when --log-bin is used, creation of a stored function (or trigger) is allowed only to users having the SUPER privilege and only if this stored function (trigger) may not break binary logging. Note that if ALL connections to this server ALWAYS use row-based binary logging, the security issues do not exist and the binary logging cannot break, so you can safely set this to TRUE', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TABLE_DEFINITION_CACHE', '', '400', 'AUTO', '400', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of cached table definitions', '400', '524288', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_CHANGED_PAGES', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_ALLOC_BLOCK_SIZE', '16384', '16384', 'COMPILE-TIME', '16384', 'SESSION', 'BIGINT UNSIGNED', 'Allocation block size for query parsing and execution', '1024', '4294967295', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_BLOCK_SIZE', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'BIGINT UNSIGNED', 'Block size to be used for MyISAM index pages', '1024', '16384', '1024', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('METADATA_LOCKS_CACHE_SIZE', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'BIGINT UNSIGNED', 'Unused', '1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_BINLOG_STMT_CACHE_SIZE', '', '18446744073709547520', 'COMPILE-TIME', '18446744073709547520', 'GLOBAL', 'BIGINT UNSIGNED', 'Sets the total size of the statement cache', '4096', '18446744073709551615', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TMP_TABLE_SIZE', '16777216', '16777216', 'COMPILE-TIME', '16777216', 'SESSION', 'BIGINT UNSIGNED', 'Alias for tmp_memory_table_size. If an internal in-memory temporary table exceeds this size, MariaDB will automatically convert it to an on-disk MyISAM or Aria table.', '1024', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_POPULATE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_HANDLING', '', 'one-thread-per-connection', 'COMPILE-TIME', 'one-thread-per-connection', 'GLOBAL', 'ENUM', 'Define threads usage for handling queries', '', '', '', 'one-thread-per-connection,no-threads,pool-of-threads', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SPIN_WAIT_DELAY', '', '6', 'COMPILE-TIME', '6', 'GLOBAL', 'INT UNSIGNED', 'Maximum delay between polling for a spin lock (6 by default)', '0', '6000', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_TYPE', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'ENUM', 'OFF = Don\'t cache or retrieve results. ON = Cache all results except SELECT SQL_NO_CACHE ... queries. DEMAND = Cache only SELECT SQL_CACHE ... queries', '', '', '', 'OFF,ON,DEMAND', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_HASH_INDEX_PARTITIONS', '', '8', 'COMPILE-TIME', '8', 'GLOBAL', 'BIGINT UNSIGNED', 'It is an alias for innodb_adaptive_hash_index_parts; only exists to allow easier upgrade from earlier XtraDB versions.', '1', '512', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION_MALLOC_LIBRARY', '', 'system', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Version of the used malloc library', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT_STATS_ACCURACY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'How many defragment stats changes there are before the stats are written to persistent storage. Set to 0 meaning disable defragment stats tracking.', '0', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_SLAVE_POS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The list of global transaction IDs that were last replicated on the server, one for each replication domain.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOCK_WAIT_TIMEOUT', '50', '50', 'COMPILE-TIME', '50', 'SESSION', 'BIGINT UNSIGNED', 'Timeout in seconds an InnoDB transaction may wait for a lock before being rolled back. Values above 100000000 disable the timeout.', '1', '1073741824', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_SP_RECURSION_DEPTH', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'BIGINT UNSIGNED', 'Maximum stored procedure recursion depth', '0', '255', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_TIME', '', '1000', 'COMPILE-TIME', '1000', 'GLOBAL', 'INT UNSIGNED', 'Move blocks to the \'new\' end of the buffer pool if the first access was at least this many milliseconds ago. The timeout is disabled if 0.', '0', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPORT_PORT', '', '3306', 'AUTO', '0', 'GLOBAL', 'INT UNSIGNED', 'Port for connecting to slave reported to the master during slave registration. Set it only if the slave is listening on a non-default port or if you have a special tunnel from the master or other clients to the slave. If not sure, leave this option unset', '0', '4294967295', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_LOG_BIN', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 0 (1 is the default), no logging to the binary log is done for the client. Only clients with the SUPER privilege can update this variable. Can have unintended consequences if set globally, see SET SQL_LOG_BIN. Starting MariaDB 10.1.7, this variable does not affect the replication of events in a Galera cluster.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLOW_QUERY_LOG_FILE', '', 'telmo-slow.log', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Log slow queries to given log file. Defaults logging to \'hostname\'-slow.log. Must be enabled to activate other slow log options', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_SYSTEM', '', 'utf8', 'COMPILE-TIME', '', 'GLOBAL', 'ENUM', 'The character set used by the server for storing identifiers', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXPIRE_LOGS_DAYS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'If non-zero, binary logs will be purged after expire_logs_days days; possible purges happen at startup and at binary log rotation', '0', '99', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMPRESSION_FAILURE_THRESHOLD_PCT', '', '5', 'COMPILE-TIME', '5', 'GLOBAL', 'BIGINT UNSIGNED', 'If the compression failure rate of a table is greater than this number more padding is added to the pages to reduce the failures. A value of zero implies no padding', '0', '100', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_EXEC_MODE', '', 'STRICT', 'COMPILE-TIME', 'STRICT', 'GLOBAL', 'ENUM', 'How replication events should be executed. Legal values are STRICT (default) and IDEMPOTENT. In IDEMPOTENT mode, replication will not stop for operations that are idempotent. For example, in row based replication attempts to delete rows that doesn\'t exist will be ignored. In STRICT mode, replication will stop on any unexpected difference between the master and the slave.', '', '', '', 'STRICT,IDEMPOTENT', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOCKED_IN_MEMORY', '', 'OFF', 'COMPILE-TIME', '', 'GLOBAL', 'BOOLEAN', 'Whether mysqld was locked in memory with --memlock', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_FORMAT', 'MIXED', 'MIXED', 'COMPILE-TIME', 'MIXED', 'SESSION', 'ENUM', 'What form of binary logging the master will use: either ROW for row-based binary logging, STATEMENT for statement-based binary logging, or MIXED. MIXED is statement-based binary logging except for those statements where only row-based is correct: those which involve user-defined functions (i.e. UDFs) or the UUID() function; for those, row-based binary logging is automatically used.', '', '', '', 'MIXED,STATEMENT,ROW', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOCKING_FAKE_CHANGES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_LENGTH_FOR_SORT_DATA', '1024', '1024', 'COMPILE-TIME', '1024', 'SESSION', 'BIGINT UNSIGNED', 'Max number of bytes in sorted records', '4', '8388608', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DISALLOW_WRITES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Tell InnoDB to stop any writes to disk', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEEP_FILES_ON_CREATE', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Don\'t overwrite stale .MYD and .MYI even if no directory is specified', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PREFIX_INDEX_CLUSTER_OPTIMIZATION', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable prefix optimization to sometimes avoid cluster index lookups.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FT_BOOLEAN_SYNTAX', '', '+ -><()~*:\"\"&|', 'COMPILE-TIME', '+ -><()~*:\"\"&|', 'GLOBAL', 'VARCHAR', 'List of operators for MATCH ... AGAINST ( ... IN BOOLEAN MODE)', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_RECOVER_OPTIONS', '', 'BACKUP,QUICK', 'COMPILE-TIME', 'BACKUP,QUICK', 'GLOBAL', 'SET', 'Specifies how corrupted tables should be automatically repaired', '', '', '', 'NORMAL,BACKUP,FORCE,QUICK,OFF', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_DDL_EXEC_MODE', '', 'IDEMPOTENT', 'COMPILE-TIME', 'IDEMPOTENT', 'GLOBAL', 'ENUM', 'How replication events should be executed. Legal values are STRICT and IDEMPOTENT (default). In IDEMPOTENT mode, replication will not stop for DDL operations that are idempotent. This means that CREATE TABLE is treated as CREATE TABLE OR REPLACE and DROP TABLE is treated as DROP TABLE IF EXISTS.', '', '', '', 'STRICT,IDEMPOTENT', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_SKIP_ERRORS', '', 'OFF', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tells the slave thread to continue replication when a query event returns an error from the provided list', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_DATABASE', 'latin1', 'latin1', 'COMPILE-TIME', 'latin1', 'SESSION', 'ENUM', 'The character set used by the default database', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_PCT', '', '25', 'COMPILE-TIME', '25', 'GLOBAL', 'BIGINT UNSIGNED', 'Dump only the hottest N% of each buffer pool, defaults to 25', '1', '100', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DATE_FORMAT', '', '%Y-%m-%d', 'COMPILE-TIME', '%Y-%m-%d', 'GLOBAL', 'VARCHAR', 'The DATE format (ignored)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ENCRYPT_TABLES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'ENUM', 'Enable encryption for tables. Don\'t forget to enable --innodb-encrypt-log too', '', '', '', 'OFF,ON,FORCE', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LONG_QUERY_TIME', '10.000000', '10.000000', 'COMPILE-TIME', '10.000000', 'SESSION', 'DOUBLE', 'Log all queries that have taken more than long_query_time seconds to execute to the slow query log file. The argument will be treated as a decimal value with microsecond precision', '0', '31536000', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('COLLATION_DATABASE', 'latin1_swedish_ci', 'latin1_swedish_ci', 'COMPILE-TIME', 'latin1_swedish_ci', 'SESSION', 'ENUM', 'The collation of the database character set', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TIMESTAMP', '1537108303.148489', '', 'COMPILE-TIME', '0.000000', 'SESSION ONLY', 'DOUBLE', 'Set the time for this client', '0', '2147483647', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_STACKTRACE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_COMPRESSED_PROTOCOL', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Use compression on master/slave protocol', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_BUFFER_SIZE', '', '16777216', 'COMPILE-TIME', '16777216', 'GLOBAL', 'BIGINT', 'The size of the buffer which InnoDB uses to write log to the log files on disk.', '262144', '9223372036854775807', '1024', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_UNDO_LOG_TRUNCATE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable or Disable Truncate of UNDO tablespace.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HOST_CACHE_SIZE', '', '279', 'AUTO', '128', 'GLOBAL', 'BIGINT UNSIGNED', 'How many host names should be cached to avoid resolving.', '0', '65536', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_CONVERT_LOCK_TO_TRX', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'To convert locking sessions into transactions', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_PURGE_LAG', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Desired maximum length of the purge queue (0 = no limit)', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPORT_USER', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The account user name of the slave to be reported to the master during slave registration', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_MAX_SORT_FILE_SIZE', '', '9223372036853727232', 'COMPILE-TIME', '9223372036853727232', 'GLOBAL', 'BIGINT UNSIGNED', 'Don\'t use the fast sort index method to created index if the temporary file would get bigger than this', '0', '9223372036854775807', '1048576', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_CHECKPOINT_INTERVAL', '', '30', 'COMPILE-TIME', '30', 'GLOBAL', 'BIGINT UNSIGNED', 'Interval between tries to do an automatic checkpoints. In seconds; 0 means \'no automatic checkpoints\' which makes sense only for testing.', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_ARCH_EXPIRE_SEC', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '18446744073709551615', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN_BASENAME', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The full path of the binary log file names, excluding the extension.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_LOG_CONFLICTS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'To log multi-master conflicts', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLOW_QUERY_LOG', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Log slow queries to a table or log file. Defaults logging to a file \'hostname\'-slow.log or a table mysql.slow_log if --log-output=TABLE is used. Must be enabled to activate other slow log options.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OLD_MODE', '', '', 'COMPILE-TIME', '', 'SESSION', 'SET', 'Used to emulate old behavior from earlier MariaDB or MySQL versions', '', '', '', 'NO_DUP_KEY_WARNINGS_WITH_IGNORE,NO_PROGRESS_INFO,ZERO_DATE_TIME_CAST', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_SIZE', '', '1048576', 'COMPILE-TIME', '1048576', 'GLOBAL', 'BIGINT UNSIGNED', 'The memory allocated to store results from old queries', '0', '18446744073709551615', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEFAULT_REGEX_FLAGS', '', '', 'COMPILE-TIME', '', 'SESSION', 'SET', 'Default flags for the regex library', '', '', '', 'DOTALL,DUPNAMES,EXTENDED,EXTRA,MULTILINE,UNGREEDY', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SST_DONOR_REJECTS_QUERIES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Reject client queries when donating state snapshot transfer', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HISTOGRAM_TYPE', 'SINGLE_PREC_HB', 'SINGLE_PREC_HB', 'COMPILE-TIME', 'SINGLE_PREC_HB', 'SESSION', 'ENUM', 'Specifies type of the histograms created by ANALYZE. Possible values are: SINGLE_PREC_HB - single precision height-balanced, DOUBLE_PREC_HB - double precision height-balanced.', '', '', '', 'SINGLE_PREC_HB,DOUBLE_PREC_HB', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPEN_FILES_LIMIT', '', '16364', 'AUTO', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'If this is not 0, then mysqld will use this value to reserve file descriptors to use with setrlimit(). If this value is 0 or autoset then mysqld will reserve max_connections*5 or max_connections + table_cache*2 (whichever is larger) number of file descriptors', '0', '4294967295', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_COMPRESS', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the zlib compression library is accessible to the server, this will be set to YES, otherwise it will be NO. The COMPRESS() and UNCOMPRESS() functions will only be available if set to YES.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('COLLATION_CONNECTION', 'utf8_general_ci', 'latin1_swedish_ci', 'COMPILE-TIME', 'latin1_swedish_ci', 'SESSION', 'ENUM', 'The collation of the connection character set', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_ATOMIC_WRITES', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable atomic writes, instead of using the doublewrite buffer, for files on devices that supports atomic writes. To use this option one must use file_per_table=1, flush_method=O_DIRECT and use_fallocate=1. This option only works on Linux with either FusionIO cards using the directFS filesystem or with Shannon cards using any file system.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TIME_FORMAT', '', '%H:%i:%s', 'COMPILE-TIME', '%H:%i:%s', 'GLOBAL', 'VARCHAR', 'The TIME format (ignored)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_CACHE_SIZE', '', '151', 'AUTO', '256', 'GLOBAL', 'BIGINT UNSIGNED', 'How many threads we should keep in a cache for reuse. These are freed after 5 minutes of idle time', '0', '16384', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PROTOCOL_VERSION', '', '10', 'COMPILE-TIME', '', 'GLOBAL', 'INT UNSIGNED', 'The version of the client/server protocol used by the MariaDB server', '0', '4294967295', '1', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DOUBLEWRITE', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable InnoDB doublewrite buffer (enabled by default). Disable with --skip-innodb-doublewrite.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('AUTO_INCREMENT_INCREMENT', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Auto-increment columns are incremented by this', '1', '65535', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'InnoDB persistent statistics enabled for all tables unless overridden at table level', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The location and name to use for relay logs.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEFAULT_WEEK_FORMAT', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'BIGINT UNSIGNED', 'The default week format used by WEEK() functions', '0', '7', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_IGNORE_DUPLICATES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'When set, different master connections in multi-source replication are allowed to receive and process event groups with the same GTID (when using GTID mode). Only one will be applied, any others will be ignored. Within a given replication domain, just the sequence number will be used to decide whether a given GTID has been already applied; this means it is the responsibility of the user to ensure that GTID sequence numbers are strictly increasing.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CHANGE_BUFFER_MAX_SIZE', '', '25', 'COMPILE-TIME', '25', 'GLOBAL', 'INT UNSIGNED', 'Maximum on-disk size of change buffer in terms of percentage of the buffer pool.', '0', '50', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DEBUG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'To enable debug level logging', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFAULT_ROW_FORMAT', '', 'dynamic', 'COMPILE-TIME', 'dynamic', 'GLOBAL', 'ENUM', 'The default ROW FORMAT for all innodb tables created without explicit ROW_FORMAT. Possible values are REDUNDANT, COMPACT, and DYNAMIC. The ROW_FORMAT value COMPRESSED is not allowed', '', '', '', 'redundant,compact,dynamic', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BASEDIR', '', '/usr/', 'ENVIRONMENT', '', 'GLOBAL', 'VARCHAR', 'Path to installation directory. All paths are usually resolved relative to this', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_NATIVE_AIO', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Use native AIO if supported on this platform.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TMP_DISK_TABLE_SIZE', '18446744073709551615', '18446744073709551615', 'COMPILE-TIME', '18446744073709551615', 'SESSION', 'BIGINT UNSIGNED', 'Max size for data for an internal temporary on-disk MyISAM or Aria table.', '1024', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_CACHE_DIVISION_LIMIT', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'The minimum percentage of warm blocks in key cache', '1', '100', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_CRYPT', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the crypt() system call is available this variable will be set to YES, otherwise it will be set to NO. If set to NO, the ENCRYPT() function cannot be used.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_CHECKSUM_ALGORITHM', '', 'DEPRECATED', 'COMPILE-TIME', 'DEPRECATED', 'GLOBAL', 'ENUM', 'Deprecated and translated to innodb_log_checksums (NONE to OFF, everything else to ON); only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'CRC32,STRICT_CRC32,INNODB,STRICT_INNODB,NONE,STRICT_NONE,DEPRECATED', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SLAVE_THREADS', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of slave appliers to launch', '1', '512', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_TRACK_CHANGED_PAGES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_OPENSSL', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Comparing have_openssl with have_ssl will indicate whether YaSSL or openssl was used. If YaSSL, have_ssl will be YES, but have_openssl will be NO.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_SHORT', '4', '4', 'COMPILE-TIME', '4', 'SESSION', 'BIGINT UNSIGNED', 'Short search depth for the two-step deadlock detection', '0', '32', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_ON', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'To enable wsrep replication ', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_MIN_TOKEN_SIZE', '', '3', 'COMPILE-TIME', '3', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search minimum token size in characters', '0', '16', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEFAULT_STORAGE_ENGINE', 'InnoDB', 'InnoDB', 'COMPILE-TIME', 'InnoDB', 'SESSION', 'VARCHAR', 'The default storage engine for new tables', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_USER_STOPWORD_TABLE', '', '', 'COMPILE-TIME', '', 'SESSION', 'VARCHAR', 'User supplied stopword table name, effective in the session level.', '', '', '', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_RECURSIVE_ITERATIONS', '4294967295', '4294967295', 'COMPILE-TIME', '4294967295', 'SESSION', 'BIGINT UNSIGNED', 'Maximum number of iterations when executing recursive queries', '0', '4294967295', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SERVER_ID', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Uniquely identifies the server instance in the community of replication partners', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('JOIN_CACHE_LEVEL', '2', '2', 'COMPILE-TIME', '2', 'SESSION', 'BIGINT UNSIGNED', 'Controls what join operations can be executed with join buffers. Odd numbers are used for plain join buffers while even numbers are used for linked buffers', '0', '8', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_MAX_SORT_FILE_SIZE', '', '9223372036853727232', 'COMPILE-TIME', '9223372036853727232', 'GLOBAL', 'BIGINT UNSIGNED', 'Don\'t use the fast sort index method to created index if the temporary file would get bigger than this.', '0', '9223372036854775807', '1048576', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ENCRYPTION_ROTATE_KEY_AGE', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'INT UNSIGNED', 'Key rotation - re-encrypt in background all pages that were encrypted with a key that many (or more) versions behind. Value 0 indicates that key rotation is disabled.', '0', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_TEMP_DATA_FILE_PATH', '', 'ibtmp1:12M:autoextend', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Path to files and their sizes making temp-tablespace.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_NOTES', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 1, the default, warning_count is incremented each time a Note warning is encountered. If set to 0, Note warnings are not recorded. mysqldump has outputs to set this variable to 0 so that no unnecessary increments occur when data is reloaded.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SCRUB_LOG_SPEED', '', '256', 'COMPILE-TIME', '256', 'GLOBAL', 'BIGINT UNSIGNED', 'Background redo log scrubbing speed in bytes/sec', '1', '50000', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_MAX_WS_SIZE', '', '2147483647', 'COMPILE-TIME', '2147483647', 'GLOBAL', 'BIGINT UNSIGNED', 'Max write set size (bytes)', '1024', '2147483647', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_SYNC_LOG_DIR', '', 'NEWFILE', 'COMPILE-TIME', 'NEWFILE', 'GLOBAL', 'ENUM', 'Controls syncing directory after log file growth and new file creation', '', '', '', 'NEVER,NEWFILE,ALWAYS', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_SEQ_NO', '0', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'Internal server usage, for replication with global transaction id. When set, next event group logged to the binary log will use this sequence number, not generate a new one, thus allowing to preserve master\'s GTID in slave\'s binlog.', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_ENABLE_STOPWORD', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Create FTS index with stopword.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYNC_RELAY_LOG_INFO', '', '10000', 'COMPILE-TIME', '10000', 'GLOBAL', 'INT UNSIGNED', 'Synchronously flush relay log info to disk after every #th transaction. Use 0 to disable synchronous flushing', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_CHECK_INTERVAL', '', '3600', 'COMPILE-TIME', '3600', 'GLOBAL', 'INT UNSIGNED', 'check if spaces needs scrubbing every innodb_background_scrub_data_check_interval seconds', '1', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SLAVE_FK_CHECKS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Should slave thread do foreign key constraint checks', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_GROUP_COMMIT_INTERVAL', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Interval between commite in microseconds (1/1000000c). 0 stands for no waiting for other threads to come and do a commit in \"hard\" mode and no sync()/commit at all in \"soft\" mode.  Option has only an effect if aria_group_commit is used', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SLAVE_UK_CHECKS', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Should slave thread do secondary index uniqueness checks', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PROFILING_HISTORY_SIZE', '15', '15', 'COMPILE-TIME', '15', 'SESSION', 'BIGINT UNSIGNED', 'Number of statements about which profiling information is maintained. If set to 0, no profiles are stored. See SHOW PROFILES.', '0', '100', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_EMPTY_FREE_LIST_ALGORITHM', '', 'DEPRECATED', 'COMPILE-TIME', 'DEPRECATED', 'GLOBAL', 'ENUM', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'LEGACY,BACKOFF,DEPRECATED', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CHANGE_BUFFERING', '', 'all', 'COMPILE-TIME', 'all', 'GLOBAL', 'VARCHAR', 'Buffer changes to reduce random access: OFF, ON, inserting, deleting, changing, or purging.', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_START_POSITION', '', '00000000-0000-0000-0000-000000000000:-1', 'COMPILE-TIME', '00000000-0000-0000-0000-000000000000:-1', 'GLOBAL', 'VARCHAR', 'global transaction position to start from ', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_MODIFIED_COUNTER', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of rows modified before we calculate new statistics (default 0 = current limits)', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_SPACE_LIMIT', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum space to use for all relay logs', '0', '18446744073709551615', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_OPTIMIZE_THREAD_SCHEDULING', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Run fast part of group commit in a single thread, to optimize kernel thread scheduling. On by default. Disable to run each transaction in group commit in its own thread, which can be slower at very high concurrency. This option is mostly for testing one algorithm versus the other, and it should not normally be necessary to change it.', '', '', '', 'OFF,ON', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_PARALLEL_REPLICATION', 'OFF', '', 'COMPILE-TIME', 'OFF', 'SESSION ONLY', 'BOOLEAN', 'If set when a transaction is written to the binlog, parallel apply of that transaction will be avoided on a slave where slave_parallel_mode is not \"aggressive\". Can be used to avoid unnecessary rollback and retry for transactions that are likely to cause a conflict if replicated in parallel.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ENCRYPT_BINLOG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Encrypt binary logs (including relay logs)', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_DIGEST_LENGTH', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'BIGINT', 'Maximum length considered for digest text, when stored in performance_schema tables.', '0', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_MTFLUSH', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'DEPRECATED. Use multi-threaded flush. Default FALSE.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_NODE_INCOMING_ADDRESS', '', 'AUTO', 'COMPILE-TIME', 'AUTO', 'GLOBAL', 'VARCHAR', 'Client connection address', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CORRUPT_TABLE_ACTION', '', 'deprecated', 'COMPILE-TIME', 'deprecated', 'GLOBAL', 'ENUM', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'assert,warn,salvage,deprecated', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ERROR_COUNT', '0', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'BIGINT UNSIGNED', 'The number of errors that resulted from the last statement that generated messages', '0', '18446744073709551615', '1', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_IGNORE_DB', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tell the slave to restrict replication to updates of tables whose names do not appear in the comma-separated list. For statement-based replication, only the default database (that is, the one selected by USE) is considered, not any explicitly mentioned tables in the query. For row-based replication, the actual names of table(s) being updated are checked.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_DO_DB', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tell the slave to restrict replication to updates of tables whose names appear in the comma-separated list. For statement-based replication, only the default database (that is, the one selected by USE) is considered, not any explicitly mentioned tables in the query. For row-based replication, the actual names of table(s) being updated are checked.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMPRESSION_ALGORITHM', '', 'zlib', 'COMPILE-TIME', 'zlib', 'GLOBAL', 'ENUM', 'Compression algorithm used on page compression. One of: none, zlib, lz4, lzo, lzma, or bzip2', '', '', '', 'none,zlib,lz4,lzo,lzma,bzip2,snappy', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_NODE_NAME', '', 'telmo', 'COMPILE-TIME', 'telmo', 'GLOBAL', 'VARCHAR', 'Name of this node. This name can be used in wsrep_sst_donor as a preferred donor. Note that multiple nodes in a cluster can have the same name.', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PLUGIN_MATURITY', '', 'unknown', 'COMPILE-TIME', 'unknown', 'GLOBAL', 'ENUM', 'The lowest desirable plugin maturity. Plugins less mature than that will not be installed or loaded', '', '', '', 'unknown,experimental,alpha,beta,gamma,stable', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_RTREE_KEYS', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If RTREE indexes (used for spatial indexes) are available, will be set to YES, otherwise will be set to NO.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PURGE_THREADS', '', '4', 'COMPILE-TIME', '4', 'GLOBAL', 'BIGINT UNSIGNED', 'Purge threads can be from 1 to 32. Default is 4.', '1', '32', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BIG_TABLES', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Old variable, which if set to 1, allows large result sets by saving all temporary sets to disk, avoiding \'table full\' errors. No longer needed, as the server now handles this automatically. sql_big_tables is a synonym.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PROFILING', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1 (0 is default), statement profiling will be enabled. See SHOW PROFILES and SHOW PROFILE.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MASTER_VERIFY_CHECKSUM', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Force checksum verification of logged events in the binary log before sending them to slaves or printing them in the output of SHOW BINLOG EVENTS', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_PAGECACHE_BUFFER_SIZE', '', '134217728', 'COMPILE-TIME', '134217728', 'GLOBAL', 'BIGINT UNSIGNED', 'The size of the buffer used for index blocks for Aria tables. Increase this to get better index handling (for all reads and multiple writes) to as much as you can afford.', '131072', '18446744073709551615', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_VERSION', '', '5.7.21', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'InnoDB version', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXPENSIVE_SUBQUERY_LIMIT', '100', '100', 'COMPILE-TIME', '100', 'SESSION', 'BIGINT UNSIGNED', 'The maximum number of rows a subquery may examine in order to be executed during optimization and used for constant optimization', '0', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_JOIN_SIZE', '18446744073709551615', '18446744073709551615', 'COMPILE-TIME', '18446744073709551615', 'SESSION', 'BIGINT UNSIGNED', 'Joins that are probably going to read more than max_join_size records return an error', '1', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_DUMP_AT_SHUTDOWN', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Dump the buffer pool into a file named @@innodb_buffer_pool_filename', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_BINLOG_POS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Last GTID logged to the binary log, per replicationdomain', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CRLPATH', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'CRL directory (check OpenSSL docs, implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_WRITE_LOCK_COUNT', '', '4294967295', 'COMPILE-TIME', '4294967295', 'GLOBAL', 'BIGINT UNSIGNED', 'After this many write locks, allow some read locks to run in between', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEADLOCK_SEARCH_DEPTH_LONG', '15', '15', 'COMPILE-TIME', '15', 'SESSION', 'BIGINT UNSIGNED', 'Long search depth for the two-step deadlock detection', '0', '33', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_PAGECACHE_AGE_THRESHOLD', '', '300', 'COMPILE-TIME', '300', 'GLOBAL', 'BIGINT UNSIGNED', 'This characterizes the number of hits a hot block has to be untouched until it is considered aged enough to be downgraded to a warm block. This specifies the percentage ratio of that number of hits to the total number of blocks in the page cache.', '100', '18446744073709551615', '100', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MRR_BUFFER_SIZE', '262144', '262144', 'COMPILE-TIME', '262144', 'SESSION', 'BIGINT UNSIGNED', 'Size of buffer to use when using MRR with range access', '8192', '2147483647', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXTRA_MAX_CONNECTIONS', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of connections on extra-port', '1', '100000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_DIRTY_PAGES_PCT_LWM', '', '0.000000', 'COMPILE-TIME', '0.000000', 'GLOBAL', 'DOUBLE', 'Percentage of dirty pages at which flushing kicks in.', '0', '99.999', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_SLAVE_SKIP_COUNTER', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'BIGINT UNSIGNED', 'Skip the next N events from the master log', '0', '4294967295', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_CACHE_AGE_THRESHOLD', '', '300', 'COMPILE-TIME', '300', 'GLOBAL', 'BIGINT UNSIGNED', 'This characterizes the number of hits a hot block has to be untouched until it is considered aged enough to be downgraded to a warm block. This specifies the percentage ratio of that number of hits to the total number of blocks in key cache', '100', '4294967295', '100', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_DOMAIN_ID', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'INT UNSIGNED', 'Used with global transaction ID to identify logically independent replication streams. When events can propagate through multiple parallel paths (for example multiple masters), each independent source server must use a distinct domain_id. For simple tree-shaped replication topologies, it can be left at its default, 0.', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FOREIGN_KEY_CHECKS', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 1 (the default) foreign key constraints (including ON UPDATE and ON DELETE behavior) InnoDB tables are checked, while if set to 0, they are not checked. 0 is not recommended for normal use, though it can be useful in situations where you know the data is consistent, but want to reload data in a different order from that that specified by parent/child relationships. Setting this variable to 1 does not retrospectively check for inconsistencies introduced while set to 0.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_QUERY_CACHE', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the server supports the query cache, will be set to YES, otherwise will be set to NO.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_KILL_IDLE_TRANSACTION', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT', 'No effect for this build.', '0', '9223372036854775807', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_SSL', '', 'DISABLED', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If the server supports secure connections, will be set to YES, otherwise will be set to NO. If set to DISABLED, the server was compiled with TLS support, but was not started with TLS support (see the mysqld options). See also have_openssl.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_AUTO_RECALC', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'InnoDB automatic recalculation of persistent statistics enabled for all tables unless overridden at table level (automatic recalculation is only done when InnoDB decides that the table has changed too much and needs a new statistics)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_WILD_IGNORE_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Tells the slave thread to not replicate to the tables that match the given wildcard pattern.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMMIT_CONCURRENCY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Helps in performance tuning in heavily concurrent environments.', '0', '1000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SECURE_FILE_PRIV', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Limit LOAD DATA, SELECT ... OUTFILE, and LOAD_FILE() to files within specified directory', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MIRRORED_LOG_GROUPS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '10', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_TC_SIZE', '', '24576', 'AUTO', '24576', 'GLOBAL', 'BIGINT UNSIGNED', 'Size of transaction coordinator log.', '12288', '18446744073709551615', '4096', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FATAL_SEMAPHORE_WAIT_THRESHOLD', '', '600', 'COMPILE-TIME', '600', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of seconds that semaphore times out in InnoDB.', '1', '4294967295', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SECURE_AUTH', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Disallow authentication for accounts that have old (pre-4.1) passwords', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_IDLE_FLUSH_PCT', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'Up to what percentage of dirty pages should be flushed when innodb finds it has spare resources to do so.', '0', '100', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DIV_PRECISION_INCREMENT', '4', '4', 'COMPILE-TIME', '4', 'SESSION', 'BIGINT UNSIGNED', 'Precision of the result of \'/\' operator will be increased on that value', '0', '38', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SUPPORT_XA', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Enable InnoDB support for the XA two-phase commit', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DATETIME_FORMAT', '', '%Y-%m-%d %H:%i:%s', 'COMPILE-TIME', '%Y-%m-%d %H:%i:%s', 'GLOBAL', 'VARCHAR', 'The DATETIME format (ignored)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SESSION_TRACK_TRANSACTION_INFO', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'ENUM', 'Track changes to the transaction attributes. OFF to disable; STATE to track just transaction state (Is there an active transaction? Does it have any data? etc.); CHARACTERISTICS to track transaction state and report all statements needed to start a transaction withthe same characteristics (isolation level, read only/read write,snapshot - but not any work done / data modified within the transaction).', '', '', '', 'OFF,STATE,CHARACTERISTICS', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_INCLUDE_DELETE_MARKED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Include delete marked records when calculating persistent statistics', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FILL_FACTOR', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT', 'Percentage of B-tree page filled during bulk insert', '10', '100', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_STRIP_COMMENTS', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Strip all comments from a query before storing it in the query cache', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ADAPTIVE_FLUSHING', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Attempt flushing dirty pages to avoid IO bursts at checkpoints.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_BUFFER_RESULT', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1 (0 is default), results from SELECT statements are always placed into temporary tables. This can help the server when it takes a long time to send the results to the client by allowing the table locks to be freed early.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FILE_PER_TABLE', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Stores each InnoDB table to an .ibd file in the database dir.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEFAULT_TMP_STORAGE_ENGINE', '', '', 'COMPILE-TIME', '', 'SESSION', 'VARCHAR', 'The default storage engine for user-created temporary tables', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('EXTRA_PORT', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'Extra port number to use for tcp connections in a one-thread-per-connection manner. 0 means don\'t use another port', '0', '4294967295', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_ANNOTATE_ROW_EVENTS', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Tells the master to annotate RBR events with the statement that caused these events', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_BLOCK_SIZE', '', '8192', 'COMPILE-TIME', '8192', 'GLOBAL', 'BIGINT UNSIGNED', 'Block size to be used for Aria index pages.', '1024', '32768', '1024', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_RELAY_LOG_SIZE', '1073741824', '1073741824', 'AUTO', '1073741824', 'SESSION', 'BIGINT UNSIGNED', 'relay log will be rotated automatically when the size exceeds this value.  If 0 at startup, it\'s set to max_binlog_size', '4096', '1073741824', '4096', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_POOL_PRIO_KICKUP_TIMER', '', '1000', 'COMPILE-TIME', '1000', 'GLOBAL', 'INT UNSIGNED', 'The number of milliseconds before a dequeued low-priority statement is moved to the high-priority queue', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_ABORT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Abort a currently running load of the buffer pool', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DATA_HOME_DIR', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The common part for InnoDB table spaces.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CERT', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'X509 cert in PEM format (implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_READ_IO_THREADS', '', '4', 'COMPILE-TIME', '4', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of background read I/O threads in InnoDB.', '1', '64', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR', '', '0.900000', 'COMPILE-TIME', '0.900000', 'GLOBAL', 'DOUBLE', 'A number between [0.7, 1] that tells defragmentation how full it should fill a page. Default is 0.9. Number below 0.7 won\'t make much sense.This variable, together with innodb_defragment_fill_factor_n_recs, is introduced so defragmentation won\'t pack the page too full and cause page split on the next insert on every page. The variable indicating more defragmentation gain is the one effective.', '0.7', '1', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SHOW_VERBOSE_LOCKS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '1', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WARNING_COUNT', '0', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'BIGINT UNSIGNED', 'The number of errors, warnings, and notes that resulted from the last statement that generated messages', '0', '18446744073709551615', '1', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_INSTANCES', '', '1', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of buffer pool instances, set to higher value on high-end machines to increase scalability', '0', '64', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LAST_INSERT_ID', '0', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'The value to be returned from LAST_INSERT_ID()', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_UNDO_DIRECTORY', '', './', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Directory where undo tablespace files live, this path can be absolute.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LICENSE', '', 'GPL', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The type of license the server has', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GENERAL_LOG_FILE', '', 'telmo.log', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Log connections and queries to given file', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INSERT_ID', '0', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'The value to be used by the following INSERT or ALTER TABLE statement when inserting an AUTO_INCREMENT value', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_RESULT_CACHE_LIMIT', '', '2000000000', 'COMPILE-TIME', '2000000000', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search query result cache limit in bytes', '1000000', '4294967295', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('IN_TRANSACTION', '0', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'BIGINT UNSIGNED', 'Whether there is an active transaction', '0', '1', '1', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SYNC_WAIT', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'INT UNSIGNED', 'Ensure \"synchronous\" read view before executing an operation of the type specified by bitmask: 1 - READ(includes SELECT, SHOW and BEGIN/START TRANSACTION); 2 - UPDATE and DELETE; 4 - INSERT and REPLACE', '0', '15', '1', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ROWID_MERGE_BUFF_SIZE', '8388608', '8388608', 'COMPILE-TIME', '8388608', 'SESSION', 'BIGINT UNSIGNED', 'The size of the buffers used [NOT] IN evaluation via partial matching', '0', '9223372036854775807', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_KEY', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'X509 key in PEM format (implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_CAUSAL_READS', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Setting this variable is equivalent to setting wsrep_sync_wait READ flag', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CLEANER_LSN_AGE_FACTOR', '', 'DEPRECATED', 'COMPILE-TIME', 'DEPRECATED', 'GLOBAL', 'ENUM', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'LEGACY,HIGH_CHECKPOINT,DEPRECATED', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LAST_GTID', '', '', 'COMPILE-TIME', '', 'SESSION ONLY', 'VARCHAR', 'The GTID of the last commit (if binlogging was enabled), or the empty string if none.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_LOAD_TMPDIR', '', '/tmp', 'AUTO', '', 'GLOBAL', 'VARCHAR', 'The location where the slave should put its temporary files when replicating a LOAD DATA INFILE command', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_TYPE_CONVERSIONS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'SET', 'Set of slave type conversions that are enabled. If the variable is empty, no conversions are allowed and it is expected that the types match exactly', '', '', '', 'ALL_LOSSY,ALL_NON_LOSSY', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FILE_FORMAT_CHECK', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Whether to perform system file format check.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_CONCURRENCY', '', '10', 'COMPILE-TIME', '10', 'GLOBAL', 'BIGINT UNSIGNED', 'Permits the application to give the threads system a hint for the desired number of threads that should be run at the same time.This variable has no effect, and is deprecated. It will be removed in a future release.', '1', '512', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_READ_AHEAD_THRESHOLD', '', '56', 'COMPILE-TIME', '56', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of pages that must be accessed sequentially for InnoDB to trigger a readahead.', '0', '64', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('KEY_CACHE_BLOCK_SIZE', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'BIGINT UNSIGNED', 'The default size of key cache blocks', '512', '16384', '512', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_READ_ONLY', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Start InnoDB in read only mode (off by default)', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_TMP_TABLES', '32', '32', 'COMPILE-TIME', '32', 'SESSION', 'BIGINT UNSIGNED', 'Unused, will be removed.', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_IMMEDIATE_SCRUB_DATA_UNCOMPRESSED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable scrubbing of data', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION_SSL_LIBRARY', '', 'OpenSSL 1.0.2g  1 Mar 2016', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Version of the used SSL library', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MONITOR_RESET_ALL', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Reset all values for a monitor counter', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_LOG_OFF', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1 (0 is the default), no logging to the general query log is done for the client. Only clients with the SUPER privilege can update this variable.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_OPTIMIZE_FULLTEXT_ONLY', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Only optimize the Fulltext index of the table', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_CHUNK_SIZE', '', '134217728', 'COMPILE-TIME', '134217728', 'GLOBAL', 'BIGINT UNSIGNED', 'Size of a single memory chunk within each buffer pool instance for resizing buffer pool. Online buffer pool resizing happens at this granularity. 0 means disable resizing buffer pool.', '1048576', '9223372036854775807', '1048576', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_METHOD', '', 'nulls_equal', 'COMPILE-TIME', 'nulls_equal', 'GLOBAL', 'ENUM', 'Specifies how InnoDB index statistics collection code should treat NULLs. Possible values are NULLS_EQUAL (default), NULLS_UNEQUAL and NULLS_IGNORED', '', '', '', 'nulls_equal,nulls_unequal,nulls_ignored', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT_FILL_FACTOR_N_RECS', '', '20', 'COMPILE-TIME', '20', 'GLOBAL', 'INT UNSIGNED', 'How many records of space defragmentation should leave on the page. This variable, together with innodb_defragment_fill_factor, is introduced so defragmentation won\'t pack the page too full and cause page split on the next insert on every page. The variable indicating more defragmentation gain is the one effective.', '1', '100', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SSL_CIPHER', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'SSL cipher to use (implies --ssl)', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_TRANSACTION_RETRIES', '', '10', 'COMPILE-TIME', '10', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of times the slave SQL thread will retry a transaction in case it failed with a deadlock or elapsed lock wait timeout, before giving up and stopping', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLOW_RATE_LIMIT', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'Write to slow log every #th slow query. Set to 1 to log everything. Increase it to reduce the size of the slow or the performance impact of slow logging', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_SQL_VERIFY_CHECKSUM', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Force checksum verification of replication events after reading them from relay log. Note: Events are always checksum-verified by slave on receiving them from the network before writing them to the relay log', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_BLOCK_SIZE', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '65536', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FT_STOPWORD_FILE', '', '(built-in)', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Use stopwords from this file instead of built-in list', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BACKGROUND_SCRUB_DATA_UNCOMPRESSED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable scrubbing of uncompressed data by background threads (same as encryption_threads)', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPTIMIZER_SEARCH_DEPTH', '62', '62', 'COMPILE-TIME', '62', 'SESSION', 'BIGINT UNSIGNED', 'Maximum depth of search performed by the query optimizer. Values larger than the number of relations in a query result in better query plans, but take longer to compile a query. Values smaller than the number of tables in a relation result in faster optimization, but may produce very bad query plans. If set to 0, the system will automatically pick a reasonable value.', '0', '62', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_SIZE', '', '134217728', 'COMPILE-TIME', '134217728', 'GLOBAL', 'BIGINT', 'The size of the memory buffer InnoDB uses to cache data and indexes of its tables.', '5242880', '9223372036854775807', '1048576', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_STATS_METHOD', 'NULLS_UNEQUAL', 'NULLS_UNEQUAL', 'COMPILE-TIME', 'NULLS_UNEQUAL', 'SESSION', 'ENUM', 'Specifies how MyISAM index statistics collection code should treat NULLs. Possible values of name are NULLS_UNEQUAL (default behavior for 4.1 and later), NULLS_EQUAL (emulate 4.0 behavior), and NULLS_IGNORED', '', '', '', 'NULLS_UNEQUAL,NULLS_EQUAL,NULLS_IGNORED', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOCAL_INFILE', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable LOAD DATA LOCAL INFILE', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMPRESSION_LEVEL', '', '6', 'COMPILE-TIME', '6', 'GLOBAL', 'INT UNSIGNED', 'Compression level used for zlib compression.  0 is no compression, 1 is fastest, 9 is best compression and default is 6.', '0', '9', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_NOW', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Trigger an immediate load of the buffer pool from a file named @@innodb_buffer_pool_filename', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_STATEMENT_CLASSES', '', '188', 'COMPILE-TIME', '188', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of statement instruments.', '0', '256', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('IGNORE_DB_DIRS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Specifies a directory to add to the ignore list when collecting database names from the datadir. Put a blank argument to reset the list accumulated so far.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_ERROR_COUNT', '64', '64', 'COMPILE-TIME', '64', 'SESSION', 'BIGINT UNSIGNED', 'Max number of errors/warnings to store for a statement', '0', '65535', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_CHECKSUM', '', 'CRC32', 'COMPILE-TIME', 'CRC32', 'GLOBAL', 'ENUM', 'Type of BINLOG_CHECKSUM_ALG. Include checksum for log events in the binary log', '', '', '', 'NONE,CRC32', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FT_MIN_WORD_LEN', '', '4', 'COMPILE-TIME', '4', 'GLOBAL', 'BIGINT UNSIGNED', 'The minimum length of the word to be included in a FULLTEXT index. Note: FULLTEXT indexes must be rebuilt after changing this variable', '1', '84', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PAGE_SIZE', '', '16384', 'COMPILE-TIME', '16384', 'GLOBAL', 'BIGINT UNSIGNED', 'Page size to use for all InnoDB tablespaces.', '4096', '65536', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_FORCED_BINLOG_FORMAT', '', 'NONE', 'COMPILE-TIME', 'NONE', 'GLOBAL', 'ENUM', 'binlog format to take effect over user\'s choice', '', '', '', 'MIXED,STATEMENT,ROW,NONE', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_ENCRYPT_TABLES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Encrypt tables (only for tables with ROW_FORMAT=PAGE (default) and not FIXED/DYNAMIC)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_FORCE_START_AFTER_RECOVERY_FAILURES', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of consecutive log recovery failures after which logs will be automatically deleted to cure the problem; 0 (the default) disables the feature.', '0', '255', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_EVENTS_STATEMENTS_HISTORY_LONG_SIZE', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Number of rows in EVENTS_STATEMENTS_HISTORY_LONG. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSH_METHOD', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'With which method to flush data.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DELAYED_INSERT_LIMIT', '', '100', 'COMPILE-TIME', '100', 'GLOBAL', 'BIGINT UNSIGNED', 'After inserting delayed_insert_limit rows, the INSERT DELAYED handler will check if there are any SELECT statements pending. If so, it allows these to execute before continuing.', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_DELAYED_THREADS', '20', '20', 'COMPILE-TIME', '20', 'SESSION', 'BIGINT UNSIGNED', 'Don\'t start more than this number of threads to handle INSERT DELAYED statements. If set to zero INSERT DELAYED will be not used', '0', '16384', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_CHECKPOINT_LOG_ACTIVITY', '', '1048576', 'COMPILE-TIME', '1048576', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of bytes that the transaction log has to grow between checkpoints before a new checkpoint is written to the log.', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SST_AUTH', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Authentication for SST connection', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSH_LOG_AT_TRX_COMMIT', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT UNSIGNED', 'Controls the durability/speed trade-off for commits. Set to 0 (write and flush redo log to disk only once per second), 1 (flush to disk at each commit), 2 (write to log at commit but flush to disk only once per second) or 3 (flush to disk at prepare and at commit, slower and usually redundant). 1 and 3 guarantees that after a crash, committed transactions will not be lost and will be consistent with the binlog and other transactional engines. 2 can get inconsistent and lose transactions if there is a power failure or kernel crash but not if mysqld crashes. 0 has no guarantees in case of crash. 0 and 2 can be faster than 1 or 3.', '0', '3', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('STORAGE_ENGINE', 'InnoDB', 'InnoDB', 'COMPILE-TIME', 'InnoDB', 'SESSION', 'VARCHAR', 'Alias for @@default_storage_engine. Deprecated', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_PROVIDER_OPTIONS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Semicolon (;) separated list of wsrep options (see wsrep_provider_options documentation).', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOCKS_UNSAFE_FOR_BINLOG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'DEPRECATED. This option may be removed in future releases. Please use READ COMMITTED transaction isolation level instead. Force InnoDB to not use next-key locking, to use only row-level locking.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_PREPARED_STMT_COUNT', '', '16382', 'COMPILE-TIME', '16382', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of prepared statements in the server', '0', '1048576', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_CERTIFY_NONPK', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Certify tables with no primary key', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SST_DONOR', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'preferred donor node for the SST', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BINLOG_ROW_IMAGE', 'FULL', 'FULL', 'COMPILE-TIME', 'FULL', 'SESSION', 'ENUM', 'Controls whether rows should be logged in \'FULL\', \'NOBLOB\' or \'MINIMAL\' formats. \'FULL\', means that all columns in the before and after image are logged. \'NOBLOB\', means that mysqld avoids logging blob columns whenever possible (eg, blob column was not changed or is not part of primary key). \'MINIMAL\', means that a PK equivalent (PK columns or full row if there is no PK in the table) is logged in the before image, and only changed columns are logged in the after image. (Default: FULL).', '', '', '', 'MINIMAL,NOBLOB,FULL', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_COMPRESSION_PAD_PCT_MAX', '', '50', 'COMPILE-TIME', '50', 'GLOBAL', 'BIGINT UNSIGNED', 'Percentage of empty space on a data page that can be reserved to make the page compressible.', '0', '75', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ENCRYPT_TMP_DISK_TABLES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Encrypt temporary on-disk tables (created as part of query execution)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUFFER_POOL_LOAD_AT_STARTUP', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Load the buffer pool from a file named @@innodb_buffer_pool_filename', '', '', '', 'OFF,ON', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_CACHE_SIZE', '', '8000000', 'COMPILE-TIME', '8000000', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search cache size in bytes', '1600000', '80000000', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_REPLICATION', 'OFF', '', 'COMPILE-TIME', 'OFF', 'SESSION ONLY', 'BOOLEAN', 'Changes are logged into the binary log with the @@skip_replication flag set. Such events will not be replicated by slaves that run with --replicate-events-marked-for-skip set different from its default of REPLICATE. See Selectively skipping replication of binlog events for more information.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('FLUSH', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Flush MyISAM tables to disk between SQL commands', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_SAFE_UPDATES', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'If set to 1, UPDATEs and DELETEs need either a key in the WHERE clause, or a LIMIT clause, or else they will aborted. Prevents the common mistake of accidentally deleting or updating every row in a table.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLAVE_UPDATES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Tells the slave to log the updates from the slave thread to the binary log. You will need to turn it on if you plan to daisy-chain the slaves.', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DELAYED_INSERT_TIMEOUT', '', '300', 'COMPILE-TIME', '300', 'GLOBAL', 'BIGINT UNSIGNED', 'How long a INSERT DELAYED thread should wait for INSERT statements before terminating', '1', '31536000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_LONG', '50000000', '50000000', 'COMPILE-TIME', '50000000', 'SESSION', 'BIGINT UNSIGNED', 'Long timeout for the two-step deadlock detection (in microseconds)', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_GTID_MODE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Automatically update the (joiner) node\'s wsrep_gtid_domain_id value with that of donor\'s (received during state transfer) and use it in place of gtid_domain_id for all galera transactions. When OFF (default), wsrep_gtid_domain_id is simply ignored (backward compatibility).', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_ANNOTATE_ROW_EVENTS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Tells the slave to write annotate rows events received from the master to its own binary log. Ignored if log_slave_updates is not set', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CMP_PER_INDEX_ENABLED', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable INFORMATION_SCHEMA.innodb_cmp_per_index, may have negative impact on performance (off by default)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEBUG_NO_THREAD_ALARM', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Disable system thread alarm calls. Disabling it may be useful in debugging or testing, never do it in production', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_ONLINE_ALTER_LOG_MAX_SIZE', '', '134217728', 'COMPILE-TIME', '134217728', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum modification log file size for online index creation', '65536', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYNC_MASTER_INFO', '', '10000', 'COMPILE-TIME', '10000', 'GLOBAL', 'INT UNSIGNED', 'Synchronously flush master info to disk after every #th event. Use 0 to disable synchronous flushing', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('UPDATABLE_VIEWS_WITH_LIMIT', 'YES', 'YES', 'COMPILE-TIME', 'YES', 'SESSION', 'ENUM', 'YES = Don\'t issue an error message (warning only) if a VIEW without presence of a key of the underlying table is used in queries with a LIMIT clause for updating. NO = Prohibit update of a VIEW, which does not contain a key of the underlying table and the query uses a LIMIT clause (usually get from GUI tools)', '', '', '', 'NO,YES', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_NAME_RESOLVE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Don\'t resolve hostnames. All hostnames are IP\'s or \'localhost\'.', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEADLOCK_DETECT', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Enable/disable InnoDB deadlock detector (default ON). if set to OFF, deadlock detection is skipped, and we rely on innodb_lock_wait_timeout in case of deadlock.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_ON_METADATA', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable statistics gathering for metadata commands such as SHOW TABLE STATUS for tables that use transient statistics (off by default)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_MAX_TOKEN_SIZE', '', '84', 'COMPILE-TIME', '84', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search maximum token size in characters', '10', '84', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('READ_BINLOG_SPEED_LIMIT', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum speed(KB/s) to read binlog from master (0 = no limit)', '0', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_REPLICATE_MYISAM', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'To enable myisam replication', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_STRICT_MODE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enforce strict seq_no ordering of events in the binary log. Slave stops with an error if it encounters an event that would cause it to generate an out-of-order binlog if executed.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SQL_QUOTE_SHOW_CREATE', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 1, the default, the server will quote identifiers for SHOW CREATE DATABASE, SHOW CREATE TABLE and SHOW CREATE VIEW statements. Quoting is disabled if set to 0. Enable to ensure replications works when identifiers require quoting.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_GROUP_HOME_DIR', '', './', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Path to InnoDB log files.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_NODE_ADDRESS', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Specifies the node\'s network address, in the format ip address[:port]. Used in situations where autoguessing is not reliable. As of MariaDB 10.1.8, supports IPv6.', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LARGE_PREFIX', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Support large index prefix length of REC_VERSION_56_MAX_INDEX_COL_LEN (3072) bytes.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_SORT_PLL_DEGREE', '', '2', 'COMPILE-TIME', '2', 'GLOBAL', 'BIGINT UNSIGNED', 'InnoDB Fulltext search parallel sort degree, will round up to nearest power of 2 number', '1', '16', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DEFRAGMENT_N_PAGES', '', '7', 'COMPILE-TIME', '7', 'GLOBAL', 'INT UNSIGNED', 'Number of pages considered at once when merging multiple pages to defragment', '2', '32', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('PERFORMANCE_SCHEMA_MAX_TABLE_HANDLES', '', '-1', 'COMPILE-TIME', '-1', 'GLOBAL', 'BIGINT', 'Maximum number of opened instrumented tables. Use 0 to disable, -1 for automated sizing.', '-1', '1048576', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_SLOW_FILTER', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk', 'COMPILE-TIME', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk', 'SESSION', 'SET', 'Log only certain types of queries', '', '', '', 'admin,filesort,filesort_on_disk,full_join,full_scan,query_cache,query_cache_miss,tmp_table,tmp_table_on_disk', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_CHECKSUMS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Whether to compute and require checksums for InnoDB redo log blocks', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PURGE_RSEG_TRUNCATE_FREQUENCY', '', '128', 'COMPILE-TIME', '128', 'GLOBAL', 'BIGINT UNSIGNED', 'Dictates rate at which UNDO records are purged. Value N means purge rollback segment(s) on every Nth iteration of purge invocation', '1', '128', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_SORT_BUFFER_SIZE', '268434432', '268434432', 'COMPILE-TIME', '268434432', 'SESSION', 'BIGINT UNSIGNED', 'The buffer that is allocated when sorting the index when doing a REPAIR or when creating indexes with CREATE INDEX or ALTER TABLE.', '4096', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_DATA_FILE_PATH', '', 'ibdata1:12M:autoextend', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Path to individual files and their sizes.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_SORT_BUFFER_SIZE', '134216704', '134216704', 'COMPILE-TIME', '134216704', 'SESSION', 'BIGINT UNSIGNED', 'The buffer that is allocated when sorting the index when doing a REPAIR or when creating indexes with CREATE INDEX or ALTER TABLE', '4096', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DATADIR', '', '/var/lib/mysql/', 'COMPILE-TIME', '/var/lib/mysql/', 'GLOBAL', 'VARCHAR', 'Path to the database root directory', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_OLD_BLOCKS_PCT', '', '37', 'COMPILE-TIME', '37', 'GLOBAL', 'INT UNSIGNED', 'Percentage of the buffer pool to reserve for \'old\' blocks.', '5', '95', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('COLLATION_SERVER', 'latin1_swedish_ci', 'latin1_swedish_ci', 'COMPILE-TIME', 'latin1_swedish_ci', 'SESSION', 'ENUM', 'The server default collation', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYISAM_REPAIR_THREADS', '1', '1', 'COMPILE-TIME', '1', 'SESSION', 'BIGINT UNSIGNED', 'If larger than 1, when repairing a MyISAM table all indexes will be created in parallel, with one thread per index. The value of 1 disables parallel repair', '1', '18446744073709551615', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SYNC_BINLOG', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'Synchronously flush binary log to disk after every #th event. Use 0 (default) to disable synchronous flushing', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LOG_BIN', '', 'OFF', 'COMPILE-TIME', '', 'GLOBAL', 'BOOLEAN', 'Whether the binary log is enabled', '', '', '', 'OFF,ON', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_AUX_TABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'FTS internal auxiliary table to be checked', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_PARALLEL_MODE', '', 'conservative', 'COMPILE-TIME', 'conservative', 'GLOBAL', 'ENUM', 'Controls what transactions are applied in parallel when using --slave-parallel-threads. Possible values: \"optimistic\" tries to apply most transactional DML in parallel, and handles any conflicts with rollback and retry. \"conservative\" limits parallelism in an effort to avoid any conflicts. \"aggressive\" tries to maximise the parallelism, possibly at the cost of increased conflict rate. \"minimal\" only parallelizes the commit steps of transactions. \"none\" disables parallel apply completely.', '', '', '', 'none,minimal,conservative,optimistic,aggressive', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_MYSQL_REPLICATION_BUNDLE', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'mysql replication group commit ', '0', '1000', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SKIP_SHOW_DATABASE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Don\'t allow \'SHOW DATABASE\' commands', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('IDENTITY', '0', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'Synonym for the last_insert_id variable', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('AUTOCOMMIT', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'If set to 1, the default, all queries are committed immediately. If set to 0, they are only committed upon a COMMIT statement, or rolled back with a ROLLBACK statement. If autocommit is set to 0, and then changed to 1, all open transactions are immediately committed.', '', '', '', 'OFF,ON', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_NUMA_INTERLEAVE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Use NUMA interleave memory policy to allocate InnoDB buffer pool.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('CHARACTER_SET_FILESYSTEM', 'binary', 'binary', 'COMPILE-TIME', 'binary', 'SESSION', 'ENUM', 'The filesystem character set', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('THREAD_STACK', '', '299008', 'COMPILE-TIME', '299008', 'GLOBAL', 'BIGINT UNSIGNED', 'The stack size for each thread', '131072', '18446744073709551615', '1024', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PAGE_CLEANERS', '', '1', 'COMPILE-TIME', '4', 'GLOBAL', 'BIGINT UNSIGNED', 'Page cleaner threads can be from 1 to 64. Default is 4.', '1', '64', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CHECKSUMS', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'DEPRECATED. Use innodb_checksum_algorithm=NONE instead of setting this to OFF. Enable InnoDB checksums validation (enabled by default). Disable with --skip-innodb-checksums.', '', '', '', 'OFF,ON', 'YES', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_LOG_PURGE_TYPE', '', 'immediate', 'COMPILE-TIME', 'immediate', 'GLOBAL', 'ENUM', 'Specifies how Aria transactional log will be purged', '', '', '', 'immediate,external,at_flush', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TRANSACTION_PREALLOC_SIZE', '4096', '4096', 'COMPILE-TIME', '4096', 'SESSION', 'BIGINT UNSIGNED', 'Persistent buffer for transactions to be stored in binary log', '1024', '134217728', '1024', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MYSQL56_TEMPORAL_FORMAT', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Use MySQL-5.6 (instead of MariaDB-5.3) format for TIME, DATETIME, TIMESTAMP columns.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('VERSION_COMPILE_OS', '', 'debian-linux-gnu', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Operating system that MariaDB was built on, for example debian-linux-gnu.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('GTID_BINLOG_STATE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'The internal GTID state of the binlog, used to keep track of all GTIDs ever logged to the binlog.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CHECKSUM_ALGORITHM', '', 'crc32', 'COMPILE-TIME', 'crc32', 'GLOBAL', 'ENUM', 'The algorithm InnoDB uses for page checksumming. Possible values are CRC32 (hardware accelerated if the CPU supports it) write crc32, allow any of the other checksums to match when reading; STRICT_CRC32 write crc32, do not allow other algorithms to match when reading; INNODB write a software calculated checksum, allow any other checksums to match when reading; STRICT_INNODB write a software calculated checksum, do not allow other algorithms to match when reading; NONE write a constant magic number, do not do any checksum verification when reading (same as innodb_checksums=OFF); STRICT_NONE write a constant magic number, do not allow values other than that magic number when reading; Files updated when this option is set to crc32 or strict_crc32 will not be readable by MariaDB versions older than 10.0.4', '', '', '', 'crc32,strict_crc32,innodb,strict_innodb,none,strict_none', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SYNC_ARRAY_SIZE', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT UNSIGNED', 'Size of the mutex/lock wait array.', '1', '1024', '0', '', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('DEADLOCK_TIMEOUT_SHORT', '10000', '10000', 'COMPILE-TIME', '10000', 'SESSION', 'BIGINT UNSIGNED', 'Short timeout for the two-step deadlock detection (in microseconds)', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_OPEN_FILES', '', '2000', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT', 'How many files at the maximum InnoDB keeps open at the same time.', '0', '9223372036854775807', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_CLUSTER_NAME', '', 'my_wsrep_cluster', 'COMPILE-TIME', 'my_wsrep_cluster', 'GLOBAL', 'VARCHAR', 'Name for the cluster', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_DESYNC', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'To desynchronize the node from the cluster', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_GROUP_COMMIT', '', 'none', 'COMPILE-TIME', 'none', 'GLOBAL', 'ENUM', 'Specifies Aria group commit mode. Possible values are \"none\" (no group commit), \"hard\" (with waiting to actual commit), \"soft\" (no wait for commit (DANGEROUS!!!))', '', '', '', 'none,hard,soft', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INIT_SLAVE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Command(s) that are executed by a slave server each time the SQL thread starts', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_FILE_SIZE', '', '50331648', 'COMPILE-TIME', '50331648', 'GLOBAL', 'BIGINT UNSIGNED', 'Size of each log file in a log group.', '1048576', '549755813888', '65536', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('REPLICATE_EVENTS_MARKED_FOR_SKIP', '', 'REPLICATE', 'COMPILE-TIME', 'REPLICATE', 'GLOBAL', 'ENUM', 'Whether the slave should replicate events that were created with @@skip_replication=1 on the master. Default REPLICATE (no events are skipped). Other values are FILTER_ON_SLAVE (events will be sent by the master but ignored by the slave) and FILTER_ON_MASTER (events marked with @@skip_replication=1 will be filtered on the master and never be sent to the slave).', '', '', '', 'REPLICATE,FILTER_ON_SLAVE,FILTER_ON_MASTER', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('TIME_ZONE', 'SYSTEM', 'SYSTEM', 'COMPILE-TIME', 'SYSTEM', 'SESSION', 'VARCHAR', 'The current time zone, used to initialize the time zone for a client when it connects. Set to SYSTEM by default, in which the client uses the system time zone value.', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_RECOVERY', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enables automatic relay log recovery right after the database startup, which means that the IO Thread starts re-fetching from the master right after the last transaction processed.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('USERSTAT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enables statistics gathering for USER_STATISTICS, CLIENT_STATISTICS, INDEX_STATISTICS and TABLE_STATISTICS tables in the INFORMATION_SCHEMA', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_OSU_METHOD', 'TOI', 'TOI', 'COMPILE-TIME', 'TOI', 'SESSION', 'ENUM', 'Method for Online Schema Upgrade', '', '', '', 'TOI,RSU', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SESSION_TRACK_SYSTEM_VARIABLES', '', '', 'COMPILE-TIME', '', 'SESSION', 'VARCHAR', 'Track changes in registered system variables. For compatibility with MySQL defaults this variable should be set to \"autocommit, character_set_client, character_set_connection, character_set_results, time_zone\"', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_TABLE_LOCKS', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Enable InnoDB locking in LOCK TABLES', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_PURGE_BATCH_SIZE', '', '300', 'COMPILE-TIME', '300', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of UNDO log pages to purge in one batch from the history list.', '1', '5000', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_SST_RECEIVE_ADDRESS', '', 'AUTO', 'COMPILE-TIME', 'AUTO', 'GLOBAL', 'VARCHAR', 'Address where node is waiting for SST contact', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FORCE_PRIMARY_KEY', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Do not allow to create table without primary key (off by default)', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_ARCH_DIR', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_LIMIT', '', '1048576', 'COMPILE-TIME', '1048576', 'GLOBAL', 'BIGINT UNSIGNED', 'Don\'t cache results that are bigger than this', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SLAVE_DOMAIN_PARALLEL_THREADS', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Maximum number of parallel threads to use on slave for events in a single replication domain. When using multiple domains, this can be used to limit a single domain from grabbing all threads and thus stalling other domains. The default of 0 means to allow a domain to grab as many threads as it wants, up to the value of slave_parallel_threads.', '0', '16383', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RELAY_LOG_PURGE', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'if disabled - do not purge relay logs. if enabled - purge them as soon as they are no more needed.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ENFORCE_STORAGE_ENGINE', '', '', 'COMPILE-TIME', '', 'SESSION', 'VARCHAR', 'Force the use of a storage engine for new tables', '', '', '', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_TOTAL_CACHE_SIZE', '', '640000000', 'COMPILE-TIME', '640000000', 'GLOBAL', 'BIGINT UNSIGNED', 'Total memory allocated for InnoDB Fulltext Search cache', '32000000', '1600000000', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FT_ENABLE_DIAG_PRINT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Whether to enable additional FTS diagnostic printout ', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_PAGE_CHECKSUM', '', 'ON', 'COMPILE-TIME', 'ON', 'GLOBAL', 'BOOLEAN', 'Maintain page checksums (can be overridden per table with PAGE_CHECKSUM clause in CREATE TABLE)', '', '', '', 'OFF,ON', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HAVE_SYMLINK', '', 'YES', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'If symbolic link support is enabled, will be set to YES, otherwise will be set to NO. Required for the INDEX DIRECTORY and DATA DIRECTORY table options (see CREATE TABLE) and Windows symlink support. Will be set to DISABLED if the server is started with the --skip-symbolic-links option.', '', '', '', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('SESSION_TRACK_SCHEMA', 'ON', 'ON', 'COMPILE-TIME', 'ON', 'SESSION', 'BOOLEAN', 'Track changes to the default schema.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FAST_SHUTDOWN', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'INT UNSIGNED', 'Speeds up the shutdown process of the InnoDB storage engine. Possible values are 0, 1 (faster) or 2 (fastest - crash-like).', '0', '2', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('OPTIMIZER_SELECTIVITY_SAMPLING_LIMIT', '100', '100', 'COMPILE-TIME', '100', 'SESSION', 'BIGINT UNSIGNED', 'Controls number of record samples to check condition selectivity', '10', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FLUSH_NEIGHBORS', '', '1', 'COMPILE-TIME', '1', 'GLOBAL', 'BIGINT UNSIGNED', 'Set to 0 (don\'t flush neighbors from buffer pool), 1 (flush contiguous neighbors from buffer pool) or 2 (flush neighbors from buffer pool), when flushing a block', '0', '2', '0', '', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('BACK_LOG', '', '80', 'AUTO', '150', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of outstanding connection requests MariaDB can have. This comes into play when the main MariaDB thread gets very many connection requests in a very short time', '0', '65535', '1', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SYNC_SPIN_LOOPS', '', '30', 'COMPILE-TIME', '30', 'GLOBAL', 'BIGINT UNSIGNED', 'Count of spin-loop rounds in InnoDB mutexes (30 by default)', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_STATS_PERSISTENT_SAMPLE_PAGES', '', '20', 'COMPILE-TIME', '20', 'GLOBAL', 'BIGINT UNSIGNED', 'The number of leaf index pages to sample when calculating persistent statistics (by ANALYZE, default 20)', '1', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_SCRUB_LOG', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Enable background redo log (ib_logfile0, ib_logfile1...) scrubbing', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('RAND_SEED2', '300779489', '', 'COMPILE-TIME', '0', 'SESSION ONLY', 'BIGINT UNSIGNED', 'Sets the internal state of the RAND() generator for replication purposes', '0', '18446744073709551615', '1', '', 'NO', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_THREAD_SLEEP_DELAY', '', '10000', 'COMPILE-TIME', '10000', 'GLOBAL', 'BIGINT UNSIGNED', 'Time of innodb thread sleeping before joining InnoDB queue (usec). Value 0 disable a sleep', '0', '1000000', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ENCRYPT_TMP_FILES', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Encrypt temporary files (created for filesort, binary log cache, etc)', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_RECOVER', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Recover database state after crash and exit', '', '', '', 'OFF,ON', 'YES', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('ARIA_STATS_METHOD', 'nulls_unequal', 'nulls_unequal', 'COMPILE-TIME', 'nulls_unequal', 'SESSION', 'ENUM', 'Specifies how Aria index statistics collection code should treat NULLs', '', '', '', 'nulls_unequal,nulls_equal,nulls_ignored', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_BUF_DUMP_STATUS_FREQUENCY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'A number between [0, 100] that tells how oftern buffer pool dump status in percentages should be printed. E.g. 10 means that buffer pool dump status is printed when every 10% of number of buffer pool pages are dumped. Default is 0 (only start and end status is printed).', '0', '100', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MONITOR_ENABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Turn on a monitor counter', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('WSREP_GTID_DOMAIN_ID', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'INT UNSIGNED', 'When wsrep_gtid_mode is set, this value is used as gtid_domain_id for galera transactions and also copied to the joiner nodes during state transfer. It is ignored, otherwise.', '0', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MONITOR_RESET', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Reset a monitor counter', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('MAX_SEEKS_FOR_KEY', '4294967295', '4294967295', 'COMPILE-TIME', '4294967295', 'SESSION', 'BIGINT UNSIGNED', 'Limit assumed max number of seeks when looking up rows based on a key', '1', '4294967295', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('HISTOGRAM_SIZE', '0', '0', 'COMPILE-TIME', '0', 'SESSION', 'BIGINT UNSIGNED', 'Number of bytes used for a histogram. If set to 0, no histograms are created by ANALYZE.', '0', '255', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_REPLICATION_DELAY', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Replication thread delay (ms) on the slave server if innodb_thread_concurrency is reached (0 by default)', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LRU_SCAN_DEPTH', '', '1024', 'COMPILE-TIME', '1024', 'GLOBAL', 'BIGINT UNSIGNED', 'How deep to scan LRU to keep it clean', '100', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('QUERY_CACHE_MIN_RES_UNIT', '', '4096', 'COMPILE-TIME', '4096', 'GLOBAL', 'BIGINT UNSIGNED', 'The minimum size for blocks allocated by the query cache', '0', '4294967295', '8', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_UNDO_TABLESPACES', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of undo tablespaces to use.', '0', '127', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_WRITE_IO_THREADS', '', '4', 'COMPILE-TIME', '4', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of background write I/O threads in InnoDB.', '1', '64', '0', '', 'YES', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MONITOR_DISABLE', '', '', 'COMPILE-TIME', '', 'GLOBAL', 'VARCHAR', 'Turn off a monitor counter', '', '', '', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_CONCURRENCY_TICKETS', '', '5000', 'COMPILE-TIME', '5000', 'GLOBAL', 'BIGINT UNSIGNED', 'Number of times a thread is allowed to enter InnoDB within the same SQL query after it has once got the ticket', '1', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_LOG_ARCHIVE', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_USE_GLOBAL_FLUSH_LOG_AT_TRX_COMMIT', '', 'OFF', 'COMPILE-TIME', 'OFF', 'GLOBAL', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'NO', 'NONE');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_MAX_BITMAP_FILE_SIZE', '', '0', 'COMPILE-TIME', '0', 'GLOBAL', 'BIGINT UNSIGNED', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '0', '18446744073709551615', '0', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('LARGE_PAGE_SIZE', '', '0', 'COMPILE-TIME', '', 'GLOBAL', 'INT UNSIGNED', 'If large page support is enabled, this shows the size of memory pages', '0', '4294967295', '1', '', 'YES', '');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FOREGROUND_PREFLUSH', '', 'DEPRECATED', 'COMPILE-TIME', 'DEPRECATED', 'GLOBAL', 'ENUM', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'SYNC_PREFLUSH,EXPONENTIAL_BACKOFF,DEPRECATED', 'NO', 'OPTIONAL');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('READ_RND_BUFFER_SIZE', '262144', '262144', 'COMPILE-TIME', '262144', 'SESSION', 'BIGINT UNSIGNED', 'When reading rows in sorted order after a sort, the rows are read through this buffer to avoid a disk seeks', '1', '2147483647', '1', '', 'NO', 'REQUIRED');
INSERT INTO `SYSTEM_VARIABLES` VALUES ('INNODB_FAKE_CHANGES', 'OFF', 'OFF', 'COMPILE-TIME', 'OFF', 'SESSION', 'BOOLEAN', 'Deprecated and ignored; only exists to allow easier upgrade from earlier XtraDB versions.', '', '', '', 'OFF,ON', 'NO', 'OPTIONAL');

#
# Table structure for table `TABLES`
#


DROP table IF EXISTS `TABLES`;
CREATE TEMPORARY TABLE `TABLES` (
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `TABLE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `ENGINE` varchar(64) DEFAULT NULL,
  `VERSION` bigint(21) unsigned DEFAULT NULL,
  `ROW_FORMAT` varchar(10) DEFAULT NULL,
  `TABLE_ROWS` bigint(21) unsigned DEFAULT NULL,
  `AVG_ROW_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `DATA_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `MAX_DATA_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `INDEX_LENGTH` bigint(21) unsigned DEFAULT NULL,
  `DATA_FREE` bigint(21) unsigned DEFAULT NULL,
  `AUTO_INCREMENT` bigint(21) unsigned DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `CHECK_TIME` datetime DEFAULT NULL,
  `TABLE_COLLATION` varchar(32) DEFAULT NULL,
  `CHECKSUM` bigint(21) unsigned DEFAULT NULL,
  `CREATE_OPTIONS` varchar(2048) DEFAULT NULL,
  `TABLE_COMMENT` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `TABLES`
#

#
# Dumping data for table `TABLES`
#

INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'ALL_PLUGINS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=9441', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'APPLICABLE_ROLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '979', '0', '16691950', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=17137', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'CHARACTER_SETS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '384', '0', '16434816', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=43690', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'COLLATIONS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '231', '0', '16704765', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=72628', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'COLLATION_CHARACTER_SET_APPLICABILITY', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '195', '0', '16357770', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=86037', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'COLUMNS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=2779', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'COLUMN_PRIVILEGES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2893', '0', '16759149', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=5799', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'ENABLED_ROLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '387', '0', '16563213', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=43351', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'ENGINES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '731', '0', '16663145', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=22951', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'EVENTS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=611', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'FILES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '4022', '0', '16767718', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=4171', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'GLOBAL_STATUS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6340', '0', '16762960', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2646', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'GLOBAL_VARIABLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6340', '0', '16762960', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2646', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'KEY_CACHES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '659', '0', '16650294', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=25458', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'KEY_COLUMN_USAGE', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '4637', '0', '16762755', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=3618', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'PARAMETERS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=6030', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'PARTITIONS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=5579', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'PLUGINS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=9441', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'PROCESSLIST', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=15335', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'PROFILING', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '308', '0', '16562084', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=54471', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'REFERENTIAL_CONSTRAINTS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '4814', '0', '16767162', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=3485', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'ROUTINES', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=576', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SCHEMATA', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '3464', '0', '16738048', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=4843', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SCHEMA_PRIVILEGES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2507', '0', '16741746', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=6692', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SESSION_STATUS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6340', '0', '16762960', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2646', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SESSION_VARIABLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6340', '0', '16762960', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2646', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'STATISTICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '5753', '0', '16752736', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2916', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SYSTEM_VARIABLES', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=651', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TABLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '14829', '0', '16771599', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=1131', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TABLESPACES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6951', '0', '16772763', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2413', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TABLE_CONSTRAINTS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2504', '0', '16721712', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=6700', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TABLE_PRIVILEGES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2700', '0', '16750800', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=6213', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TRIGGERS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=563', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'USER_PRIVILEGES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2314', '0', '16732534', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=7250', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'VIEWS', 'SYSTEM VIEW', 'Aria', '11', 'Page', '', '0', '8192', '4503599627288576', '8192', '0', '', '2018-09-16 14:31:44', '2018-09-16 14:31:44', '', 'utf8_general_ci', '', 'max_rows=6019', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'GEOMETRY_COLUMNS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '4244', '0', '16759556', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=3953', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'SPATIAL_REF_SYS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '7691', '0', '16758689', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2181', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'CLIENT_STATISTICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '386', '0', '16520414', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=43464', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INDEX_STATISTICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1743', '0', '16765917', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=9625', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_DATAFILES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '12007', '0', '16773779', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=1397', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'USER_STATISTICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '567', '0', '16747479', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=29589', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESTATS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1215', '0', '16763355', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=13808', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_LOCKS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '31244', '0', '16746784', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=536', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_MUTEXES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '24017', '0', '16763866', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=698', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMPMEM', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '29', '0', '15204352', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=578524', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1755', '0', '16728660', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=9559', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMP', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '25', '0', '13107200', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=671088', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_DELETED', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '9', '0', '9437184', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=1864135', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMP_RESET', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '25', '0', '13107200', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=671088', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_LOCK_WAITS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '599', '0', '16749238', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=28008', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'TABLE_STATISTICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1181', '0', '16733589', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=14205', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_ENCRYPTION', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2012', '0', '16743864', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=8338', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE_LRU', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6669', '0', '16765866', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2515', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_FIELDS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '594', '0', '16609428', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=28244', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMPMEM_RESET', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '29', '0', '15204352', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=578524', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_COLUMNS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '610', '0', '16613350', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=27503', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_TABLE', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1054', '0', '16744898', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=15917', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_CMP_PER_INDEX_RESET', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1755', '0', '16728660', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=9559', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'user_variables', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6630', '0', '16767270', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2530', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_INDEX_CACHE', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1054', '0', '16744898', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=15917', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN_COLS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1748', '0', '16738848', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=9597', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_BEING_DELETED', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '9', '0', '9437184', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=1864135', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_BUFFER_POOL_STATS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '257', '0', '16332350', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=65280', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_TRX', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '4534', '0', '16766732', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=3700', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_FOREIGN', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1752', '0', '16700064', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=9576', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2091', '0', '16736364', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=8023', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_DEFAULT_STOPWORD', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '56', '0', '14680064', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=299593', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_FT_CONFIG', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '1163', '0', '16705332', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=14425', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_BUFFER_PAGE', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '6852', '0', '16766844', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=2448', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_TABLESPACES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2133', '0', '16752582', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=7865', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_METRICS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '3003', '0', '16747731', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=5586', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_INDEXES', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '618', '0', '16615548', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=27147', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_VIRTUAL', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '17', '0', '11883850', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=986895', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_TABLESPACES_SCRUBBING', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '2020', '0', '16743780', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=8305', '');
INSERT INTO `TABLES` VALUES ('def', 'information_schema', 'INNODB_SYS_SEMAPHORE_WAITS', 'SYSTEM VIEW', 'MEMORY', '11', 'Fixed', '', '72196', '0', '16749472', '0', '0', '', '2018-09-16 14:31:44', '', '', 'utf8_general_ci', '', 'max_rows=232', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_commentmeta', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '32768', '0', '1', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_comments', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '81920', '0', '2', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_links', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '16384', '0', '1', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_options', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '245', '4480', '1097728', '0', '16384', '4194304', '1346', '2018-05-29 09:24:54', '2018-09-16 14:30:08', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_postmeta', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '3003', '92', '278528', '0', '262144', '0', '3339', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_posts', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '303', '1568', '475136', '0', '98304', '0', '479', '2018-05-29 09:24:54', '2018-09-16 14:18:00', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_relationships', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '112', '146', '16384', '0', '16384', '0', '', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '23', '712', '16384', '0', '32768', '0', '24', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_termmeta', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '32768', '0', '1', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_terms', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '19', '862', '16384', '0', '32768', '0', '24', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_usermeta', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '59', '277', '16384', '0', '32768', '0', '65', '2018-05-29 09:24:54', '2018-09-16 14:18:00', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_users', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '49152', '0', '3', '2018-05-29 09:24:54', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '1', '16384', '16384', '0', '0', '0', '3', '2018-07-06 16:43:32', '', '', 'utf8mb4_unicode_ci', '', '', '');
INSERT INTO `TABLES` VALUES ('def', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'BASE TABLE', 'InnoDB', '10', 'Dynamic', '0', '0', '16384', '0', '0', '0', '1', '2018-09-16 14:18:46', '', '', 'utf8mb4_unicode_ci', '', '', '');

#
# Table structure for table `TABLESPACES`
#


DROP table IF EXISTS `TABLESPACES`;
CREATE TEMPORARY TABLE `TABLESPACES` (
  `TABLESPACE_NAME` varchar(64) NOT NULL DEFAULT '',
  `ENGINE` varchar(64) NOT NULL DEFAULT '',
  `TABLESPACE_TYPE` varchar(64) DEFAULT NULL,
  `LOGFILE_GROUP_NAME` varchar(64) DEFAULT NULL,
  `EXTENT_SIZE` bigint(21) unsigned DEFAULT NULL,
  `AUTOEXTEND_SIZE` bigint(21) unsigned DEFAULT NULL,
  `MAXIMUM_SIZE` bigint(21) unsigned DEFAULT NULL,
  `NODEGROUP_ID` bigint(21) unsigned DEFAULT NULL,
  `TABLESPACE_COMMENT` varchar(2048) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `TABLESPACES`
#

#
# Dumping data for table `TABLESPACES`
#


#
# Table structure for table `TABLE_CONSTRAINTS`
#


DROP table IF EXISTS `TABLE_CONSTRAINTS`;
CREATE TEMPORARY TABLE `TABLE_CONSTRAINTS` (
  `CONSTRAINT_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `CONSTRAINT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `CONSTRAINT_NAME` varchar(64) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `CONSTRAINT_TYPE` varchar(64) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `TABLE_CONSTRAINTS`
#

#
# Dumping data for table `TABLE_CONSTRAINTS`
#

INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_commentmeta', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_comments', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_links', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_options', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'option_name', 'superbiajuridico_test_development', 'wp_options', 'UNIQUE');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_postmeta', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_posts', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_term_relationships', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'term_id_taxonomy', 'superbiajuridico_test_development', 'wp_term_taxonomy', 'UNIQUE');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_termmeta', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_terms', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_usermeta', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_users', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_wpgdprc_consents', 'PRIMARY KEY');
INSERT INTO `TABLE_CONSTRAINTS` VALUES ('def', 'superbiajuridico_test_development', 'PRIMARY', 'superbiajuridico_test_development', 'wp_xcloner_scheduler', 'PRIMARY KEY');

#
# Table structure for table `TABLE_PRIVILEGES`
#


DROP table IF EXISTS `TABLE_PRIVILEGES`;
CREATE TEMPORARY TABLE `TABLE_PRIVILEGES` (
  `GRANTEE` varchar(190) NOT NULL DEFAULT '',
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `PRIVILEGE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `IS_GRANTABLE` varchar(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `TABLE_PRIVILEGES`
#

#
# Dumping data for table `TABLE_PRIVILEGES`
#


#
# Table structure for table `TRIGGERS`
#


DROP table IF EXISTS `TRIGGERS`;
CREATE TEMPORARY TABLE `TRIGGERS` (
  `TRIGGER_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TRIGGER_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TRIGGER_NAME` varchar(64) NOT NULL DEFAULT '',
  `EVENT_MANIPULATION` varchar(6) NOT NULL DEFAULT '',
  `EVENT_OBJECT_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `EVENT_OBJECT_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `EVENT_OBJECT_TABLE` varchar(64) NOT NULL DEFAULT '',
  `ACTION_ORDER` bigint(4) NOT NULL DEFAULT 0,
  `ACTION_CONDITION` longtext DEFAULT NULL,
  `ACTION_STATEMENT` longtext NOT NULL DEFAULT '',
  `ACTION_ORIENTATION` varchar(9) NOT NULL DEFAULT '',
  `ACTION_TIMING` varchar(6) NOT NULL DEFAULT '',
  `ACTION_REFERENCE_OLD_TABLE` varchar(64) DEFAULT NULL,
  `ACTION_REFERENCE_NEW_TABLE` varchar(64) DEFAULT NULL,
  `ACTION_REFERENCE_OLD_ROW` varchar(3) NOT NULL DEFAULT '',
  `ACTION_REFERENCE_NEW_ROW` varchar(3) NOT NULL DEFAULT '',
  `CREATED` datetime(2) DEFAULT NULL,
  `SQL_MODE` varchar(8192) NOT NULL DEFAULT '',
  `DEFINER` varchar(189) NOT NULL DEFAULT '',
  `CHARACTER_SET_CLIENT` varchar(32) NOT NULL DEFAULT '',
  `COLLATION_CONNECTION` varchar(32) NOT NULL DEFAULT '',
  `DATABASE_COLLATION` varchar(32) NOT NULL DEFAULT ''
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `TRIGGERS`
#

#
# Dumping data for table `TRIGGERS`
#


#
# Table structure for table `USER_PRIVILEGES`
#


DROP table IF EXISTS `USER_PRIVILEGES`;
CREATE TEMPORARY TABLE `USER_PRIVILEGES` (
  `GRANTEE` varchar(190) NOT NULL DEFAULT '',
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `PRIVILEGE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `IS_GRANTABLE` varchar(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `USER_PRIVILEGES`
#

#
# Dumping data for table `USER_PRIVILEGES`
#

INSERT INTO `USER_PRIVILEGES` VALUES ('\'superbiajuridico_test\'@\'localhost\'', 'def', 'USAGE', 'NO');

#
# Table structure for table `VIEWS`
#


DROP table IF EXISTS `VIEWS`;
CREATE TEMPORARY TABLE `VIEWS` (
  `TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VIEW_DEFINITION` longtext NOT NULL DEFAULT '',
  `CHECK_OPTION` varchar(8) NOT NULL DEFAULT '',
  `IS_UPDATABLE` varchar(3) NOT NULL DEFAULT '',
  `DEFINER` varchar(189) NOT NULL DEFAULT '',
  `SECURITY_TYPE` varchar(7) NOT NULL DEFAULT '',
  `CHARACTER_SET_CLIENT` varchar(32) NOT NULL DEFAULT '',
  `COLLATION_CONNECTION` varchar(32) NOT NULL DEFAULT '',
  `ALGORITHM` varchar(10) NOT NULL DEFAULT ''
) ENGINE=Aria DEFAULT CHARSET=utf8 PAGE_CHECKSUM=0;

#
# End Structure for table `VIEWS`
#

#
# Dumping data for table `VIEWS`
#


#
# Table structure for table `GEOMETRY_COLUMNS`
#


DROP table IF EXISTS `GEOMETRY_COLUMNS`;
CREATE TEMPORARY TABLE `GEOMETRY_COLUMNS` (
  `F_TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `F_TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `F_TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `F_GEOMETRY_COLUMN` varchar(64) NOT NULL DEFAULT '',
  `G_TABLE_CATALOG` varchar(512) NOT NULL DEFAULT '',
  `G_TABLE_SCHEMA` varchar(64) NOT NULL DEFAULT '',
  `G_TABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `G_GEOMETRY_COLUMN` varchar(64) NOT NULL DEFAULT '',
  `STORAGE_TYPE` tinyint(2) NOT NULL DEFAULT 0,
  `GEOMETRY_TYPE` int(7) NOT NULL DEFAULT 0,
  `COORD_DIMENSION` tinyint(2) NOT NULL DEFAULT 0,
  `MAX_PPR` tinyint(2) NOT NULL DEFAULT 0,
  `SRID` smallint(5) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `GEOMETRY_COLUMNS`
#

#
# Dumping data for table `GEOMETRY_COLUMNS`
#


#
# Table structure for table `SPATIAL_REF_SYS`
#


DROP table IF EXISTS `SPATIAL_REF_SYS`;
CREATE TEMPORARY TABLE `SPATIAL_REF_SYS` (
  `SRID` smallint(5) NOT NULL DEFAULT 0,
  `AUTH_NAME` varchar(512) NOT NULL DEFAULT '',
  `AUTH_SRID` int(5) NOT NULL DEFAULT 0,
  `SRTEXT` varchar(2048) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `SPATIAL_REF_SYS`
#

#
# Dumping data for table `SPATIAL_REF_SYS`
#

INSERT INTO `SPATIAL_REF_SYS` VALUES ('-1', 'Not defined', '-1', 'LOCAL_CS[\"Spatial reference wasn\'t specified\",LOCAL_DATUM[\"Unknown\",0],UNIT[\"m\",1.0],AXIS[\"x\",EAST],AXIS[\"y\",NORTH]]');
INSERT INTO `SPATIAL_REF_SYS` VALUES ('0', 'EPSG', '404000', 'LOCAL_CS[\"Wildcard 2D cartesian plane in metric unit\",LOCAL_DATUM[\"Unknown\",0],UNIT[\"m\",1.0],AXIS[\"x\",EAST],AXIS[\"y\",NORTH],AUTHORITY[\"EPSG\",\"404000\"]]');

#
# Table structure for table `CLIENT_STATISTICS`
#


DROP table IF EXISTS `CLIENT_STATISTICS`;
CREATE TEMPORARY TABLE `CLIENT_STATISTICS` (
  `CLIENT` varchar(64) NOT NULL DEFAULT '',
  `TOTAL_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `CONCURRENT_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `CONNECTED_TIME` bigint(21) NOT NULL DEFAULT 0,
  `BUSY_TIME` double NOT NULL DEFAULT 0,
  `CPU_TIME` double NOT NULL DEFAULT 0,
  `BYTES_RECEIVED` bigint(21) NOT NULL DEFAULT 0,
  `BYTES_SENT` bigint(21) NOT NULL DEFAULT 0,
  `BINLOG_BYTES_WRITTEN` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_READ` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_SENT` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_DELETED` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_INSERTED` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_UPDATED` bigint(21) NOT NULL DEFAULT 0,
  `SELECT_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `UPDATE_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `OTHER_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `COMMIT_TRANSACTIONS` bigint(21) NOT NULL DEFAULT 0,
  `ROLLBACK_TRANSACTIONS` bigint(21) NOT NULL DEFAULT 0,
  `DENIED_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `LOST_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `ACCESS_DENIED` bigint(21) NOT NULL DEFAULT 0,
  `EMPTY_QUERIES` bigint(21) NOT NULL DEFAULT 0,
  `TOTAL_SSL_CONNECTIONS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MAX_STATEMENT_TIME_EXCEEDED` bigint(21) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `CLIENT_STATISTICS`
#

#
# Dumping data for table `CLIENT_STATISTICS`
#


#
# Table structure for table `INDEX_STATISTICS`
#


DROP table IF EXISTS `INDEX_STATISTICS`;
CREATE TEMPORARY TABLE `INDEX_STATISTICS` (
  `TABLE_SCHEMA` varchar(192) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(192) NOT NULL DEFAULT '',
  `INDEX_NAME` varchar(192) NOT NULL DEFAULT '',
  `ROWS_READ` bigint(21) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INDEX_STATISTICS`
#

#
# Dumping data for table `INDEX_STATISTICS`
#


#
# Table structure for table `INNODB_SYS_DATAFILES`
#


DROP table IF EXISTS `INNODB_SYS_DATAFILES`;
CREATE TEMPORARY TABLE `INNODB_SYS_DATAFILES` (
  `SPACE` int(11) unsigned NOT NULL DEFAULT 0,
  `PATH` varchar(4000) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_DATAFILES`
#

#
# Dumping data for table `INNODB_SYS_DATAFILES`
#


#
# Table structure for table `USER_STATISTICS`
#


DROP table IF EXISTS `USER_STATISTICS`;
CREATE TEMPORARY TABLE `USER_STATISTICS` (
  `USER` varchar(128) NOT NULL DEFAULT '',
  `TOTAL_CONNECTIONS` int(11) NOT NULL DEFAULT 0,
  `CONCURRENT_CONNECTIONS` int(11) NOT NULL DEFAULT 0,
  `CONNECTED_TIME` int(11) NOT NULL DEFAULT 0,
  `BUSY_TIME` double NOT NULL DEFAULT 0,
  `CPU_TIME` double NOT NULL DEFAULT 0,
  `BYTES_RECEIVED` bigint(21) NOT NULL DEFAULT 0,
  `BYTES_SENT` bigint(21) NOT NULL DEFAULT 0,
  `BINLOG_BYTES_WRITTEN` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_READ` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_SENT` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_DELETED` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_INSERTED` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_UPDATED` bigint(21) NOT NULL DEFAULT 0,
  `SELECT_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `UPDATE_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `OTHER_COMMANDS` bigint(21) NOT NULL DEFAULT 0,
  `COMMIT_TRANSACTIONS` bigint(21) NOT NULL DEFAULT 0,
  `ROLLBACK_TRANSACTIONS` bigint(21) NOT NULL DEFAULT 0,
  `DENIED_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `LOST_CONNECTIONS` bigint(21) NOT NULL DEFAULT 0,
  `ACCESS_DENIED` bigint(21) NOT NULL DEFAULT 0,
  `EMPTY_QUERIES` bigint(21) NOT NULL DEFAULT 0,
  `TOTAL_SSL_CONNECTIONS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MAX_STATEMENT_TIME_EXCEEDED` bigint(21) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `USER_STATISTICS`
#

#
# Dumping data for table `USER_STATISTICS`
#


#
# Table structure for table `INNODB_SYS_TABLESTATS`
#


DROP table IF EXISTS `INNODB_SYS_TABLESTATS`;
CREATE TEMPORARY TABLE `INNODB_SYS_TABLESTATS` (
  `TABLE_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(193) NOT NULL DEFAULT '',
  `STATS_INITIALIZED` varchar(193) NOT NULL DEFAULT '',
  `NUM_ROWS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `CLUST_INDEX_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `OTHER_INDEX_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MODIFIED_COUNTER` bigint(21) unsigned NOT NULL DEFAULT 0,
  `AUTOINC` bigint(21) unsigned NOT NULL DEFAULT 0,
  `REF_COUNT` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_TABLESTATS`
#

#
# Dumping data for table `INNODB_SYS_TABLESTATS`
#


#
# Table structure for table `INNODB_LOCKS`
#


DROP table IF EXISTS `INNODB_LOCKS`;
CREATE TEMPORARY TABLE `INNODB_LOCKS` (
  `lock_id` varchar(81) NOT NULL DEFAULT '',
  `lock_trx_id` varchar(18) NOT NULL DEFAULT '',
  `lock_mode` varchar(32) NOT NULL DEFAULT '',
  `lock_type` varchar(32) NOT NULL DEFAULT '',
  `lock_table` varchar(1024) NOT NULL DEFAULT '',
  `lock_index` varchar(1024) DEFAULT NULL,
  `lock_space` bigint(21) unsigned DEFAULT NULL,
  `lock_page` bigint(21) unsigned DEFAULT NULL,
  `lock_rec` bigint(21) unsigned DEFAULT NULL,
  `lock_data` varchar(8192) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_LOCKS`
#

#
# Dumping data for table `INNODB_LOCKS`
#


#
# Table structure for table `INNODB_MUTEXES`
#


DROP table IF EXISTS `INNODB_MUTEXES`;
CREATE TEMPORARY TABLE `INNODB_MUTEXES` (
  `NAME` varchar(4000) NOT NULL DEFAULT '',
  `CREATE_FILE` varchar(4000) NOT NULL DEFAULT '',
  `CREATE_LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `OS_WAITS` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_MUTEXES`
#

#
# Dumping data for table `INNODB_MUTEXES`
#


#
# Table structure for table `INNODB_CMPMEM`
#


DROP table IF EXISTS `INNODB_CMPMEM`;
CREATE TEMPORARY TABLE `INNODB_CMPMEM` (
  `page_size` int(5) NOT NULL DEFAULT 0,
  `buffer_pool_instance` int(11) NOT NULL DEFAULT 0,
  `pages_used` int(11) NOT NULL DEFAULT 0,
  `pages_free` int(11) NOT NULL DEFAULT 0,
  `relocation_ops` bigint(21) NOT NULL DEFAULT 0,
  `relocation_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMPMEM`
#

#
# Dumping data for table `INNODB_CMPMEM`
#


#
# Table structure for table `INNODB_CMP_PER_INDEX`
#


DROP table IF EXISTS `INNODB_CMP_PER_INDEX`;
CREATE TEMPORARY TABLE `INNODB_CMP_PER_INDEX` (
  `database_name` varchar(192) NOT NULL DEFAULT '',
  `table_name` varchar(192) NOT NULL DEFAULT '',
  `index_name` varchar(192) NOT NULL DEFAULT '',
  `compress_ops` int(11) NOT NULL DEFAULT 0,
  `compress_ops_ok` int(11) NOT NULL DEFAULT 0,
  `compress_time` int(11) NOT NULL DEFAULT 0,
  `uncompress_ops` int(11) NOT NULL DEFAULT 0,
  `uncompress_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMP_PER_INDEX`
#

#
# Dumping data for table `INNODB_CMP_PER_INDEX`
#


#
# Table structure for table `INNODB_CMP`
#


DROP table IF EXISTS `INNODB_CMP`;
CREATE TEMPORARY TABLE `INNODB_CMP` (
  `page_size` int(5) NOT NULL DEFAULT 0,
  `compress_ops` int(11) NOT NULL DEFAULT 0,
  `compress_ops_ok` int(11) NOT NULL DEFAULT 0,
  `compress_time` int(11) NOT NULL DEFAULT 0,
  `uncompress_ops` int(11) NOT NULL DEFAULT 0,
  `uncompress_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMP`
#

#
# Dumping data for table `INNODB_CMP`
#


#
# Table structure for table `INNODB_FT_DELETED`
#


DROP table IF EXISTS `INNODB_FT_DELETED`;
CREATE TEMPORARY TABLE `INNODB_FT_DELETED` (
  `DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_DELETED`
#

#
# Dumping data for table `INNODB_FT_DELETED`
#


#
# Table structure for table `INNODB_CMP_RESET`
#


DROP table IF EXISTS `INNODB_CMP_RESET`;
CREATE TEMPORARY TABLE `INNODB_CMP_RESET` (
  `page_size` int(5) NOT NULL DEFAULT 0,
  `compress_ops` int(11) NOT NULL DEFAULT 0,
  `compress_ops_ok` int(11) NOT NULL DEFAULT 0,
  `compress_time` int(11) NOT NULL DEFAULT 0,
  `uncompress_ops` int(11) NOT NULL DEFAULT 0,
  `uncompress_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMP_RESET`
#

#
# Dumping data for table `INNODB_CMP_RESET`
#


#
# Table structure for table `INNODB_LOCK_WAITS`
#


DROP table IF EXISTS `INNODB_LOCK_WAITS`;
CREATE TEMPORARY TABLE `INNODB_LOCK_WAITS` (
  `requesting_trx_id` varchar(18) NOT NULL DEFAULT '',
  `requested_lock_id` varchar(81) NOT NULL DEFAULT '',
  `blocking_trx_id` varchar(18) NOT NULL DEFAULT '',
  `blocking_lock_id` varchar(81) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_LOCK_WAITS`
#

#
# Dumping data for table `INNODB_LOCK_WAITS`
#


#
# Table structure for table `TABLE_STATISTICS`
#


DROP table IF EXISTS `TABLE_STATISTICS`;
CREATE TEMPORARY TABLE `TABLE_STATISTICS` (
  `TABLE_SCHEMA` varchar(192) NOT NULL DEFAULT '',
  `TABLE_NAME` varchar(192) NOT NULL DEFAULT '',
  `ROWS_READ` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_CHANGED` bigint(21) NOT NULL DEFAULT 0,
  `ROWS_CHANGED_X_INDEXES` bigint(21) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `TABLE_STATISTICS`
#

#
# Dumping data for table `TABLE_STATISTICS`
#


#
# Table structure for table `INNODB_TABLESPACES_ENCRYPTION`
#


DROP table IF EXISTS `INNODB_TABLESPACES_ENCRYPTION`;
CREATE TEMPORARY TABLE `INNODB_TABLESPACES_ENCRYPTION` (
  `SPACE` int(11) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(655) DEFAULT NULL,
  `ENCRYPTION_SCHEME` int(11) unsigned NOT NULL DEFAULT 0,
  `KEYSERVER_REQUESTS` int(11) unsigned NOT NULL DEFAULT 0,
  `MIN_KEY_VERSION` int(11) unsigned NOT NULL DEFAULT 0,
  `CURRENT_KEY_VERSION` int(11) unsigned NOT NULL DEFAULT 0,
  `KEY_ROTATION_PAGE_NUMBER` bigint(21) unsigned DEFAULT NULL,
  `KEY_ROTATION_MAX_PAGE_NUMBER` bigint(21) unsigned DEFAULT NULL,
  `CURRENT_KEY_ID` int(11) unsigned NOT NULL DEFAULT 0,
  `ROTATING_OR_FLUSHING` int(1) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_TABLESPACES_ENCRYPTION`
#

#
# Dumping data for table `INNODB_TABLESPACES_ENCRYPTION`
#


#
# Table structure for table `INNODB_BUFFER_PAGE_LRU`
#


DROP table IF EXISTS `INNODB_BUFFER_PAGE_LRU`;
CREATE TEMPORARY TABLE `INNODB_BUFFER_PAGE_LRU` (
  `POOL_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LRU_POSITION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `SPACE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGE_NUMBER` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGE_TYPE` varchar(64) DEFAULT NULL,
  `FLUSH_TYPE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `FIX_COUNT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `IS_HASHED` varchar(3) DEFAULT NULL,
  `NEWEST_MODIFICATION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `OLDEST_MODIFICATION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `ACCESS_TIME` bigint(21) unsigned NOT NULL DEFAULT 0,
  `TABLE_NAME` varchar(1024) DEFAULT NULL,
  `INDEX_NAME` varchar(1024) DEFAULT NULL,
  `NUMBER_RECORDS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DATA_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `COMPRESSED_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `COMPRESSED` varchar(3) DEFAULT NULL,
  `IO_FIX` varchar(64) DEFAULT NULL,
  `IS_OLD` varchar(3) DEFAULT NULL,
  `FREE_PAGE_CLOCK` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_BUFFER_PAGE_LRU`
#

#
# Dumping data for table `INNODB_BUFFER_PAGE_LRU`
#


#
# Table structure for table `INNODB_SYS_FIELDS`
#


DROP table IF EXISTS `INNODB_SYS_FIELDS`;
CREATE TEMPORARY TABLE `INNODB_SYS_FIELDS` (
  `INDEX_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(193) NOT NULL DEFAULT '',
  `POS` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_FIELDS`
#

#
# Dumping data for table `INNODB_SYS_FIELDS`
#


#
# Table structure for table `INNODB_CMPMEM_RESET`
#


DROP table IF EXISTS `INNODB_CMPMEM_RESET`;
CREATE TEMPORARY TABLE `INNODB_CMPMEM_RESET` (
  `page_size` int(5) NOT NULL DEFAULT 0,
  `buffer_pool_instance` int(11) NOT NULL DEFAULT 0,
  `pages_used` int(11) NOT NULL DEFAULT 0,
  `pages_free` int(11) NOT NULL DEFAULT 0,
  `relocation_ops` bigint(21) NOT NULL DEFAULT 0,
  `relocation_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMPMEM_RESET`
#

#
# Dumping data for table `INNODB_CMPMEM_RESET`
#


#
# Table structure for table `INNODB_SYS_COLUMNS`
#


DROP table IF EXISTS `INNODB_SYS_COLUMNS`;
CREATE TEMPORARY TABLE `INNODB_SYS_COLUMNS` (
  `TABLE_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(193) NOT NULL DEFAULT '',
  `POS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MTYPE` int(11) NOT NULL DEFAULT 0,
  `PRTYPE` int(11) NOT NULL DEFAULT 0,
  `LEN` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_COLUMNS`
#

#
# Dumping data for table `INNODB_SYS_COLUMNS`
#


#
# Table structure for table `INNODB_FT_INDEX_TABLE`
#


DROP table IF EXISTS `INNODB_FT_INDEX_TABLE`;
CREATE TEMPORARY TABLE `INNODB_FT_INDEX_TABLE` (
  `WORD` varchar(337) NOT NULL DEFAULT '',
  `FIRST_DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LAST_DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DOC_COUNT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `POSITION` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_INDEX_TABLE`
#

#
# Dumping data for table `INNODB_FT_INDEX_TABLE`
#


#
# Table structure for table `INNODB_CMP_PER_INDEX_RESET`
#


DROP table IF EXISTS `INNODB_CMP_PER_INDEX_RESET`;
CREATE TEMPORARY TABLE `INNODB_CMP_PER_INDEX_RESET` (
  `database_name` varchar(192) NOT NULL DEFAULT '',
  `table_name` varchar(192) NOT NULL DEFAULT '',
  `index_name` varchar(192) NOT NULL DEFAULT '',
  `compress_ops` int(11) NOT NULL DEFAULT 0,
  `compress_ops_ok` int(11) NOT NULL DEFAULT 0,
  `compress_time` int(11) NOT NULL DEFAULT 0,
  `uncompress_ops` int(11) NOT NULL DEFAULT 0,
  `uncompress_time` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_CMP_PER_INDEX_RESET`
#

#
# Dumping data for table `INNODB_CMP_PER_INDEX_RESET`
#


#
# Table structure for table `user_variables`
#


DROP table IF EXISTS `user_variables`;
CREATE TEMPORARY TABLE `user_variables` (
  `VARIABLE_NAME` varchar(64) NOT NULL DEFAULT '',
  `VARIABLE_VALUE` varchar(2048) DEFAULT NULL,
  `VARIABLE_TYPE` varchar(64) NOT NULL DEFAULT '',
  `CHARACTER_SET_NAME` varchar(32) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `user_variables`
#

#
# Dumping data for table `user_variables`
#


#
# Table structure for table `INNODB_FT_INDEX_CACHE`
#


DROP table IF EXISTS `INNODB_FT_INDEX_CACHE`;
CREATE TEMPORARY TABLE `INNODB_FT_INDEX_CACHE` (
  `WORD` varchar(337) NOT NULL DEFAULT '',
  `FIRST_DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LAST_DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DOC_COUNT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `POSITION` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_INDEX_CACHE`
#

#
# Dumping data for table `INNODB_FT_INDEX_CACHE`
#


#
# Table structure for table `INNODB_SYS_FOREIGN_COLS`
#


DROP table IF EXISTS `INNODB_SYS_FOREIGN_COLS`;
CREATE TEMPORARY TABLE `INNODB_SYS_FOREIGN_COLS` (
  `ID` varchar(193) NOT NULL DEFAULT '',
  `FOR_COL_NAME` varchar(193) NOT NULL DEFAULT '',
  `REF_COL_NAME` varchar(193) NOT NULL DEFAULT '',
  `POS` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_FOREIGN_COLS`
#

#
# Dumping data for table `INNODB_SYS_FOREIGN_COLS`
#


#
# Table structure for table `INNODB_FT_BEING_DELETED`
#


DROP table IF EXISTS `INNODB_FT_BEING_DELETED`;
CREATE TEMPORARY TABLE `INNODB_FT_BEING_DELETED` (
  `DOC_ID` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_BEING_DELETED`
#

#
# Dumping data for table `INNODB_FT_BEING_DELETED`
#


#
# Table structure for table `INNODB_BUFFER_POOL_STATS`
#


DROP table IF EXISTS `INNODB_BUFFER_POOL_STATS`;
CREATE TEMPORARY TABLE `INNODB_BUFFER_POOL_STATS` (
  `POOL_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `POOL_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `FREE_BUFFERS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DATABASE_PAGES` bigint(21) unsigned NOT NULL DEFAULT 0,
  `OLD_DATABASE_PAGES` bigint(21) unsigned NOT NULL DEFAULT 0,
  `MODIFIED_DATABASE_PAGES` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PENDING_DECOMPRESS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PENDING_READS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PENDING_FLUSH_LRU` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PENDING_FLUSH_LIST` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGES_MADE_YOUNG` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGES_NOT_MADE_YOUNG` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGES_MADE_YOUNG_RATE` double NOT NULL DEFAULT 0,
  `PAGES_MADE_NOT_YOUNG_RATE` double NOT NULL DEFAULT 0,
  `NUMBER_PAGES_READ` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NUMBER_PAGES_CREATED` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NUMBER_PAGES_WRITTEN` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGES_READ_RATE` double NOT NULL DEFAULT 0,
  `PAGES_CREATE_RATE` double NOT NULL DEFAULT 0,
  `PAGES_WRITTEN_RATE` double NOT NULL DEFAULT 0,
  `NUMBER_PAGES_GET` bigint(21) unsigned NOT NULL DEFAULT 0,
  `HIT_RATE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `YOUNG_MAKE_PER_THOUSAND_GETS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NOT_YOUNG_MAKE_PER_THOUSAND_GETS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NUMBER_PAGES_READ_AHEAD` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NUMBER_READ_AHEAD_EVICTED` bigint(21) unsigned NOT NULL DEFAULT 0,
  `READ_AHEAD_RATE` double NOT NULL DEFAULT 0,
  `READ_AHEAD_EVICTED_RATE` double NOT NULL DEFAULT 0,
  `LRU_IO_TOTAL` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LRU_IO_CURRENT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `UNCOMPRESS_TOTAL` bigint(21) unsigned NOT NULL DEFAULT 0,
  `UNCOMPRESS_CURRENT` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_BUFFER_POOL_STATS`
#

#
# Dumping data for table `INNODB_BUFFER_POOL_STATS`
#


#
# Table structure for table `INNODB_TRX`
#


DROP table IF EXISTS `INNODB_TRX`;
CREATE TEMPORARY TABLE `INNODB_TRX` (
  `trx_id` varchar(18) NOT NULL DEFAULT '',
  `trx_state` varchar(13) NOT NULL DEFAULT '',
  `trx_started` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `trx_requested_lock_id` varchar(81) DEFAULT NULL,
  `trx_wait_started` datetime DEFAULT NULL,
  `trx_weight` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_mysql_thread_id` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_query` varchar(1024) DEFAULT NULL,
  `trx_operation_state` varchar(64) DEFAULT NULL,
  `trx_tables_in_use` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_tables_locked` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_lock_structs` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_lock_memory_bytes` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_rows_locked` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_rows_modified` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_concurrency_tickets` bigint(21) unsigned NOT NULL DEFAULT 0,
  `trx_isolation_level` varchar(16) NOT NULL DEFAULT '',
  `trx_unique_checks` int(1) NOT NULL DEFAULT 0,
  `trx_foreign_key_checks` int(1) NOT NULL DEFAULT 0,
  `trx_last_foreign_key_error` varchar(256) DEFAULT NULL,
  `trx_adaptive_hash_latched` int(1) NOT NULL DEFAULT 0,
  `trx_is_read_only` int(1) NOT NULL DEFAULT 0,
  `trx_autocommit_non_locking` int(1) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_TRX`
#

#
# Dumping data for table `INNODB_TRX`
#


#
# Table structure for table `INNODB_SYS_FOREIGN`
#


DROP table IF EXISTS `INNODB_SYS_FOREIGN`;
CREATE TEMPORARY TABLE `INNODB_SYS_FOREIGN` (
  `ID` varchar(193) NOT NULL DEFAULT '',
  `FOR_NAME` varchar(193) NOT NULL DEFAULT '',
  `REF_NAME` varchar(193) NOT NULL DEFAULT '',
  `N_COLS` int(11) unsigned NOT NULL DEFAULT 0,
  `TYPE` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_FOREIGN`
#

#
# Dumping data for table `INNODB_SYS_FOREIGN`
#


#
# Table structure for table `INNODB_SYS_TABLES`
#


DROP table IF EXISTS `INNODB_SYS_TABLES`;
CREATE TEMPORARY TABLE `INNODB_SYS_TABLES` (
  `TABLE_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(655) NOT NULL DEFAULT '',
  `FLAG` int(11) NOT NULL DEFAULT 0,
  `N_COLS` int(11) NOT NULL DEFAULT 0,
  `SPACE` int(11) NOT NULL DEFAULT 0,
  `FILE_FORMAT` varchar(10) DEFAULT NULL,
  `ROW_FORMAT` varchar(12) DEFAULT NULL,
  `ZIP_PAGE_SIZE` int(11) unsigned NOT NULL DEFAULT 0,
  `SPACE_TYPE` varchar(10) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_TABLES`
#

#
# Dumping data for table `INNODB_SYS_TABLES`
#


#
# Table structure for table `INNODB_FT_DEFAULT_STOPWORD`
#


DROP table IF EXISTS `INNODB_FT_DEFAULT_STOPWORD`;
CREATE TEMPORARY TABLE `INNODB_FT_DEFAULT_STOPWORD` (
  `value` varchar(18) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_DEFAULT_STOPWORD`
#

#
# Dumping data for table `INNODB_FT_DEFAULT_STOPWORD`
#

INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('a');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('about');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('an');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('are');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('as');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('at');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('be');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('by');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('com');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('de');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('en');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('for');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('from');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('how');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('i');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('in');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('is');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('it');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('la');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('of');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('on');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('or');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('that');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('the');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('this');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('to');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('was');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('what');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('when');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('where');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('who');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('will');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('with');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('und');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('the');
INSERT INTO `INNODB_FT_DEFAULT_STOPWORD` VALUES ('www');

#
# Table structure for table `INNODB_FT_CONFIG`
#


DROP table IF EXISTS `INNODB_FT_CONFIG`;
CREATE TEMPORARY TABLE `INNODB_FT_CONFIG` (
  `KEY` varchar(193) NOT NULL DEFAULT '',
  `VALUE` varchar(193) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_FT_CONFIG`
#

#
# Dumping data for table `INNODB_FT_CONFIG`
#


#
# Table structure for table `INNODB_BUFFER_PAGE`
#


DROP table IF EXISTS `INNODB_BUFFER_PAGE`;
CREATE TEMPORARY TABLE `INNODB_BUFFER_PAGE` (
  `POOL_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `BLOCK_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `SPACE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGE_NUMBER` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGE_TYPE` varchar(64) DEFAULT NULL,
  `FLUSH_TYPE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `FIX_COUNT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `IS_HASHED` varchar(3) DEFAULT NULL,
  `NEWEST_MODIFICATION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `OLDEST_MODIFICATION` bigint(21) unsigned NOT NULL DEFAULT 0,
  `ACCESS_TIME` bigint(21) unsigned NOT NULL DEFAULT 0,
  `TABLE_NAME` varchar(1024) DEFAULT NULL,
  `INDEX_NAME` varchar(1024) DEFAULT NULL,
  `NUMBER_RECORDS` bigint(21) unsigned NOT NULL DEFAULT 0,
  `DATA_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `COMPRESSED_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `PAGE_STATE` varchar(64) DEFAULT NULL,
  `IO_FIX` varchar(64) DEFAULT NULL,
  `IS_OLD` varchar(3) DEFAULT NULL,
  `FREE_PAGE_CLOCK` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_BUFFER_PAGE`
#

#
# Dumping data for table `INNODB_BUFFER_PAGE`
#


#
# Table structure for table `INNODB_SYS_TABLESPACES`
#


DROP table IF EXISTS `INNODB_SYS_TABLESPACES`;
CREATE TEMPORARY TABLE `INNODB_SYS_TABLESPACES` (
  `SPACE` int(11) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(655) NOT NULL DEFAULT '',
  `FLAG` int(11) unsigned NOT NULL DEFAULT 0,
  `FILE_FORMAT` varchar(10) DEFAULT NULL,
  `ROW_FORMAT` varchar(22) DEFAULT NULL,
  `PAGE_SIZE` int(11) unsigned NOT NULL DEFAULT 0,
  `ZIP_PAGE_SIZE` int(11) unsigned NOT NULL DEFAULT 0,
  `SPACE_TYPE` varchar(10) DEFAULT NULL,
  `FS_BLOCK_SIZE` int(11) unsigned NOT NULL DEFAULT 0,
  `FILE_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `ALLOCATED_SIZE` bigint(21) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_TABLESPACES`
#

#
# Dumping data for table `INNODB_SYS_TABLESPACES`
#


#
# Table structure for table `INNODB_METRICS`
#


DROP table IF EXISTS `INNODB_METRICS`;
CREATE TEMPORARY TABLE `INNODB_METRICS` (
  `NAME` varchar(193) NOT NULL DEFAULT '',
  `SUBSYSTEM` varchar(193) NOT NULL DEFAULT '',
  `COUNT` bigint(21) NOT NULL DEFAULT 0,
  `MAX_COUNT` bigint(21) DEFAULT NULL,
  `MIN_COUNT` bigint(21) DEFAULT NULL,
  `AVG_COUNT` double DEFAULT NULL,
  `COUNT_RESET` bigint(21) NOT NULL DEFAULT 0,
  `MAX_COUNT_RESET` bigint(21) DEFAULT NULL,
  `MIN_COUNT_RESET` bigint(21) DEFAULT NULL,
  `AVG_COUNT_RESET` double DEFAULT NULL,
  `TIME_ENABLED` datetime DEFAULT NULL,
  `TIME_DISABLED` datetime DEFAULT NULL,
  `TIME_ELAPSED` bigint(21) DEFAULT NULL,
  `TIME_RESET` datetime DEFAULT NULL,
  `STATUS` varchar(193) NOT NULL DEFAULT '',
  `TYPE` varchar(193) NOT NULL DEFAULT '',
  `COMMENT` varchar(193) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_METRICS`
#

#
# Dumping data for table `INNODB_METRICS`
#


#
# Table structure for table `INNODB_SYS_INDEXES`
#


DROP table IF EXISTS `INNODB_SYS_INDEXES`;
CREATE TEMPORARY TABLE `INNODB_SYS_INDEXES` (
  `INDEX_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(193) NOT NULL DEFAULT '',
  `TABLE_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `TYPE` int(11) NOT NULL DEFAULT 0,
  `N_FIELDS` int(11) NOT NULL DEFAULT 0,
  `PAGE_NO` int(11) NOT NULL DEFAULT 0,
  `SPACE` int(11) NOT NULL DEFAULT 0,
  `MERGE_THRESHOLD` int(11) NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_INDEXES`
#

#
# Dumping data for table `INNODB_SYS_INDEXES`
#


#
# Table structure for table `INNODB_SYS_VIRTUAL`
#


DROP table IF EXISTS `INNODB_SYS_VIRTUAL`;
CREATE TEMPORARY TABLE `INNODB_SYS_VIRTUAL` (
  `TABLE_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `POS` int(11) unsigned NOT NULL DEFAULT 0,
  `BASE_POS` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_VIRTUAL`
#

#
# Dumping data for table `INNODB_SYS_VIRTUAL`
#


#
# Table structure for table `INNODB_TABLESPACES_SCRUBBING`
#


DROP table IF EXISTS `INNODB_TABLESPACES_SCRUBBING`;
CREATE TEMPORARY TABLE `INNODB_TABLESPACES_SCRUBBING` (
  `SPACE` bigint(21) unsigned NOT NULL DEFAULT 0,
  `NAME` varchar(655) DEFAULT NULL,
  `COMPRESSED` int(11) unsigned NOT NULL DEFAULT 0,
  `LAST_SCRUB_COMPLETED` datetime DEFAULT NULL,
  `CURRENT_SCRUB_STARTED` datetime DEFAULT NULL,
  `CURRENT_SCRUB_ACTIVE_THREADS` int(11) unsigned DEFAULT NULL,
  `CURRENT_SCRUB_PAGE_NUMBER` bigint(21) unsigned NOT NULL DEFAULT 0,
  `CURRENT_SCRUB_MAX_PAGE_NUMBER` bigint(21) unsigned NOT NULL DEFAULT 0,
  `ROTATING_OR_FLUSHING` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_TABLESPACES_SCRUBBING`
#

#
# Dumping data for table `INNODB_TABLESPACES_SCRUBBING`
#


#
# Table structure for table `INNODB_SYS_SEMAPHORE_WAITS`
#


DROP table IF EXISTS `INNODB_SYS_SEMAPHORE_WAITS`;
CREATE TEMPORARY TABLE `INNODB_SYS_SEMAPHORE_WAITS` (
  `THREAD_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `OBJECT_NAME` varchar(4000) DEFAULT NULL,
  `FILE` varchar(4000) DEFAULT NULL,
  `LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `WAIT_TIME` bigint(21) unsigned NOT NULL DEFAULT 0,
  `WAIT_OBJECT` bigint(21) unsigned NOT NULL DEFAULT 0,
  `WAIT_TYPE` varchar(16) DEFAULT NULL,
  `HOLDER_THREAD_ID` bigint(21) unsigned NOT NULL DEFAULT 0,
  `HOLDER_FILE` varchar(4000) DEFAULT NULL,
  `HOLDER_LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `CREATED_FILE` varchar(4000) DEFAULT NULL,
  `CREATED_LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `WRITER_THREAD` bigint(21) unsigned NOT NULL DEFAULT 0,
  `RESERVATION_MODE` varchar(16) DEFAULT NULL,
  `READERS` int(11) unsigned NOT NULL DEFAULT 0,
  `WAITERS_FLAG` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LOCK_WORD` bigint(21) unsigned NOT NULL DEFAULT 0,
  `LAST_READER_FILE` varchar(4000) DEFAULT NULL,
  `LAST_READER_LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `LAST_WRITER_FILE` varchar(4000) DEFAULT NULL,
  `LAST_WRITER_LINE` int(11) unsigned NOT NULL DEFAULT 0,
  `OS_WAIT_COUNT` int(11) unsigned NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

#
# End Structure for table `INNODB_SYS_SEMAPHORE_WAITS`
#

#
# Dumping data for table `INNODB_SYS_SEMAPHORE_WAITS`
#


#
# Finished at: Sep 16, 2018 at 14:32
#